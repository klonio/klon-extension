/**
 *
 *  This object deals with all of the prompts within the application
 *  Example: prompts to upgrade account to premium.
 *
 */
var prompt = {
  upgradeAccount : function(type = "premium") {
    var upgradeConf = confirm("This feature is reserved only for " + type + " members. Would you like to upgrade now?");
    if (upgradeConf) {
      // var createProperties = {
      //   "windowId" : null,
      //   "index" : null,
      //   "url" : "https://klon.io/upgrade/" + type,
      //   "active" : true,
      //   "pinned" : false,
      //   "openerTabId" : null
      // }
      // chrome.tabs.create(createProperties);
      window.location.pathname = "/subscription.html";
      return true;
    } else {
      return false;
    }
  }
}
