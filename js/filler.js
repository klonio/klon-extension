/**
 *
 *  filler.js exists to automatically fill the fields that are given to it by the
 *  crawler.js script. Filler and crawler work together to provide an autofill system
 *  for forms that involve registration or logging in.
 *
 */

var filler = {

  /**
   *
   *  Fills in the fields of the form that were found and bound to the form object
   *
   *  @param   form        Object     Object containing all of the found fields in the target form
   *  @param   identity    Object     Object containing a newly created identity
   *
   *  @return              Boolean
   *
   */
  fillForm : function(form, identity) {
    // console.log("Form: ", form);
    // console.log("Identity: ", identity);
    //
    Object.getOwnPropertyNames(form).forEach(
      function (val, idx, array) {
        if (form[val] != null && form[val] != undefined && form[val] != "") {
          form[val].focus();
          form[val].value = identity[val];
          form[val].blur();
          switch (val) {
            case "passwordConf" :
            form[val].value = identity['password'];
            break;
            case "emailConf" :
            form[val].value = identity['email'];
            break;
            case "fullName" :
            form[val].value = identity['firstName'] + " " + identity['lastName'];
            break;
            case "genderF" :
            if (identity['gender'] === "F") {
              form['genderF'].checked = true;
            }
            break;
            case "genderM" :
            if (identity['gender'] === "M") {
              form['genderM'].checked = true;
            }
            break;
            case "dobMonth":
            if (form['dobMonth'].tagName.toLowerCase() === "select") {
              var month = form['dobMonth'].querySelector('[value="' + identity['month'] + '"]');
              month.selected = 1;
            }
            break;
            case "dobDay":
            if (form['dobDay'].tagName.toLowerCase() === "select") {
              var day = form['dobDay'].querySelector('[value="' + identity['day'] + '"]');
              day.selected = 1;
            }
            break;
            case "dobYear":
            if (form['dobYear'].tagName.toLowerCase() === "select") {
              var year = form['dobYear'].querySelector('[value="' + identity['year'] + '"]');
              year.selected = 1;
            }
            break;
          }
        }
      }
    );
  }
}
