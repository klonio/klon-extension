//
// MOVED TO identities.js
//
// /**
// *
// *  Fetches the previously generated identity from the localStorage then displays it on the page
// *
// *
// */
// function displayGeneratedIdentity() {
//   var idWrapper = document.querySelector("#idWrapper");
//   var identity  = localStorage.getItem("generatedId");
//   if (identity) {
//     identity = JSON.parse(identity);
//     // Clear any previous results
//     idWrapper.innerHTML  = "";
//     // Convert date to words not just numbers, less confusing for the user
//     const date = new Date(identity.year, Number(identity.month - 1), identity.day);
//     const month = date.toLocaleString('en-us', { month: 'long' });
//     const day = date.toLocaleString('en-us', { day: '2-digit' });
//     // If the gender returned is "M" then gender = male otherwise gender = female
//     var gender = identity.gender == "M" ? gender = "Male" : gender = "Female";
//     // Display the new results...
//     idWrapper.innerHTML   = "<div class='main__userRow' data-field='Full name'><div class='main__key'>Name:</div><div class='main__value'> " + identity.firstName + " " + identity.middleInitial + ". " + identity.lastName + "</div></div>";
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Gender'><div class='main__key'>Gender: </div><div class='main__value'> " + gender + "</div></div>";
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='DOB'><div class='main__key'>Birthday:</div><div class='main__value'> " + month + " " + day + ", " + identity.year + "</div></div>";
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Address'><div class='main__key'>Address:</div><div class='main__value'> " + identity.streetAddress + "<br>" + identity.city + ", " + identity.state + " " + identity.zip + "</div></div>";
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Email address'><div class='main__key'>Email: </div><div class='main__value'> " + identity.email + "</div></div>";
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Username'><div class='main__key'>Username: </div><div class='main__value'> " + identity.username + "</div></div>";
//     // Generate password locally on the fly
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Password'><div class='main__key'>Password: </div><div class='main__value'> " + identity.password + "</div></div>";
//     copyPasta();
//   }
// }
//
//
// /**
// *
// *  Close the popup and let the contentScript know that the user wants to select the form to fill
// *
// */
// function fillForm() {
//   var identity = JSON.parse(localStorage.getItem("generatedId"));
//   chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
//     chrome.tabs.sendMessage(tabs[0].id, {msg:"prepareFillForm", identity: identity}, function(response) {
//       // chrome.tabs.sendMessage(tabs[0].id, {msg:"prepareFillForm", identity: identity, autofill : autofill, autogenerate : autogenerate}, function(response) {
//       window.close();
//       // alert(response)
//       // $("#text").text(response);
//     });
//   });
// }
//
//
// /**
// *
// *
// */
// function clearIdentity() {
//   var result = confirm("Do you want to clear this identity from the screen?");
//   if (result) {
//     var identity = localStorage.getItem("generatedId");
//     if (identity) {
//       var idWrapper = document.querySelector("#idWrapper");
//       try {
//         localStorage.removeItem("generatedId");
//         idWrapper.innerHTML = "To manually create a new kl&#x14D;n click the button below...";
//         return true;
//       } catch(e) {
//         idWrapper.innerHTML = "Error: ", e.message;
//         return false;
//       }
//     }
//   } else {
//     return false;
//   }
// }
// /**
//  *
//  *
//  *
//  */
// function copyDetails(e) {
//   // Get the value of the row clicked
//   // If statement clears up an issue when the user clicks the :after pseudo element
//   if (e.target.classList.contains("main__userRow")) {
//     var parent = e.target;
//   } else {
//     var parent = e.target.parentNode;
//   }
//   var value  = parent.querySelector(".main__value");
//   var field  = parent.dataset.field;
//   var errorHandler = document.querySelector("#errorHandler");
//   value      = value.innerText;
//   // Copy the value to the clipboard
//   var textArea = document.createElement("textarea");
//   textArea.style.background = "transparent";
//   textArea.style.border     = "none";
//   textArea.style.boxShadow  = "none";
//   textArea.style.height     = "2em";
//   textArea.style.outline    = "none";
//   textArea.style.position   = "fixed";
//   textArea.style.bottom     = "-4em";
//   textArea.style.left       = "-4em";
//   textArea.style.width      = "2em";
//   textArea.value = value;
//   document.body.appendChild(textArea);
//   textArea.focus();
//   textArea.select();
//   try {
//     document.execCommand("copy");
//     errorHandler.innerText = "";
//     errorHandler.innerText = field + " successfully copied to the clipboard!";
//     errorHandler.style.opacity = 1;
//     errorHandler.style.height  = "20px";
//     errorHandler.style.margin  = "0 0 10px 0";
//       var t = setTimeout(function() {
//         errorHandler.style.opacity = 0;
//         errorHandler.style.height  = 0;
//         errorHandler.style.margin  = 0;
//         clearTimeout(t);
//       }, 2000);
//   } catch(e) {
//     console.log("Value is: ", value);
//     errorHandler.innerText = e.message;
//   }
//   document.body.removeChild(textArea);
//   return null;
// }
//
//
//
//
// /**
// *
// *  Make all of the rows copy-able
// *
// */
// function copyPasta() {
//   var rows = document.querySelectorAll(".main__userRow");
//   for (let i = 0; i < rows.length; i++) {
//     rows[i].addEventListener("click", copyDetails, false);
//   }
//   return true;
// }
//
//
// /**
// *
// *  Check to see if there is a previously generated identity stored in the localStorage
// *  If a previous identity is found it is fetched, parsed, then displayed on the page for the user to see
// *
// */
// function checkPreviousGeneratedId() {
//   var prevId = localStorage.getItem("generatedId");
//   prevId = JSON.parse(prevId);
//   if (prevId) {
//     displayGeneratedIdentity();
//   }
//   return true;
// }
// /**
//  *
//  *  Generates an identity for the user at their request
//  *
//  */
// function genManualIdentity() {
//   var genId = generateIdentity();
//   var errorHandler = document.querySelector("#errorHandler");
//   var idWrapper = document.querySelector("#idWrapper");
//   errorHandler.innerHTML  = "Generating...";
//   errorHandler.style.opacity = 1;
//   errorHandler.style.height = "20px";
//   genId
//   .then(identity => {
//     // Clear any previous results
//     idWrapper.innerHTML  = "";
//     // Convert date to words not just numbers, less confusing for the user
//     const date = new Date(identity.year, Number(identity.month - 1), identity.day);
//     const month = date.toLocaleString('en-us', { month: 'long' });
//     const day = date.toLocaleString('en-us', { day: '2-digit' });
//     // If the gender returned is "M" then gender = male otherwise gender = female
//     var gender = identity.gender == "M" ? gender = "Male" : gender = "Female";
//     // Display the new results...
//     idWrapper.innerHTML   = "<div class='main__userRow' data-field='Full name'><div class='main__key'>Name:</div><div class='main__value'> " + identity.firstName + " " + identity.middleInitial + ". " + identity.lastName + "</div></div>";
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Gender'><div class='main__key'>Gender: </div><div class='main__value'> " + gender + "</div></div>";
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='DOB'><div class='main__key'>Birthday:</div><div class='main__value'> " + month + " " + day + ", " + identity.year + "</div></div>";
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Address'><div class='main__key'>Address:</div><div class='main__value'> " + identity.streetAddress + "<br>" + identity.city + ", " + identity.state + " " + identity.zip + "</div></div>";
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Email Address'><div class='main__key'>Email: </div><div class='main__value'> " + identity.email + "</div></div>";
//     // Generate username locally on-the-fly
//     usernameGen.firstName = identity.firstName;
//     usernameGen.lastName  = identity.lastName;
//     var username = usernameGen.genUsername();
//     var password = generatePassword(12, true, true, true, true);
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Username'><div class='main__key'>Username: </div><div class='main__value'> " + username + "</div></div>";
//     // Generate password locally on the fly
//     idWrapper.innerHTML  += "<div class='main__userRow' data-field='Password'><div class='main__key'>Password: </div><div class='main__value'> " + password + "</div></div>";
//     // Reset the errorHandler
//     errorHandler.style.opacity = 0;
//     errorHandler.style.height = 0;
//     errorHandler.innerText = "";
//     // Set the generated identity in the localStorage
//     var generatedIdentity = identity;
//     generatedIdentity.username = username;
//     generatedIdentity.password = password;
//     localStorage.setItem("generatedId", JSON.stringify(generatedIdentity));
//     copyPasta();
//   })
//   .catch(err => {
//     console.log("Error: ", err);
//     idWrapper.innerText = err.message;
//     return false;
//   });
// }

// window.onload = function() {
//   // Check to see if the user has a token stored in the memory
//   credential.hasToken();
//   // //
//   // if (localStorage.getItem("sessionTab")) {
//   //   console.log("Session tab is set");
//   //   var page = localStorage.getItem("sessionTab");
//   //   console.log("Page: " + page);
//   //   console.log("Pathname: " + window.location.pathname);
//   //   // Make sure we're not already on this tab
//   //   if (window.location.pathname !== "/" + page + ".html") {
//   //     console.log("Changing location");
//   //     // Change location
//   //     window.location = page + ".html";
//   //   }
//   //   // Avoid the never ending loop, remove from localStorage
//   //   // localStorage.removeItem("sessionTab");
//   // }
//   // If the location is main.html then bind the functions to the buttons
//   if (window.location.pathname === "/main.html") {
//     //
//     var btnGenId = document.querySelector("#btnGenId");
//     btnGenId.addEventListener("click", genManualIdentity, false);
//     //
//     var btnFillForm = document.querySelector("#btnFillForm");
//     btnFillForm.addEventListener("click", fillForm, false);
//     //
//     var btnClearId = document.querySelector("#btnClearId");
//     btnClearId.addEventListener("click", clearIdentity, false);
//   }
//   localStorage.sessionTab = "main";
// }
