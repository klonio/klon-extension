//@prepros-append crawler.js
//@prepros-append filler.js
//@prepros-append comms.js
//@prepros-append identities.js
//@prepros-append parasite.js
var content = {};
var generatedId = {};

/**
 *
 *  When the mouse hovers over an element highlight it
 *
 */
function highlightNode(e) {
  try {
    var el = e.target;
    style = window.getComputedStyle(el),
    background = style.getPropertyValue('background-color');
    border = style.getPropertyValue('border');
    content.oldBackground = background;
    content.oldBorder     = border;
    // Highlight the element by changing the background color and border
    el.style.backgroundColor = "#83a2ea";
    el.style.border = "solid 2px #3b6ce1";
    return true;
  } catch (err) {
    console.log("Error: ", err.message);
    return false;
  }
}


/**
 *
 *  Return things to normal on mouseout
 *
 */
function dehighlightNode(e) {
  var el = e.target;
  // Highlight the element by changing the background color and border
  if (content.oldBackground) {
    el.style.backgroundColor = content.oldBackground;
  } else {
    el.style.backgroundColor = "transparent";
  }
  //
  if (content.oldBorder) {
    el.style.border = content.oldBorder;
  } else {
    el.style.border = "none";
  }
}


/**
 *
 *  This is the form the user selected to be manually filled
 *
 */
function nodeSelect(e) {
  var el = e.target;
  // Find the form based on the node the user selected
  var form = el.querySelector("form");
  // If a form is found, cool proceed
  if (!form) {
    // If no form is search up the DOM
    form = e.target.closest("form");
  }

  // Fill the form with the identity
  // 1. Get the inputs organized from the form the user has selected
  var result = theRinger("signup", form);
  // console.log("The Result of the Ringer: ", result);
  // 2. Fill the form with the identity
  var fill   = filler.fillForm(result, generatedId);
  if (fill) {
    alert("Successfully filled form!");
  }
  // console.log("nodeSelect Identity is: ", generatedId);

  // console.log("Form: ", form);
  // Add the new id to the user's klon identities

  // Remove all the event listeners and remove their effects
  dehighlightNode(e);
  document.body.removeEventListener("mouseover", highlightNode);
  document.body.removeEventListener("mouseout", dehighlightNode);
  document.body.removeEventListener("click", nodeSelect);
  return true;
}


/**
 *
 *  Show the klonLoginMenu when the user clicks the klon logo contained inside the <input>
 *
 */
function klonBtnClick(evt) {
  // Show the menu where the user can select between different id's for the current URL
  var injectLoginKlon = document.querySelector("#klonLoginMenu");
  //
  if (injectLoginKlon != undefined || injectLoginKlon != null) {
    // Set the "Left" style property/value to be 30 px more than the button that was clicked to activate it
    var l = evt.target.style.left;
    // Convert the value of the "left" property from ie: "210px" to 210
    l = Number(l.slice(0,-2));
    // Evaluate the mathematical equation as opposed to concatenation
    l = eval(l - 277);
    // Add the "px" units to the end of the thing again
    l = l + "px";
    // Set the "Top" style property/value to be 30 px more than the button that was clicked to activate it
    var t = evt.target.style.top;
    t = Number(t.slice(0,-2));
    // eval forces the evaluation of the mathematical equation as opposed to concatenation
    t = eval(t + 30);
    t = t + "px";
    //
    injectLoginKlon.setAttribute("style", "left:" + l + ";top:" + t + ";");
    //
    injectLoginKlon.classList.toggle("klonHide");
    return true;
  } else {
    return false;
  }
}


/**
 *
 *  This thing here nigga
 *
 */
function determineAction() {
  // Determine what we are doing based on the URL ie signing up or logging in to the site
  var action = crawler.signupRegisterURL(window.location.href);
  // If we are neither obviously signing up nor logging in then we're most likely logging in.
  if (action != "signup" && action != "login") {
    action = "login";
  }
  return action;
}


/**
 *
 *  Find all the inputs for the given form and assign them to their respective key pair. example: <input name="username" type="text"> is assigned to identity.username
 *
 *
 *
 */
function theRinger(action, form = null) {
  // Find all of the forms on the page related to the particular action we have decided upon (registration or login)
  var formFields = crawler.seek(action, form);
  // Setup all the regular expressions...
  // Name RegExp
  var regExAutoCompName = new RegExp(/^given-name$|^additional-name$|^family-name$/i);
  var regExNameSmall    = new RegExp(/^name$/i);
  var regExNameBig      = new RegExp(/fullname|full.name|^name$|firstname|first.name|lastname|last.name|middleinitial|^mi$|middlename|middle.name|\bfname|\bname.f\b|\bname.l\b/i);
  var regExNameArray  = [regExAutoCompName, regExNameSmall, regExNameBig];
  // Email RegExp
  var regExEmail = new RegExp(/email|e.mail/i);
  var regExEmailArray = [regExEmail];
  // Username RegExp
  var regExUsername      = new RegExp(/username|user.name/i);
  var regExUsernameArray = [regExUsername];
  // Password RegExp
  var regExAutoPassword = new RegExp(/new.password|current-password|current.password|^password$/i);
  var regExPassword     = new RegExp(/^password|^pass$|pass.word|^pw$|passwd/i);
  var regExPasswdArray  = [regExAutoPassword, regExPassword];
  // Address RegExp
  var regExAddress      = new RegExp(/^address|street.address|streetaddress|address.line|^country$|zip|postal.code|postalcode|country.name|countryname/i);
  var regExAddressArray = [regExAddress];
  // DOB RegExp
  var regExBirthday  = new RegExp(/^bday$|^dob$|^birthday$/i);
  var regExBdayMonth = new RegExp(/bday.month|dobmonth|dob.month|birthday.month|birth.month|birthmonth/i);
  var regExBdayDay   = new RegExp(/bday.day|dobday|dob.day|birthday.day|birth.day/i);
  var regExBdayYear  = new RegExp(/bday.year|dobyear|dob.year|birthday.year|birthyear|birth.year/i);
  var regExDobArray  = [regExBirthday, regExBdayMonth, regExBdayDay, regExBdayYear];
  // Gender Array RegExp
  var regExGender      = new RegExp(/sex|gender|male|female/i);
  var regExGenderArray = [regExGender];
  //
  var nameFields       = new Array();
  var emailFields      = new Array();
  var usernameFields   = new Array();
  var passwdFields     = new Array();
  var addressFields    = new Array();
  var dobFields        = new Array();
  var genderFields     = new Array();
  //
  for (var i = 0; i < formFields.length; i++) {
    // Find name related fields
    if (crawler.isField(formFields[i], regExNameArray) != null) {
      nameFields[i]     = crawler.isField(formFields[i], regExNameArray);
    }
    // Find email related fields
    if (crawler.isField(formFields[i], regExEmailArray) != null) {
      emailFields[i]    = crawler.isField(formFields[i], regExEmailArray);
    }
    // Find username related fields
    if (crawler.isField(formFields[i], regExUsernameArray) != null) {
      usernameFields[i] = crawler.isField(formFields[i], regExUsernameArray);
    }
    // Find password related fields
    if (crawler.isField(formFields[i], regExPasswdArray) != null) {
      passwdFields[i]   = crawler.isField(formFields[i], regExPasswdArray);
    }
    // Find address related fields
    if (crawler.isField(formFields[i], regExAddressArray) != null) {
      addressFields[i]  = crawler.isField(formFields[i], regExAddressArray);
    }
    // Find dob related fields
    if (crawler.isField(formFields[i], regExDobArray) != null) {
      dobFields[i]      = crawler.isField(formFields[i], regExDobArray);
    }
    // Find gender related fields
    if (crawler.isField(formFields[i], regExGenderArray) != null) {
      genderFields[i]   = crawler.isField(formFields[i], regExGenderArray);
    }
  }
  /**
  *
  *  Remove the null values from the arrays...
  *
  */
  nameFields = nameFields.filter(function (el) {
    return el != null;
  });
  emailFields = emailFields.filter(function (el) {
    return el != null;
  });
  usernameFields = usernameFields.filter(function (el) {
    return el != null;
  });
  passwdFields = passwdFields.filter(function (el) {
    return el != null;
  });
  addressFields = addressFields.filter(function (el) {
    return el != null;
  });
  dobFields = dobFields.filter(function (el) {
    return el != null;
  });
  genderFields = genderFields.filter(function (el) {
    return el != null;
  });
  // Create the identity object to store all of the <inputs> as to their relationship with the identity
  var identity = {
    firstName : '',
    middleInitial : '',
    lastName : '',
    fullName: '',
    gender : '',
    genderM : '',
    genderF : '',
    streetAddress : '',
    road : '',
    state : '',
    city : '',
    zip : '',
    dob : '',
    dobMonth : '',
    dobDay : '',
    dobYear : '',
    email : '',
    emailConf : '',
    username : '',
    password : '',
    passwordConf : ''
  };
  // Loop through each name field
  for (var i = 0; i < nameFields.length; i++) {
    crawler.translateField(nameFields[i], "name", identity);
  }
  for (var i = 0; i < emailFields.length; i++) {
    crawler.translateField(emailFields[i], "email", identity);
  }
  for (var i = 0; i < usernameFields.length; i++) {
    crawler.translateField(usernameFields[i], "username", identity);
  }
  for (var i = 0; i < passwdFields.length; i++) {
    crawler.translateField(passwdFields[i], "password", identity);
  }
  for (var i = 0; i < addressFields.length; i++) {
    crawler.translateField(addressFields[i], "address", identity);
  }
  for (var i = 0; i < dobFields.length; i++) {
    crawler.translateField(dobFields[i], "dob", identity);
  }
  for (var i = 0; i < genderFields.length; i++) {
    crawler.translateField(genderFields[i], "gender", identity);
  }
  //
  return identity;
}


/**
 *
 *  Start the routine...
 *
 */
var t = setTimeout(() => {
  // If the document is loaded
  if (document.readyState == "complete") {
    var action   = determineAction();
    // console.log("action = " + action);
    var identity = theRinger(action);
    // console.log("identity: ", identity);
    //
    clearTimeout(t);
    // If we are signing up
    if (action === "signup") {
      //
      parasite.identityFields = identity;
      // Pop the form into the content
      parasite.injectPayload();
      // Add Event listeners to the buttons on the form so that they work when you click them
      parasite.injectFunctions();
      // Request a new identity from the extension. The extension then gets the
      // request and attempts to get a new identity from the API
      var respo = parasite.createNewIdentity();
    }
    //
    if (action === "login") {
      // Stores all of the inputs and maps them to their respective property ie {username : <input type="text" name="username">}
      parasite.identityFields = identity;
      // Find and get a handle on the login for on the page
      var loginForm = crawler.targetForms("login");
      // Inject the button into the form so the user can auto login
      if (typeof loginForm === "object") {
        //
        var getAssocIds = comm.sendMessage({"msg" : "getAssociatedIdentities", "url" : window.location.protocol + "//" + window.location.host});
        getAssocIds.then(assocIds => {
          console.log("IDENTITIES : ", assocIds);
          // Set these identities in the parasite object for later use.
          parasite.assocId = assocIds;
          console.log("assocIds Type: ", typeof parasite.assocId);
          console.log("assocIds Length: ", parasite.assocId.length);
          // If the URL is found in the user's identities then inject into the form...
          if (parasite.assocId.length > 0) {
            console.log("SHOE BUDDY");
            // Add the login menu to the page
            var ijlb = parasite.injectLoginMenu(assocIds);
            ijlb.then(ijlbResult => {
              // Add the button inside the email <input>
              var btnInjectEmail    = parasite.injectInputButtons(parasite.identityFields.email);
              btnInjectEmail.then(btnInjectEmailResult => {
                // Add the button inside the username <input>
                var btnInjectUsername = parasite.injectInputButtons(parasite.identityFields.username);
                btnInjectUsername.then(btnInjectUsernameResult => {
                  //
                  var bindLoginMenuFunctions = parasite.bindLoginMenuFunctions();
                  bindLoginMenuFunctions.then(bound => {
                    // Loop through all of the elements on the page that have the class klonBtn and add functionality to them
                    var klonBtns = document.querySelectorAll(".klonBtn");
                    for (let c = 0; c < klonBtns.length; c++) {
                      var myId = klonBtns[c].id;
                      klonBtns[c].addEventListener("click", klonBtnClick);
                    }
                  })
                  .catch(error => {
                    console.log("Error: ", error);
                  });
                }).catch(error => {
                  console.log("Error: ", error);
                });
              })
              .catch(error => {
                console.log("Error: ", error);
              });
            })
            .catch(error => {
              console.log("Error: ", error);
            });
          }
          //
        }).catch(error => {
          console.log("Error: ", error);
        });
      }
      //

    }
    clearTimeout(t);
  }
}, 1000);


/**
 *
 *  Make sure the buttons are always in the correct position within the <input>
 *
 */
function positionLoginButtons() {
  var klonBtns = document.querySelectorAll(".klonBtn");
  var klonLoginMenu = document.querySelector("#klonLoginMenu");
  // If the klonLoginMenu is not hidden, hide it.
  if (!klonLoginMenu.classList.contains("klonHide")) {
    klonLoginMenu.classList.add("klonHide");
  }
  // If there are klonBtns on the page let's position them properly
  if (klonBtns.length > 0) {
    for (let i = 0; i < klonBtns.length; i++) {
      try {
        // Get the id of the current klonButton
        var id = klonBtns[i].id;
        // Remove the klonBtn_ portion of the id
        id = id.slice(8);
        // Get a handle on the proper input for the klonBtn
        var input = document.querySelector("[data-klon='" + id + "']");
        // Get the distance from the top of the page
        var t = window.pageYOffset + input.getBoundingClientRect().top;
        // Get the distance from the left of the page
        var l = window.pageXOffset + input.getBoundingClientRect().left;
        // Get the height of the input
        var h = input.clientHeight;
        // Get the width of the input
        var w = input.clientWidth;
        // Get the width of the button to inject
        var btnW = 20;
        // Get the height of the button to inject
        var btnH = 20;
        // Set the top and left style properties on the button
        klonBtns[i].style.top  = t + ((h - btnH) / 2) + "px";
        klonBtns[i].style.left = l + (w - btnW) + "px";
        return true;
      } catch (e) {
        console.log("Error: ", e.message);
        return false;
      }
    }
  }
}

// Bind the positionLoginButtons function to the window.onResize event
window.addEventListener("resize", positionLoginButtons);
// document.body.addEventListener("mouseover", highlightNode, false);
// document.body.addEventListener("mouseout", dehighlightNode, false);
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  switch (request.msg) {
  //
  case "prepareFillForm":
    generatedId = request.identity;
    // Begin highlighting elements on mouseover
    document.body.addEventListener("mouseover", highlightNode, false);
    // Undo the highlight effect on mouseout
    document.body.addEventListener("mouseout", dehighlightNode, false);
    // When the user clicks on the form return the result
    document.body.addEventListener("click", nodeSelect, false);
    // sendResponse("Response");
    break;
  }
});

/**
 *
 *  Crawler is the object that will go out and find all the fields on a page
 *
 *  It is also the responsibility of crawler to determine what type of field he is
 *  dealing with and then to forward that field on to be formatted as such
 *
 *  Crawler also works closely with the settings to figure out what he is supposed
 *  to do
 *
 */
var crawler = {
  // Settings
  autofill : true,

  // Identity object
  firstName : '',
  middleInitial : '',
  lastName : '',
  road : '',
  city : '',
  DOB : '',
  username: '',
  email : '',
  emailConfirm : '',
  username : '',
  password : '',


  /**
   *
   *  Determine the different forms on the page and target the one we want
   *
   *
   */
  targetForms : function(target) {
    // Get all of the forms on the
    var forms = document.querySelectorAll('form');
    // Convert the NodeList into an Array
    forms = Array.prototype.slice.call(forms);
    // Create our new Array that will store the valid form values (forms that are visible)
    var validForms = new Array();
    // Make sure that the form(s) is visible to the user
    for (let k = 0; k < forms.length; k++) {
      if (forms[k].offsetParent === null) {
        continue;
      } else {
        validForms.push(forms[k]);
      }
    }
    // Re-assign the value of forms to only the visible forms
    forms = validForms;
    //
    var fbForm = new RegExp(/fb|facebook|google/i);
    var signup = new RegExp(/signup|sign.up|^reg|register|registration|\bnew./i);
    var login  = new RegExp(/login|log.in|^log|sign.in|signin/i);
    //
    for (var i = 0; i < forms.length; i++) {
      switch (target) {
        case "signup" :
          console.log("Form: ", forms[i]);
          for (var n = 0; n < forms[i].attributes.length; n++) {
            if (fbForm.test(forms[i].attributes[n].value)) {
              break;
            }
            if (signup.test(forms[i].attributes[n].value)) {
              return forms[i];
            }
          }
          break;
        case "login" :
          console.log("Form: ", forms[i]);
          for (var n = 0; n < forms[i].attributes.length; n++) {
            if (fbForm.test(forms[i].attributes[n].value)) {
              break;
            }
            if (login.test(forms[i].attributes[n].value)) {
              console.log("FORMS RETURNED: ", forms[i]);
              return forms[i];
            }
          }
          break;
        default:
          return false;
      }
    }
  },


  /**
   *
   *  crawler.seek();
   *  Gets all of the <inputs> on a page and returns them for later investigation
   *
   *  @param    target     String        "login" or "signup"
   *
   *  @return
   *
   */
  seek : function(target = "login", form = null) {
    //
    if (form == null) {
      var intel = this.targetForms(target);
      if (intel === null || intel === undefined || intel === false) {
        intel = document;
      }
    } else {
      intel = form;
    }
    //
    switch (target) {
      case 'signup' :
        var inputs = intel.querySelectorAll('input:not([type=hidden]):not([type=submit]):not([type=reset]):not([type=color]):not([type=search]):not([type=button]):not([type=image]):not([type=file]):not([type=range]), select');
        break;
      case 'login' :
        var inputs = intel.querySelectorAll('input:not([type=hidden]):not([type=submit]):not([type=reset]):not([type=color]):not([type=search]):not([type=button]):not([type=image]):not([type=file]):not([type=range]), select');
        break;
      case false :
      var inputs  = document.querySelectorAll('input:not([type=hidden]):not([type=submit]):not([type=reset]):not([type=color]):not([type=search]):not([type=button]):not([type=image]):not([type=file]):not([type=range]), select');
        break;
      default :
      var inputs  = document.querySelectorAll('input:not([type=hidden]):not([type=submit]):not([type=reset]):not([type=color]):not([type=search]):not([type=button]):not([type=image]):not([type=file]):not([type=range]), select');
        break;
    }
    return inputs;
  },


  /**
   *
   *  Identify the purpose of this field
   *
   */
  identify : function(input) {

    // Check if name field
    var isName = this.isNameField(input);
    if (isName != false) {
      return "Name: " + isName;
    }

    // Check if email address field
    var isEmail = this.isEmailField(input);
    if (isEmail != false) {
      return "Email: " + isEmail;
    }

    // Check if Address field
    var isAddress = this.isAddressField(input);
    if (isAddress != false) {
      return "Address: " + isAddress;
    }

    //
    var isPassword = this.isPasswordField(input);
    if (isPassword != false) {
      return "Password: " + isPassword;
    }

    // Check if Date of Birth input
    var isDob = this.isDobField(input);
    if (isDob != false) {
      return "Date: " + isDob;
    }

    return "No clue.";
  },


  /**
   *
   *  Determine if a field is required
   *
   */
  isRequired : function(input) {
    if (input.required == "true") {
      return true;
    } else {
      var req = input.getAttribute('aria-required');
      if (req != null) {
        req = req.toLowerCase();
        if (req == "true") {
          return true;
        } else {
          return false;
        }
      }
    }
    return false;
  },


  /**
   *
   *  Test each input
   *
   *  @param   input    HTMLObject
   *  @param   regex    Array             Array of regular expressions to test against
   *                                      Example: regex: 0 => /^autocomplete$/i, 1 => /^otherRegExToFollow$/
   *
   *  @return           Boolean
   *
   */
  isField : function(input, regex) {
    // Turn the input attributes object into an array to iterate through
    var attrs = Array.prototype.slice.call(input.attributes);
    // Loop through each attribute to determine what it is
    for (var i = 0; i < attrs.length; i++) {
      for (var n = 0; n < regex.length; n++) {
        // If the current attribute is the "id"
        if (attrs[i].name == "id") {
          // Search the document for the corresponding <label>
          var inputLabel = document.querySelector("label[for=" + attrs[i].value + "]");
          // If the label is found...
          if (inputLabel != undefined && inputLabel != null) {
            // Test the innerHTML of the <label> item against the Regular Expression
            if (regex[n].test(inputLabel.innerHTML)) {
              return input;
            }
          }
        }
        if (regex[n].test(attrs[i].value)) {
          return input;
        }
      }
    }
    return null;
  },


  /**
   *
   *  Translates the input fields into parts of an identity object
   *
   *  @param      input     HTMLObject   HTML Element to be translated
   *  @param      filter    String       Filter type
   *  @param      identity  Object       Identity object
   *
   *  @return                          Puts the object into the proper identity object
   *
   */
  translateField : function(input, filter, identity) {
    // Get the attribute values of the <input> element in the form of an array then the object into a string to be easily tested against for regular expressions
    var inputStr = Array.from(input.attributes).map(a => a.value).toString();
    switch (filter) {
      case "name" :
        var regexFullName   = new RegExp(/fullname|full.name|\bname\b|first.last/i);
        var regexFirstName  = new RegExp(/given.name|firstname|first.name|\bfname|\bname.f\b/i);
        var regexMiddleName = new RegExp(/additional.name|middleinitial|^mi$|middlename|middle.name|mname/i);
        var regexLastName   = new RegExp(/family.name|lastname|last.name|\blname|\bname.l\b/i);

        // Check if firstName
        if (regexFirstName.test(inputStr)) {
          identity.firstName = input;
          return true;
        }
        //
        if (regexMiddleName.test(inputStr)) {
          identity.middleInitial = input;
          return true;
        }
        //
        if (regexLastName.test(inputStr)) {
          identity.lastName = input;
          return true;
        }
        //
        if (regexFullName.test(inputStr)) {
          identity.fullName = input;
          return true;
        }
        break;

      case "gender" :
        var regexGender  = new RegExp(/sex|gender/i);
        var regexGenderM = new RegExp(/\bmale/i);
        var regexGenderF = new RegExp(/female/i);
        var inputType    = input.getAttribute('type');
        var inputId      = input.getAttribute("id");
        if (regexGender.test(inputStr)) {
          if (regexGenderM.test(inputStr)) {
            // console.log('Male');
            identity.genderM = input;
            return true;
          }
          if (regexGenderF.test(inputStr)) {
            // console.log('Female');
            identity.genderF = input;
            return true;
          }
          if (inputType == "radio" && inputId != null) {
            var label = document.querySelector('[for=' + inputId + ']');
            if (regexGenderF.test(label.innerText)) {
              identity.genderF = input;
              return true;
            }
            if (regexGenderM.test(label.innerText)) {
              identity.genderM = input;
              return true;
            }
          }
          identity.gender = input;
          return true;
        }
        break;

      case "address" :
        // Example: 123 Main Street
        var regexStreetAddress = new RegExp(/street.address|streetaddress|^staddress|address.line1/i);
        // NY
        var regexState         = new RegExp(/address.level1|state/i);
        // Buffalo
        var regexCity          = new RegExp(/address.level2|city|town|village/i);
        // 80001
        var regexZipCode       = new RegExp(/zip|postal.code|postalcode/i);
        //
        if (regexStreetAddress.test(inputStr)) {
          identity.streetAddress = input;
          return true;
        }
        if (regexState.test(inputStr)) {
          identity.state = input;
          return true;
        }
        if (regexCity.test(inputStr)) {
          identity.city = input;
          return true;
        }
        if (regexZipCode.test(inputStr)) {
          identity.zip = input;
          return true;
        }
        break;

      case "email" :
        var regexEmail = new RegExp(/email|e.mail/i);
        var fieldType = input.getAttribute('type');
        if (regexEmail.test(inputStr)) {
          if (fieldType != null && (fieldType == "text" || fieldType == "email")) {
            if (identity.email == null || identity.email == undefined || identity.email == "") {
              identity.email = input;
            } else {
              identity.emailConf = input;
            }
            return true;
          }
        }
        break;

      case "username" :
        var regExUsername = new RegExp(/username|user.name/i);
        var fieldType     = input.getAttribute('type');
        var inputId       = input.getAttribute('id');
        if (fieldType != null && fieldType == "text") {
          if (inputId != null && inputId != undefined && inputId != "") {
            var inputLabel = document.querySelector("label[for=" + inputId + "]");
            if (inputLabel != null && inputLabel != undefined) {
              if (regExUsername.test(inputLabel.innerHTML)) {
                identity.username = input;
                return true;
              }
            }
          }
          if (regExUsername.test(inputStr)) {
            identity.username = input;
            return true;
          }
        }
        break;

      case "password" :
        var regexPassword = new RegExp(/new.password|^password$|^passwd|register.password|sign.up.password|\bpassword/i);
        if (regexPassword.test(inputStr)) {
          if (identity.password == null || identity.password == undefined || identity.password == '') {
            identity.password = input;
          } else {
            identity.passwordConf = input;
          }
          return true;
        }
        break;

      case "dob" :
        var regexDob      = new RegExp(/^bday$|^dob$|^birthday$/i);
        var regexDobMonth = new RegExp(/bday.month|dobmonth|dob.month|birthday.month|birth.month|birthmonth/i);
        var regexDobDay   = new RegExp(/bday.day|dobday|dob.day|birthday.day|birth.day/i);
        var regexDobYear  = new RegExp(/bday.year|dobyear|dob.year|birthday.year|birthyear|birth.year/i);
        if (regexDob.test(inputStr)) {
          identity.dob = input;
          return true;
        }
        if (regexDobMonth.test(inputStr)) {
          identity.dobMonth = input;
          return true;
        }
        if (regexDobDay.test(inputStr)) {
          identity.dobDay = input;
          return true;
        }
        if (regexDobYear.test(inputStr)) {
          identity.dobYear = input;
          return true;
        }
        break;
      return false;
    }
  },


  /**
   *
   *  Based on the URL determine if we're signing up or registering
   *
   *  @param   url    String           URL to be checked
   *
   *  @return         String|Boolean   Returns signup or login or false
   *
   */
  signupRegisterURL : function(url) {
    //
    var signup = new RegExp(/signup|sign.up|^reg|register|registration|join/i);
    var login  = new RegExp(/login|log.in|^log|sign.in/i);
    //
    if (signup.test(url)) {
      return "signup";
    } else if (login.test(url)) {
      return "login";
    } else {
      return false;
    }
  }

}

/**
 *
 *  filler.js exists to automatically fill the fields that are given to it by the
 *  crawler.js script. Filler and crawler work together to provide an autofill system
 *  for forms that involve registration or logging in.
 *
 */

var filler = {

  /**
   *
   *  Fills in the fields of the form that were found and bound to the form object
   *
   *  @param   form        Object     Object containing all of the found fields in the target form
   *  @param   identity    Object     Object containing a newly created identity
   *
   *  @return              Boolean
   *
   */
  fillForm : function(form, identity) {
    console.log("Form: ", form);
    console.log("Identity: ", identity);
    //
    Object.getOwnPropertyNames(form).forEach(
      function (val, idx, array) {
        if (form[val] != null && form[val] != undefined && form[val] != "") {
          form[val].focus();
          form[val].value = identity[val];
          form[val].blur();
          switch (val) {
            case "passwordConf" :
            form[val].value = identity['password'];
            break;
            case "emailConf" :
            form[val].value = identity['email'];
            break;
            case "fullName" :
            form[val].value = identity['firstName'] + " " + identity['lastName'];
            break;
            case "genderF" :
            if (identity['gender'] === "F") {
              form['genderF'].checked = true;
            }
            break;
            case "genderM" :
            if (identity['gender'] === "M") {
              form['genderM'].checked = true;
            }
            break;
            case "dobMonth":
            if (form['dobMonth'].tagName.toLowerCase() === "select") {
              var month = form['dobMonth'].querySelector('[value="' + identity['month'] + '"]');
              month.selected = 1;
            }
            break;
            case "dobDay":
            if (form['dobDay'].tagName.toLowerCase() === "select") {
              var day = form['dobDay'].querySelector('[value="' + identity['day'] + '"]');
              day.selected = 1;
            }
            break;
            case "dobYear":
            if (form['dobYear'].tagName.toLowerCase() === "select") {
              var year = form['dobYear'].querySelector('[value="' + identity['year'] + '"]');
              year.selected = 1;
            }
            break;
          }
        }
      }
    );
  }
}

/**
 *
 *  This script handles all of the communication between the content script,
 *  background script, and the extension itself.
 *
 */
var comm = {

  /**
   *
   *  Sends out a message and returns the response inside of a promise
   *
   *  @param    message     Object
   *
   *  @return               Promise      Response (if any) from the callback
   *
   */
  sendMessage : function(message = {"text" : "helo"}) {
    return new Promise((resolve, reject) => {
      var id = chrome.runtime.id;
      // console.log("COMM ID: ", id);
      chrome.runtime.sendMessage(id, message, function talkBack(response) {
        // Return any response that we may receive
        console.log("**COMMS Response", response);
        resolve(response);
      });
      if (chrome.runtime.lastError) {
        console.log("Error: ", chrome.runtime.lastError.message);
        // Something bad happened
        reject("Error ", chrome.runtime.lastError.message);
      }
    });
  }

}


/**
 *
 *
 */
function storeData(key, data) {
  chrome.storage.local.set({key: data}, function() {
    return true;
  });
}


/**
 *
 *  Pulls data from the chrome storage. This was a mistake to use, never again.
 *  From now on only use localStorage as it is cross-browser compatible
 *
 */
 function getData(key) {
  chrome.storage.local.get([key], function(result) {
    return result.key;
  });
 }


/**
 *
 *  This function gets the identities for the subscriber from the remote server and stores them in localStorage.identities
 *  First it checks against the localStorage.hash with the hash that the server has stored for the user
 *  If the hashes match, then the identities are up-to-date. Otherwise, we have to fetch the new identities
 *
 */

function getIdentities() {
  return new Promise((resolve, reject) => {
    var token = localStorage.getItem('token');
    // This will be the hash for requesting identities from the server
    var hash  = localStorage.getItem('hash');
    // If the hash is not set then set it to 0
    if (!hash) {
      hash = "KL0N";
    }
    var str   = {"token" : token, "hash" : hash};
    console.log('str: ', str);
    var payload = formatPayload(str).then( payload => {
      if (typeof payload != "object") {
        var strPayload = JSON.stringify(payload);
      } else {
        strPayload = payload;
      }
      var url = apiUrl + "/klon/getIdentities";
      var results = ajx.req("POST", url, strPayload, "json");
      results.then(function(result) {
        // console.log("Results: ", result);
        if (result['data'] != "up-to-date") {
          // console.log("result data does not equal: 'up-to-date'");
          if (result['message'] != "no identities found") {
            // Save it using the Chrome extension storage API.
            var setId    = localStorage.setItem('identities', result['data']);
            // Hash the data
            var bitArray = sjcl.hash.sha1.hash(result['data']);
            // Convert the bitArray into hex
            var idHash   = sjcl.codec.hex.fromBits(bitArray);
            // Set the hash in the localStorage
            var addHash  = localStorage.setItem('hash', idHash);
            // Resolve the promise
            resolve("Yepp");
          } else {
            chrome.storage.local.set({identities: null}, function() {
              console.log('Value is set to ' + null);
            });
            resolve("No identities found");
          }
        } else {
          console.log('up-to-date!');
          errorHandler.setError("Success!", "Identities are up-to-date!", false);
          errorHandler.showError();
          resolve("Up to date");
        }
      })
      .catch( err => {
        console.log('getIdentities Error: ', err);
        if (err == "Unauthorized.") {
          localStorage.removeItem('token');
          window.location = "login.html";
        }
        reject(false);
      });
    })
    .catch( err => {
      console.log("Error: ", err);
      errorHandler.setError("Error", err);
      errorHandler.showError();
      reject(false);
    });
  });
}


/**
 *
 *  Get a particular identity by the identity number
 *
 *  @param    id      Number       Identity number you want to get from the server
 *
 *  @return           Object       Identity
 *
 */
function getIdentity(id) {
  return new Promise((resolve, reject) => {
    var token = localStorage.getItem("token");
    var url   = apiUrl + "/klon/getIdentity";
    var data  = {"token" : token, "id" : id};
    var payload = formatPayload(data).then( payload => {
      var strPayload = JSON.stringify(payload);
      var results = ajx.req("POST", url, strPayload, "json");
      results.then(function(result) {
        console.log("This is the response: ", result);
        resolve(result);
      })
      .catch(err => {
        console.log("Error: ", err);
        reject(err);
      });
    });
  });
}

/**
 *
 *  This pulls the identities out of the localStorage.identities holder and turns them into a JSON object
 *
 *
 *
 */
async function getStorageData() {
  var data    = localStorage.getItem('identities');
   // Check to make sure there are identities stored for the user.
  if (!data) {
    // Return null, this means there's no identities found for the user
    return null;
  }
  var rawKey  = localStorage.getItem('key');
  try {
    console.log("Data: ", data);
    console.log("Raw Key: ", rawKey);
    var key   = await splitHexValue(rawKey, 8);
     // Assemble our lovely JSON object below...
     // Determine if the data is base64 encoded or not
    if (base64.isEncoded(data) === true) {
      console.log("Base64 Encoded");
      // Base64 decode data
      data = window.atob(data);
    } else {
      console.log("Not Base64 Encoded.");
    }
    // Get the index of the pipe (this is the end of the column names)
    var pipe = data.indexOf("|");
    console.log("Pipe: ", pipe);
    // Split the string at the pipe index
    var columns = data.substr(0, pipe);
    // Split the string into column names in an array
    var columnNames = columns.split(",");
    // Get the identities (from the first pipe to the end of the string)
    var identities = data.substr(++pipe);
    identities = identities.split("|");
    var jsonId = "[";
    for (var i = 0; i < identities.length; i++) {
      var breakItUp = identities[i].split(",");
      for (var n = 0; n < breakItUp.length; n++) {
        if (n == 0) {
          jsonId = jsonId + "{" + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"' + ",";
        } else if (n == (breakItUp.length - 1)) {
          jsonId = jsonId + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"},';
        } else {
          jsonId = jsonId + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"' + ",";
        }
      }
      var prev = i;
    }
    // Remove the last comma
    jsonId = jsonId.substr(0,(jsonId.length - 1));
    // Add the closing bracket for the JSON object
    jsonId = jsonId + "]";
    // console.log("JSON BB: ", jsonId);
    jsonId = JSON.parse(jsonId);
    //
    chrome.storage.local.set({identities: jsonId});
    // Remove the old version of this thing
    localStorage.removeItem('identities');
    return true;
  } catch (err) {
    console.log('getStorageData Error: ', err);
    return false;
  }
}

// chrome.storage.local.set({identities: value}, () => {return value});


/**
 *
 *  Get all of the URL's stored in the encrypted data so that we know what sites
 *  to look out for when running the background.js script
 *
 *  Sets the urls in localStorage.urls
 *
 *  Example: var o = localStorage.getItem('urls');
 *               o = o.split(",");
 *           Now...
 *               o[0] = "http://example.com"
 *               o[1] = "http://twitter.com"
 *               o[2] = "http://google.com"
 *               etc...
 *
 *
 */
function getAllURLs() {
  return new Promise((resolve, reject) => {
    var urls = new Array();
    // Get the identities from the chrome local storage
    var id = chrome.storage.local.get(['identities'], result => {
      if (chrome.runtime.lastError) {
        console.log("Error: ", chrome.runtime.lastError);
        reject(chrome.runtime.lastError);
      }
      console.log("Result: ", result);
      if (typeof result != "object") {
        result = JSON.parse(result.identities);
      }
      result = result["identities"];
      // console.log("Result: ", result);
      if (result != undefined && result.length > 0 && result.length != undefined) {
        // Convert hex key into Int32Array
        var key = splitHexValue(localStorage.getItem('key'), 8).then(val => {
          result.forEach( (item,index) => {
            // Make sure the item isn't already in the list. -- That would be stupid to add twice
            if (urls.indexOf(result[index].url) == -1) {
              // Decrypt the URL's and push them onto the urls array
              urls.push(decryptIt(val, result[index].url));
            }
          });
          // Set the urls item inside the localStorage space
          localStorage.setItem('urls', urls);
          // console.log("URLS have been set");
          resolve(urls);
        })
        .catch(err => {
          console.log("Error: " + err);
          reject(err);
        });
      } else {
        // URLs are not presently in chrome.storage.local attempt to get them before giving up
        var getId = getIdentities();
        getId.then(ids => {
          // Good we got the identities from the remote server now get them out of the local data
          var makeObject = getStorageData();
          if (makeObject === true) {
            console.log("DO THE THING");
            return getAllURLs();
          } else if (makeObject === null) {
            console.log("No identities found");
            resolve("No identities found");
          } else {
            reject("Error: I did what I could");
          }
        }).catch(err => {
          reject("Error: ", err);
        });
        // console.log("Error: identities not found");
        // The user has no identities...
        localStorage.setItem("urls", 0);
        resolve("identities not found");
      }
    });
  });
}


/**
 *
 *  Count the number of times a URL appears in the localStorage('urls') setting
 *
 *  @param     URL    String | Object | Array      The URL to check for
 *
 *  @return           Integer                      Number of matches
 *
 */
async function countURLs(url) {
  var urls;
  // Check to see if urls is set.
  if (localStorage.getItem("urls") != undefined && localStorage.getItem("urls") != null) {
    var urls  = localStorage.getItem("urls");
  } else {
    urls = await getAllURLs();
  }
  // Setup the regular expression with case insensitivity and global flags
  var regEx = new RegExp(url, "ig");
  // Check for matches within the urls string
  var matches = urls.match(regEx);
  //
  if (matches) {
    // Spit the answer out
    return matches.length;
  } else {
    return 0;
  }
  return null;
}


/**
 *
 *  Build a list of identities for the user to see on the screen.
 *
 */
function createIdentityListItem() {
  return new Promise(function(resolve, reject) {
    // Get the identities out of chrome.storage.local
    var id = chrome.storage.local.get(['identities'], function(result) {
      var sandbox = document.querySelector('.idList');
      // console.log("Sandbox: ", sandbox);
      var key = localStorage.getItem('key');
      // console.log("Key: ", key);
      if (key != undefined && key != null && key != "") {
        key = splitHexValue(key, 8);
        key.then(arrKey => {
          // console.log("Key Arr: ", arrKey);
          // console.log("Id length: ", id);
          if (typeof result != "object") {
            result = JSON.parse(result.identities);
          }
          // If the result has no identities...
          if (!result.identities) {
            //
            sandbox.innerHTML = '<li class="idList__item idList__item--none"><div class="idList__details"><div class="idList__username">You have no identities stored yet.</div></div></a></li>';
            resolve("Complete");
            return "Complete";
          }
          result = result.identities;
          // console.log("$$$ RESULT: ", result);
          // console.log("### RESULT LENGTH: ", result.length);
          // console.log("### RESULT TYPE: ", typeof result);
          for (var i = 0; i < result.length; i++) {
            var email   = decryptIt(arrKey, result[i].assignedEmail);
            var url     = decryptIt(arrKey, result[i].url);
            var slashes = url.indexOf("//");
            url = url.slice(slashes + 2);
            // console.log("Email: ", email);
            sandbox.innerHTML += '<li class="idList__item idList__item--hide"><a class="idList__identityWrapper" href="identity-details.html?id=' + i + '"><div class="idList__imageWrapper"><img class="idList__image" height="48px" width="48px" src="https://protonmail.com/images/safari.svg"></div><div class="idList__details"><div class="idList__title">' + url + '</div><div class="idList__username">' + email + '</div></div></a><div class="idList__itemOptions"></div></li>';
          }
        });
        resolve("Complete");
      } else {
        console.log("invalid key");
        reject(false);
      }
    });
  });
}


/**
 *
 *  Get the evt.parentNode get text or innerHTML copy to clipboard
 *
 */
function bindIdListItemAfter(evt) {
  var textToCopy = evt.target.parentNode.querySelector(".idList__username").innerText;
  // Put the text into a textarea to be copied
  var textarea = document.createElement("textarea");
  textarea.style.height     = "2em";
  textarea.style.width      = "2em";
  textarea.style.background = "transparent";
  textarea.style.border     = "none";
  textarea.style.boxShadow  = "none";
  textarea.style.outline    = "none";
  textarea.style.position   = "fixed";
  textarea.style.bottom     = "-4em";
  textarea.style.left       = "-4em";
  // Set the text in the textarea
  textarea.value = textToCopy;
  // Add the textarea to the body of the document
  document.body.appendChild(textarea);
  // Focus on the textarea
  textarea.focus();
  // Select the textarea text
  textarea.select();
  // Copy the text
  try {
      var successful = document.execCommand("copy");
      var msg = successful ? "successful" : "unsuccessful";
      errorHandler.setError("","The email address was " + msg + " copied");
      errorHandler.showError(3500);
    } catch (err) {
      errorHandler.setError("Error", err.message);
      errorHandler.showError(3500);
    }
  // Remove the textarea from the page
  document.body.removeChild(textarea);
}


function showListItems() {
  return new Promise(function(resolve, reject) {
    var loadingText = document.querySelector("#idLoading");
    var listItems   = document.querySelector('.idList');
    var n = 20;
    var c = 0;
    var itemArray = new Array();
    for (var i = 0; i < listItems.children.length; i++) {
      var item = listItems.children[i];
      var itemAfter = item.querySelector(".idList__itemOptions");
      itemAfter.addEventListener("click", bindIdListItemAfter);
      if (item.classList && item.classList.contains('idList__item--hide')) {
        // console.log('Item: ', item);
        itemArray[c] = item;
        c++;
      }
    }
    // console.log('Item Array: ', itemArray);
    itemArray.forEach(function(item, index) {
      n = n + 25;
      setTimeout( () => {
        if (item.classList.contains('idList__item--hide')) {
          item.classList.remove('idList__item--hide');
        } else {
          // console.log('item: ', item);
          // console.log('error');
        }
      }, n, item);
    });
    // Hide the "loading" text
    loadingText.style.display = "none";
    resolve("Items Shown");
  });
}

/**
 *
 *  The identity object holds all the functions and properties related to identities.
 *  Eventually all the functions contained here will be moved inside this object for organization pruposes and ease-of-use.
 *
 */
var identities = {
  /**
  *
  *  Return all of the identitites for a particular URL
  *
  */
  fetchIdentitiesForURL : function(url, key = null) {
    return new Promise(function(resolve, reject) {
      // Check to see if the key was sent as a parameter
      if (key === null || key === undefined) {
        // Get the user's private key
        var prvKey = localStorage.getItem("key");
        // Make sure the key exists
        if (prvKey != null && prvKey != undefined) {
          try {
            // Split the key into a usable typed array
            arrKey = splitHexValue(prvKey, 8);
            arrKey.then(key => {
              key = key;
            })
            .catch(err => {
              console.log("Error: ", err);
            });
          } catch (e) {
            // console.log("key value could not be split");
            console.log("Error: ", e.message);
            reject(false);
          }
        } else {
          console.log("Private key could not be found.");
          reject(false);
        }
      }
      // The key was successfully split into a typed array...
      // Check if we have the identities stored
      chrome.storage.local.get("identities", result => {
        // Get the array of identities
        result = result["identities"];
        var relevantIdentities = [];
        // Loop through all the URL's of each identity to get relevant identities
        result.forEach((item, index) => {
          // Decrypt the URL's
          var decryptedUrl = decryptIt(key, result[index].url);
          if (decryptedUrl === url) {
            relevantIdentities.push(result[index]);
          }
        });
        // console.log("relevant identities: ", relevantIdentities);
        resolve(relevantIdentities);
      });
    });
  },


  /**
   *
   *  Decrypt Identity
   *
   *  @param    id           Object     The identity object to be decrypted. Can decrypt 1 or more identities.
   *                         Example:   {identities : [{firstName: "Bob", lastName : "Smith"}, {firstName : "John", lastName : "Doe"} etc... ]}
   *
   *  @return                Object     Returns a decrypted identity for use in forms
   *
   */
  decryptIdentity : async function(id, key = null) {
    // Check if the users key was included as a parameter and that it is the correct type
    if (key === null || key === undefined || Object.prototype.toString.call(key) != "[object Int32Array]") {
      console.log("Key not included");
      // Get the users private key
      try {
        var key = await decrypt.getKey();
      } catch(e) {
        console.log("Error: ", e);
        return false;
      }
    }
    // Verify the object is setup properly
    if (typeof id === "object") {
      // Check to see that the object contains the property "identities"
      if (id.hasOwnProperty("identities") === true) {
        // Check if id.identities is an object
        if (typeof id.identities === "object") {
          //
          // All the requirements are met let's loop through the array and decrypt the values
          try {
            for (let i = 0; i < id.identities.length; i++) {
              //
              Object.keys(id.identities[i]).forEach(function(index) {
                //
                if (index != "id" && index != "salt") {
                  // console.log("Encrypted Value: ", id.identities[i][index]);
                  id.identities[i][index] = decryptIt(key, id.identities[i][index]);
                  // console.log("Decrypted Value: ", id.identities[i][index]);
                }
                //
              });
            }
            return id.identities;
          } catch(e) {
            console.log("Error: ", e);
          }
        } else {
          console.log("Invalid type");
          return false;
        }
      } else {
        console.log("Does not contain identity property");
        return false;
      }
    } else {
      console.log("Invalid parameter type");
      return false;
    }
  }


    // var ids;
    // // Get the user's private key
    // var prvKey = await decrypt.getKey();
    // // We have the user's key
    // // console.log("Private Key: ", prvKey);
    // // Make sure that id is an object with the property "identities" and that this "identities" is of type "array"
    // if (typeof id === "object") {
    //   // Check that id has the property of "identities"
    //   if (id.hasOwnProperty("identities") === true) {
    //     // Check that id.identities is an array as it should be
    //     if (typeof id.identities === "object" || typeof id.identities === "array") {
    //       //
    //       console.log("ID IDENTITIES: ", id.identities);
    //       // All the requirements are met let's loop through the array and decrypt the values
    //       try {
    //         for (let i = 0; i < (id.identities.length - 1); i++) {
    //           console.log("ID IDENTITIES LENGTH: ", id.identities.length);
    //           console.log("CURRENT I: ", i);
    //           //
    //           console.log("Object Keys: ", Object.keys(id.identities[i]));
    //           Object.keys(id.identities[i]).forEach(function(index) {
    //             //
    //             if (index != "id" && index != "salt") {
    //               // console.log("Encrypted Value: ", id.identities[i][index]);
    //               id.identities[i][index] = decryptIt(prvKey, id.identities[i][index]);
    //               // console.log("Decrypted Value: ", id.identities[i][index]);
    //             }
    //             //
    //           });
    //           //
    //           console.log("ID IDENTITY: ", id.identities[i]);
    //           console.log("Iteration i: ", i);
    //         }
    //       } catch(e) {
    //         console.log("Error: ", e);
    //       }
    //       // debugger;
    //       console.log("ID IDENTITIES: ", id.identities);
    //       ids = id.identities;
    //       return ids;
    //     } else {
    //       // console.log("Typeof id.identities: ", typeof id.identities);
    //       // console.log("id: ", JSON.stringify(id));
    //       // console.log("id: ", id);
    //       // console.log("id.identities: ", JSON.stringify(id.identities));
    //       // console.log("id.identities: ", id.identities);
    //       return "object must contain an array of identities";
    //     }
    //   } else {
    //     return "parameter missing property identities";
    //   }
    // } else {
    //   return "incorrect parameter type";
    // }
    // Loop through the identities and decrypt them
    // for (let i = 0; i < id.identities.length; i++) {
    //
    // }


};



// Get the identities from the chrome local storage

// var id = chrome.storage.local.get(['identities'], result => {
//   console.log("Result: ", result);
//   if (typeof result != "object") {
//     result = JSON.parse(result.identities);
//   }
//   result = result["identities"];
//   // console.log("Result: ", result);
//   if (result != undefined && result.length > 0 && result.length != undefined) {
//     // Convert hex key into Int32Array
//     var key = splitHexValue(localStorage.getItem('key'), 8).then(val => {
//       result.forEach( (item,index) => {
//         // Make sure the item isn't already in the list. -- That would be stupid to add twice
//         if (urls.indexOf(result[index].url) == -1) {
//           // Decrypt the URL's and push them onto the urls array
//           urls.push(decryptIt(val, result[index].url));
//         }
//       });
//       // Set the urls item inside the localStorage space
//       localStorage.setItem('urls', urls);
//       // console.log("URLS have been set");
//       resolve(urls);
//     })
//     .catch(err => {
//       console.log("Error: " + err);
//       reject(err);
//     });
//   } else {
//     // URLs are not presently in chrome.storage.local attempt to get them before giving up
//     var getId = getIdentities();
//     getId.then( ids => {
//       // Good we got the identities from the remote server now get them out of the local data
//       var makeObject = getStorageData();
//       if (makeObject === true) {
//         console.log("DO THE THING");
//         return getAllURLs();
//       } else {
//         reject("Error: I did what I could");
//       }
//     }).catch(err => {
//       reject("Error: ", err);
//     });
//     console.log("Error: identities not found");
//     reject("identities not found");
//   }
// });


/**
 *
 *  Fetches the previously generated identity from the localStorage then displays it on the page
 *
 *
 */
function displayGeneratedIdentity() {
  var idWrapper = document.querySelector("#idWrapper");
  var identity  = localStorage.getItem("generatedId");
  if (identity) {
    identity = JSON.parse(identity);
    // Clear any previous results
    idWrapper.innerHTML  = "";
    // Convert date to words not just numbers, less confusing for the user
    const date = new Date(identity.year, Number(identity.month - 1), identity.day);
    const month = date.toLocaleString('en-us', { month: 'long' });
    const day = date.toLocaleString('en-us', { day: '2-digit' });
    // If the gender returned is "M" then gender = male otherwise gender = female
    var gender = identity.gender == "M" ? gender = "Male" : gender = "Female";
    // Display the new results...
    idWrapper.innerHTML   = "<div class='id__userRow' data-field='Full name'><div class='id__key'>Name:</div><div class='id__value'> " + identity.firstName + " " + identity.middleInitial + ". " + identity.lastName + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Gender'><div class='id__key'>Gender: </div><div class='id__value'> " + gender + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='DOB'><div class='id__key'>Birthday:</div><div class='id__value'> " + month + " " + day + ", " + identity.year + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Address'><div class='id__key'>Address:</div><div class='id__value'> " + identity.streetAddress + "<br>" + identity.city + ", " + identity.state + " " + identity.zip + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Email address'><div class='id__key'>Email: </div><div class='id__value'> " + identity.email + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Username'><div class='id__key'>Username: </div><div class='id__value'> " + identity.username + "</div></div>";
    // Generate password locally on the fly
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Password'><div class='id__key'>Password: </div><div class='id__value'> " + identity.password + "</div></div>";
    copyPasta();
  }
}


/**
 *
 *  Close the popup and let the contentScript know that the user wants to select the form to fill
 *
 */
function fillForm() {
  var identity = JSON.parse(localStorage.getItem("generatedId"));
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {msg:"prepareFillForm", identity: identity}, function(response) {
    // chrome.tabs.sendMessage(tabs[0].id, {msg:"prepareFillForm", identity: identity, autofill : autofill, autogenerate : autogenerate}, function(response) {
      window.close();
      // alert(response)
      // $("#text").text(response);
    });
});
}


/**
 *
 *
 */
function clearIdentity() {
  var result = confirm("Do you want to clear this identity from the screen?");
  if (result) {
    var identity = localStorage.getItem("generatedId");
    if (identity) {
      var idWrapper = document.querySelector("#idWrapper");
      try {
        localStorage.removeItem("generatedId");
        idWrapper.innerHTML = "";
        // Remove the buttons
        showClearButton();
        // Remove the fill button
        showFillButton();
        // Toggle the showUserGen button
        toggleShowUserGen();
        // Close the user gen menu
        closeUserGen();
        return true;
      } catch(e) {
        idWrapper.innerHTML = "Error: ", e.message;
        return false;
      }
    }
  } else {
    return false;
  }
}


/**
 *
 *
 *
 */
function copyDetails(e) {
  // Get the value of the row clicked
  // If statement clears up an issue when the user clicks the :after pseudo element
  if (e.target.classList.contains("id__userRow")) {
    var parent = e.target;
  } else {
    var parent = e.target.parentNode;
  }
  var value  = parent.querySelector(".id__value");
  var field  = parent.dataset.field;
  value      = value.innerText;
  // Copy the value to the clipboard
  var textArea = document.createElement("textarea");
  textArea.style.background = "transparent";
  textArea.style.border     = "none";
  textArea.style.boxShadow  = "none";
  textArea.style.height     = "2em";
  textArea.style.outline    = "none";
  textArea.style.position   = "fixed";
  textArea.style.bottom     = "-4em";
  textArea.style.left       = "-4em";
  textArea.style.width      = "2em";
  textArea.value = value;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();
  try {
    document.execCommand("copy");
    errorHandler.setError(field, " successfully copied!", false);
    errorHandler.showError(3500);
  } catch(e) {
    console.log("Value is: ", value);
    errorHandler.setError("Error", e.message);
    errorHandler.showError(3500);
  }
  document.body.removeChild(textArea);
  return null;
}


/**
 *
 *  Make all of the rows copy-able
 *
 */
function copyPasta() {
  var rows = document.querySelectorAll(".id__userRow");
  for (let i = 0; i < rows.length; i++) {
    rows[i].addEventListener("click", copyDetails, false);
  }
  return true;
}


/**
 *
 *  Check to see if there is a previously generated identity stored in the localStorage
 *  If a previous identity is found it is fetched, parsed, then displayed on the page for the user to see
 *
 */
function checkPreviousGeneratedId() {
  var prevId = localStorage.getItem("generatedId");
  prevId = JSON.parse(prevId);
  if (prevId) {
    displayGeneratedIdentity();
  }
  return true;
}


/**
 *
 *  Generates an identity for the user at their request
 *
 */
function genManualIdentity() {
  try {
    var genId = generateIdentity();
    var errorHandler = document.querySelector("#errorHandler");
    var idWrapper = document.querySelector("#idWrapper");
    errorHandler.innerHTML  = "Generating...";
    errorHandler.style.opacity = 1;
    errorHandler.style.height = "20px";
    genId.then(identity => {
      // Clear any previous results
      idWrapper.innerHTML  = "";
      // Convert DOB date to words not just numbers, less confusing for the user
      // Ex: February 07, 1988
      const date = new Date(identity.year, Number(identity.month - 1), identity.day);
      const month = date.toLocaleString('en-us', { month: 'long' });
      const day = date.toLocaleString('en-us', { day: '2-digit' });
      // If the gender returned is "M" then gender = male otherwise gender = female
      var gender = identity.gender == "M" ? gender = "Male" : gender = "Female";
      // Display the new results...
      idWrapper.innerHTML  = "<div class='id__userRow' data-field='Full name'><div class='id__key'>Name:</div><div class='id__value'> " + identity.firstName + " " + identity.middleInitial + ". " + identity.lastName + "</div></div>";
      idWrapper.innerHTML  += "<div class='id__userRow' data-field='Gender'><div class='id__key'>Gender: </div><div class='id__value'> " + gender + "</div></div>";
      idWrapper.innerHTML  += "<div class='id__userRow' data-field='DOB'><div class='id__key'>Birthday:</div><div class='id__value'> " + month + " " + day + ", " + identity.year + "</div></div>";
      idWrapper.innerHTML  += "<div class='id__userRow' data-field='Address'><div class='id__key'>Address:</div><div class='id__value'> " + identity.streetAddress + "<br>" + identity.city + ", " + identity.state + " " + identity.zip + "</div></div>";
      idWrapper.innerHTML  += "<div class='id__userRow' data-field='Email Address'><div class='id__key'>Email: </div><div class='id__value'> " + identity.email + "</div></div>";
      // Generate username locally on-the-fly
      usernameGen.firstName = identity.firstName;
      usernameGen.lastName  = identity.lastName;
      var username = usernameGen.genUsername();
      // Generate password locally
      var password = generatePassword(18, true, true, true, true);
      idWrapper.innerHTML  += "<div class='id__userRow' data-field='Username'><div class='id__key'>Username: </div><div class='id__value'> " + username + "</div></div>";
      // Generate password locally on the fly
      idWrapper.innerHTML  += "<div class='id__userRow' data-field='Password'><div class='id__key'>Password: </div><div class='id__value'> " + password + "</div></div>";
      // Reset the errorHandler
      errorHandler.style.opacity = 0;
      errorHandler.style.height = 0;
      errorHandler.innerText = "";
      // Set the generated identity in the localStorage
      var generatedIdentity = identity;
      generatedIdentity.username = username;
      generatedIdentity.password = password;
      localStorage.setItem("generatedId", JSON.stringify(generatedIdentity));
      // Show the fill form button
      showFillButton();
      // Show the clear identity button
      showClearButton();
      // Bind functionality to the close button
      bindBtnClose();
      // Show the user generated menu
      showUserGen();
      // ionno
      copyPasta();
    })
    .catch(err => {
      console.log("Error: ", err);
      idWrapper.innerText = err.message;
      return false;
    });
  } catch(e) {
    errorHandler.setError("Error", e.message);
    errorHandler.showError(3500);
    return false;
  }
}


/**
 *
 *  This will determine wether or not to display the clear button
 *
 */
function showClearButton() {
  // Get a handle on the clearButton
  var btnClear = document.querySelector("#btnClearId");
  // If an identity has been prepared for the user, display the clear button
  if (localStorage.generatedId !== undefined && localStorage.generatedId !== null) {
    try {
      // Check if the button is hidden
      if (btnClear.classList.contains("id__button--hidden")) {
        btnClear.classList.remove("id__button--hidden");
        if (btnClear.hasAttribute("disabled")) {
          btnClear.removeAttribute("disabled");
        }
        return true;
      }
    } catch(e) {
      errorHandler.setError("Error", e.message);
      errorHandler.showError(3500);
      console.log("Error: ", e.message);
      return false;
    }
  } else {
      if (!btnClear.classList.contains("id__button--hidden")) {
        btnClear.classList.add("id__button--hidden");
        // Disable the button
        btnClear.setAttribute("disabled", true);
      }
    return false;
  }
}


/**
 *
 *  show or hide the showUserGen button
 *
 */
function toggleShowUserGen() {
  try {
    var btnShowUserGen = document.querySelector("#showUserGen");
    // If the element is found in the DOM
    if (document.body.contains(btnShowUserGen)) {
      // If there is NOT a generatedId stored in the localStorage
      if (localStorage.generatedId === null || localStorage.generatedId === undefined) {
        // If the button doesnt have the hidden class applied then apply it and disable the button
        if (!btnShowUserGen.classList.contains("id__showUserGen--hidden")) {
          // Hide the button
          btnShowUserGen.classList.add("id__showUserGen--hidden");
          return true;
        } else {
          return true;
        }
        btnShowUserGen.classList.add();
        btnShowUserGen.innerText = "Hide generated ID";
        btnShowUserGen.removeEventListener("click", showUserGen);
        btnShowUserGen.addEventListener("click", closeUserGen);
      } else {
        // Determine if the Show generated identity menu is hidden or not
        var showGenUserMenu = document.querySelector("#idWrapper");
        if (showGenUserMenu.classList.contains("id__usergen--show")) {
          btnShowUserGen.innerText = "Hide generated ID";
          btnShowUserGen.removeEventListener("click", showUserGen);
          btnShowUserGen.addEventListener("click", closeUserGen);
          if (btnShowUserGen.classList.contains("id__showUserGen--hidden")) {
            btnShowUserGen.classList.remove("id__showUserGen--hidden");
          }
          return true;
          // If the show user generated button is not hidden then hide it
          // if (!btnShowUserGen.classList.contains("id__showUserGen--hidden")) {
            // Remove the hidden class
            // btnShowUserGen.classList.add("id__showUserGen--hidden");
            // Remove disabled attribute
            // btnShowUserGen.setAttribute("disabled", true);
          // }
        } else {
          btnShowUserGen.innerText = "Show generated ID";
          btnShowUserGen.removeEventListener("click", closeUserGen);
          btnShowUserGen.addEventListener("click", showUserGen);
        }
        return false;
      }
    } else {
      return false;
    }
  } catch(e) {
    // Set and show the error
    errorHandler.setError("Error", e.message);
    erorrHandler.showError(3500);
    return false;
  }
}


/**
 *
 *  Bind the functionality to the #idGenClose button
 *
 */
function bindBtnClose() {
  var btnClose = document.querySelector("#idGenClose");
  if (document.body.contains(btnClose)) {
    btnClose.addEventListener("click", closeUserGen, false);
    return true;
  } else {
    return false;
  }
}


/**
 *
 *  add functionality to the Show Genrated ID menu
 *
 */
function bindShowUserGen() {
  //
  var btnShowMenu = document.querySelector("#showUserGen");
  if (document.body.contains(btnShowMenu)) {
    btnShowMenu.addEventListener("click", showUserGen, false);
  } else {
    return false;
  }
}


/**
 *
 *  Close the userGen menu
 *
 */
function closeUserGen() {
  try {
    var userGenMenu    = document.querySelector("#idWrapper");
    var btnShowUserGen = document.querySelector("#showUserGen");
    if (document.body.contains(userGenMenu) && document.body.contains(btnShowUserGen)) {
      if (userGenMenu.classList.contains("id__usergen--show")) {
        userGenMenu.classList.remove("id__usergen--show");
        // Toggle the showing of the showUserGen button
        toggleShowUserGen();
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  } catch(e) {
    errorHandler.setError("Error", e.message);
    errorHandler.showError(3500);
    return false;
  }
}


/**
 *
 *  Show the userGen menu
 *
 */
function showUserGen() {
  try {
    var userGenMenu = document.querySelector("#idWrapper");
    // If the element exists...
    if (document.body.contains(userGenMenu)) {
      if (!userGenMenu.classList.contains("id__usergen--show")) {
        userGenMenu.classList.add("id__usergen--show");
        // Add the close functionality to the button
        bindBtnClose();
        // Hide the showUserGenMenu button
        toggleShowUserGen();
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  } catch(e) {
    errorHandler.setError("Error", e.message);
    errorHandler.showError(3500);
    return false;
  }
}


/**
 *
 *  This will determine wether or not to display the fill button
 *
 */
function showFillButton() {
  // Get a handle on the clearButton
  var btnFill = document.querySelector("#btnFillForm");
  // If an identity has been prepared for the user, display the clear button
  if (localStorage.generatedId !== undefined && localStorage.generatedId !== null) {
    try {
      // Check if the button is hidden
      if (btnFill.classList.contains("id__button--hidden")) {
        btnFill.classList.remove("id__button--hidden");
        if (btnFill.hasAttribute("disabled")) {
          btnFill.removeAttribute("disabled");
        }
        return true;
      }
    } catch(e) {
      errorHandler.setError("Error", e.message);
      errorHandler.showError(3500);
      console.log("Error: ", e.message);
      return false;
    }
  } else {
      if (!btnFill.classList.contains("id__button--hidden")) {
        btnFill.classList.add("id__button--hidden");
        // Disable the button
        btnFill.setAttribute("disabled", true);
      }
    return false;
    }
  }


window.addEventListener("load", function() {
  if (window.location.pathname === "/identities.html") {
    // Bind the functions to the id generate buttons
    var btnGenId = document.querySelector("#btnGenId");
    btnGenId.addEventListener("click", genManualIdentity, false);
    //
    var btnFillForm = document.querySelector("#btnFillForm");
    btnFillForm.addEventListener("click", fillForm, false);
    //
    var btnClearId = document.querySelector("#btnClearId");
    btnClearId.addEventListener("click", clearIdentity, false);
    // Set the session tab so we can return later after closing the extension
    localStorage.sessionTab = "identities";
    // Clear out all the old stuff -- not necessarily a good idea every time
    // localStorage.removeItem("identities");
    // localStorage.removeItem("hash");
    // console.log("Removed identities and hash from local storage");
    // Do not need to get the identities from the server unless they're expired or something has been added/deleted
    var getIds = getIdentities();
    // console.log("Getting identities...");
    getIds.then( ids => {
      //
      if (ids == "No identities found") {
        var loading = document.querySelector("#idLoading");
        loading.innerHTML = "No identities found.";
      }
      console.log("getIdentities: ", ids);
      var getStorage = getStorageData();
      getStorage.then( result => {
        console.log("getStorage: ", result);
        var getURLs = getAllURLs();
        getURLs.then(urls => {
          console.log("getURLs: ", urls);
          var createListItems = createIdentityListItem();
          createListItems.then(result => {
            console.log("createListItems: ", result);
            var showItems = showListItems();
            showItems.then(result => {
              console.log("Boom Done.", result);
            }).catch(err => {
              console.log("Error: ", err);
            });
          }).catch(err => {
            console.log("Error: ", err);
          });
        }).catch(err => {
          console.log("Error: ", err);
          return false;
        });
      }).catch(err => {
        console.log("Error: ", err);
        return false;
      });
    }).catch( err => {
      console.log("Error: ", err);
    });
    // Bind the functionality to the nav
    nav.bindNav();
    // Check to see if there was a previously generated identity
    checkPreviousGeneratedId();
    // Decide wether or not to show the genID buttons
    showFillButton();
    showClearButton();
    bindBtnClose();
    bindShowUserGen();
    toggleShowUserGen();
  }
});

//@prepros-append passwordGenerator.js
//@prepros-append usernameGenerator.js
//@prepros-append time.js

/**
 *
 *  parasite.js will find the form we are hooking to and embed HTML, CSS, and JS
 *  into the host <form> This way we can display our button and give the user the
 *  ability to select a different identity on the page.
 *
 */
var parasite = {
  config : {},
  identityFields : {},
  identities : [],
  currentPos : 0,
  limiter : 0,
  smLogo : "url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2NCA2NCI+PGNpcmNsZSBjeD0iMTEuOSIgY3k9IjkuMSIgcj0iOC45IiBmaWxsPSIjMTQzZGExIi8+PGNpcmNsZSBjeD0iMTEuOSIgY3k9IjU1IiByPSI4LjkiIGZpbGw9IiMxNDNkYTEiLz48cGF0aCBmaWxsPSIjMTQzZGExIiBkPSJNNTIuNyA0NS4yYy0yLjItLjMtNC40LTEtNi4zLTJhMTMuMiAxMy4yIDAgMCAxIDEuMy0yMy4xYzEuNS0uNyAzLjEtMS4xIDQuNy0xLjNhOS40IDkuNCAwIDAgMCA1LjMtMTYuNSA5LjMgOS4zIDAgMCAwLTEyLjQuMSA5LjUgOS41IDAgMCAwLTMuMSA1LjlMNDIgOS43YTEzLjMgMTMuMyAwIDAgMS0xMi44IDEyLjhsLTEuNC4yLS4zLjEtLjYuMS0xIC4zLS43LjMtLjguNGEzIDMgMCAwIDAtLjcuNWwtLjcuNS0uMi4yLS4zLjQtLjUuNS0uMy4zLS41LjYtLjYuOS0uMy43LS4zLjktLjIuOC0uMS45LS4xLjkuMS45LjIgMS4yLjQgMS4zLjMuNi41IDEuMS40LjYuNS43LjIuMi4yLjMuNy42LjUuNCAxLjQuOC4yLjEgMS43LjYuNi4xLjkuMmMxIC4xIDIuMS4zIDMgLjYgNS42LjggMTAuMSA1LjYgMTAuNiAxMS42bC4yIDEuNWE5LjQgOS40IDAgMCAwIDE2LjUgNS4zIDkuMyA5LjMgMCAwIDAtLjEtMTIuNCA5LjUgOS41IDAgMCAwLTUuOS0zLjF6Ii8+PC9zdmc+)",

  /**
   *
   *
   */
  injectPayload : function() {
    // Enter CSS to inject into the <head>
    var css = ".klonLabel{color:#fff !important;display:block !important;font-size:14px !important;text-align:left !important}#klonWrapper{align-items:center;background-color:rgba(0, 0, 0, 0.6);display:flex;height:100vh;justify-content:center;position:fixed;right:0;top:0;width:100vw;z-index:999}#klonWrapper *{box-sizing:border-box !important}#klonForm{background:rgb(20, 61, 161);border-radius:12px;height:600px;padding:8px;width:360px;display:grid;grid-template-rows:0.5fr 0.5fr auto 1fr;position:relative}.klonHeader{color:rgb(255, 255, 255);font-size:32px;font-family:system-ui;text-align:center;margin:0 auto;display:flex;align-self:flex-end}.klonIdentityInputField{height:32px !important;width:100% !important;display:block !important;margin:0 0 8px !important;border:none !important;border-radius:6px !important;padding:4px 8px !important;font-size:18px !important;background:rgba(255,255,255,0.1) !important;color:#fff !important}.klonIdentityInputField--inline{display:inline-block !important}.klonInputGroup{margin:0 auto;width:100%;max-width:320px !important;display:block}.klonInputGroup--inline{display:grid !important;grid-template-columns:1fr 1fr !important}[name=klonFirstName],[name=klonLastName]{width:136px !important}[name=klonMiddleName]{margin:0 4px !important;width:36px !important}.klonButtons{align-items:center !important;display:flex !important;justify-content:center !important}.klonGroupHalf{padding:0 4px 0 0}.klonGroupHalf:last-child{padding:0 0 0 4px}#klonInnerWrapper{display:grid;grid-template-columns:1fr;overflow:hidden}.klonButton{background:#255bde !important;background-image:url('data:image/svg+xml; base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0iI2ZmZiI+PHBhdGggZD0iTTIwLjMgMkw5IDEzLjZsLTUuMy01TDAgMTIuMyA5IDIxIDI0IDUuN3oiLz48L3N2Zz4=') !important;background-size:36px 36px !important;background-repeat:no-repeat !important;background-position:center center !important;border:none !important;border-radius:50% !important;box-shadow:0 0 10px 1px rgba(0,0,0,0.2);cursor:pointer !important;display:inline-block !important;height:60px !important;margin:8px auto !important;width:60px !important}.klonButton--AcceptId{background-image:url('data:image/svg+xml; base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0iI2ZmZiI+PHBhdGggZD0iTTE5LjUgMTVhNC41IDQuNSAwIDEgMCAwIDkgNC41IDQuNSAwIDAgMCAwLTl6bTIuNSA1aC0ydjJoLTF2LTJoLTJ2LTFoMnYtMmgxdjJoMnYxem0tNy4yIDRIMHYtMS4yYzAtMi42LjItNCAzLjItNC43IDMuMy0uOCA2LjctMS41IDUtNC40QzMuNiA0LjkgNyAwIDEyIDBjNi44IDAgNy41IDcuNiAzLjYgMTMuNy0xLjMgMi0yLjYgMy42LTIuNiA1LjggMCAxLjcuNyAzLjMgMS44IDQuNXoiLz48L3N2Zz4=') !important}#klonClose{color:#fff;font-size:24px;position:absolute;right:12px;top:0;cursor:pointer;padding:4px}#klonScrollLeft,#klonScrollRight{cursor:pointer;height:16px}#klonScrollLeft{margin:0 8px 0 0}#klonScrollRight{margin:0 0 0 8px}#klonScroll{text-align:center;color:#fff;font-size:16px;display:flex;justify-content:center;align-items:center;font-weight:700}[name=klonPassword]{display:inline-block !important;width: calc(100% - 54px) !important}.klonPasswordButton{cursor:pointer;height:16px;margin:8px 4px 0;width:16px;}.klonHidden{display:none !important;}#klonPopUpBtn{align-items:center;background-color:#fff;border-radius:50%;bottom:2vh;box-shadow:0px 1px 4px 0px rgba(0, 0, 0, 0.3);color:#fff;cursor:pointer;display:flex;height:24px;justify-content:center;padding:8px;position:fixed;right:2vw;transform:rotate(360deg);width:24px;z-index:998;}";
    var style = document.createElement('style');
    style.innerText = css;
    document.head.appendChild(style);
    var wrapper  = document.createElement("DIV");
    wrapper.classList.add("klonHidden");
    wrapper.id   = "klonWrapper";
    var klonHTML = '<div id="klonForm" style=""><div id="klonClose">&times;</div><h1 class="klonHeader">New Identity</h1><div id="klonScroll"><span id="klonScrollLeft"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTExLjkgMEwxNiA0bC04IDggOCA4LTQuMSA0TDAgMTIgMTEuOSAweiIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==" height="16" width="16"></span><span id="klonScrollCurrent">1</span>/<span id="klonScrollTotal">1</span><span id="klonScrollRight"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTguMSAyNEw0IDIwbDgtOC04LTggNC4xLTRMMjAgMTJ6IiBmaWxsPSIjZmZmIi8+PC9zdmc+" height="16" width="16"></span></div><div id="klonInnerWrapper"><div class="klonInputGroup"><label class="klonLabel">Name</label><input name="klonFirstName" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"><input name="klonMiddleName" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"><input name="klonLastName" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"></div><div class="klonInputGroup"><label class="klonLabel">Email</label><input name="klonEmail" type="text" readonly="" class="klonIdentityInputField"></div><div class="klonInputGroup"><label class="klonLabel">Username</label><input name="klonUsername" type="text" readonly="" class="klonIdentityInputField"></div><div class="klonInputGroup"><label class="klonLabel">Password</label><input name="klonPassword" type="password" readonly="" class="klonIdentityInputField"><span id="klonShowPassword"><img class="klonPasswordButton" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTEyIDdjNC44IDAgOCAzIDkuNSA0LjYtMS40IDEuOS00LjcgNS40LTkuNSA1LjQtNC40IDAtOC0zLjUtOS41LTUuNEM0IDkuOSA3LjQgNyAxMiA3em0wLTJDNC40IDUgMCAxMS42IDAgMTEuNlM0LjggMTkgMTIgMTljNy43IDAgMTItNy40IDEyLTcuNFMxOS43IDUgMTIgNXptMCAzYTQgNCAwIDEgMCAwIDggNCA0IDAgMCAwIDAtOHptMCA0YTEuNCAxLjQgMCAxIDEtMi0yIDEuNCAxLjQgMCAwIDEgMiAyeiIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==" height="16" width="16"></span><span id="klonGeneratePassword"><img class="klonPasswordButton" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTEzLjUgMkM3LjkgMiAzLjMgNi40IDMgMTJIMGw0LjUgNkw5IDEySDZhNy41IDcuNSAwIDEgMSAxLjYgNS4xbC0xLjggMi41QTEwLjUgMTAuNSAwIDAgMCAyNCAxMi41QzI0IDYuNyAxOS4zIDIgMTMuNSAyeiIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==" height="16" width="16"></span></div><div class="klonInputGroup klonInputGroup--inline"><div class="klonGroupHalf"><label class="klonLabel">Gender</label><input name="klonGender" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"></div><div class="klonGroupHalf"><label class="klonLabel">Date of Birth</label><input name="klonDOB" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"></div></div><div class="klonInputGroup"><label class="klonLabel">Address</label><input name="klonAddress1" type="text" readonly="" class="klonIdentityInputField"><input name="klonAddress2" type="text" class="klonIdentityInputField"></div></div><div class="klonButtons"><button class="klonButton klonButton--AcceptId" type="button" id="klonNewIdentity"></button><button class="klonButton" type="button" id="klonAcceptIdentity"></button></div></div>';
    //
    document.body.appendChild(wrapper);
    wrapper.insertAdjacentHTML("beforeend", klonHTML);
    var popUpButton     = document.createElement("DIV");
    var popUpButtonHTML = '<div id="klonPopUpBtn"><img alt="Klon" height="20" width="20" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2NCA2NCI+PGNpcmNsZSBjeD0iMTEuOSIgY3k9IjkuMSIgcj0iOC45IiBmaWxsPSIjMTQzZGExIi8+PGNpcmNsZSBjeD0iMTEuOSIgY3k9IjU1IiByPSI4LjkiIGZpbGw9IiMxNDNkYTEiLz48cGF0aCBmaWxsPSIjMTQzZGExIiBkPSJNNTIuNyA0NS4yYy0yLjItLjMtNC40LTEtNi4zLTJhMTMuMiAxMy4yIDAgMCAxIDEuMy0yMy4xYzEuNS0uNyAzLjEtMS4xIDQuNy0xLjNhOS40IDkuNCAwIDAgMCA1LjMtMTYuNSA5LjMgOS4zIDAgMCAwLTEyLjQuMSA5LjUgOS41IDAgMCAwLTMuMSA1LjlMNDIgOS43YTEzLjMgMTMuMyAwIDAgMS0xMi44IDEyLjhsLTEuNC4yLS4zLjEtLjYuMS0xIC4zLS43LjMtLjguNGEzIDMgMCAwIDAtLjcuNWwtLjcuNS0uMi4yLS4zLjQtLjUuNS0uMy4zLS41LjYtLjYuOS0uMy43LS4zLjktLjIuOC0uMS45LS4xLjkuMS45LjIgMS4yLjQgMS4zLjMuNi41IDEuMS40LjYuNS43LjIuMi4yLjMuNy42LjUuNCAxLjQuOC4yLjEgMS43LjYuNi4xLjkuMmMxIC4xIDIuMS4zIDMgLjYgNS42LjggMTAuMSA1LjYgMTAuNiAxMS42bC4yIDEuNWE5LjQgOS40IDAgMCAwIDE2LjUgNS4zIDkuMyA5LjMgMCAwIDAtLjEtMTIuNCA5LjUgOS41IDAgMCAwLTUuOS0zLjF6Ii8+PC9zdmc+"></div>';
    popUpButton.id      = "klonPopUpBtn";
    document.body.insertAdjacentHTML("beforeend", popUpButtonHTML);
  },


  /**
   *
   *
   */
  closeKlonCreate : function() {
    var wrapper  = document.querySelector("#klonWrapper");
    var popUpBtn = document.querySelector("#klonPopUpBtn");
    if (!wrapper.classList.contains("klonHidden")) {
      wrapper.classList.add("klonHidden");
      popUpBtn.classList.remove("klonHidden");
    }
  },


  /**
   *
   */
  showForm : function() {
    var popUpBtn = document.querySelector("#klonPopUpBtn");
    var wrapper = document.querySelector("#klonWrapper");
    if (wrapper.classList.contains("klonHidden")) {
      popUpBtn.classList.add("klonHidden");
      wrapper.classList.remove("klonHidden");
    }
  },


  /**
   *
   *  Get the subscribers settings from the other side of the extension
   *
   */
  getSettings : function() {
    return new Promise((resolve, reject) => {
      console.log("Let's get the settings!");
      // This is the number of minutes to allow between checking settings
      var threshold = 5;
      // Determine if it has been less than 5 minutes since the last check
      console.log("Parasite Config Expiration: ", parasite.config.expiration);
      if (parasite.config.hash == undefined || parasite.config.urls == undefined || parasite.config.enabled == undefined || parasite.config.subscription != undefined || parasite.config.expiration != undefined || parasite.config.expiration + (threshold * 60) > time.timestamp) {
        // Get the subscribers account type
        var url = window.location.protocol + "//" + window.location.host;
        comm.sendMessage({"msg" : "get settings", "url" : url}).then(result => {
          //
          console.log("Result: ", result);
          parasite.config.hash         = result.hash;
          parasite.config.urls         = result.urls;
          parasite.config.enabled      = Number(result.enabled);
          parasite.config.subscription = Number(result.subscription);
          parasite.config.expiration   = time.timestamp();
          //
          console.log("Parasite Config: ", parasite.config);
          resolve(parasite.config);
        }).catch(err => {
          console.log("Error: ", err);
          reject(err);
        });
      } else {
        console.log("Settings already saved or do not exist.");
        resolve(parasite.config);
      }
    });
  },


  /**
   *
   *
   *
   */
  checkSettings : function() {
    return new Promise((resolve, reject) => {
      // Get the current URL
      var url    = window.location.protocol + "//" + window.location.host;
      console.log("Parasite Config Enabled: ", parasite.config);
      // Make sure the account is enabled
      if (parasite.config.enabled === 1) {
        // If the account is enabled lets figure out what their subscription level is...
        switch (parasite.config.subscription) {
          case 0:
            // Not an account -- Or maybe reserved for super saiyans like myself
            alert("Nope.");
            reject(false);
            break;
          case 1:
            // Do whatever they want they're paying the big $$$
            console.log("Premium Account, carry on");
            resolve("premium");
            break;
          case 2:
            // Do whatever they want, as long as it's within reason...
            console.log("Regular Account");
            // Check to see if this account already has 5 accounts at this URL
            if (typeof parasite.config.urls === "number" && parasite.config.urls < 5) {
              console.log("Not maxed out, carry on");
              resolve("regular");
            } else {
              // The user is maxed out. Plug the premium account thing
              console.log("Config: ", parasite.config.urls);
              console.log("You are maxed out on accounts for this URL. Consider a premium account @ [Insert Link Here]");
              reject("maxed out");
            }
            break;
          case 3:
            // Reserved for free trials and what not
            console.log("Idk?");
            reject(false);
          break;
          default:
            reject(false);
            break;
        }
      } else {
        alert("Klon account is disabled.");
        reject(false);
      }
    });
  },


  /**
   *
   *  Assemble a new identity candidate for the user to choose from
   *
   *  @return                 Object             Identity that can be selected to be used for a website
   *
   */
  createNewIdentity : function() {
    var btnNewIdentity = document.querySelector("#klonNewIdentity");
    var getSet         = parasite.getSettings();
    getSet.then(setting => {
      console.log("The settings have been had.");
      var checkSet = parasite.checkSettings();
      checkSet.then(check => {
        console.log("Alrighty, ");
        // Determine if the subscriber has the ability to do this
        if (parasite.identities.length < 5) {
          comm.sendMessage({"msg" : "new identity"}).then(result => {
            // console.log("createNewIdentity Result: ", result);
            usernameGen.firstName = result.firstName;
            usernameGen.lastName  = result.lastName;
            result.username = usernameGen.genUsername();
            result.password = generatePassword(16, true, true, true, true);
            result.mgemail  = result.email;
            parasite.identities[parasite.identities.length] = result;
            parasite.currentPos = parasite.currentPos + 1;
            parasite.setScrollNavNumbers();
            parasite.displayIdentity(parasite.identityFields, parasite.identities[parasite.currentPos - 1]);
          });
        } else {
          // console.log("Derp.");
          try {
            btnNewIdentity.removeEventListener("click", parasite.createNewIdentity);
          } catch {
            console.log("Event listener not found");
          }
        }
      })
      .catch(err => {
        console.log("Error: ", err);
      });
    }).catch(err => {
      console.log("Error: ", err);
      return false;
    });
  },


  /**
   *
   *  Display the current identity in the fields on the popup for the user to view
   *
   *  @param    identity    Object     Object full of fields found on the page
   *  @param    result      Object     Pass the single identity object to the function
   *
   *  @return               Boolean    True on success, False on error
   *
   */
  displayIdentity : function(identity, result) {
    //
    var klonFirstName  = document.querySelector('[name=klonFirstName]');
    var klonMiddleName = document.querySelector('[name=klonMiddleName]');
    var klonLastName   = document.querySelector('[name=klonLastName]');
    var klonEmail      = document.querySelector('[name=klonEmail]');
    var klonUsername   = document.querySelector('[name=klonUsername]');
    var klonPassword   = document.querySelector('[name=klonPassword]');
    var klonGender     = document.querySelector('[name=klonGender]');
    var klonDOB        = document.querySelector('[name=klonDOB]');
    var klonAddress1   = document.querySelector('[name=klonAddress1]');
    var klonAddress2   = document.querySelector('[name=klonAddress2]');
    //
    klonFirstName.value   = result.firstName;
    klonMiddleName.value  = result.middleInitial;
    klonLastName.value    = result.lastName;
    klonEmail.value       = result.email;
    klonUsername.value    = result.username;
    klonPassword.value    = result.password;
    klonGender.value      = result.gender;
    klonDOB.value         = result.month + "-" + result.day + "-" + result.year;
    klonAddress1.value    = result.streetAddress;
    klonAddress2.value    = result.city + ", " + result.state + " " + result.zip;
  },


  /**
  *
   * Scrolls the Identity to the left
   *
   */
  scrollIdLeft : function() {
    if (parasite.identities[parasite.currentPos - 1] != undefined && Number(parasite.currentPos - 1) > 0) {
      parasite.currentPos = parasite.currentPos - 1;
      parasite.setScrollNavNumbers();
      var boolIdentity = parasite.displayIdentity(parasite.identityFields, parasite.identities[parasite.currentPos - 1]);
    } else {
      // console.log("Nope");
      return false;
    }
  },

// Scrolls the Identity to the right
  scrollIdRight : function() {
    if (Number(parasite.currentPos + 1) <= parasite.identities.length) {
      parasite.currentPos = parasite.currentPos + 1;
      parasite.setScrollNavNumbers();
      var boolIdentity = parasite.displayIdentity(parasite.identityFields, parasite.identities[parasite.currentPos - 1]);
    } else {
      return false;
    }
  },


  /**
   *
   *  Toggle the password field between type "password" and "text"
   *  Also changes the image from a show to hide icon
   *
   */
  showPassword : function() {
    var passwordField      = document.querySelector("[name=klonPassword]");
    var showPasswordButton = document.querySelector("#klonShowPassword");
    var showPasswordImage  = showPasswordButton.querySelector(".klonPasswordButton");
    if (passwordField.type === "password") {
      passwordField.type = "text";
      showPasswordImage.src = "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTE5LjYgMi42bC0zLjMgMy4xQzE1IDUuMyAxMy42IDUgMTIgNSA0LjQgNSAwIDExLjYgMCAxMS42czIgMi45IDUuMSA1bC0yLjkgM0wzLjYgMjEgMjEgNGwtMS40LTEuNHptLTYgNS43YTQgNCAwIDAgMC01LjMgNS4ybC0xLjcgMS44YTE4LjIgMTguMiAwIDAgMS00LTMuN0M0IDkuOSA3LjMgNyAxMiA3YzEgMCAxLjguMSAyLjYuM2wtMSAxem0tMi45IDcuNWw1LTVjMS4xIDMtMiA2LTUgNXpNMjQgMTEuNlMxOS43IDE5IDEyIDE5Yy0xLjQgMC0yLjYtLjMtMy44LS43bDEuNi0xLjZjLjcuMiAxLjQuMyAyLjIuMyA0LjggMCA4LjEtMy41IDkuNS01LjQtLjctLjgtMi0yLTMuNi0zbDEuNS0xLjRjMyAyIDQuNiA0LjQgNC42IDQuNHoiIGZpbGw9IiNmZmYiLz48L3N2Zz4=";
    } else {
      passwordField.type = "password";
      showPasswordImage.src = "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTEyIDdjNC44IDAgOCAzIDkuNSA0LjYtMS40IDEuOS00LjcgNS40LTkuNSA1LjQtNC40IDAtOC0zLjUtOS41LTUuNEM0IDkuOSA3LjQgNyAxMiA3em0wLTJDNC40IDUgMCAxMS42IDAgMTEuNlM0LjggMTkgMTIgMTljNy43IDAgMTItNy40IDEyLTcuNFMxOS43IDUgMTIgNXptMCAzYTQgNCAwIDEgMCAwIDggNCA0IDAgMCAwIDAtOHptMCA0YTEuNCAxLjQgMCAxIDEtMi0yIDEuNCAxLjQgMCAwIDEgMiAyeiIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==";
    }
  },


  /**
   *
   *  Generate a password (TODO: Based on the users settings)
   *
   */
  generatePwd : function() {
    console.log("Current Position: " + parasite.currentPos);
    console.log("Identities: ", parasite.identities);
    var length = 12;
    var useLowerChars = true;
    var useUpperChars = true;
    var useNumericChars = true;
    var useSpecialChars = true;
    var passwd = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    var klonPassword   = document.querySelector('[name=klonPassword]');
    parasite.identities[parasite.currentPos - 1].password = passwd;
    klonPassword.value = passwd;
  },


  /**
   *
   *
   *
   *
   */
  setScrollNavNumbers : function() {
    var scrollCurrent = document.querySelector("#klonScrollCurrent");
    var scrollTotal   = document.querySelector("#klonScrollTotal");
    scrollCurrent.innerHTML = parasite.currentPos;
    scrollTotal.innerHTML   = parasite.identities.length;
  },


  /**
   *
   *  The user has decided they want to use a particular identity for a given website
   *  This function will encrypt and store the identity in the database for the user
   *  to use again in the future.
   *
   *  @param       identity      Object      The identity object
   *
   *  @return                    Boolean     True or False depending on success
   *
   */
  useCandidate : function() {
    // Set the URL property of the identity object
    parasite.identities[parasite.currentPos - 1].url = window.location.protocol + "//" + window.location.host;
    console.log(window.location.protocol + "//" + window.location.host);
    console.log("Identity: ", parasite.identities[parasite.currentPos - 1]);
    comm.sendMessage({"msg" : "use identity", "identity" : parasite.identities[parasite.currentPos - 1]}).then(result => {
      console.log("Parasite Response: ", result);
      // If the API returns a successful result remove the Klon New Identity form from the screen and fill the form with these credentials
      if (result.status === "success") {
        // Remove the thing from the screen by adding the "klonHidden" class
        var klonWrapper = document.querySelector("#klonWrapper");
        if (!klonWrapper.classList.contains("klonHidden")) {
          klonWrapper.classList.add("klonHidden");
        }
        // Fill the form with the details
        filler.fillForm(parasite.identityFields, parasite.identities[parasite.currentPos - 1]);
      }
      // Let the user know there was an issue selecting this identity.
      else {
        console.log("Error");
        return false;
      }
    });
  },


  /**
   *
   *  This will inject the controls into the form for the "Login" autofill feature
   *
   */
  injectLoginMenu : function(identities) {
    return new Promise(function(resolve, reject) {
      //
      var loginFormHTML = '';
      var loginFormCSS  = '';
      // Get the settings
      var settings = parasite.getSettings();
      settings.then(result => {
        // console.log("Parasite Object: ", parasite);
        //
        // console.log("Get Settings Result: ", result);
        //
        // console.log("Parasite Config: ", parasite.config);
        //
        // console.log("Form: ", form);
        //
        console.log("Identities: ", identities);
        //
        var loginButtonHTML = "<div class='klonHide' id='klonLoginMenu'></div>";
        var loginButtonCSS  = "#klonLoginMenu{background-color: #12368F;border-radius: 2px;box-shadow: 0px 2px 6px 1px rgba(25, 25, 25, 0.4);box-sizing:border-box;color: #fff;height: 200px;padding:8px 16px;position: absolute;width: 300px;z-index: 998;}#klonLoginMenu::before{content: '';z-index: 998;display: block;margin: -14px -12px 0 auto;width: 0;height: 0;border-left: 8px solid transparent;border-right: 8px solid transparent;border-bottom: 8px solid #12368F;}.klonLoginListItem{background-color:#143DA1;border-radius:10px;cursor:pointer;display:block;font-size:10px;list-style-type:none;text-transform:uppercase;letter-spacing:1px;margin:12px auto;padding:8px;text-align:center;transition:background 0.2s ease-out;width:90%;}.klonLoginListItem:hover{background-color: #255BDE;}.klonLoginMenuHeader{color:#fff;text-align:center;margin:8px auto 10px;}.klonLoginList{list-style-type:none;height:150px;overflow:hidden;overflow-y:scroll;}";
        //
        var style = document.createElement('style');
        style.innerText = loginButtonCSS;
        document.head.appendChild(style);
        // Insert the login menu right after the opening <body> tag
        var body = document.querySelector("body");
        body.insertAdjacentHTML("afterbegin", loginButtonHTML);
        var loginKlonMenu = document.querySelector("#klonLoginMenu");
        // Add the header to the login menu
        var menuHeading = document.createElement("h2");
        menuHeading.setAttribute("class", "klonLoginMenuHeader");
        var headingText = document.createTextNode("Login As...");
        menuHeading.appendChild(headingText);
        loginKlonMenu.appendChild(menuHeading);
        // Create the unordered list element
        var unorderedList = document.createElement("UL");
        unorderedList.setAttribute("class", "klonLoginList");
        loginKlonMenu.appendChild(unorderedList);
        // Loop through and put in the relevant identities
        for (let i = 0; i < identities.length; i++) {
          var listItem = document.createElement("LI");
          listItem.setAttribute("class", "klonLoginListItem");
          listItem.dataset.email    = identities[i].assignedEmail;
          listItem.dataset.username = identities[i].username;
          listItem.dataset.url      = identities[i].url;
          listItem.dataset.id       = i;
          // listItem.dataset.id       = identities[i].id;
          var textNode = document.createTextNode(identities[i].assignedEmail);
          listItem.appendChild(textNode);
          // Add the listItem to the menu
          unorderedList.appendChild(listItem);
          // listItem.setAttribute("attribute", "value")
        }
        resolve(true);
      })
      .catch(err => {
        console.log("Error: ", err);
        reject(false);
      });
    });
  },


  /**
   *
   *  This is the function to run when a #klonLoginMenu <li> is clicked.
   *
   */
  loginAs : function(evt, identity) {
    parasite.assocId[Number(evt.target.dataset.id)].email = parasite.assocId[Number(evt.target.dataset.id)].assignedEmail;
    console.log("Identity fields found: ", parasite.identityFields);
    console.log("Identity to use: ", parasite.assocId[Number(evt.target.dataset.id)]);
    // Fill the form
    filler.fillForm(parasite.identityFields, parasite.assocId[Number(evt.target.dataset.id)]);
    // Hide the menu
    try {
      var menu = document.querySelector("#klonLoginMenu");
      if (!menu.classList.contains("klonHide")) {
        menu.classList.add("klonHide");
      }
      return true;
    } catch (e) {
      console.log("loginAs Error: ", e.message);
      return false;
    }
  },


  /**
   *
   *  Bind functions to the <li> elements within the #klonLoginMenu
   *
   */
  bindLoginMenuFunctions : function() {
    return new Promise(function(resolve, reject) {
      //
      var menu = document.querySelector("#klonLoginMenu");
      // If the menu element is found within the document
      if (menu != null && menu != undefined) {
        // Get the list items within the menu
        var listItems = menu.querySelectorAll("li");
        // Make sure we found some list items to bind to
        if (listItems != null && listItems != undefined) {
          //
          for (let n = 0; n < listItems.length; n++) {
            console.log(listItems[n]);
            listItems[n].addEventListener("click", parasite.loginAs);
          }
          resolve(true);
        } else {
          reject(false);
        }
      } else {
        // The menu element is not found within the page
        reject(false);
      }
    });
  },


  injectInputButtons : function(input) {
    return new Promise((resolve, reject) => {
      // Pass the form inputs (username/email/password) and inject background image into those inputs
      if (input) {
        // Check to make sure the field does not already have the BG Image then inject the input field with the KLON background image
        if (!input.hasAttribute('data-klon')) {
          // Inject the background image
          // Get distance from top
          try {
            var t = window.pageYOffset + input.getBoundingClientRect().top;
            // console.log("Top: ", t);
            // Get distance from left
            var l = window.pageXOffset + input.getBoundingClientRect().left;
            // console.log("Left: ", l);
            // Get element height
            var h = input.clientHeight;
            // console.log("Height: ", h);
            // Get element width
            var w = input.clientWidth;
            // console.log("Width: ", w);
            // Get the width of the button to inject
            var btnW = 20;
            // Get the height of the button to inject
            var btnH = 20;
            // Assign a random unique ID to the button
            var klonBtnId    = Math.random().toString(36).substr(2, 12);
            //
            input.dataset.klon = klonBtnId;
            // The HTML of the button
            var klonBtn      = "<div class='klonBtn' id='klonBtn_" + klonBtnId + "'></div>";
            // The CSS for the button
            var klonBtnStyle = ".klonBtn {position:absolute;height:" + btnH + "px;width:" + btnW + "px;background:url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxOCIgaGVpZ2h0PSIyMCI+PHBhdGggZmlsbD0iIzg4OCIgZD0iTTE0IDBINEMxLjggMCAwIDEuOCAwIDR2MTJjMCAyLjIgMS44IDQgNCA0aDEwYzIuMiAwIDQtMS44IDQtNFY0YzAtMi4yLTEuOC00LTQtNHpNNC4xIDJjMS4yIDAgMi4xIDEgMi4xIDIuMiAwIDEuMi0xIDIuMi0yLjEgMi4yQzMgNi41IDIgNS41IDIgNC4yIDIgMyAzIDIgNC4xIDJ6bTAgMTZDMyAxOCAyIDE3IDIgMTUuOGMwLTEuMiAxLTIuMiAyLjEtMi4yIDEuMiAwIDIuMSAxIDIuMSAyLjIuMSAxLjItLjkgMi4yLTIuMSAyLjJ6bTguNC01LjJjLjUuMyAxIC40IDEuNS41LjYuMSAxIC4zIDEuNC44LjguOS44IDIuMiAwIDMuMS0uOSAxLjEtMi40IDEuMS0zLjMuMS0uNC0uNC0uNi0uOS0uNi0xLjUgMC0uMSAwLS4zLS4xLS40YTMuMyAzLjMgMCAwIDAtMi42LTNjLS4yLS4xLS41LS4xLS43LS4yaC0uMi0uMWMtLjEtLjEtLjMtLjEtLjQtLjJoLS4xYy0uMSAwLS4yLS4xLS4zLS4yLS4xIDAtLjEtLjEtLjEtLjFzLS4xLS4xLS4yLS4xbC0uMS0uMS0uMS0uMWMtLjEgMC0uMS0uMS0uMS0uMmwtLjEtLjFjMC0uMS0uMS0uMi0uMS0uMyAwLS4xIDAtLjEtLjEtLjIgMC0uMS0uMS0uMi0uMS0uM1YxMHYtLjItLjItLjJjMC0uMSAwLS4yLjEtLjIgMC0uMSAwLS4yLjEtLjJzMCAwIC4xLS4xLjEtLjIuMS0uMmMuMS0uMS4xLS4xLjEtLjIgMCAwIDAtLjEuMS0uMWwuMS0uMS4xLS4xaC4xYzAtLjEuMS0uMS4xLS4yLjEgMCAuMiAwIC4yLS4xLjEgMCAuMS0uMS4yLS4xIDAgMCAuMSAwIC4yLS4xLjEgMCAuMiAwIC4yLS4xSDguMWMuMSAwIC4yIDAgLjMtLjEgMS43IDAgMy0xLjQgMy4xLTMuMiAwLS4xLjEtLjIuMS0uNC4xLS42LjMtMS4xLjctMS41LjktLjggMi4xLS44IDMgMCAxIC45IDEgMi41LjEgMy41LS40LjQtLjkuNi0xLjQuNy0uNCAwLS44LjItMS4xLjNhMy40MyAzLjQzIDAgMCAwLS40IDUuOXoiLz48L3N2Zz4=') no-repeat;background-size:18px 18px;border-radius:4px;cursor:pointer;z-index:999} .klonHide{display:none;}";
            // Insert the style into the head of the document
            var style = document.createElement('style');
            style.innerText = klonBtnStyle;
            document.head.appendChild(style);
            // Insert the button after the opening <body> tag
            document.body.insertAdjacentHTML("afterbegin", klonBtn);
            var klonBtnHandle = document.querySelector("#klonBtn_" + klonBtnId);
            klonBtnHandle.style.top  = t + ((h - btnH) / 2) + "px";
            klonBtnHandle.style.left = l + (w - btnW) + "px";
          } catch(e) {
            console.log("Error: ", e.message);
            reject(e);
          }
        } else {
          // The input already has the klon button injected
          resolve(true);
        }
        resolve(true);
      } else {
        // The element does not exist
        resolve(true);
      }
    });
  },


  /**
   *
   *  This function will get all of the users identities that apply to this URL
   *
   */
  getAssociatedIdentities : function(url) {
    //

  },


  /**
   *
   *
   *
   */
  injectFunctions : function() {
    // Get a handle on the close button
    var btnClose = document.querySelector("#klonClose");
    btnClose.addEventListener("click", parasite.closeKlonCreate);
    // Get a handle on the 'Create new identity' button
    var btnNewIdentity = document.querySelector("#klonNewIdentity");
    btnNewIdentity.addEventListener("click", function (){parasite.createNewIdentity();});
    // Get a handle on the 'Accept Identity' button
    var btnAcceptIdentity = document.querySelector("#klonAcceptIdentity");
    btnAcceptIdentity.addEventListener("click", parasite.useCandidate);
    // Set the current position in the identity scrolling...
    // Get a handle on the 'Scroll Button Left'
    var btnScrollLeft = document.querySelector("#klonScrollLeft");
    btnScrollLeft.addEventListener("click", function(){parasite.scrollIdLeft()});
    // Get a handle on the 'Scroll Button Right'
    var btnScrollRight = document.querySelector("#klonScrollRight");
    btnScrollRight.addEventListener("click", function(){parasite.scrollIdRight()});
    //
    var showPassword = document.querySelector("#klonShowPassword");
    showPassword.addEventListener("click", parasite.showPassword);
    //
    var generatePasswordBtn = document.querySelector("#klonGeneratePassword");
    generatePasswordBtn.addEventListener("click", parasite.generatePwd);
    //
    var popUpButton = document.querySelector("#klonPopUpBtn");
    popUpButton.addEventListener("click", parasite.showForm);
  },


  //
}


/**
 *
 *  Generate Password  Example:            generatePassword(12, true, true, true, false);
 *
 *  @param   String    length              Length of the password to be generated
 *  @param   Boolean   useLowerChars       Use lower case letters in the password
 *  @param   Boolean   useUpperChars       Use upper case letters in the password
 *  @param   Boolean   useNumericChars     Use numbers in the password
 *  @param   Boolean   useSpecialChars     Use special characters in the password
 *
 *  @return  String    Returns a password based on the arguments provided
 *
 */

function generatePassword(length = 12, useLowerChars, useUpperChars, useNumericChars, useSpecialChars) {
  length = Number(length);
  if (length == undefined || typeof length == "string" || length == "") {
    length = 12;
  }
  // Add characters to the charset based on parameters
  var charset = "";
  var lowerCaseChars = "abcdefghjkmnpqrstuvwxyz";
  var upperCaseChars = "ABCDEFGHJKMNPQRSTUVWXYZ";
  var numericChars   = "123456789";
  var specialChars   = "~!@#$%^&*_+{}=";
  if (useLowerChars == true) {
    charset = charset + lowerCaseChars;
  }
  if (useUpperChars == true) {
    charset = charset + upperCaseChars;
  }
  if (useNumericChars == true) {
    charset = charset + numericChars;
  }
  if (useSpecialChars == true) {
    charset = charset + specialChars;
  }
  //
  var retVal = "";
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }

  // Make sure the string contains all of the things it is supposed to contain
  if (useLowerChars == true) {
    if (!retVal.match(/[a-z]/g)) {
      retVal = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    }
  }
  if (useUpperChars == true) {
    if (!retVal.match(/[A-Z]/g)) {
      retVal = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    }
  }
  if (useNumericChars == true) {
    if (!retVal.match(/[1-9]/g)) {
      retVal = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    }
  }
  if (useSpecialChars == true) {
    if (!retVal.match(/\~|\!|\@|\#|\$|\%|\^|\&|\*|\_|\+|\{|\}|\=/g)) {
      retVal = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    }
  }
  return retVal;
}


/**
 *
 *  Make the copy password function work
 *
 */
function copyPassword() {
  var password     = document.querySelector("#password");
  var errorHandler = document.querySelector("#errorHandler");
  if (errorHandler) {
    try {
      //
      password.removeAttribute("readonly");
      // Focus on the field
      password.focus();
      // Select the password
      password.select();
      // Copy the password
      var success = document.execCommand("copy");
      // Deselect the text
      window.getSelection().removeAllRanges();
      // Add the readonly attribute back to the input
      password.setAttribute("readonly", "true");
      // Determine if copying was successful
      var msg = success ? "successfully" : "unsuccessfully";
      // Display message to the user
      errorHandler.innerText = "Password was " + msg + " copied.";
    } catch (err) {
      errorHandler.innerText = "Error: Password could not be copied.";
    }
  } else {
    alert("An error has occurred please contact support.");
  }
}


/**
 *
 *
 */
  function strengthTest() {

    var password = document.querySelector("#password");
    var meter    = document.querySelector("#strengthMeter");
    // var text     = document.querySelector("password-strength-text");

    var val  = password.value;
    var text = document.querySelector("#strengthText");
    var strength = {0 : "Worst", 1 : "Bad", 2 : "Weak", 3 : "Good", 4 : "Strong"};
    var result = zxcvbn(val);

    // Update the password strength meter
    meter.value = result.score;

    // Add text
    if (val !== "") {
      text.innerHTML = strength[result.score];
    } else {
      text.innerHTML = "";
    }
    return;
  }

/**
*
*  Generates random usernames based off of the full name of the user in question
*
*  @param      firstName    String     The users first name
*  @param      lastName     String     The users last name
*
*  @return                  String     Username based off of first and last name
*
*/
var usernameGen = {
  firstName : "",
  lastName : "",
  username : "",

  //
  getRandomInt : function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
  },

  //
  genUsername : function() {
    if (usernameGen.firstName.length > 0 && usernameGen.lastName.length > 0) {
      // Get a number between 0 and 1 (true and false)
      var useFullFirstName = usernameGen.getRandomInt(0,2);
      // Number to be appended to the end of the username
      var randomNumber     = usernameGen.getRandomInt(10,10000);
      //
      if (useFullFirstName === 0) {
        // In this case we are NOT using the full first name in the username
        usernameGen.username =  usernameGen.firstName[0];
        usernameGen.username += usernameGen.lastName;
        usernameGen.username += randomNumber;
        return usernameGen.username;
      } else {
        // In this case we are NOT using the full first name in the username
        usernameGen.username =  usernameGen.firstName;
        usernameGen.username += usernameGen.lastName[0];
        usernameGen.username += randomNumber;
        return usernameGen.username;
      }
    } else {
      console.log("First and last name not set");
    }
  }
}

var time = {
  /**
   *
   *  This function returns a timestamp in seconds -- not milliseconds
   *
   */
  timestamp : function() {
    return Math.floor(new Date().getTime() / 1000);
  }
}

