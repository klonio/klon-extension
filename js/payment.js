/**
 *
 *  This is the payment object which handles everything having to do with creating
 *  a new payment to the Klon system. This object redirects the user to the page
 *  containing and iframe with the api.klon.io/v1/payment/{email} page within the
 *  extension. It then retrieves the response from the page and displays success
 *  or failure messages to the user.
 *
 */

var payment = {
  /**
   *
   *  Begin payment
   *
   *
   */
   buildPage: function() {
     // Get the subscriber's email address
     var email = localStorage.getItem('user');
     if (email !== null) {
       //
       email = email.trim();
       email = email.toLowerCase();
     } else {
       // Return error. Cannot process payment
       alert("Cannot process payment at this time. Please try again laster.");
       return false;
       // Prompt the user for their email address?
       // email = prompt("Please enter the email address associated with this account.");
       // Make sure the email address entered is a real subscriber
       // if () {}
       // If this is not a real subscriber and they do not have a subscription move on...
     }
     // Assemble the iframe
     var iframe = document.querySelector('#iframePayment');
     // Set the iframe attributes
     iframe.setAttribute('height', '100%');
     iframe.setAttribute('width', '100%');
     iframe.setAttribute('src', 'https://api.klon.io/v1/payment/' + email);
     return true;
   },


   /**
    *
    *
    *
    *
    */

}


/**
 *
 *  Build the payment page with the correct user data
 *
 */
if (window.location.pathname === "/payment.html") {
  window.addEventListener("load", function() {
    payment.buildPage();
  });
}
