window.addEventListener("load", function() {
  if (window.location.pathname === "/termsofservice.html" || window.location.pathname === "/privacypolicy.html") {
    //
    var historyButtons = document.querySelectorAll("#backBtn");
    if (historyButtons.length > 0) {
      for (var i = 0; i < historyButtons.length; i++) {
        historyButtons[i].addEventListener("click", function() {
          history.back();
        });
      }
    }
  }
});
