//@prepros-append passwordGenerator.js
//@prepros-append usernameGenerator.js
//@prepros-append time.js

/**
 *
 *  parasite.js will find the form we are hooking to and embed HTML, CSS, and JS
 *  into the host <form> This way we can display our button and give the user the
 *  ability to select a different identity on the page.
 *
 */
var parasite = {
  config : {},
  identityFields : {},
  identities : [],
  currentPos : 0,
  limiter : 0,
  smLogo : "url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2NCA2NCI+PGNpcmNsZSBjeD0iMTEuOSIgY3k9IjkuMSIgcj0iOC45IiBmaWxsPSIjMTQzZGExIi8+PGNpcmNsZSBjeD0iMTEuOSIgY3k9IjU1IiByPSI4LjkiIGZpbGw9IiMxNDNkYTEiLz48cGF0aCBmaWxsPSIjMTQzZGExIiBkPSJNNTIuNyA0NS4yYy0yLjItLjMtNC40LTEtNi4zLTJhMTMuMiAxMy4yIDAgMCAxIDEuMy0yMy4xYzEuNS0uNyAzLjEtMS4xIDQuNy0xLjNhOS40IDkuNCAwIDAgMCA1LjMtMTYuNSA5LjMgOS4zIDAgMCAwLTEyLjQuMSA5LjUgOS41IDAgMCAwLTMuMSA1LjlMNDIgOS43YTEzLjMgMTMuMyAwIDAgMS0xMi44IDEyLjhsLTEuNC4yLS4zLjEtLjYuMS0xIC4zLS43LjMtLjguNGEzIDMgMCAwIDAtLjcuNWwtLjcuNS0uMi4yLS4zLjQtLjUuNS0uMy4zLS41LjYtLjYuOS0uMy43LS4zLjktLjIuOC0uMS45LS4xLjkuMS45LjIgMS4yLjQgMS4zLjMuNi41IDEuMS40LjYuNS43LjIuMi4yLjMuNy42LjUuNCAxLjQuOC4yLjEgMS43LjYuNi4xLjkuMmMxIC4xIDIuMS4zIDMgLjYgNS42LjggMTAuMSA1LjYgMTAuNiAxMS42bC4yIDEuNWE5LjQgOS40IDAgMCAwIDE2LjUgNS4zIDkuMyA5LjMgMCAwIDAtLjEtMTIuNCA5LjUgOS41IDAgMCAwLTUuOS0zLjF6Ii8+PC9zdmc+)",

  /**
   *
   *
   */
  injectPayload : function() {
    // Enter CSS to inject into the <head>
    var css = ".klonLabel{color:#fff !important;display:block !important;font-size:14px !important;text-align:left !important}#klonWrapper{align-items:center;background-color:rgba(0, 0, 0, 0.6);display:flex;height:100vh;justify-content:center;position:fixed;right:0;top:0;width:100vw;z-index:999}#klonWrapper *{box-sizing:border-box !important}#klonForm{background:rgb(20, 61, 161);border-radius:12px;height:600px;padding:8px;width:360px;display:grid;grid-template-rows:0.5fr 0.5fr auto 1fr;position:relative}.klonHeader{color:rgb(255, 255, 255);font-size:32px;font-family:system-ui;text-align:center;margin:0 auto;display:flex;align-self:flex-end}.klonIdentityInputField{height:32px !important;width:100% !important;display:block !important;margin:0 0 8px !important;border:none !important;border-radius:6px !important;padding:4px 8px !important;font-size:18px !important;background:rgba(255,255,255,0.1) !important;color:#fff !important}.klonIdentityInputField--inline{display:inline-block !important}.klonInputGroup{margin:0 auto;width:100%;max-width:320px !important;display:block}.klonInputGroup--inline{display:grid !important;grid-template-columns:1fr 1fr !important}[name=klonFirstName],[name=klonLastName]{width:136px !important}[name=klonMiddleName]{margin:0 4px !important;width:36px !important}.klonButtons{align-items:center !important;display:flex !important;justify-content:center !important}.klonGroupHalf{padding:0 4px 0 0}.klonGroupHalf:last-child{padding:0 0 0 4px}#klonInnerWrapper{display:grid;grid-template-columns:1fr;overflow:hidden}.klonButton{background:#255bde !important;background-image:url('data:image/svg+xml; base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0iI2ZmZiI+PHBhdGggZD0iTTIwLjMgMkw5IDEzLjZsLTUuMy01TDAgMTIuMyA5IDIxIDI0IDUuN3oiLz48L3N2Zz4=') !important;background-size:36px 36px !important;background-repeat:no-repeat !important;background-position:center center !important;border:none !important;border-radius:50% !important;box-shadow:0 0 10px 1px rgba(0,0,0,0.2);cursor:pointer !important;display:inline-block !important;height:60px !important;margin:8px auto !important;width:60px !important}.klonButton--AcceptId{background-image:url('data:image/svg+xml; base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0iI2ZmZiI+PHBhdGggZD0iTTE5LjUgMTVhNC41IDQuNSAwIDEgMCAwIDkgNC41IDQuNSAwIDAgMCAwLTl6bTIuNSA1aC0ydjJoLTF2LTJoLTJ2LTFoMnYtMmgxdjJoMnYxem0tNy4yIDRIMHYtMS4yYzAtMi42LjItNCAzLjItNC43IDMuMy0uOCA2LjctMS41IDUtNC40QzMuNiA0LjkgNyAwIDEyIDBjNi44IDAgNy41IDcuNiAzLjYgMTMuNy0xLjMgMi0yLjYgMy42LTIuNiA1LjggMCAxLjcuNyAzLjMgMS44IDQuNXoiLz48L3N2Zz4=') !important}#klonClose{color:#fff;font-size:24px;position:absolute;right:12px;top:0;cursor:pointer;padding:4px}#klonScrollLeft,#klonScrollRight{cursor:pointer;height:16px}#klonScrollLeft{margin:0 8px 0 0}#klonScrollRight{margin:0 0 0 8px}#klonScroll{text-align:center;color:#fff;font-size:16px;display:flex;justify-content:center;align-items:center;font-weight:700}[name=klonPassword]{display:inline-block !important;width: calc(100% - 54px) !important}.klonPasswordButton{cursor:pointer;height:16px;margin:8px 4px 0;width:16px;}.klonHidden{display:none !important;}#klonPopUpBtn{align-items:center;background-color:#fff;border-radius:50%;bottom:2vh;box-shadow:0px 1px 4px 0px rgba(0, 0, 0, 0.3);color:#fff;cursor:pointer;display:flex;height:24px;justify-content:center;padding:8px;position:fixed;right:2vw;transform:rotate(360deg);width:24px;z-index:998;}";
    var style = document.createElement('style');
    style.innerText = css;
    document.head.appendChild(style);
    var wrapper  = document.createElement("DIV");
    wrapper.classList.add("klonHidden");
    wrapper.id   = "klonWrapper";
    var klonHTML = '<div id="klonForm" style=""><div id="klonClose">&times;</div><h1 class="klonHeader">New Identity</h1><div id="klonScroll"><span id="klonScrollLeft"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTExLjkgMEwxNiA0bC04IDggOCA4LTQuMSA0TDAgMTIgMTEuOSAweiIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==" height="16" width="16"></span><span id="klonScrollCurrent">1</span>/<span id="klonScrollTotal">1</span><span id="klonScrollRight"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTguMSAyNEw0IDIwbDgtOC04LTggNC4xLTRMMjAgMTJ6IiBmaWxsPSIjZmZmIi8+PC9zdmc+" height="16" width="16"></span></div><div id="klonInnerWrapper"><div class="klonInputGroup"><label class="klonLabel">Name</label><input name="klonFirstName" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"><input name="klonMiddleName" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"><input name="klonLastName" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"></div><div class="klonInputGroup"><label class="klonLabel">Email</label><input name="klonEmail" type="text" readonly="" class="klonIdentityInputField"></div><div class="klonInputGroup"><label class="klonLabel">Username</label><input name="klonUsername" type="text" readonly="" class="klonIdentityInputField"></div><div class="klonInputGroup"><label class="klonLabel">Password</label><input name="klonPassword" type="password" readonly="" class="klonIdentityInputField"><span id="klonShowPassword"><img class="klonPasswordButton" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTEyIDdjNC44IDAgOCAzIDkuNSA0LjYtMS40IDEuOS00LjcgNS40LTkuNSA1LjQtNC40IDAtOC0zLjUtOS41LTUuNEM0IDkuOSA3LjQgNyAxMiA3em0wLTJDNC40IDUgMCAxMS42IDAgMTEuNlM0LjggMTkgMTIgMTljNy43IDAgMTItNy40IDEyLTcuNFMxOS43IDUgMTIgNXptMCAzYTQgNCAwIDEgMCAwIDggNCA0IDAgMCAwIDAtOHptMCA0YTEuNCAxLjQgMCAxIDEtMi0yIDEuNCAxLjQgMCAwIDEgMiAyeiIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==" height="16" width="16"></span><span id="klonGeneratePassword"><img class="klonPasswordButton" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTEzLjUgMkM3LjkgMiAzLjMgNi40IDMgMTJIMGw0LjUgNkw5IDEySDZhNy41IDcuNSAwIDEgMSAxLjYgNS4xbC0xLjggMi41QTEwLjUgMTAuNSAwIDAgMCAyNCAxMi41QzI0IDYuNyAxOS4zIDIgMTMuNSAyeiIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==" height="16" width="16"></span></div><div class="klonInputGroup klonInputGroup--inline"><div class="klonGroupHalf"><label class="klonLabel">Gender</label><input name="klonGender" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"></div><div class="klonGroupHalf"><label class="klonLabel">Date of Birth</label><input name="klonDOB" type="text" readonly="" class="klonIdentityInputField klonIdentityInputField--inline"></div></div><div class="klonInputGroup"><label class="klonLabel">Address</label><input name="klonAddress1" type="text" readonly="" class="klonIdentityInputField"><input name="klonAddress2" type="text" class="klonIdentityInputField"></div></div><div class="klonButtons"><button class="klonButton klonButton--AcceptId" type="button" id="klonNewIdentity"></button><button class="klonButton" type="button" id="klonAcceptIdentity"></button></div></div>';
    //
    document.body.appendChild(wrapper);
    wrapper.insertAdjacentHTML("beforeend", klonHTML);
    var popUpButton     = document.createElement("DIV");
    var popUpButtonHTML = '<div id="klonPopUpBtn"><img alt="Klon" height="20" width="20" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2NCA2NCI+PGNpcmNsZSBjeD0iMTEuOSIgY3k9IjkuMSIgcj0iOC45IiBmaWxsPSIjMTQzZGExIi8+PGNpcmNsZSBjeD0iMTEuOSIgY3k9IjU1IiByPSI4LjkiIGZpbGw9IiMxNDNkYTEiLz48cGF0aCBmaWxsPSIjMTQzZGExIiBkPSJNNTIuNyA0NS4yYy0yLjItLjMtNC40LTEtNi4zLTJhMTMuMiAxMy4yIDAgMCAxIDEuMy0yMy4xYzEuNS0uNyAzLjEtMS4xIDQuNy0xLjNhOS40IDkuNCAwIDAgMCA1LjMtMTYuNSA5LjMgOS4zIDAgMCAwLTEyLjQuMSA5LjUgOS41IDAgMCAwLTMuMSA1LjlMNDIgOS43YTEzLjMgMTMuMyAwIDAgMS0xMi44IDEyLjhsLTEuNC4yLS4zLjEtLjYuMS0xIC4zLS43LjMtLjguNGEzIDMgMCAwIDAtLjcuNWwtLjcuNS0uMi4yLS4zLjQtLjUuNS0uMy4zLS41LjYtLjYuOS0uMy43LS4zLjktLjIuOC0uMS45LS4xLjkuMS45LjIgMS4yLjQgMS4zLjMuNi41IDEuMS40LjYuNS43LjIuMi4yLjMuNy42LjUuNCAxLjQuOC4yLjEgMS43LjYuNi4xLjkuMmMxIC4xIDIuMS4zIDMgLjYgNS42LjggMTAuMSA1LjYgMTAuNiAxMS42bC4yIDEuNWE5LjQgOS40IDAgMCAwIDE2LjUgNS4zIDkuMyA5LjMgMCAwIDAtLjEtMTIuNCA5LjUgOS41IDAgMCAwLTUuOS0zLjF6Ii8+PC9zdmc+"></div>';
    popUpButton.id      = "klonPopUpBtn";
    document.body.insertAdjacentHTML("beforeend", popUpButtonHTML);
  },


  /**
   *
   *
   */
  closeKlonCreate : function() {
    var wrapper  = document.querySelector("#klonWrapper");
    var popUpBtn = document.querySelector("#klonPopUpBtn");
    if (!wrapper.classList.contains("klonHidden")) {
      wrapper.classList.add("klonHidden");
      popUpBtn.classList.remove("klonHidden");
    }
  },


  /**
   *
   */
  showForm : function() {
    var popUpBtn = document.querySelector("#klonPopUpBtn");
    var wrapper = document.querySelector("#klonWrapper");
    if (wrapper.classList.contains("klonHidden")) {
      popUpBtn.classList.add("klonHidden");
      wrapper.classList.remove("klonHidden");
    }
  },


  /**
   *
   *  Get the subscribers settings from the other side of the extension
   *
   */
  getSettings : function() {
    return new Promise((resolve, reject) => {
      console.log("Let's get the settings!");
      // This is the number of minutes to allow between checking settings
      var threshold = 5;
      // Determine if it has been less than 5 minutes since the last check
      console.log("Parasite Config Expiration: ", parasite.config.expiration);
      if (parasite.config.hash == undefined || parasite.config.urls == undefined || parasite.config.enabled == undefined || parasite.config.role != undefined || parasite.config.expiration != undefined || parasite.config.expiration + (threshold * 60) > time.timestamp) {
        // Get the subscribers account type
        var url = window.location.protocol + "//" + window.location.host;
        comm.sendMessage({"msg" : "get settings", "url" : url}).then(result => {
          //
          console.log("Result: ", result);
          parasite.config.hash         = result.hash;
          parasite.config.urls         = result.urls;
          parasite.config.enabled      = Number(result.enabled);
          parasite.config.role         = Number(result.role);
          parasite.config.expiration   = time.timestamp();
          //
          console.log("Parasite Config: ", parasite.config);
          resolve(parasite.config);
        }).catch(err => {
          console.log("Error: ", err);
          reject(err);
        });
      } else {
        console.log("Settings already saved or do not exist.");
        resolve(parasite.config);
      }
    });
  },


  /**
   *
   *
   *
   */
  checkSettings : function() {
    return new Promise((resolve, reject) => {
      // Get the current URL
      var url    = window.location.protocol + "//" + window.location.host;
      console.log("Parasite Config Enabled: ", parasite.config);
      // Make sure the account is enabled
      if (parasite.config.enabled === 1) {
        // If the account is enabled lets figure out what their role is...
        switch (parasite.config.role) {
          case 0:
            // Not an account -- Banned user, the Void
            alert("Nope.");
            reject(false);
            break;
            // Basic account
          case 1:
              resolve("basic");
            break;
            // Regular account
          case 2:
            // Do whatever they want, as long as it's within reason...
            console.log("Regular Account");
            // Check to see if this account already has 5 accounts at this URL
            if (typeof parasite.config.urls === "number" && parasite.config.urls < 5) {
              console.log("Not maxed out, carry on");
              resolve("regular");
            } else {
              // The user is maxed out. Plug the premium account thing
              console.log("Config: ", parasite.config.urls);
              console.log("You are maxed out on accounts for this URL. Consider a premium account @ [Insert Link Here]");
              reject("maxed out");
            }
            break;
          case 3:
            // Check to see if this account already has 5 accounts at this URL
            if (typeof parasite.config.urls === "number" && parasite.config.urls < 20) {
              console.log("Not maxed out, carry on");
              resolve("premium");
            } else {
              // The user is maxed out. Plug the premium account thing
              console.log("Config: ", parasite.config.urls);
              console.log("You are maxed out on accounts for this URL.");
              reject("maxed out");
            }
            break;
          case 9:
            // Reserved for free trials and what not
            console.log("Trial Account");
            // Check to see if this account already has 5 accounts at this URL
            if (typeof parasite.config.urls === "number" && parasite.config.urls < 1) {
              console.log("Not maxed out, carry on");
              resolve("trial");
            } else {
              // The user is maxed out. Plug the premium account thing
              console.log("Config: ", parasite.config.urls);
              reject("maxed out");
            }
            reject(false);
          break;
          default:
            reject(false);
            break;
        }
      } else {
        alert("Klon account is disabled.");
        reject(false);
      }
    });
  },


  /**
   *
   *  Assemble a new identity candidate for the user to choose from
   *
   *  @return                 Object             Identity that can be selected to be used for a website
   *
   */
  createNewIdentity : function() {
    var btnNewIdentity = document.querySelector("#klonNewIdentity");
    var getSet         = parasite.getSettings();
    getSet.then(setting => {
      console.log("The settings have been had.");
      var checkSet = parasite.checkSettings();
      checkSet.then(check => {
        console.log("Alrighty, ");
        // Determine if the subscriber has the ability to do this
        if (parasite.identities.length < 5) {
          comm.sendMessage({"msg" : "new identity"}).then(result => {
            // console.log("createNewIdentity Result: ", result);
            usernameGen.firstName = result.firstName;
            usernameGen.lastName  = result.lastName;
            result.username = usernameGen.genUsername();
            result.password = generatePassword(16, true, true, true, true);
            result.mgemail  = result.email;
            parasite.identities[parasite.identities.length] = result;
            parasite.currentPos = parasite.currentPos + 1;
            parasite.setScrollNavNumbers();
            parasite.displayIdentity(parasite.identityFields, parasite.identities[parasite.currentPos - 1]);
          });
        } else {
          // console.log("Derp.");
          try {
            btnNewIdentity.removeEventListener("click", parasite.createNewIdentity);
          } catch {
            console.log("Event listener not found");
          }
        }
      })
      .catch(err => {
        console.log("Error: ", err);
      });
    }).catch(err => {
      console.log("Error: ", err);
      return false;
    });
  },


  /**
   *
   *  Display the current identity in the fields on the popup for the user to view
   *
   *  @param    identity    Object     Object full of fields found on the page
   *  @param    result      Object     Pass the single identity object to the function
   *
   *  @return               Boolean    True on success, False on error
   *
   */
  displayIdentity : function(identity, result) {
    //
    var klonFirstName  = document.querySelector('[name=klonFirstName]');
    var klonMiddleName = document.querySelector('[name=klonMiddleName]');
    var klonLastName   = document.querySelector('[name=klonLastName]');
    var klonEmail      = document.querySelector('[name=klonEmail]');
    var klonUsername   = document.querySelector('[name=klonUsername]');
    var klonPassword   = document.querySelector('[name=klonPassword]');
    var klonGender     = document.querySelector('[name=klonGender]');
    var klonDOB        = document.querySelector('[name=klonDOB]');
    var klonAddress1   = document.querySelector('[name=klonAddress1]');
    var klonAddress2   = document.querySelector('[name=klonAddress2]');
    //
    klonFirstName.value   = result.firstName;
    klonMiddleName.value  = result.middleInitial;
    klonLastName.value    = result.lastName;
    klonEmail.value       = result.email;
    klonUsername.value    = result.username;
    klonPassword.value    = result.password;
    klonGender.value      = result.gender;
    klonDOB.value         = result.month + "-" + result.day + "-" + result.year;
    klonAddress1.value    = result.streetAddress;
    klonAddress2.value    = result.city + ", " + result.state + " " + result.zip;
  },


  /**
  *
   * Scrolls the Identity to the left
   *
   */
  scrollIdLeft : function() {
    if (parasite.identities[parasite.currentPos - 1] != undefined && Number(parasite.currentPos - 1) > 0) {
      parasite.currentPos = parasite.currentPos - 1;
      parasite.setScrollNavNumbers();
      var boolIdentity = parasite.displayIdentity(parasite.identityFields, parasite.identities[parasite.currentPos - 1]);
    } else {
      // console.log("Nope");
      return false;
    }
  },

// Scrolls the Identity to the right
  scrollIdRight : function() {
    if (Number(parasite.currentPos + 1) <= parasite.identities.length) {
      parasite.currentPos = parasite.currentPos + 1;
      parasite.setScrollNavNumbers();
      var boolIdentity = parasite.displayIdentity(parasite.identityFields, parasite.identities[parasite.currentPos - 1]);
    } else {
      return false;
    }
  },


  /**
   *
   *  Toggle the password field between type "password" and "text"
   *  Also changes the image from a show to hide icon
   *
   */
  showPassword : function() {
    var passwordField      = document.querySelector("[name=klonPassword]");
    var showPasswordButton = document.querySelector("#klonShowPassword");
    var showPasswordImage  = showPasswordButton.querySelector(".klonPasswordButton");
    if (passwordField.type === "password") {
      passwordField.type = "text";
      showPasswordImage.src = "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTE5LjYgMi42bC0zLjMgMy4xQzE1IDUuMyAxMy42IDUgMTIgNSA0LjQgNSAwIDExLjYgMCAxMS42czIgMi45IDUuMSA1bC0yLjkgM0wzLjYgMjEgMjEgNGwtMS40LTEuNHptLTYgNS43YTQgNCAwIDAgMC01LjMgNS4ybC0xLjcgMS44YTE4LjIgMTguMiAwIDAgMS00LTMuN0M0IDkuOSA3LjMgNyAxMiA3YzEgMCAxLjguMSAyLjYuM2wtMSAxem0tMi45IDcuNWw1LTVjMS4xIDMtMiA2LTUgNXpNMjQgMTEuNlMxOS43IDE5IDEyIDE5Yy0xLjQgMC0yLjYtLjMtMy44LS43bDEuNi0xLjZjLjcuMiAxLjQuMyAyLjIuMyA0LjggMCA4LjEtMy41IDkuNS01LjQtLjctLjgtMi0yLTMuNi0zbDEuNS0xLjRjMyAyIDQuNiA0LjQgNC42IDQuNHoiIGZpbGw9IiNmZmYiLz48L3N2Zz4=";
    } else {
      passwordField.type = "password";
      showPasswordImage.src = "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTEyIDdjNC44IDAgOCAzIDkuNSA0LjYtMS40IDEuOS00LjcgNS40LTkuNSA1LjQtNC40IDAtOC0zLjUtOS41LTUuNEM0IDkuOSA3LjQgNyAxMiA3em0wLTJDNC40IDUgMCAxMS42IDAgMTEuNlM0LjggMTkgMTIgMTljNy43IDAgMTItNy40IDEyLTcuNFMxOS43IDUgMTIgNXptMCAzYTQgNCAwIDEgMCAwIDggNCA0IDAgMCAwIDAtOHptMCA0YTEuNCAxLjQgMCAxIDEtMi0yIDEuNCAxLjQgMCAwIDEgMiAyeiIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==";
    }
  },


  /**
   *
   *  Generate a password (TODO: Based on the users settings)
   *
   */
  generatePwd : function() {
    console.log("Current Position: " + parasite.currentPos);
    console.log("Identities: ", parasite.identities);
    var length = 12;
    var useLowerChars = true;
    var useUpperChars = true;
    var useNumericChars = true;
    var useSpecialChars = true;
    var passwd = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    var klonPassword   = document.querySelector('[name=klonPassword]');
    parasite.identities[parasite.currentPos - 1].password = passwd;
    klonPassword.value = passwd;
  },


  /**
   *
   *
   *
   *
   */
  setScrollNavNumbers : function() {
    var scrollCurrent = document.querySelector("#klonScrollCurrent");
    var scrollTotal   = document.querySelector("#klonScrollTotal");
    scrollCurrent.innerHTML = parasite.currentPos;
    scrollTotal.innerHTML   = parasite.identities.length;
  },


  /**
   *
   *  The user has decided they want to use a particular identity for a given website
   *  This function will encrypt and store the identity in the database for the user
   *  to use again in the future.
   *
   *  @param       identity      Object      The identity object
   *
   *  @return                    Boolean     True or False depending on success
   *
   */
  useCandidate : function() {
    // Set the URL property of the identity object
    parasite.identities[parasite.currentPos - 1].url = window.location.protocol + "//" + window.location.host;
    console.log(window.location.protocol + "//" + window.location.host);
    console.log("Identity: ", parasite.identities[parasite.currentPos - 1]);
    comm.sendMessage({"msg" : "use identity", "identity" : parasite.identities[parasite.currentPos - 1]}).then(result => {
      console.log("Parasite Response: ", result);
      // If the API returns a successful result remove the Klon New Identity form from the screen and fill the form with these credentials
      if (result.status === "success") {
        // Remove the thing from the screen by adding the "klonHidden" class
        var klonWrapper = document.querySelector("#klonWrapper");
        if (!klonWrapper.classList.contains("klonHidden")) {
          klonWrapper.classList.add("klonHidden");
        }
        // Fill the form with the details
        filler.fillForm(parasite.identityFields, parasite.identities[parasite.currentPos - 1]);
      }
      // Let the user know there was an issue selecting this identity.
      else {
        console.log("Error");
        return false;
      }
    });
  },


  /**
   *
   *  This will inject the controls into the form for the "Login" autofill feature
   *
   */
  injectLoginMenu : function(identities) {
    return new Promise(function(resolve, reject) {
      //
      var loginFormHTML = '';
      var loginFormCSS  = '';
      // Get the settings
      var settings = parasite.getSettings();
      settings.then(result => {
        // console.log("Parasite Object: ", parasite);
        //
        // console.log("Get Settings Result: ", result);
        //
        // console.log("Parasite Config: ", parasite.config);
        //
        // console.log("Form: ", form);
        //
        console.log("Identities: ", identities);
        //
        var loginButtonHTML = "<div class='klonHide' id='klonLoginMenu'></div>";
        var loginButtonCSS  = "#klonLoginMenu{background-color: #12368F;border-radius: 2px;box-shadow: 0px 2px 6px 1px rgba(25, 25, 25, 0.4);box-sizing:border-box;color: #fff;height: 200px;padding:8px 16px;position: absolute;width: 300px;z-index: 998;}#klonLoginMenu::before{content: '';z-index: 998;display: block;margin: -14px -12px 0 auto;width: 0;height: 0;border-left: 8px solid transparent;border-right: 8px solid transparent;border-bottom: 8px solid #12368F;}.klonLoginListItem{background-color:#143DA1;border-radius:10px;cursor:pointer;display:block;font-size:10px;list-style-type:none;text-transform:uppercase;letter-spacing:1px;margin:12px auto;padding:8px;text-align:center;transition:background 0.2s ease-out;width:90%;}.klonLoginListItem:hover{background-color: #255BDE;}.klonLoginMenuHeader{color:#fff;text-align:center;margin:8px auto 10px;}.klonLoginList{list-style-type:none;height:150px;overflow:hidden;overflow-y:scroll;}";
        //
        var style = document.createElement('style');
        style.innerText = loginButtonCSS;
        document.head.appendChild(style);
        // Insert the login menu right after the opening <body> tag
        var body = document.querySelector("body");
        body.insertAdjacentHTML("afterbegin", loginButtonHTML);
        var loginKlonMenu = document.querySelector("#klonLoginMenu");
        // Add the header to the login menu
        var menuHeading = document.createElement("h2");
        menuHeading.setAttribute("class", "klonLoginMenuHeader");
        var headingText = document.createTextNode("Login As...");
        menuHeading.appendChild(headingText);
        loginKlonMenu.appendChild(menuHeading);
        // Create the unordered list element
        var unorderedList = document.createElement("UL");
        unorderedList.setAttribute("class", "klonLoginList");
        loginKlonMenu.appendChild(unorderedList);
        // Loop through and put in the relevant identities
        for (let i = 0; i < identities.length; i++) {
          var listItem = document.createElement("LI");
          listItem.setAttribute("class", "klonLoginListItem");
          listItem.dataset.email    = identities[i].assignedEmail;
          listItem.dataset.username = identities[i].username;
          listItem.dataset.url      = identities[i].url;
          listItem.dataset.id       = i;
          // listItem.dataset.id       = identities[i].id;
          var textNode = document.createTextNode(identities[i].assignedEmail);
          listItem.appendChild(textNode);
          // Add the listItem to the menu
          unorderedList.appendChild(listItem);
          // listItem.setAttribute("attribute", "value")
        }
        resolve(true);
      })
      .catch(err => {
        console.log("Error: ", err);
        reject(false);
      });
    });
  },


  /**
   *
   *  This is the function to run when a #klonLoginMenu <li> is clicked.
   *
   */
  loginAs : function(evt, identity) {
    parasite.assocId[Number(evt.target.dataset.id)].email = parasite.assocId[Number(evt.target.dataset.id)].assignedEmail;
    console.log("Identity fields found: ", parasite.identityFields);
    console.log("Identity to use: ", parasite.assocId[Number(evt.target.dataset.id)]);
    // Fill the form
    filler.fillForm(parasite.identityFields, parasite.assocId[Number(evt.target.dataset.id)]);
    // Hide the menu
    try {
      var menu = document.querySelector("#klonLoginMenu");
      if (!menu.classList.contains("klonHide")) {
        menu.classList.add("klonHide");
      }
      return true;
    } catch (e) {
      console.log("loginAs Error: ", e.message);
      return false;
    }
  },


  /**
   *
   *  Bind functions to the <li> elements within the #klonLoginMenu
   *
   */
  bindLoginMenuFunctions : function() {
    return new Promise(function(resolve, reject) {
      //
      var menu = document.querySelector("#klonLoginMenu");
      // If the menu element is found within the document
      if (menu != null && menu != undefined) {
        // Get the list items within the menu
        var listItems = menu.querySelectorAll("li");
        // Make sure we found some list items to bind to
        if (listItems != null && listItems != undefined) {
          //
          for (let n = 0; n < listItems.length; n++) {
            console.log(listItems[n]);
            listItems[n].addEventListener("click", parasite.loginAs);
          }
          resolve(true);
        } else {
          reject(false);
        }
      } else {
        // The menu element is not found within the page
        reject(false);
      }
    });
  },


  injectInputButtons : function(input) {
    return new Promise((resolve, reject) => {
      // Pass the form inputs (username/email/password) and inject background image into those inputs
      if (input) {
        // Check to make sure the field does not already have the BG Image then inject the input field with the KLON background image
        if (!input.hasAttribute('data-klon')) {
          // Inject the background image
          // Get distance from top
          try {
            var t = window.pageYOffset + input.getBoundingClientRect().top;
            // console.log("Top: ", t);
            // Get distance from left
            var l = window.pageXOffset + input.getBoundingClientRect().left;
            // console.log("Left: ", l);
            // Get element height
            var h = input.clientHeight;
            // console.log("Height: ", h);
            // Get element width
            var w = input.clientWidth;
            // console.log("Width: ", w);
            // Get the width of the button to inject
            var btnW = 20;
            // Get the height of the button to inject
            var btnH = 20;
            // Assign a random unique ID to the button
            var klonBtnId    = Math.random().toString(36).substr(2, 12);
            //
            input.dataset.klon = klonBtnId;
            // The HTML of the button
            var klonBtn      = "<div class='klonBtn' id='klonBtn_" + klonBtnId + "'></div>";
            // The CSS for the button
            var klonBtnStyle = ".klonBtn {position:absolute;height:" + btnH + "px;width:" + btnW + "px;background:url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxOCIgaGVpZ2h0PSIyMCI+PHBhdGggZmlsbD0iIzg4OCIgZD0iTTE0IDBINEMxLjggMCAwIDEuOCAwIDR2MTJjMCAyLjIgMS44IDQgNCA0aDEwYzIuMiAwIDQtMS44IDQtNFY0YzAtMi4yLTEuOC00LTQtNHpNNC4xIDJjMS4yIDAgMi4xIDEgMi4xIDIuMiAwIDEuMi0xIDIuMi0yLjEgMi4yQzMgNi41IDIgNS41IDIgNC4yIDIgMyAzIDIgNC4xIDJ6bTAgMTZDMyAxOCAyIDE3IDIgMTUuOGMwLTEuMiAxLTIuMiAyLjEtMi4yIDEuMiAwIDIuMSAxIDIuMSAyLjIuMSAxLjItLjkgMi4yLTIuMSAyLjJ6bTguNC01LjJjLjUuMyAxIC40IDEuNS41LjYuMSAxIC4zIDEuNC44LjguOS44IDIuMiAwIDMuMS0uOSAxLjEtMi40IDEuMS0zLjMuMS0uNC0uNC0uNi0uOS0uNi0xLjUgMC0uMSAwLS4zLS4xLS40YTMuMyAzLjMgMCAwIDAtMi42LTNjLS4yLS4xLS41LS4xLS43LS4yaC0uMi0uMWMtLjEtLjEtLjMtLjEtLjQtLjJoLS4xYy0uMSAwLS4yLS4xLS4zLS4yLS4xIDAtLjEtLjEtLjEtLjFzLS4xLS4xLS4yLS4xbC0uMS0uMS0uMS0uMWMtLjEgMC0uMS0uMS0uMS0uMmwtLjEtLjFjMC0uMS0uMS0uMi0uMS0uMyAwLS4xIDAtLjEtLjEtLjIgMC0uMS0uMS0uMi0uMS0uM1YxMHYtLjItLjItLjJjMC0uMSAwLS4yLjEtLjIgMC0uMSAwLS4yLjEtLjJzMCAwIC4xLS4xLjEtLjIuMS0uMmMuMS0uMS4xLS4xLjEtLjIgMCAwIDAtLjEuMS0uMWwuMS0uMS4xLS4xaC4xYzAtLjEuMS0uMS4xLS4yLjEgMCAuMiAwIC4yLS4xLjEgMCAuMS0uMS4yLS4xIDAgMCAuMSAwIC4yLS4xLjEgMCAuMiAwIC4yLS4xSDguMWMuMSAwIC4yIDAgLjMtLjEgMS43IDAgMy0xLjQgMy4xLTMuMiAwLS4xLjEtLjIuMS0uNC4xLS42LjMtMS4xLjctMS41LjktLjggMi4xLS44IDMgMCAxIC45IDEgMi41LjEgMy41LS40LjQtLjkuNi0xLjQuNy0uNCAwLS44LjItMS4xLjNhMy40MyAzLjQzIDAgMCAwLS40IDUuOXoiLz48L3N2Zz4=') no-repeat;background-size:18px 18px;border-radius:4px;cursor:pointer;z-index:999} .klonHide{display:none;}";
            // Insert the style into the head of the document
            var style = document.createElement('style');
            style.innerText = klonBtnStyle;
            document.head.appendChild(style);
            // Insert the button after the opening <body> tag
            document.body.insertAdjacentHTML("afterbegin", klonBtn);
            var klonBtnHandle = document.querySelector("#klonBtn_" + klonBtnId);
            klonBtnHandle.style.top  = t + ((h - btnH) / 2) + "px";
            klonBtnHandle.style.left = l + (w - btnW) + "px";
          } catch(e) {
            console.log("Error: ", e.message);
            reject(e);
          }
        } else {
          // The input already has the klon button injected
          resolve(true);
        }
        resolve(true);
      } else {
        // The element does not exist
        resolve(true);
      }
    });
  },


  /**
   *
   *  This function will get all of the users identities that apply to this URL
   *
   */
  getAssociatedIdentities : function(url) {
    //

  },


  /**
   *
   *
   *
   */
  injectFunctions : function() {
    // Get a handle on the close button
    var btnClose = document.querySelector("#klonClose");
    btnClose.addEventListener("click", parasite.closeKlonCreate);
    // Get a handle on the 'Create new identity' button
    var btnNewIdentity = document.querySelector("#klonNewIdentity");
    btnNewIdentity.addEventListener("click", function (){parasite.createNewIdentity();});
    // Get a handle on the 'Accept Identity' button
    var btnAcceptIdentity = document.querySelector("#klonAcceptIdentity");
    btnAcceptIdentity.addEventListener("click", parasite.useCandidate);
    // Set the current position in the identity scrolling...
    // Get a handle on the 'Scroll Button Left'
    var btnScrollLeft = document.querySelector("#klonScrollLeft");
    btnScrollLeft.addEventListener("click", function(){parasite.scrollIdLeft()});
    // Get a handle on the 'Scroll Button Right'
    var btnScrollRight = document.querySelector("#klonScrollRight");
    btnScrollRight.addEventListener("click", function(){parasite.scrollIdRight()});
    //
    var showPassword = document.querySelector("#klonShowPassword");
    showPassword.addEventListener("click", parasite.showPassword);
    //
    var generatePasswordBtn = document.querySelector("#klonGeneratePassword");
    generatePasswordBtn.addEventListener("click", parasite.generatePwd);
    //
    var popUpButton = document.querySelector("#klonPopUpBtn");
    popUpButton.addEventListener("click", parasite.showForm);
  },


  //
}
