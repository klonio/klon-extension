var postMessageHandler = {
  // Different methods for handling different messages
}

//
if (window.location.pathname === "/payment.html") {
  window.addEventListener("message", function(e) {
    // console.log("Message: ", e);
    if (typeof e === "object") {
      // Successful cases
      if (e.data.status === "success") {
        switch(e.data.message) {
          case "payment successful":
            localStorage.clear();
            break;
          case "redirect login":
            window.location.pathname = "/login.html";
            break;
          default:
            alert("An error has occurred");
        }
        return true;
      } else if (e.data.status === "fail") {
        switch(e.data.message) {
          case "":
            break;
          default:
            alert("An error has occurred");
            break;
        }
        return false;
      } else {
        // idk??
        return false;
      }
    }
  });
}
