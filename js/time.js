var time = {
  /**
   *
   *  This function returns a timestamp in seconds -- not milliseconds
   *
   */
  timestamp : function() {
    return Math.floor(new Date().getTime() / 1000);
  }
}
