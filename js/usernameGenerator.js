/**
*
*  Generates random usernames based off of the full name of the user in question
*
*  @param      firstName    String     The users first name
*  @param      lastName     String     The users last name
*
*  @return                  String     Username based off of first and last name
*
*/
var usernameGen = {
  firstName : "",
  lastName : "",
  username : "",

  //
  getRandomInt : function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
  },

  //
  genUsername : function() {
    if (usernameGen.firstName.length > 0 && usernameGen.lastName.length > 0) {
      // Get a number between 0 and 1 (true and false)
      var useFullFirstName = usernameGen.getRandomInt(0,2);
      // Number to be appended to the end of the username
      var randomNumber     = usernameGen.getRandomInt(10,10000);
      //
      if (useFullFirstName === 0) {
        // In this case we are NOT using the full first name in the username
        usernameGen.username =  usernameGen.firstName[0];
        usernameGen.username += usernameGen.lastName;
        usernameGen.username += randomNumber;
        return usernameGen.username;
      } else {
        // In this case we are NOT using the full first name in the username
        usernameGen.username =  usernameGen.firstName;
        usernameGen.username += usernameGen.lastName[0];
        usernameGen.username += randomNumber;
        return usernameGen.username;
      }
    } else {
      console.log("First and last name not set");
    }
  }
}
