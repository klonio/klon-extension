/**
 *
 *  Name:         decrypt.js
 *
 *  Description:  Anything related to decrypting data is contained within this object
 *
 */

var decrypt = {
  /**
   *
   *  Run the bit that gets the users private key and turns it into a usable typed array
   *
   */
  getKey : function() {
    return new Promise(function(resolve, reject) {
      var keyExists  = localStorage.getItem("key");
      if (keyExists != null && keyExists != undefined) {
        var prvKey   = localStorage.getItem("key");
        arrKey       = splitHexValue(prvKey, 8);
        arrKey
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          reject(err);
        });
      } else {
        reject("key not defined.");
      }
    });
  },


}
