/**
 *
 *  This is the subscription object. Manages everything related to the users subscription to Klon
 *
 *  Object is used mainly on the subscription.html page to display information to the user
 *
 */
var subscription = {
  /**
   *
   *  Get subscriptions by email
   *
   *  @param     String     email       The users email address. Usually found within localStorage.user
   *
   *  @param     Object                 Returns an array of objects containing subscription data
   *
   */
  getSubscriptions : function(email) {
    return new Promise(function(resolve, reject) {
      // Verify that the data sent was proper
      if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
        // Send the request to the API endpoint
        // (method = 'POST', url, data = '', conType = 'json')
        var url  = apiUrl + '/subscriber/getSubscriptions';
        let token = localStorage.getItem('token');
        if (token !== null) {
          var data = {'email' : email, 'token' : token};
          var request = ajx.req('POST', url, data, 'json');
          request.then(result => {
            // console.log("RESULT: ", result);
            resolve(result);
          })
          .catch(err => {
            // console.log("Error: ", err);
            return err;
            reject(err);
          });
        } else {
          errorHandler.setError("Error", "token not found");
          errorHandler.showError();
          reject(false);
        }
      } else {
        throw("invalid email address provided");
        reject(false);
      }
    });
  },


  /**
   *
   *  Compose the subscription page
   *
   *  @param      Object      subscriptions       An array of subscription objects
   *
   *  @return     Boolean                         If successfully composes the subscription page, true, otherwise false
   *
   */
  composeSubscriptionPage : function(subscriptions) {
    // Make sure we have some subscriptions in the arguments sent to the function
    if (typeof subscriptions === "object" && typeof subscriptions.length === "number") {
      //
      if (subscriptions.length > 0) {
        // Get the elements we want to populate with data
        var list   = document.getElementById("subscriptionList");
        var noSubs = document.getElementById("noSubs") || false;
        var html = "";
        console.log("List: ", list);
        console.log("Subscriptions: ", subscriptions);
        for (i = 0; i < subscriptions.length; i++) {
          // Create a new JavaScript Date object based on the timestamp
          // multiplied by 1000 so that the argument is in milliseconds, not seconds.
          var expiresTimestamp = subscriptions[i]['expires'];
          var currentTimestamp = Math.floor(Date.now()/1000);
          // Subtract the expiration date from now then divide by 86400 seconds to determine the number of days left
          // 86400 seconds in a day
          var expires = (expiresTimestamp - currentTimestamp);
          var strExpires = "";
          // If this is a positive number continue on. Otherwise this is expired.
          if (expires > 0) {
            // Always round down so that we don't give any surprises
            expires = Math.floor(expires / 86400);
            // If less than 1 full day remains let the user know today is the day the thing expires
            if (expires < 1) {
              expires = "Today";
            }
            //
            if (expires > 1) {
              strExpires = "Days";
            } else if (expires === 1) {
              strExpires = "Day";
            } else {
              strExpires = "";
            }
          } else {
            // This thing is expired!!
            expires = "Expired";
          }
          // If expiring at period end include this bit...
          var cancelAtPeriodEnd;
          if (subscriptions[i]['cancelAtPeriodEnd'] === true) {
            cancelAtPeriodEnd = "<div class='subscription__badge'>Not Renewing</div>";
            html = '<li class="detail__optionItem"><div class="subscription__grid"><div class="subscription"><div class="subscription__title" id="plan">' + subscriptions[i]['name'] + '</div>' + cancelAtPeriodEnd + '<div class="subscription__grid--2col"><div class="subscription__group"><div class="subscription__label">Status</div><span class="subscription__status" id="status">' + subscriptions[i]['status'] + '</span></div><div class="subscription__group"><div class="subscription__label">Expires</div><div class="subscription__expires" id="expires">' + expires + ' ' + strExpires + '</div></div></div><div><button class="subscription__btn" id="btnModify' + i + '" data-subscription="' + i + '">Modify</button></div></div></div></li>';
          } else {
            cancelAtPeriodEnd = "";
            html = '<li class="detail__optionItem"><div class="subscription__grid"><div class="subscription"><div class="subscription__title" id="plan">' + subscriptions[i]['name'] + '</div>' + cancelAtPeriodEnd + '<div class="subscription__grid--2col"><div class="subscription__group"><div class="subscription__label">Status</div><span class="subscription__status" id="status">' + subscriptions[i]['status'] + '</span></div><div class="subscription__group"><div class="subscription__label">Expires</div><div class="subscription__expires" id="expires">' + expires + ' ' + strExpires + '</div></div></div><div><button class="subscription__btn" id="btnModify' + i + '" data-subscription="' + i + '">Modify</button></div></div></div></li>';
          }
          // console.log("HTML: ", html);
          list.insertAdjacentHTML('afterbegin', html);
          if (noSubs !== false) {
            noSubs.remove();
          }
        }
        return true;
      } else {
        // No subscription objects returned
        errorHandler.setError("", "No subscriptions found for user", false);
        errorHandler.showError();
        return false;
      }
    } else {
      // Wrong type of argument. Must be an array of objects
      errorHandler.setError("Error", "Invalid argument supplied");
      errorHandler.showError();
      return false;
    }
  },


  /**
   *
   *  Bind events to buttons
   *
   *
   */
  bindButtonEvents : function() {
    // Get the cancel button
    let btnCancel = document.querySelectorAll("button[id^='btnCancel']") || false;
    // Get the renew button
    let btnRenew  = document.querySelectorAll("button[id^='btnRenew']") || false;
    // Get the modify button
    let btnModify = document.querySelectorAll("button[id^='btnModify']") || false;
    // Make sure there are values stored in the array
    if (btnCancel.length > 0) {
      // Loop through each button and add the event
      for (var i = 0; i < btnCancel.length; i++) {
        // Make sure the script found the cancel button and its respective dataset attribute
        if (btnCancel[i] !== false && btnCancel[i].dataset.subscriptionid !== "undefined") {
          // Bind the event to the cancel button
          btnCancel[i].addEventListener("click", function(e) {
            var ans = confirm("Are you sure you want to cancel this subscription?");
            if (ans) {
              var res = subscription.cancelSubscription(e.target.dataset.subscriptionid);
              res.then(result => {
                window.location.pathname = "/subscription.html";
              }).catch(err => {
                errorHandler.setError("Error", err);
                errorHandler.showError();
                return false;
              })
            } else {
              return false;
            }
          });
        }
      }
    }
    if (btnRenew.length > 0) {
      // Loop through each button and add the event
      for (var i = 0; i < btnRenew.length; i++) {
        // If the renew button is found run things differently
        if (btnRenew[i] !== false && btnRenew[i].dataset.subscriptionid !== "undefined") {
          btnRenew[i].addEventListener("click", function(e) {
            var ans = confirm("This will set the subscription to automatically renew. Do you wish to continue?");
            if (ans) {
              var res = subscription.renewSubscription(e.target.dataset.subscriptionid);
              res.then(result => {
                window.location.pathname = "/subscription.html";
              })
              .catch(err => {
                errorHandler.setError("Error", err);
                errorHandler.showError();
                return false;
              });
            } else {
              return false;
            }
          });
        }
      }
    }
    if (btnModify.length > 0) {
      // Loop through each button and add the event
      for (var i = 0; i < btnModify.length; i++) {
        // If the renew button is found run things differently
        if (btnModify[i] !== false && btnModify[i].dataset.subscription !== "undefined") {
          btnModify[i].addEventListener("click", function(e) {
            var sub = JSON.parse(localStorage.getItem('objSubscription'));
            if (sub !== null) {
              var res = localStorage.setItem('curSubscription', JSON.stringify(sub[Number(e.target.dataset.subscription)]));
              window.location.pathname = "/choice.html";
            } else {
              errorHandler.setError("Error", "Cannot set subscription object");
              errorHandler.showError();
              return false;
            }
          });
        }
      }
    }
    return true;
  },


  /**
   *
   *  Cancel a specific subscription
   *
   *  @param     String     subscriptionId      The id of the subscription to cancel
   *
   *  @return    Boolean                        Returns true if successful, false otherwise
   *
   */
  cancelSubscription : function(subscriptionId = false) {
    // Make sure the subscription ID was set
    if (subscriptionId !== false) {
      let url  = apiUrl + '/subscriber/cancel';
      let data = {'token' : localStorage.getItem('token'), 'subscriptionId' : subscriptionId};
      // Send a request to the endpoint
      var request = ajx.req('POST', url, data, 'json');
      return new Promise(function(resolve, reject) {
        request.then(
          result => {
            console.log("Result: " + result);
            resolve(result);
          }
        ).catch(err => {
          console.log("Error: " + err);
          reject(err);
        });
      });
    } else {
      errorHandler.setError("Error","Subscription ID not set");
      errorHandler.showError();
      return false;
    }
  },


/**
 *
 *  Renew a specific subscription
 *
 *  @param      String       subscriptionId       The ID of the subscription to renew
 *
 *  @return     Boolean                           Returns true upon success, false otherwise
 *
 */
  renewSubscription : function(subscriptionId = false) {
    // Make sure the subscriptionId was set
    if (subscriptionId !== false) {
      // Set the parameters for the sending of data
      let url  = apiUrl + '/subscriber/renew';
      let data = {'token' : localStorage.getItem('token'), 'subscriptionId' : subscriptionId};
      // Configure and setup the request to send to the server
      var request = ajx.req('POST', url, data, 'json');
      return new Promise(function(resolve, reject) {
        request.then(result => {
          console.log("Result: ", result);
          resolve(result);
        }).catch(err => {
          console.log("Error: ", err);
          reject(err);
        });
      });
    } else {
      errorHandler.setError("Error", "Subscription ID not set");
      errorHandler.showError();
      return false;
    }
  },


}


// If we are on the subscription page then automatically compose the page
if (window.location.pathname === "/subscription.html") {
  var getSubs = subscription.getSubscriptions(localStorage.getItem('user'));
  getSubs.then(result => {
    // Set in the localStorage
    localStorage.setItem('objSubscription', JSON.stringify(result.message));
    // Compose the page
    var compose = subscription.composeSubscriptionPage(result.message);
    // Bind the events to the buttons
    subscription.bindButtonEvents();
  }).catch(err => {
    console.log("Error: ", err);
    errorHandler.setError("Error", err);
    errorHandler.showError();
    return false;
  });
}
