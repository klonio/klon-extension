var credential = {
  hasToken : async function() {
    var token = localStorage.getItem("token");
    if (token) {
      // Check to make sure the token is valid
      var url  = apiUrl + "/auth/checkToken";
      var data = {"token" : token};
      var req  = await ajx.req('POST', url, data, 'json');
      try {
        // console.log("Response:", req);
        try {
          if (req.message == true) {
            // Check if they are a new user
            var url = apiUrl + "/subscriber/isNew";
            var req = await ajx.req('POST', url, data, 'json');
            if (req.message == true) {
              window.location.pathname = "/welcome.html";
              return null;
            } else {
              //
              return true;
            }
          } else {
            // Clear out the localStorage
            localStorage.removeItem("token");
            if (window.location.pathname != "/login.html") {
              // Login fool
              window.location = "login.html";
            }
            // return false
            return false;
          }
        } catch(e) {
          var errHandler = errorHandler.getHandler();
          errorHandler.setError("Error", e);
          // console.log("Response:", response);
          return false;
        }
      } catch(e) {
        // Set error in the error handler
        errorHandler.setError("Error", e);
        // Show the error to the user for 3500 ms
        errorHandler.showError();
        // console.log("Response:", e);
        // Clear out the localStorage
        localStorage.removeItem("token");
        if (window.location.pathname != "/login.html") {
          // Send them back to the login page
          window.location = "login.html";
        }
        return false;
      }
    } else {
      // If no token is found, redirect the user to the login page
      if (window.location.pathname != "/login.html") {
        // Send them back to the login page
        window.location = "login.html";
      }
      return false;
    }
  },


  /**
   *
   *  Force the user to login
   *
   */
  forceLogin : function() {
    localStorage.removeItem("token");
    localStorage.removeItem("tokenExpiration");
    if (window.location.pathname != "/login.html") {
      // Send them back to the login page
      window.location = "login.html";
    }
  }

}
