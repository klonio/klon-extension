window.onload = function() {
  if (window.location.pathname == "/settings.html") {
    // Check credentials
    credential.hasToken();
    // Set the current session
    localStorage.sessionTab = "settings";
  }
  let btnLogout = document.querySelector("#logout");
  if (btnLogout !== null) {
    btnLogout.addEventListener("click", logout, false);
  }
}
