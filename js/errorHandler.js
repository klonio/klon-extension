errorHandler = {
  // Initiate the error Object
  error: {
    title : null,
    message : null
  },

  /**
   *
   *  Initialize styles for the errorHandler
   *
   */
  init : function() {
    var handler = this.getHandler();
    // If the handler is found...
    if (handler) {
      // Set the styles
      handler.style.opacity = 0;
      handler.style.height  = 0;
      handler.style.margin  = 0;
      return true;
    }
    return false;
  },

  /**
   *
   *  Determine if the page has an error handler spot
   *
   *  @return              Boolean|Object      Pass the error handler back if found, False otherwise
   *
   */
  getHandler : function() {
    try {
      //
      var handler = document.querySelector("#errorHandler");
      //
      if (typeof handler === "object") {
        return handler;
      } else {
        return false;
      }
    } catch(e) {
      console.log("Error: ", e);
      return false;
    }
  },


  /**
   *
   *  This is the function that will display error messages on the page for the user to read
   *
   *  @param     title       String                   Title -or- type of the error message
   *  @param     message     String                   Error message to display
   *  @param     showColon   Boolean    *Optional*    Include the colon in the error message like this... "Error: Some message" -or- without like this "Error Some message"
   *
   *  @return              Boolean    True upon success, False otherwise
   */
  setError : function(title, message, showColon = true) {
    // console.log("Error: ", message);
    try {
      //
      var handler = errorHandler.getHandler();
      //
      if (handler !== false) {
        // Set the error title and message
        this.error.title   = title;
        this.error.message = message;
        if (showColon === true) {
          handler.innerHTML  = "<span class='errorHandler__title'>" + title + ":&nbsp;</span><span class='errorHandler__text'>" + message + "</span>";
        } else {
          handler.innerHTML  = "<span class='errorHandler__title'>" + title + "&nbsp;</span><span class='errorHandler__text'>" + message + "</span>";
        }
        return true;
      } else {
        console.log("Error handler not found");
        return false;
      }
    } catch(e) {
      console.log("Error: ", e.message);
      return false;
    }
  },


  /**
   *
   *  Show the error message that has been set
   *
   *  @param      ms     Number      Number of milliseconds to show the error
   *
   */
  showError : function(ms = 5000) {
    var handler = errorHandler.getHandler();
    if (handler) {
      try {
        var errH = document.querySelector("#errorHandler");
        errH.style.opacity = 1;
        errH.style.height  = "20px";
        errH.style.margin  = "0 0 10px 0";
          var t = setTimeout(function() {
            errH.style.opacity = 0;
            errH.style.height  = 0;
            errH.style.margin  = 0;
            clearTimeout(t);
          }, Number(ms));
      } catch(e) {
        console.log("Value is: ", value);
        errH.innerText = e.message;
      }
    }
    return false;
  },


  /**
   *
   *  Hide the error messages from the user
   *
   *  @return              Boolean    True upon success, False otherwise
   *
   */
  hideError : function() {
    // Get the errorHandler object from the page
    var handler = errorHandler.getHandler();
    if (handler !== false) {
      // If the klonFadeOut class is not found then add it
      if (!handler.classList.contains("klonFadeOut")) {
        handler.classList.add("klonFadeOut");
        return true;
      } else {
        return true;
      }
    } else {
      console.log("Error handler not found");
      return false;
    }
  }

}
