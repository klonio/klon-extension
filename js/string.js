/**
 *
 *  Anything related to string manipulation goes here
 *
 */

var string = {

  /**
   *
   *  Go from something like https://www.example.com/this/ to example.com
   *
   */
  distillUrl : function(url) {
    let regExUrl = url.match(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?/gmi).toString();
    // If there is a "www." let's remove it
    // let wwwIndex = regExUrl.indexOf("www.");
    // if (wwwIndex !== -1) {
    //   regExUrl = regExUrl.substring(wwwIndex + 4);
    // }
    // Are there any slashes?
    // let slashIndex = regExUrl.indexOf("/");
    // If so, remove them and everything after them
    // if (slashIndex !== -1) {
    //   regExUrl = regExUrl.substring(0, slashIndex);
    // }
    return regExUrl;
  },


  /**
   *
   *
   *
   */
}
