/**
 *
 *  This is the details object. The purpose of this object is to handle everything
 *  that goes on realted to or within the identity-details.html page.
 *
 */
var details = {

  // This is your private key to decrypt your identities
  key : null,
  identity : {},

  /**
   *
   *  Get the subscriber's key to decrypt their identities
   *
   */
  getKey : function() {
    return new Promise((resolve, reject) => {
      // If the key is not set within the details yet then set it
      if (details.key === null || details.key === undefined || details.key === "") {
        // If the key IS set within the localStorage, get it and split it into a useful array
        if (localStorage.getItem("key") != undefined && localStorage.getItem("key") != null && localStorage.getItem("key") != "") {
          var key = splitHexValue(localStorage.getItem("key"), 8);
          key
          .then(arrKey => {
            details.key = arrKey;
            resolve(arrKey);
          })
          .catch(err => {
            console.log("Error: ", err);
            reject(false);
          });
        }
      } else {
        resolve();
      }
    });
  },


  /**
   *
   *  This gets the identities from chrome.storage.local ['identities']
   *  and returns as a promise
   *
   *  @return        Promise        Identity Object | Error Message
   *
   */
  getId : function() {
    return new Promise((resolve, reject) => {
      chrome.storage.local.get(['identities'], function(result) {
        // Check for Errors...
        if (chrome.runtime.lastError) {
          reject(chrome.runtime.lastError.message);
        } else {
          resolve(result);
        }
      });
    })
  },


  /**
   *
   *  Get the identity requested from the subscriber
   *
   *  @param      idNum       Integer        This is the specific ID[index] to return
   *
   *  @return                 Object         This is the ID returned
   *
   */
  getIdentity : async function(idNum) {
    try {
      let getId = await details.getId();
      if (getId === undefined || getId === null || getId === "") {
        try {
          // Need to get the identities
          let getIds = await getIdentities();
          // Get the storage data
          let getStorageData = await getStorageData();
          // If there was an issue getting the storage data return false
          if (getStorageData !== true) {
            return false;
          }
          // Attempt to get the identity now
          let getIdentity = await details.getId();
          // Make sure that it is of type object
          if (typeof getIdentity !== "object") {
            try {
              getIdentity = JSON.parse(getIdentity);
            } catch(err) {
                console.log("Details.getIdentity Error: ", e);
                return false;
            }
          }
          // Retrieve specific identity
          result = getIdentity["identities"][idNum];
          // Return the specific identity
          localStorage.setItem("currentId", JSON.stringify(result));
          // Return the result
          return result;
        } catch(e) {
          console.log("Details.getIdentity Error: ", e);
          return false;
        }
      } else {
        // Convert to Object
        if (typeof getId != "object") {
          try {
            getId = JSON.parse(getId);
          } catch (err) {
            console.log(err);
            return err;
          }
        }
        // Retrieve specific identity
        getId = getId["identities"][idNum];
        // Return the specific identity
        localStorage.setItem("currentId", JSON.stringify(getId));
        console.log("Details Identity: ", getId);
        return getId;
      }
    } catch(e) {
      console.log("Details.getIdentity Error: ", e);
      return false;
    }
  },


  loaded : async function() {
    // Get the id # we want to load from the URL
    var idNumber = window.location.search;
    idSlice = idNumber.indexOf("?id=");
    idNumber = Number(idNumber.slice(idSlice + 4));
    // Make sure idNumber is actually a number
    if (typeof idNumber === "number" && isNaN(idNumber) === false) {
      try {
        // Good, get the key.
        let getIds   = await details.getIdentity(idNumber);
        let identity = identityDetails.getCurrentIdentity();
        try {
          // Prepare the identity to go on the page
          let prepare  = await identityDetails.prepareIdentity(identity);
          // Build the page
          let compose  = identityDetails.composePage(prepare);
          //
          return true;
        } catch(e) {
          console.log("Error: ", e);
          return false;
        }
      } catch(e) {
        console.log("Error: ", err);
        return false;
      }
    } else {
      console.log("It's not a number!");
    }
  }
}


// if (window.location.pathname == "/identity-details.html") {
//   // Get the id # we want to load
//   var idNumber = window.location.search;
//   console.log("idNumber: " + idNumber);
//   idSlice = idNumber.indexOf("?id=");
//   console.log("idSlice: " + idSlice);
//   idNumber = Number(idNumber.slice(idSlice + 4));
//   console.log("idNumber: " + idNumber);
//   // Make sure idNumber is actually a number
//   if (typeof idNumber === "number" && isNaN(idNumber) === false) {
//     console.log("It's a number!");
//     // Good, get the key.
//     var getKey = decrypt.getKey();
//     getKey.then(result => {
//       console.log("Got the key, ", result);
//       var getIds = details.getIdentity(idNumber);
//       console.log("idNumber is set to: ", idNumber);
//       getIds.then(ids => {
//         try {
//           details.compose();
//           return true;
//         } catch(err) {
//           console.log("Error: ", err);
//           return false;
//         }
//       })
//       .catch(err => {
//         console.log("Error: ", err);
//         return false;
//       })
//     })
//     .catch(err => {
//       console.log("Error: ", err);
//       return false;
//     });
//   } else {
//     console.log("It's not a number!");
//   }
// }


if (window.location.pathname == "/identity-details.html") {
  details.loaded();
}
