/**
 *
 *  Author: Klon.io
 *
 *  This is a complete rewrite of the login.js
 *  This time we are using promises so that
 *  can have more control over when certain functions
 *  are ran.
 *
 */

  /**
   *
   *  Hide the error messages
   *
   */
  function hideErrors() {
    return new Promise(function(resolve, reject){
      var errorMsg = document.querySelector('.error__message');
      if (errorMsg.classList.contains('error__show')) {
        errorMsg.classList.remove('error__show');
        resolve('error hidden');
      } else {
        resolve('error already hidden');
      }
    });
  }


  /**
   *
   *  Generate a random salt of specified length
   *
   *  @param   length      String    Password to be hashed
   *
   *  @return  salt        String    Random salt of the length specified
   *
   */
  function generateSalt(length) {
    // initialize the salt variable
    var salt = '';
    // expand the salt variable until we hit the desired length or beyond
    for(var i = 1; salt.length <= length; i++) {
      var salt = salt + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }
    // trim the salt to size
    salt = salt.substring(0, length);
    // return the salt
    return salt;
  }


  /**
   *
   *  Generate a random salt of specified length
   *
   *  @param   length      String    Password to be hashed
   *
   *  @return  salt        String    Random salt of the length specified
   *
   */
  function formArray(data) {
    // get length of password
    dataLength = data.length;
    // pad data to 128 char with random data
    data = data.padStart(512,generateSalt(512));
    var strArray = new Array();
	  for (var i = 0; i < data.length; i++) {
      n = Math.floor(i/16);
    	strArray[n] = data.slice(n*16, (n+1)*16);
    }
    // Add length of password as last key/value pair
    strArray[32] = dataLength;
    return strArray;
  }


  /**
   *
   *  Split a hexadecimal hash into a typed array to be used as an AES encryption key
   *
   *  @param   hexVal    String                    Hex value of hash * 1,000 (username + password)
   *  @param   t         Number                    A Number, 2 or 8 depending on what the hex needs to be converted to
   *                                                 2  -- Returns a  Uint8Array
   *                                                 8  -- Returns an Int32Array
   *
   *  @return            Int32Array | Uint8Array   This is to be used as an encryption key
   *
   */
  function splitHexValue(hexVal, t=2) {
    return new Promise(function(resolve, reject) {
      if (hexVal.length % 8 == 0) {
        var a = new Array();
        for (var i = 0; i < hexVal.length; i++) {
          var n = Math.floor(i/t);
          a[n] = hexVal.substr(n*t,t);
          a[n] = Number("0x" + a[n]);
        }
        if (t == 2) {
          var h = new Uint8Array(a);
        } else if (t == 8) {
          var h = new Int32Array(a);
        }
        resolve(h);
      } else {
        reject(false);
      }
    });
  }


  /**
   *
   *  Reverse a given string (not unicode safe, really don't care)
   *
   *  @param    data    String   The data to be reversed
   *
   *  @return           String   Reversed string
   *
   */
  function reverseString(data) {
    return data.split('').reverse().join('');
  }


  function manipulateData(data) {
    // console.log(data);
    // Get the array
    var dataSalted = formArray(data);
    // Pull the length from the salted data
    var dataLength = dataSalted[32];
    // Pop the password length off of the end of the array
    dataSalted.pop();
    // Reverse the odd keyed arrays
    dataSalted.forEach(function(index) {
      if (index % 2 != 0) {
        dataSalted[index] = reverseString(dataSalted[index]);
      }
    });
    // console.log(dataSalted);
  }


  /**
   *
   *  Hash a password
   *
   *  @param   password    String    Password to be hashed
   *  @param   iterations  String    The password to be hashed
   *
   *  @return              String    Hash of the password
   *
   */
  function hashPassword(password, iterations) {
    var hash = password;
    for(var i = 1; i <= iterations; i++) {
      var hash = sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(hash));
    }
    return hash;
  }


  /**
   *
   *  SHA512 a string
   *
   *  @param   str         String    String to be hashed
   *  @param   iterations  String    The password to be hashed
   *
   *  @return              String    Hash of the password
   *
   */
  function sha512(str, iterations) {
    var hash = str;
    for(var i = 1; i <= iterations; i++) {
      hash = sjcl.codec.hex.fromBits(sjcl.hash.sha512.hash(hash));
    }
    return hash;
  }


  /**
   *
   *  Hash the password
   *
   *  @param    email       String       Email address to be used with JWT
   *  @param    password    String       Password string
   *  @param    object      HTMLElement  HTML Element to put the status in
   *
   *  @return               String       Hash of the email & password
   *
   */
  function hashScrypt(email, password, object) {
    return new Promise(function(resolve, reject) {
      // Update object text
      if (object) {
        object.innerHTML = "Generating Hashes...";
      }
      var salt = hashPassword(email, 1000);
      // Difficulty
      var n = 16384;
      // Block size
      var r = 16;
      var p = 1;
      var key = sjcl.misc.scrypt(password, salt, n, r, p);
      key = sjcl.codec.hex.fromBits(key);
      resolve(key + "." + salt);
    });
  }


  /**
   *
   *
   *
   */
  function hashPBKDF2(email, password) {
    return new Promise(function(resolve, reject){
      // Each random "word" is 4 bytes, so 8 would be 32 bytes
      // var saltBits = "fc8c1e5bee2334be3d580dc5cf71e2950873e087b256432f4dd2aadd39e0aee82c0c60ac9f0610de0decf86bd5a09ba163a6666d0f34635df26e3cfe917ac96e";
      var saltBits = hashPassword(email, 500);
      // eg. [588300265, -1755622410, -533744668, 1408647727, -876578935, 12500664, 179736681, 1321878387]
      // I left out the 5th argument, which defaults to HMAC which in turn defaults to use SHA256
      var derivedKey = sjcl.misc.pbkdf2(password, saltBits, 500, 512);
      // eg. [-605875851, 757263041, -993332615, 465335420, 1306210159, -1270931768, -1185781663, -477369628]
      // Storing the key is probably easier encoded and not as a bitArray
      // I choose base64 just because the output is shorter, but you could use sjcl.codec.hex.fromBits
      var key = sjcl.codec.hex.fromBits(derivedKey);
      // eg. "2+MRdS0i6sHEyvJ5G7x0fE3bL2+0Px7IuVJoYeOL6uQ="
      // console.log("key: " + key + " salt: " + saltBits);
      // return key + "." + saltBits;
      resolve(key);
    });
  }


  /**
   *
   *  Show an error message
   *
   *  @param  error   String    Error message to display
   *
   */
  function showError(error) {
    return new Promise(function(resolve, reject) {
      var errorMsg = document.querySelector('.error__message');
      errorMsg.innerHTML = error;
      if (!errorMsg.classList.contains('error__show')) {
        errorMsg.classList.add('error__show');
      }
      resolve(error);
    });
  }


  /**
   *
   *  Count the number of commas in a given string
   *
   *  @param   String    haystack    The string to search through
   *
   *  @return  Number                The number of occurances in the string
   *
   */
  function countCommas(haystack) {
    var count = (haystack.match(/,/g) || []).length;
    return Number(count);
  }


  async function getStorageData() {
    var data    = localStorage.getItem('identities');
    var rawKey  = localStorage.getItem('key');
    try {
      var key   = await splitHexValue(rawKey, 8);
      // Assemble our lovely JSON object below...
      // console.log("Key: ", key);
      // console.log("Data: ", data);
      // Base64 decode data
      data = window.atob(data);
      // console.log("Decoded: ", data);
      // Decrypt the data
      // data = decryptIt(key, data);
      // console.log("Decrypted: ", data);
      // Get the index of the pipe (this is the end of the column names)
      var pipe = data.indexOf("|");
      // Split the string to the pipe index
      var columns = data.substr(0, pipe);
      // Split the string into column names in an array
      var columnNames = columns.split(",");
      // Get the identities (from the first pipe to the end of the string)
      var identities = data.substr(++pipe);
      identities = identities.split("|");
      var jsonId = "{ \"identities\" : [";
      for (var i = 0; i < identities.length; i++) {
        var breakItUp = identities[i].split(",");
        for (var n = 0; n < breakItUp.length; n++) {
          if (n == 0) {
            jsonId = jsonId + "{" + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"' + ",";
          } else if (n == (breakItUp.length - 1)) {
            jsonId = jsonId + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"},';
          } else {
            jsonId = jsonId + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"' + ",";
          }
        }
        var prev = i;
      }
      // Remove the last comma
      jsonId = jsonId.substr(0,(jsonId.length - 1));
      // Add the closing bracket for the JSON object
      jsonId = jsonId + "]}";
      // console.log("JSON: ", jsonId);
      chrome.storage.local.set({identities: jsonId}, function() {
        return true;
      });
    } catch (err) {
      console.log('getStorageData Error: ', err);
      return false;
    }
  }


  /**
   *
   *  Create a key for the user to encrypt and decrypt their data
   *
   *  @param    user       String           Username for the account
   *  @param    password   String           Password for the account
   *
   *  @return   key        String|Boolean   Unique key used to encrypt and decrypt data must be broken down into Int32Array to use
   *
   */
  function genKey(user, password) {
    return new Promise(function(resolve, reject) {
      // Convert to lowercase and trim the username
      user = user.toLowerCase().trim();
      // sCrypt hash(emailAddress + password)
      var salt = hashPassword(user + password, 1000);
      // Difficulty level
      var n = 16384;
      // Block size
      var r = 8;
      //
      var p = 1;
      // Get the hexadecimal form of the scrypt hash
      var hash = sjcl.codec.hex.fromBits(sjcl.misc.scrypt(password, salt, n, r, p));
      // SHA512 the scrypt hash 2,500 rounds
      var hashed = hashPassword(hash, 2500);
      if (hashed) {
        // This is your private AES key
        var key = hashed;
        resolve(key);
      } else {
        // Return false let the calling function that this thing failed
        reject(false);
      }
    });
  }

  /**
   *
   *  This is the encryption function unique to each subscriber
   *
   *  @param     key       String       Usually found in localStorage.getItem('key');
   *  @param     data      Any          Data that you want to encrypt
   *
   *  @return              String       Cipher Text $ delimited
   *
   */
  function encryptDataWithKey(key, data) {
    // Encrypt the data with the provided key
    var encrypted = sjcl.encrypt(key, data, {ks: 256, mode : "gcm"});
    // Turn the thing into a JSON Object
    encrypted = JSON.parse(encrypted);
    // Example: {"iv":"XQ+Ugs04keiVlW2R2plZxw==","v":1,"iter":10000,"ks":256,"ts":64,"mode":"gcm","adata":"","cipher":"aes","ct":"2TDnAnQSOigIlvgVeUiRoBcA9+U="}
    // Return the cipherText and unique IV delimited with "$"
    return encrypted.ct + "$" + encrypted.iv
  }


  /**
   *
   *  Decrypt encrypted text with a key
   *
   *  @param   key     Int32Array    This is the decryption key
   *                                 Example: new Int32Array([-520615157,947483824,-1599373281,-1608669598,-83085555,1337441572,789524302,1436682178])
   *
   *  @param   data    String        Base64 encoded cipher text delimited iv with "$"
   *                                 Example: ZqGxqt7AvuU3Bp4UOZYnYhWHvbncxPr2qfoq4lDLaQ==$i5AwqFgD4qignoMO7rLszA==
   *
   *  @return          String        Decrypted text
   *
   */
  function decryptIt(key, data) {
    // Find the delimiter location in the data string
    var delimiterPos   = data.indexOf("$");
    if (delimiterPos === -1) {
      // Delimiter not found
      return false;
    }
    // Cut out the cipher text from the data
    var ct  = data.substr(0, delimiterPos);
    // Cut out the iv from the data string
    var iv  = data.substr(++delimiterPos);
    // Assemble the json object
    var cipher  = {"iv":iv,"mode":"gcm","cipher":"aes","ct":ct};
    cipher = JSON.stringify(cipher);
    // Decrypt the cipher text
    var decrypt = sjcl.decrypt(key, cipher);
    // Return the the decrypted text
    return decrypt;
  }


  /**
   *
   *  Check to see if we have the key stored in localStorage
   *
   *  @param     key    String    This is the key to check for in localStorage
   *
   *  @return           Boolean   If key is stored return true else false
   *
   */
  function checkIfKeyExists(key) {
    var hasPubKey;
    if (localStorage.getItem(key)) {
      hasPubKey = true;
      return hasPubKey;
    } else {
      hasPubKey = false;
      return hasPubKey;
    }
    return false;
  }
