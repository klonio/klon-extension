// Listen for messages
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  console.log("Got it");
  switch (request.msg) {
  //
    case "new identity":
      // Get a new identity
      var newIdentity = generateIdentity();
      newIdentity
      .then(result => {
        // console.log("new identity: ", result);
        sendResponse(result);
      })
      .catch(error => {
        // console.log("Error in generatePromise Promise: ", error);
        if (error === "Unauthorized.") {
          console.log("Your session has expired! You must login to Klon Extension in order to continue");
          // Send a message to the extension itself
          comm.sendMessage({"msg":"ext auth"});
        }
      });
      break;
  //
    case "use identity":
      // Use this identity on the URL provided
      console.log("REQ ID: ", request.identity);
      useIdentity(request.identity).then(result => {
        console.log("RES ID: ", result);
        var payload = formatPayload(result).then( encPayload => {
          encPayload = JSON.stringify(encPayload);
          var url     = apiUrl + "/klon/useCandidateForURL";
          var request = ajx.req('POST', url, encPayload).then( response => {
            if (response.status != undefined && response.status != null && response.status === "success") {
              sendResponse(response);
            }
          });
        });
      });
      break;
  //
    case "getAssociatedIdentities":
      // Use this identity on the URL provided
      // console.log("REQUEST URL: ", request.url);
      // Get the key and turn into typed array
      k = decrypt.getKey();
      k.then(key => {
        var fetch = identities.fetchIdentitiesForURL(request.url, key);
        fetch.then(result => {
          var decId = identities.decryptIdentity({"identities" : result}, key);
          decId
          .then(resp => {
            sendResponse(resp);
          })
          .catch(err => {
            console.log("Error: ", err);
            sendResponse(err);
          });
        }).catch(err => {
          console.log("Error: ", err);
          sendResponse(err);
        });
      })
      .catch(err => {
        console.log("Error: ", err);
        sendResponse(err);
      });
      break;
    //
    case "get settings":
      // console.log("Getting Settings.......");
      var getSet = settings.passSettings(request.url);
      getSet.then(result => {
        console.log("Get Set: ", result);
        sendResponse(result);
      }).catch(err => {
        console.log("Le fail: ", err);
      });
      break;
  }
  // Allows us to use this function asynchronously
  return true;
});

