/**
 *
 *  Gets the data for and composes the choice.html page
 *
 */
var choice = {


  /**
   *
   *  @param       Object       objSubscription      Subscription Object
   *
   *  @return      Boolean                           True if successful, false otherwise
   *
   */
  composePage : function(objSubscription = localStorage.getItem('curSubscription')) {
    // Make sure the array was set
    if (objSubscription !== null) {
      try {
        objSubscription = JSON.parse(objSubscription);
      } catch(e) {
        errorHandler.setError("Error", e);
        errorHandler.showError();
        console.log("Error: ", e);
        return false;
      }
      console.log("OBJSUB: ", objSubscription);
      // Make sure the object is actually an object
      if (typeof objSubscription === "object" && objSubscription.length === undefined) {
        // Find the radio buttons
        var elRadios = document.querySelectorAll("[name='modifySubscription']");
        console.log("elRadios: ", elRadios);
        console.log("Name: ", objSubscription.name);
        switch(objSubscription.name) {
          case "Basic Plan":
            elRadios[0].checked = true;
            break;
          case "Regular Plan":
            elRadios[1].checked = true;
            break;
          case "Premium Plan":
            elRadios[2].checked = true;
            break;
          default:
            console.log('idk?');
        }
        // Find the Auto Renew toggle switch
        var elAutoRenewSwitch = document.querySelector("#toggleAutoRenew");
        // Set the toggle switch
        if (objSubscription.cancelAtPeriodEnd === false) {
          elAutoRenewSwitch.checked = true;
        } else {
          elAutoRenewSwitch.checked = false;
        }
        return true;
      } else {
        console.log("Not an object");
        return false;
      }
    } else {
      console.log("Object is null");
      return false;
    }
  },


  /**
   *
   *  Change the subscription for the user on the next billing cycle
   *
   *  @param      String      token              User authentication token (JWT)
   *  @param      String      subscriptionId     ID of the subscription to modify
   *  @param      String      newPlan            The new plan the user wants to be signed up for
   *
   *  @return     Boolean                        True if successful, False otherwise
   *
   */
  modifySubscription : function() {
    // Get the users token
    var token = localStorage.getItem('token');
    if (token !== null) {
      // Get the Subscription ID
      var subscriptionId = localStorage.getItem('curSubscription');
      if (subscriptionId !== null) {
        try {
          subscriptionId = JSON.parse(subscriptionId).id;
        } catch (e) {
          errorHandler.setError("Error", e.message);
          errorHandler.showError();
          return false;
        }
        //
        try {
          var elNewPlan = document.querySelectorAll("[name=modifySubscription]");
        } catch (e) {
          errorHandler.setError("Error", e.message);
          errorHandler.showError();
          return false;
        }
        //
        if (elNewPlan !== null) {
          var newPlan;
          // Loop through the elements and determine the one that is checked
          for (i = 0; i < elNewPlan.length; i++) {
            if (elNewPlan[i].checked) {
              newPlan = elNewPlan[i].value;
              break;
            }
          }
          // Get the spinner elements and apply their classes
          var loader    = document.querySelector("#loaderModSub");
          var spinner   = loader.querySelector('.spinner');
          var spinSpin  = spinner.querySelector('#s3');
          var spinCheck = spinner.querySelector('#s4');
          // Make the spinner background and spinner fade in
          loader.classList.remove("choice__loader--fadeOut");
          spinSpin.classList  = "spinner__visible";
          spinCheck.classList = "spinner__hidden";
          // Send the data to the server and retrieve the response
          var data    = {'token' : token, 'subscriptionId' : subscriptionId, 'newPlan' : newPlan};
          var url     = apiUrl + '/subscriber/changePlan';
          var request = ajx.req('POST', url, data, 'json');
          return new Promise(function(resolve, reject) {
            request.then(result => {
              // Do the fancy spinner animation
              spinSpin.classList  = "spinner__hidden";
              spinCheck.classList = "spinner__visible";
              setTimeout(function(){
                loader.classList.add("choice__loader--fadeOut");
              }, 2000);
              // Update the current subscription in localStorage
              var curSub = JSON.parse(localStorage.getItem('curSubscription'));
              curSub.cancelAtPeriodEnd = false;
              //
              curSub.name = newPlan[0].toUpperCase() + newPlan.substring(1) + " Plan";
              localStorage.setItem('curSubscription', JSON.stringify(curSub));
              // Resolve the promise
              resolve(result);
            }).catch(err => {
              // Do the fancy spinner animation
              spinSpin.classList  = "spinner__hidden";
              spinCheck.classList = "spinner__visible";
              setTimeout(function(){
                loader.classList.add("choice__loader--fadeOut");
              }, 2000);
              // Update the current subscription in localStorage
              var curSub = JSON.parse(localStorage.getItem('curSubscription'));
              curSub.cancelAtPeriodEnd = !curSub.cancelAtPeriodEnd;
              localStorage.setItem('curSubscription', JSON.stringify(curSub));
              // Resolve the promise
              reject(err);
            });
          });
        } else {
          errorHandler.setError("Error", "New plan elements not found");
          errorHandler.showError();
          return false;
        }
      } else {
        errorHandler.setError("Error", "Subscription not found");
        errorHandler.showError();
        return false;
      }
    } else {
      errorHandler.setError("Error", "Token not found");
      errorHandler.showError();
      return false;
    }
  },


  /**
   *
   *  Toggle the automatic renewing attribute for the particular subscription
   *
   *  @param      String       token                The users authentication token (JWT)
   *  @param      String       subscriptionId       The id of the subscription to be modified
   *  @param      Boolean      autoRenew            true if you want it on, false if you want it off
   *
   *  @return     Object                            Returns a promise containing information about the success/failure of the operation
   *
   */
  toggleAutoRenew : function () {
    // Get the users token
    var token = localStorage.getItem('token');
    // Make sure the token was found
    if (token !== null) {
      var subscriptionId = localStorage.getItem('curSubscription');
      if (subscriptionId !== null) {
        // Try to parse the JSON string
        try {
          subscriptionId = JSON.parse(subscriptionId).id;
        } catch (e) {
          errorHandler.setError("Error", e.message);
          errorHandler.showError();
          return false;
        }
        // Get the autoRenew toggle element and spit out the checked value
        try {
          var elAutoRenew = document.querySelector("#toggleAutoRenew");
        } catch (e) {
          errorHandler.setError("Error", e.message);
          errorHandler.showError();
          return false;
        }
        // Make sure the element was found in the DOM
        if (elAutoRenew !== null) {
          var autoRenew = Boolean(elAutoRenew.checked);
          // Get the spinner elements and apply their classes
          var loader    = document.querySelector("#loaderAutoRenew");
          var spinner   = loader.querySelector('.spinner');
          var spinSpin  = spinner.querySelector('#s1');
          var spinCheck = spinner.querySelector('#s2');
          // Make the spinner background and spinner fade in
          loader.classList.remove("choice__loader--fadeOut");
          spinSpin.classList  = "spinner__visible";
          spinCheck.classList = "spinner__hidden";
          // Send the data to the server and get back a response to show the user
          var data    = {'token' : token, 'subscriptionId' : subscriptionId, 'autoRenew' : autoRenew};
          var url     = apiUrl + '/subscriber/autoRenew';
          var request = ajx.req('POST', url, data, 'json');
          return new Promise(function(resolve, reject) {
            request.then(result => {
              // Do the fancy spinner animation
              spinSpin.classList  = "spinner__hidden";
              spinCheck.classList = "spinner__visible";
              setTimeout(function(){
                loader.classList.add("choice__loader--fadeOut");
              }, 2000);
              // Update the current subscription in localStorage
              var curSub = JSON.parse(localStorage.getItem('curSubscription'));
              curSub.cancelAtPeriodEnd = !curSub.cancelAtPeriodEnd;
              localStorage.setItem('curSubscription', JSON.stringify(curSub));
              // Resolve the promise
              resolve(result);
            }).catch(err => {
              // Do the fancy spinner animation
              spinSpin.classList  = "spinner__hidden";
              spinCheck.classList = "spinner__visible";
              setTimeout(function(){
                loader.classList.add("choice__loader--fadeOut");
              }, 2000);
              // Update the current subscription in localStorage
              var curSub = JSON.parse(localStorage.getItem('curSubscription'));
              curSub.cancelAtPeriodEnd = !curSub.cancelAtPeriodEnd;
              localStorage.setItem('curSubscription', JSON.stringify(curSub));
              // Resolve the promise
              reject(err);
            });
          });
        } else {
          errorHandler.setError("Error", "Can not find auto renew toggle");
          errorHandler.showError();
          return false;
        }
      } else {
        errorHandler.setError("Error", "Subscription ID not found");
        errorHandler.showError();
        return false;
      }
    } else {
      errorHandler.setError("Error", "Token not found");
      errorHandler.showError();
      return false;
    }
  },


  /**
   *
   *  Bind the events to the buttons in choice.html
   *
   */
  bindEvents : function() {
    // Get the buttons
    var btnSubscription = document.getElementById('btnSubscription');
    var btnAutoRenew    = document.getElementById('btnAutoRenew');
    // Make sure the buttons exist
    if (btnSubscription !== undefined && btnSubscription !== null) {
      btnSubscription.addEventListener("click", function() {
        return choice.modifySubscription();
      });
    } else {
      console.log("Could not bind event to btnSubscribe");
    }
    if (btnAutoRenew  !== undefined && btnAutoRenew !== null) {
      btnAutoRenew.addEventListener("click", function() {
        return choice.toggleAutoRenew();
      });
    } else {
      console.log("Could not bind event to btnAutoRenew");
      return false;
    }
  }
};


/**
 *
 *  If the user is on the choice.html page then run the bit
 *
 */
if (window.location.pathname === "/choice.html") {
  window.addEventListener("load", function() {
    choice.composePage();
    choice.bindEvents();
  });
}
