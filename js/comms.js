/**
 *
 *  This script handles all of the communication between the content script,
 *  background script, and the extension itself.
 *
 */
var comm = {

  /**
   *
   *  Sends out a message and returns the response inside of a promise
   *
   *  @param    message     Object
   *
   *  @return               Promise      Response (if any) from the callback
   *
   */
  sendMessage : function(message = {"text" : "helo"}) {
    return new Promise((resolve, reject) => {
      var id = chrome.runtime.id;
      // console.log("COMM ID: ", id);
      chrome.runtime.sendMessage(id, message, function talkBack(response) {
        // Return any response that we may receive
        console.log("**COMMS Response", response);
        resolve(response);
      });
      if (chrome.runtime.lastError) {
        console.log("Error: ", chrome.runtime.lastError.message);
        // Something bad happened
        reject("Error ", chrome.runtime.lastError.message);
      }
    });
  }

}
