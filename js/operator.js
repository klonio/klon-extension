/**
 *
 *  This object is responsible for flow of the extension
 *
 *  Operator will find and restore sessions from the extension making the app
 *  easier to use.  
 *
 */

var operator = {
  /**
   *
   *  Check to see if there is a previous session stored for the user
   *
   */
  hasSession : function () {
    var sTab = localStorage.getItem("sessionTab");
    if (sTab) {
      return true;
    } else {
      return false;
    }
  },


  /**
   *
   *  Redirect the user to the page specified
   *
   *  @param      tabName      String       name of the tab to redirect the user to
   *
   *  @return                  Boolean
   *
   */
  redirect : function(tabName) {
    // Add the / and .html to the end of the pageName
    tabName += ".html";
    //
    try {
      window.location = tabName;
      return true;
    } catch(e) {
      console.log("Unable to redirect the user");
      window.location = "mail.html";
      // errorHandler.setError("Error", "Unable to redirect the user");
      return false;
    }
  }


}
