/**
 *
 *  The rate limiter is just that, a client side rate limiter
 *  For the not-so-technically gifted this will serve as a good enough
 *  catch-all for those that would love to spam requests to the API
 *
 *  For the tech savvy there will also be a server side version
 *  implemented on the API. This is not to be depended upon for full rate limiting
 *  protection.
 *
 */
var rateLimiter = {


  /**
   *
   *  This function is only to be run initially upon login to give the user some limit ability to request a token and get started
   *
   */
  setTempValues : function() {
    return new Promise(function(resolve, reject) {
      try {
        // Get the current time for use later in the lastRequestTime
        let currentTime = Math.round((new Date()).getTime() / 1000);
        // Set the values
        localStorage.setItem("tempRole", 9);
        localStorage.setItem("tempRateLimit", 10);
        localStorage.setItem("tempLastRequestTime", (currentTime - 6));
        // Resolve the promise
        resolve(true);
      } catch(e) {
        // Something messed up, display an error
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        // Reject the promise
        reject(e.message);
      }
    });
  },


  /**
  *
  *  Determines if this is the users first request... Is the user logging in?
  *
  */
  isFirstRequest : function() {
    return new Promise(function(resolve, reject) {
      // Are the tempRole and tempRateLimit set in the localStorage?
      let tempRole        = localStorage.getItem("tempRole");
      let tempRateLimit   = localStorage.getItem("tempRateLimit");
      let tempLastRequest = localStorage.getItem("tempLastRequestTime");
      if (tempRole === null && tempRateLimit === null && tempLastRequest === null) {
        resolve(false);
      } else {
        resolve(true);
      }
    });
  },


  /**
   *
   *  Remove the temporary values and set the new ones
   *
   */
  removeTemporaryValues : function() {
    return new Promise(function(resolve, reject) {
      try {
        localStorage.removeItem("tempRole");
        localStorage.removeItem("tempRateLimit");
        localStorage.removeItem("tempLastRequestTime");
        resolve(true);
      } catch (e) {
        errorHandler.setError("Error", e.message || "Could not remove items from localStorage");
        errorHandler.showError();
        reject(e.message || "Could not remove items from localStorage");
      }
    });
  },


  /**
   *
   *  This will set the rate limit based on the users role
   *
   */
  setRateLimit : function() {
    return new Promise(function(resolve, reject) {
      // Get the user's role if it is set
      let role = localStorage.getItem("role") || localStorage.getItem("tempRole") || null;
      // Make sure that is data we can work with
      if (typeof Number(role) == "number" && !isNaN(role)) {
        // Set the rate limit based on the users role
        switch (userRoleResult) {
          // Welcome to the void
          case 0:
            console.log("Welcome to the void");
            // 61 -- This user can't do anything because they're in the void, a sort of 4th dimensional hell
            var rateLimit = 61;
            localStorage.setItem("rateLimit", rateLimit);
            rateLimiter.removeTemporaryValues();
            break;
          // Economy Account
          case 1:
            // 12 -- The user can send a request to the server every 3 seconds
            var rateLimit = 20;
            localStorage.setItem("rateLimit", rateLimit);
            rateLimiter.removeTemporaryValues();
            break;
          // Regular Account
          case 2:
            // 20 -- The user can send a request to the server every 2 seconds
            var rateLimit = 30;
            localStorage.setItem("rateLimit", rateLimit);
            rateLimiter.removeTemporaryValues();
            break;
          // Premium Account
          case 3:
            // 30 -- The user can send a request to the server every 1.2 seconds
            var rateLimit = 50;
            localStorage.setItem("rateLimit", rateLimit);
            rateLimiter.removeTemporaryValues();
            break;
          // This is someone logging in -- They get a low rate so they can't sit and spam the server with massive requests
          case 9:
            var rateLimit = 10;
            localStorage.setItem("rateLimit", rateLimit);
            localStorage.setItem("tempRateLimit", rateLimit);
          default:
            resolve(false);
            break;
        }
        resolve(rateLimit);
      } else {
        resolve(false);
      }
    });
  },


  /**
   *
   *  Get the rate limit, provided it is set otherwise return false
   *
   *  @return     Promise     Will resolve to Number|False  -or-  reject(Error)
   *
   */
  getRateLimit : function() {
    return new Promise(function(resolve, reject) {
      try {
        // Check to see if the rate limit is set
        let rateLimit = localStorage.getItem("rateLimit");
        if (rateLimit === null) {
          resolve(false);
        } else {
          resolve(rateLimit);
        }
      } catch(e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        reject(e);
      }
    });
  },


  /**
   *
   *  Gets the delay (in seconds) that are required between requests
   *
   *  @return     Number|False     Returns the delay in seconds
   *
   */
  getDelay : function() {
    return new Promise(function(resolve, reject) {
      // Check for rate limit
      let rateLimit = Number(rateLimiter.getRateLimit());
      //
      if (rateLimit !== false && !isNaN(rateLimit)) {
        let delaySeconds = Math.round(60 / rateLimit);
        resolve(delaySeconds);
      } else {
        // Can't do it
        reject(false);
      }
    });
  },


  /**
   *
   *  Set the time of the last request
   *
   */
  setLastRequestTime : function() {
    return new Promise(function(resolve, reject) {
      try {
        // Get the current time
        let currentTime = Math.round((new Date()).getTime() / 1000);
        // Set the value in the localStorage
        // Has the users role been determined from the server yet?
        let userRole = localStorage.getItem("role");
        if (userRole !== null) {
          localStorage.setItem("lastRequestTime", currentTime);
          resolve(currentTime);
        } else {
          localStorage.setItem("tempLastRequestTime", currentTime);
          resolve(currentTime);
        }
      } catch(e) {
        // Display the error and reject the promise
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        reject(e.message);
      }
    });
  },


  /**
   *
   *  Get the last request time
   *
   */
  getLastRequestTime : function() {
    return new Promise(function(resolve, reject) {
      // Check to see if there is a last request time set
      try {
        let lastRequestTime = Number(localStorage.getItem("lastRequestTime")) || Number(localStorage.getItem("tempLastRequestTime")) || null;
        console.log("LRT: ", lastRequestTime);
        if (lastRequestTime !== null && !isNaN(lastRequestTime)) {
          resolve(lastRequestTime);
        } else {
          reject(false);
        }
      } catch(e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        reject(e.message);
      }
    });
  },


  /**
   *
   *  Determine if a given request is within the limits of the rateLimiter
   *
   *  @return       Boolean       True|False -or- (Error)
   *
   */
  withinLimits : async function() {
    // First determine what information we have...
    let firstTime;
    //
    try {
      // Is this the users first time sending requests?
      firstTime = await rateLimiter.isFirstRequest();
    } catch (e) {
      errorHandler.setError("Error", e.message);
      errorHandler.showError();
      return false;
    }
    //
    if (firstTime === true) {
      // This is the users first request so don't be a douche.
      return true;
    } else {
      try {
        // Get the current time and the required delay then compare
        let currentTime     = Math.round((new Date()).getTime() / 1000);
        console.log("current time: ", currentTime);
        let lastRequestTime = await rateLimiter.getLastRequestTime();
        console.log("lastRequestTime: ", lastRequestTime);
        let requiredDelay   = await rateLimiter.getDelay();
        console.log("required delay: ", requiredDelay);
        // Is the current time minus the last request time greater than or equal to the required delay?
        let result          = ((currentTime - lastRequestTime) >= requiredDelay);
        if (result) {
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.log("Error: ", e);
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        return false;
      }
    }
  },


  /**
   *
   *  This function is the combined effort of all the other functions to determine if a request is able to go to the server
   *
   */
  allowRequest : async function() {
    // Define all our variables
    let isFirstRequest;
    let userRole;
    // Check to see if this is the first request
    try {
      let validToken = await credential.hasToken();
      if (validToken === false) {
        isFirstRequest = rateLimiter.isFirstRequest();
      }
    } catch(e) {
      errorHandler.setError("Error","An error has occurred");
    }
    //
    if (isFirstRequest === true) {
      return true;
    }
    //

  }


};
