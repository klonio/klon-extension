'use strict';

function removeItemsFromStorage() {
  return new Promise(function (resolve, reject) {
    if (localStorage.getItem('token')) {
      localStorage.removeItem('token');
    }
    if (localStorage.getItem('tokenExpiration')) {
      localStorage.removeItem('tokenExpiration');
    }
    if (localStorage.getItem('key')) {
      localStorage.removeItem('key');
    }
    resolve();
  });
}

async function logout() {
  try {
    var removeStoredItems = await removeItemsFromStorage();
    console.log(removeStoredItems);
  } catch (err) {
    console.log('Error: ' + err);
    return false;
  }
  window.location.pathname = '/login.html';
  return true;
}

if (window.location.pathname === "/settings.html") {
  var btnLogout = document.querySelector("#logout");
  if (btnLogout !== undefined && btnLogout !== null) {
    btnLogout.addEventListener("click", function () {
      logout();
    });
  }
}