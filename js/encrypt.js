/**
 *
 *  Encrypt data to send to the server with Klon.io public key obtained from
 *  URL:    /subscriber/getPubKey
 *  Method: GET
 *
 *  @param       String           data        The data that you want to encrypt
 *
 *  @return      Promise Object               Returns a resolved object with encrypted data or rejected promise object
 *
 */
async function pubEncData(data) {
  // Check if we have a public key for the server stored.
  if (localStorage.getItem('pubKey')) {
    // try catch for errors
    try {
      // Convert the hex key to an Uint8Array to be used within JS
      var pubKey = await splitHexValue(localStorage.getItem('pubKey'), 2);
    } catch (err) {
      console.log('Error: ' + err);
    }
  } else {
    // try catch for errors
    try {
      // Get the public key from the server
      var hexKey = await ajx.req("GET", apiUrl + "/subscriber/getPubKey");
      // Store the key for future use
      localStorage.setItem('pubKey', hexKey.message);
      // Convert the hexadecimal value to an Uint8Array to work with in javascript
      var pubKey = await splitHexValue(hexKey.message, 2);
    } catch(err) {
      console.log('Error: ' + err);
    }
  }
  // Encrypt the data
  var sBox = sodium.crypto_box_seal(data, pubKey);
  // Return hexadecimal value
  sBox = sodium.to_hex(sBox);
  return sBox;
}


/**
 *
 *  Format JSON data to be encrypted and turned into a proper JSON Payload Ex: {payload : "23e4f098c...0ab09d0e"}
 *
 *  @param     jsonData    JSONObject     This is a JSON Object of what you want to send to the server
 *
 *  @return                JSONObject     A perfectly formatted JSON Object ready to be hurled at the server - or - false
 *
 */
async function formatPayload(jsonData) {
  try {
    // Stringify the JSON Object
    var strJson       = JSON.stringify(jsonData);
    // Encrypt the data
    var encryptedData = await pubEncData(strJson);
    // Assemble the payload
    var payload     = {"payload" : encryptedData};
    return payload;
  } catch(err) {
      console.log('formatPayload Error: ', err);
      return false;
  }
}
