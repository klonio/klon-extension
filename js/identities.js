
/**
 *
 *
 */
function storeData(key, data) {
  chrome.storage.local.set({key: data}, function() {
    return true;
  });
}


/**
 *
 *  Pulls data from the chrome storage. This was a mistake to use, never again.
 *  From now on only use localStorage as it is cross-browser compatible
 *
 */
 function getData(key) {
  chrome.storage.local.get([key], function(result) {
    return result.key;
  });
 }


/**
 *
 *  This function gets the identities for the subscriber from the remote server and stores them in localStorage.identities
 *  First it checks against the localStorage.hash with the hash that the server has stored for the user
 *  If the hashes match, then the identities are up-to-date. Otherwise, we have to fetch the new identities
 *
 */

function getIdentities() {
  return new Promise((resolve, reject) => {
    var token = localStorage.getItem('token');
    if (token === null || token === undefined || token == "") {
      reject("No token set");
    }
    // This will be the hash for requesting identities from the server
    var hash  = localStorage.getItem('hash');
    // If the hash is not set then set it to KL0N
    if (hash === null || hash === undefined || hash == "") {
      hash = "KL0N";
    }
    console.log("HASH: ", hash);
    var str   = {"token" : token, "hash" : hash};
    console.log('str: ', str);
    var payload = formatPayload(str).then( payload => {
      if (typeof payload != "object") {
        var strPayload = JSON.stringify(payload);
      } else {
        strPayload = payload;
      }
      var url = apiUrl + "/klon/getIdentities";
      var results = ajx.req("POST", url, strPayload, "json");
      results.then(function(result) {
        if (result['data'] !== "up-to-date") {
          // console.log("result data does not equal: 'up-to-date'");
          if (result['message'] != "no identities found") {
            // Save it using the Chrome extension storage API.
            var setId    = localStorage.setItem('identities', result['data']);
            // Hash the data
            // var bitArray = sjcl.hash.sha1.hash(result['data']);
            // Convert the bitArray into hex
            // var idHash   = sjcl.codec.hex.fromBits(bitArray);
            // Set the hash in the localStorage
            // var addHash  = localStorage.setItem('hash', idHash);
            // Resolve the promise
            resolve("Yepp");
          } else {
            chrome.storage.local.set({identities: null}, function() {
              console.log('Value is set to ' + null);
            });
            resolve("No identities found");
          }
        } else {
          console.log('up-to-date!');
          errorHandler.setError("Success!", "Identities are up-to-date!", false);
          errorHandler.showError();
          resolve("Up to date");
        }
      })
      .catch( err => {
        console.log('getIdentities Error: ', err);
        if (err == "Unauthorized.") {
          localStorage.removeItem('token');
          window.location = "login.html";
        }
        reject(false);
      });
    })
    .catch( err => {
      console.log("Error: ", err);
      errorHandler.setError("Error", err);
      errorHandler.showError();
      reject(false);
    });
  });
}


/**
 *
 *  Get a particular identity by the identity number
 *
 *  @param    id      Number       Identity number you want to get from the server
 *
 *  @return           Object       Identity
 *
 */
function getIdentity(id) {
  return new Promise((resolve, reject) => {
    var token = localStorage.getItem("token");
    var url   = apiUrl + "/klon/getIdentity";
    var data  = {"token" : token, "id" : id};
    var payload = formatPayload(data).then( payload => {
      var strPayload = JSON.stringify(payload);
      var results = ajx.req("POST", url, strPayload, "json");
      results.then(function(result) {
        console.log("This is the response: ", result);
        resolve(result);
      })
      .catch(err => {
        console.log("Error: ", err);
        reject(err);
      });
    });
  });
}

/**
 *
 *  This pulls the identities out of the localStorage.identities holder and turns them into a JSON object
 *
 *
 *
 */
async function getStorageData() {
  var data    = localStorage.getItem('identities');
   // Check to make sure there are identities stored for the user.
  if (!data) {
    // Return null, this means there's no identities found for the user
    return null;
  }
  var rawKey  = localStorage.getItem('key');
  try {
    // console.log("Data: ", data);
    // console.log("Raw Key: ", rawKey);
    var key   = await splitHexValue(rawKey, 8);
     // Assemble our lovely JSON object below...
     // Determine if the data is base64 encoded or not
    if (base64.isEncoded(data) === true) {
      // console.log("Base64 Encoded");
      // Base64 decode data
      data = window.atob(data);
    }
    // Get the index of the pipe (this is the end of the column names)
    var pipe = data.indexOf("|");
    // console.log("Pipe: ", pipe);
    // Split the string at the pipe index
    var columns = data.substr(0, pipe);
    // Split the string into column names in an array
    var columnNames = columns.split(",");
    // Get the identities (from the first pipe to the end of the string)
    var identities = data.substr(++pipe);
    identities = identities.split("|");
    var jsonId = "[";
    for (var i = 0; i < identities.length; i++) {
      var breakItUp = identities[i].split(",");
      for (var n = 0; n < breakItUp.length; n++) {
        if (n == 0) {
          jsonId = jsonId + "{" + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"' + ",";
        } else if (n == (breakItUp.length - 1)) {
          jsonId = jsonId + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"},';
        } else {
          jsonId = jsonId + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"' + ",";
        }
      }
      var prev = i;
    }
    // Remove the last comma
    jsonId = jsonId.substr(0,(jsonId.length - 1));
    // Add the closing bracket for the JSON object
    jsonId = jsonId + "]";
    // console.log("JSON BB: ", jsonId);
    jsonId = JSON.parse(jsonId);
    //
    chrome.storage.local.set({identities: jsonId});
    // Remove the old version of this thing
    localStorage.removeItem('identities');
    return true;
  } catch (err) {
    console.log('getStorageData Error: ', err);
    return false;
  }
}

// chrome.storage.local.set({identities: value}, () => {return value});


/**
 *
 *  Get all of the URL's stored in the encrypted data so that we know what sites
 *  to look out for when running the background.js script
 *
 *  Sets the urls in localStorage.urls
 *
 *  Example: var o = localStorage.getItem('urls');
 *               o = o.split(",");
 *           Now...
 *               o[0] = "http://example.com"
 *               o[1] = "http://twitter.com"
 *               o[2] = "http://google.com"
 *               etc...
 *
 *
 */
function getAllURLs() {
  return new Promise((resolve, reject) => {
    var urls = new Array();
    // Get the identities from the chrome local storage
    var id = chrome.storage.local.get(['identities'], result => {
      if (chrome.runtime.lastError) {
        console.log("Error: ", chrome.runtime.lastError);
        reject(chrome.runtime.lastError);
      }
      console.log("Result: ", result);
      if (typeof result != "object") {
        result = JSON.parse(result.identities);
      }
      result = result["identities"];
      // console.log("Result: ", result);
      if (result != undefined && result.length > 0 && result.length != undefined) {
        // Convert hex key into Int32Array
        var key = splitHexValue(localStorage.getItem('key'), 8).then(val => {
          result.forEach( (item,index) => {
            // Make sure the item isn't already in the list. -- That would be stupid to add twice
            if (urls.indexOf(result[index].url) == -1) {
              // Decrypt the URL's and push them onto the urls array
              urls.push(decryptIt(val, result[index].url));
            }
          });
          // Set the urls item inside the localStorage space
          localStorage.setItem('urls', urls);
          // console.log("URLS have been set");
          resolve(urls);
        })
        .catch(err => {
          console.log("Error: " + err);
          reject(err);
        });
      } else {
        // URLs are not presently in chrome.storage.local attempt to get them before giving up
        console.log("NEED TO GET IDENTITIES");
        var getId = getIdentities();
        getId.then(ids => {
          // Good we got the identities from the remote server now get them out of the local data
          var makeObject = getStorageData();
          if (makeObject === true) {
            console.log("DO THE THING");
            return getAllURLs();
          } else if (makeObject === null) {
            console.log("No identities found");
            resolve("No identities found");
          } else {
            reject("Error: I did what I could");
          }
        }).catch(err => {
          reject("Error: ", err);
        });
        // console.log("Error: identities not found");
        // The user has no identities...
        localStorage.setItem("urls", 0);
        resolve("identities not found");
      }
    });
  });
}


/**
 *
 *  Count the number of times a URL appears in the localStorage('urls') setting
 *
 *  @param     URL    String | Object | Array      The URL to check for
 *
 *  @return           Integer                      Number of matches
 *
 */
async function countURLs(url) {
  var urls;
  // Check to see if urls is set.
  if (localStorage.getItem("urls") !== null) {
    urls  = localStorage.getItem("urls");
  } else {
    urls = await getAllURLs();
  }
  // Setup the regular expression with case insensitivity and global flags
  var regEx = new RegExp(url, "ig");
  // Check for matches within the urls string
  var matches = urls.match(regEx);
  //
  if (matches) {
    // Spit the answer out
    return matches.length;
  } else {
    return 0;
  }
  return null;
}


/**
 *
 *  Build a list of identities for the user to see on the screen.
 *
 */
function createIdentityListItem() {
  return new Promise(function(resolve, reject) {
    // Get the identities out of chrome.storage.local
    var id = chrome.storage.local.get(['identities'], function(result) {
      var sandbox = document.querySelector('.idList');
      // console.log("Sandbox: ", sandbox);
      var key = localStorage.getItem('key');
      // console.log("Key: ", key);
      if (key != undefined && key != null && key != "") {
        key = splitHexValue(key, 8);
        key.then(arrKey => {
          // console.log("Key Arr: ", arrKey);
          // console.log("Id length: ", id);
          if (typeof result != "object") {
            result = JSON.parse(result.identities);
          }
          // If the result has no identities...
          if (!result.identities) {
            //
            sandbox.innerHTML = '<li class="idList__item idList__item--none"><div class="idList__details"><div class="idList__username"></div></div></a></li>';
            resolve("Complete");
            return "Complete";
          }
          result = result.identities;
          console.log("$$$ RESULT: ", result);
          // console.log("### RESULT LENGTH: ", result.length);
          // console.log("### RESULT TYPE: ", typeof result);
          for (var i = 0; i < result.length; i++) {
            var email;
            if (result[i].assignedEmail !== undefined) {
              email   = decryptIt(arrKey, result[i].assignedEmail)
            } else if (result[i].email !== undefined) {
              email   = decryptIt(arrKey, result[i].email);
            } else {
              email   = null;
            }
            var url     = decryptIt(arrKey, result[i].url);
            var slashes = url.indexOf("//");
            // If the slashes are found, then slice the string
            if (slashes !== -1) {
              url = url.slice(slashes + 2);
            }
            // console.log("Email: ", email);
            sandbox.innerHTML += '<li class="idList__item idList__item--hide"><a class="idList__identityWrapper" href="identity-details.html?id=' + i + '"><div class="idList__imageWrapper"><img class="idList__image" height="48px" width="48px" src="images/user-1.svg"></div><div class="idList__details"><div class="idList__title">' + url + '</div><div class="idList__username">' + email + '</div></div></a><div class="idList__itemOptions"></div></li>';
          }
        });
        resolve("Complete");
      } else {
        console.log("invalid key");
        reject(false);
      }
    });
  });
}


/**
 *
 *  Get the evt.parentNode get text or innerHTML copy to clipboard
 *
 */
function bindIdListItemAfter(evt) {
  var textToCopy = evt.target.parentNode.querySelector(".idList__username").innerText;
  // Put the text into a textarea to be copied
  var textarea = document.createElement("textarea");
  textarea.style.height     = "2em";
  textarea.style.width      = "2em";
  textarea.style.background = "transparent";
  textarea.style.border     = "none";
  textarea.style.boxShadow  = "none";
  textarea.style.outline    = "none";
  textarea.style.position   = "fixed";
  textarea.style.bottom     = "-4em";
  textarea.style.left       = "-4em";
  // Set the text in the textarea
  textarea.value = textToCopy;
  // Add the textarea to the body of the document
  document.body.appendChild(textarea);
  // Focus on the textarea
  textarea.focus();
  // Select the textarea text
  textarea.select();
  // Copy the text
  try {
      var successful = document.execCommand("copy");
      var msg = successful ? "successful" : "unsuccessful";
      errorHandler.setError("","The email address was " + msg + " copied", false);
      errorHandler.showError();
    } catch (err) {
      errorHandler.setError("Error", err.message);
      errorHandler.showError();
    }
  // Remove the textarea from the page
  document.body.removeChild(textarea);
}


function showListItems() {
  return new Promise(function(resolve, reject) {
    var loadingText = document.querySelector("#idLoading");
    if (loadingText === null) {
      reject("Missing #idLoading in DOM");
    }
    var listItems   = document.querySelector('.idList');
    if (listItems === null) {
      reject("Missing .idList in DOM");
    }
    var n = 20;
    var c = 0;
    var itemArray = new Array();
    for (var i = 0; i < listItems.children.length; i++) {
      var item = listItems.children[i];
      var itemAfter = item.querySelector(".idList__itemOptions");
      if (itemAfter === null) {
        reject("Missing .idList__itemOptions from DOM");
      }
      itemAfter.addEventListener("click", bindIdListItemAfter);
      if (item.classList && item.classList.contains('idList__item--hide')) {
        // console.log('Item: ', item);
        itemArray[c] = item;
        c++;
      }
    }
    // console.log('Item Array: ', itemArray);
    itemArray.forEach(function(item, index) {
      n = n + 25;
      setTimeout( () => {
        if (item.classList.contains('idList__item--hide')) {
          item.classList.remove('idList__item--hide');
        } else {
          // console.log('item: ', item);
          // console.log('error');
        }
      }, n, item);
    });
    // Hide the "loading" text
    loadingText.style.display = "none";
    resolve("Items Shown");
  });
}

/**
 *
 *  The identity object holds all the functions and properties related to identities.
 *  Eventually all the functions contained here will be moved inside this object for organization pruposes and ease-of-use.
 *
 */
var identities = {

  /**
   *
   *  Get all of the identities in chrome.storage.local
   *
   *  @param      id               Integer       This is the id number of the identity to be replaced
   *  @param      replacement      Object        The object containing the new replacement identity
   *
   */
  replaceIdentity : function(id = false, replacement = false) {
    if (id !== false && replacement !== false) {
      if (typeof id === "number" && !isNaN(id)) {
        if (typeof replacement === "object") {
          return new Promise(function(resolve, reject) {
            // Get the identities from chromeStorage
            var getStorage       = chrome.storage.local.get('identities', function(result) {
              // Get the identities
              let ids            = result.identities;
              console.log("id: ", id);
              console.log("replacement: ", replacement);
              console.log("ids: ", ids);
              // Get the total length of these identities
              let identitiesLen  = ids.length;
              console.log("idsLen: ", identitiesLen);
              // Get the index of the array that is storing our identity that needs replaced
              var targetIdentity = ids.findIndex(x => x.id == id);
              console.log("targetIdentity: ", targetIdentity);
              // Make sure a value was found
              if (targetIdentity !== -1) {
                ids[targetIdentity].assignedEmail = replacement.assignedEmail;
                ids[targetIdentity].firstName     = replacement.firstName;
                ids[targetIdentity].lastName      = replacement.lastName;
                ids[targetIdentity].middleInitial = replacement.middleInitial;
                ids[targetIdentity].city          = replacement.city;
                ids[targetIdentity].dobDay        = replacement.dobDay;
                ids[targetIdentity].dobMonth      = replacement.dobMonth;
                ids[targetIdentity].dobYear       = replacement.dobYear;
                ids[targetIdentity].gender        = replacement.gender;
                ids[targetIdentity].id            = replacement.id;
                ids[targetIdentity].password      = replacement.password;
                ids[targetIdentity].salt          = replacement.salt;
                ids[targetIdentity].state         = replacement.state;
                ids[targetIdentity].streetAddress = replacement.streetAddress;
                ids[targetIdentity].url           = replacement.url;
                ids[targetIdentity].username      = replacement.username;
                ids[targetIdentity].zip           = replacement.zip;
                // Replace the contents of that targetArray with the new identity
                // Where a0 is all the identities before this one. and a1 is all the identities after this one
                chrome.storage.local.set({identities : ids}, function() {console.log("All set.");});
                resolve(true);
              } else {
                reject(false);
              }
            });
          });
        } else {
          errorHandler.setError("Error", "incorrect type for argument 'replacement'");
          errorHandler.showError();
          return false;
        }
      } else {
        errorHandler.setError("Error", "incorrect type for argument 'id'");
        errorHandler.showError();
        return false;
      }
    } else {
      errorHandler.setError("Error", "missing arguments");
      errorHandler.showError();
      return false;
    }
  },


  /**
   *
   *  Get the users hash
   *
   */
  getHash : async function() {
    // Get the users token
    let token = localStorage.getItem('token');
    // If a token was found do some stuff
    if (token !== null) {
      // Declare the data we want to send to the server
      var data       = {"token" : token};
      // Format the payload for the server
      try {
        data  = await formatPayload(data);
      } catch (e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        return false;
      }
      // Declare the URL to send the payload to
      var url       = apiUrl + "/klon/getHash";
      // Send the request to the server and await response
      try {
        var request = await ajx.req('POST', url, data, 'json');
        console.log("request: ", request);
        console.log("request hash: ", request['message']);
        // Return the hash
        return request['message'];
      } catch (e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        return false;
      }
    } else {
      return false;
    }
  },


  /**
   *
   *  Append the identity to the end of the list of objects within chrome.storage.local
   *
   *  @return       Boolean      True|False  -- Depends on if it was successful or not
   *
   */
  appendIdentity : function(identity) {
    console.log("IDENTITY PASSED IN: ", identity);
    try {
      chrome.storage.local.get(["identities"], function(old) {
        // Push the new identity onto the array
        console.log("BEFORE PUSH: ", old);
        console.log("OLD.IDENTITIES: ", old.identities);
        let ids = old.identities;
        if (ids === null) {
          ids = identity;
        } else {
          ids.push(identity);
        }
        console.log("AFTER PUSH: ", ids);
        // Set the new value
        try {
          console.log("WITHIN TRY: ", ids);
          chrome.storage.local.set({"identities" : ids}, function() {
            return true;
          });
        } catch (e) {
          console.log("Klon Error: ", e);
          errorHandler.setError("Error", e);
          errorHandler.showError();
          return false;
        }
      });
    } catch(e) {
      console.log("Klon Error: ", e);
      errorHandler.setError("Error", e);
      errorHandler.showError();
      return false;
    }
  },


  /**
   *
   *  Hash the last identity
   *
   *  @return                  String        SHA1 hash of the last identity
   *
   */
  hashLastIdentity : function() {
    return new Promise(function(resolve, reject) {
      let ids;
      // Get the identities from chrome storage
      chrome.storage.local.get(['identities'], function(result) {
        console.log("Hash Last Identity Result: ", result);
        try {
          if (Object.entries(result).length === 0 && result.constructor === Object) {
            // SHA1 hash of klon
            resolve("977ea8930bfd5a1b48136681a8e0391966ef2150");
          }
          // Get the users identities
          ids = result.identities;
          if (ids === null || ids === undefined) {
            resolve("977ea8930bfd5a1b48136681a8e0391966ef2150");
          }
          console.log("ids: ", ids);
          //
          if (ids !== undefined) {
            // Get the last identity
            ids = ids[ids.length - 1];
            // Get the email address and streetAddress
            let email  = ids.email || ids.assignedEmail;
            let street = ids.streetAddress;
            // Get the key
            key = decrypt.getKey();
            key.then(keyResult => {
              // Decrypt them both
              email  = decryptIt(keyResult, email);
              street = decryptIt(keyResult, street);
              console.log("Email: ", email);
              console.log("Street: ", street);
              // Hash the data
              var bitArray = sjcl.hash.sha1.hash(email + street);
              // Convert the bitArray into hex
              var idHash   = sjcl.codec.hex.fromBits(bitArray);
              console.log("HASH: ", idHash);
              // Return the hash
              resolve(idHash);
            }).catch(err => {
              console.log("hashLastIdentity Error: ", err);
              errorHandler.setError("Error", err);
              errorHandler.showError();
              reject(err);
            });
          } else {
            // SHA1 hash of klon
            resolve("977ea8930bfd5a1b48136681a8e0391966ef2150");
          }
        } catch(e) {
          errorHandler.setError("Error", e.message);
          errorHandler.showError();
          reject(e);
        }
      });
    });
  },


  /**
   *
   *  Hash the given identity
   *
   *  @param     identity     String        Decrypted Email + Street Address of identity
   *
   *  @return                 String        SHA1 hash of the email address + street address
   *
   */
  hashIdentity : function(identity = null) {
    return new Promise(function(resolve, reject) {
      try {
        if (identity === null) {
          // SHA1 hash of klon
          resolve("977ea8930bfd5a1b48136681a8e0391966ef2150");
        }
        if (typeof identity != "string") {
          try {
            identity = JSON.stringify(identity);
          } catch(e) {
            errorHandler.setError("Error", e);
            errorHandler.showError();
            reject(false);
          }
        }
        // Hash the data
        var bitArray = sjcl.hash.sha1.hash(identity);
        // Convert the bitArray into hex
        var idHash   = sjcl.codec.hex.fromBits(bitArray);
        // console.log("HASH: ", idHash);
        // Return the hash
        resolve(idHash);
      } catch(e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        reject(e);
      }
    });
  },


  /**
   *
   *  Update the identity hash on the remote server
   *
   *  @return     Boolean     true|false  --  If updated, return true else return false
   *
   */
  updateHash : async function(hash) {
    try {
      // Make sure this is a legit SHA1 hash
      if (hash.match(/^[A-Fa-f0-9]{40}$/) !== null) {
        // Get the users token
        let token = localStorage.getItem("token");
        if (token !== null) {
          // Format the payload and send to the server
          let url     = apiUrl + "/klon/updateHash";
          let str     = {"token" : token, "hash" : hash};
          let payload = await formatPayload(str);
          if (typeof payload !== "object") {
            var strPayload = JSON.stringify(payload);
          } else {
            strPayload = payload;
          }
          // Set the new hash locally
          localStorage.setItem("hash", hash);
          // Send the request to the server
          var results = await ajx.req("PUT", url, strPayload, "json");
          console.log("UPDATE HASH RESULT: ", results);
          // results Example: {status: "success", message: "updated hash"}
          if (results.status === "success") {
            return true;
          } else {
            return false;
          }
        } else {
          // Force the user to login
          credential.forceLogin();
          return false;
        }
      } else {
        errorHandler.setError("Error", "Invalid hash, cannot update value");
        errorHandler.showError();
        return false;
      }
    } catch(e) {
      errorHandler.setError("Error", e.message);
      errorHandler.showError();
      return false;
    }
  },


  /**
  *
  *  Return all of the identitites for a particular URL
  *
  */
  fetchIdentitiesForURL : function(url, key = null) {
    return new Promise(function(resolve, reject) {
      // Check to see if the key was sent as a parameter
      if (key === null || key === undefined) {
        // Get the user's private key
        var prvKey = localStorage.getItem("key");
        // Make sure the key exists
        if (prvKey != null && prvKey != undefined) {
          try {
            // Split the key into a usable typed array
            arrKey = splitHexValue(prvKey, 8);
            arrKey.then(key => {
              key = key;
            })
            .catch(err => {
              console.log("Error: ", err);
            });
          } catch (e) {
            // console.log("key value could not be split");
            console.log("Error: ", e.message);
            reject(false);
          }
        } else {
          console.log("Private key could not be found.");
          reject(false);
        }
      }
      // The key was successfully split into a typed array...
      // Check if we have the identities stored
      chrome.storage.local.get("identities", result => {
        try {
          // Get the array of identities
          result = result["identities"];
          console.log("RESULT OF FUNCTION = ", result);
          if (result === null || result === undefined) {
            reject(null);
          } else {
            var relevantIdentities = [];
            // Loop through all the URL's of each identity to get relevant identities
            result.forEach((item, index) => {
              // Decrypt the URL's
              var decryptedUrl = decryptIt(key, result[index].url);
              if (decryptedUrl === url) {
                relevantIdentities.push(result[index]);
              }
            });
          }
          // console.log("relevant identities: ", relevantIdentities);
          resolve(relevantIdentities);
        } catch(e) {
          console.log("Error: ", e);
          reject(e);
        }
      });
    });
  },


  /**
   *
   *  Decrypt Identity
   *
   *  @param    id           Object     The identity object to be decrypted. Can decrypt 1 or more identities.
   *                         Example:   {identities : [{firstName: "Bob", lastName : "Smith"}, {firstName : "John", lastName : "Doe"} etc... ]}
   *  @param    key          Array      *Optional*  Key to decrypt the data
   *
   *
   *  @return                Object     Returns a decrypted identity for use in forms
   *
   */
  decryptIdentity : async function(id, key = null) {
    // Check if the users key was included as a parameter and that it is the correct type
    if (key === null || key === undefined || Object.prototype.toString.call(key) != "[object Int32Array]") {
      console.log("Key not included");
      // Get the users private key
      try {
        var key = await decrypt.getKey();
      } catch(e) {
        console.log("Error: ", e);
        return false;
      }
    }
    // Verify the object is setup properly
    if (typeof id === "object") {
      // Check to see that the object contains the property "identities"
      if (id.hasOwnProperty("identities") === true) {
        // Check if id.identities is an object
        if (typeof id.identities === "object") {
          //
          // All the requirements are met let's loop through the array and decrypt the values
          try {
            for (let i = 0; i < id.identities.length; i++) {
              //
              Object.keys(id.identities[i]).forEach(function(index) {
                //
                if (index != "id" && index != "salt") {
                  // console.log("Encrypted Value: ", id.identities[i][index]);
                  id.identities[i][index] = decryptIt(key, id.identities[i][index]);
                  // console.log("Decrypted Value: ", id.identities[i][index]);
                }
                //
              });
            }
            return id.identities;
          } catch(e) {
            console.log("Error: ", e);
          }
        } else {
          console.log("Invalid type");
          return false;
        }
      } else {
        console.log("Does not contain identity property");
        return false;
      }
    } else {
      console.log("Invalid parameter type");
      return false;
    }
  }


    // var ids;
    // // Get the user's private key
    // var prvKey = await decrypt.getKey();
    // // We have the user's key
    // // console.log("Private Key: ", prvKey);
    // // Make sure that id is an object with the property "identities" and that this "identities" is of type "array"
    // if (typeof id === "object") {
    //   // Check that id has the property of "identities"
    //   if (id.hasOwnProperty("identities") === true) {
    //     // Check that id.identities is an array as it should be
    //     if (typeof id.identities === "object" || typeof id.identities === "array") {
    //       //
    //       console.log("ID IDENTITIES: ", id.identities);
    //       // All the requirements are met let's loop through the array and decrypt the values
    //       try {
    //         for (let i = 0; i < (id.identities.length - 1); i++) {
    //           console.log("ID IDENTITIES LENGTH: ", id.identities.length);
    //           console.log("CURRENT I: ", i);
    //           //
    //           console.log("Object Keys: ", Object.keys(id.identities[i]));
    //           Object.keys(id.identities[i]).forEach(function(index) {
    //             //
    //             if (index != "id" && index != "salt") {
    //               // console.log("Encrypted Value: ", id.identities[i][index]);
    //               id.identities[i][index] = decryptIt(prvKey, id.identities[i][index]);
    //               // console.log("Decrypted Value: ", id.identities[i][index]);
    //             }
    //             //
    //           });
    //           //
    //           console.log("ID IDENTITY: ", id.identities[i]);
    //           console.log("Iteration i: ", i);
    //         }
    //       } catch(e) {
    //         console.log("Error: ", e);
    //       }
    //       // debugger;
    //       console.log("ID IDENTITIES: ", id.identities);
    //       ids = id.identities;
    //       return ids;
    //     } else {
    //       // console.log("Typeof id.identities: ", typeof id.identities);
    //       // console.log("id: ", JSON.stringify(id));
    //       // console.log("id: ", id);
    //       // console.log("id.identities: ", JSON.stringify(id.identities));
    //       // console.log("id.identities: ", id.identities);
    //       return "object must contain an array of identities";
    //     }
    //   } else {
    //     return "parameter missing property identities";
    //   }
    // } else {
    //   return "incorrect parameter type";
    // }
    // Loop through the identities and decrypt them
    // for (let i = 0; i < id.identities.length; i++) {
    //
    // }


};



// Get the identities from the chrome local storage

// var id = chrome.storage.local.get(['identities'], result => {
//   console.log("Result: ", result);
//   if (typeof result != "object") {
//     result = JSON.parse(result.identities);
//   }
//   result = result["identities"];
//   // console.log("Result: ", result);
//   if (result != undefined && result.length > 0 && result.length != undefined) {
//     // Convert hex key into Int32Array
//     var key = splitHexValue(localStorage.getItem('key'), 8).then(val => {
//       result.forEach( (item,index) => {
//         // Make sure the item isn't already in the list. -- That would be stupid to add twice
//         if (urls.indexOf(result[index].url) == -1) {
//           // Decrypt the URL's and push them onto the urls array
//           urls.push(decryptIt(val, result[index].url));
//         }
//       });
//       // Set the urls item inside the localStorage space
//       localStorage.setItem('urls', urls);
//       // console.log("URLS have been set");
//       resolve(urls);
//     })
//     .catch(err => {
//       console.log("Error: " + err);
//       reject(err);
//     });
//   } else {
//     // URLs are not presently in chrome.storage.local attempt to get them before giving up
//     var getId = getIdentities();
//     getId.then( ids => {
//       // Good we got the identities from the remote server now get them out of the local data
//       var makeObject = getStorageData();
//       if (makeObject === true) {
//         console.log("DO THE THING");
//         return getAllURLs();
//       } else {
//         reject("Error: I did what I could");
//       }
//     }).catch(err => {
//       reject("Error: ", err);
//     });
//     console.log("Error: identities not found");
//     reject("identities not found");
//   }
// });


/**
 *
 *  Fetches the previously generated identity from the localStorage then displays it on the page
 *
 *
 */
function displayGeneratedIdentity() {
  var idWrapper = document.querySelector("#idWrapper");
  var identity  = localStorage.getItem("generatedId");
  if (identity) {
    identity = JSON.parse(identity);
    // Clear any previous results
    idWrapper.innerHTML  = "";
    // Convert date to words not just numbers, less confusing for the user
    const date = new Date(identity.year, Number(identity.month - 1), identity.day);
    const month = date.toLocaleString('en-us', { month: 'long' });
    const day = date.toLocaleString('en-us', { day: '2-digit' });
    // If the gender returned is "M" then gender = male otherwise gender = female
    var gender = identity.gender == "M" ? gender = "Male" : gender = "Female";
    // Display the new results...
    idWrapper.innerHTML   = "<div class='id__userRow' data-field='Full name'><div class='id__key'>Name:</div><div class='id__value'> " + identity.firstName + " " + identity.middleInitial + ". " + identity.lastName + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Gender'><div class='id__key'>Gender: </div><div class='id__value'> " + gender + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='DOB'><div class='id__key'>Birthday:</div><div class='id__value'> " + month + " " + day + ", " + identity.year + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Address'><div class='id__key'>Address:</div><div class='id__value'> " + identity.streetAddress + "<br>" + identity.city + ", " + identity.state + " " + identity.zip + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Email address'><div class='id__key'>Email: </div><div class='id__value'> " + identity.email + "</div></div>";
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Username'><div class='id__key'>Username: </div><div class='id__value'> " + identity.username + "</div></div>";
    // Generate password locally on the fly
    idWrapper.innerHTML  += "<div class='id__userRow' data-field='Password'><div class='id__key'>Password: </div><div class='id__value'> " + identity.password + "</div></div>";
    copyPasta();
  }
}


function paintForm(identity) {
  return new Promise(function(resolve, reject) {
    chrome.tabs.query({active: true, currentWindow: true}, function(tab) {
      chrome.tabs.sendMessage(tab[0].id, {"msg":"prepareFillForm", "identity" : identity}, function(fillFormResponse) {
        window.close();
        // console.log("THIS IS WHERE THE WINDOW WouLD CLOSE");
        console.log("Error: ", chrome.runtime.lastError);
        console.log("Response: ", fillFormResponse);
        resolve(true);
      });
    });
  });
}


async function fillForm() {
  // Get the url of the current tab
  var getUrl = await settings.getCurrentURL();
  // Get the generated identity
  var identity = JSON.parse(localStorage.getItem("generatedId"));
  // Add the URL property to the object
  identity.url = string.distillUrl(getUrl);
  try {
    // Append the identity to our identity object
    var saveIdentity = await useIdentity(identity);
    console.log("------------------------");
    console.log("identity: ", identity);
    console.log("save identity: ", saveIdentity);
    console.log("------------------------");
    // Hash the current and last identity to compare for similarities
    var hashIdentity     = await identities.hashIdentity(identity.email + identity.streetAddress);
    var hashLastIdentity = await identities.hashLastIdentity();
    // If it is different then append the new identity to the storage object else, just fill the form
    // console.log("Hash identity: ", hashIdentity);
    // console.log("Hash last identity: ", hashLastIdentity);
    if (hashIdentity === hashLastIdentity) {
      // The hashes are the same, just fill the form
      var paintIt = await paintForm(identity);
    } else {
      // Confirm the user wants to use this identity for this site
      let useId = confirm("Are you sure you want to use this identity?");
      if (useId) {
        // console.log("IDENTITY TO SEND: ", saveIdentity);
        // Let the remote server know about this identity
        var payload      = await formatPayload(saveIdentity);
        var url          = apiUrl + "/klon/useCandidateForURL";
        var request      = await ajx.req("POST", url, payload);
        // Get the response from the server
        if (request.status === "success") {
          // Get the identity from the remote server and append this to the thing
          var fetchIdentity = modify.getIdentityByProperties(saveIdentity.firstName, saveIdentity.lastName, saveIdentity.email, saveIdentity.streetAddress);
          fetchIdentity.then(result => {
            console.log("fetchIdentity: ", result);
            // Append the new identity to the end of the lot at chrome.storage.local
            var appendId = identities.appendIdentity(result);
          }).catch(err => {
            errorHandler.setError("Error", err);
            errorHandler.showError();
            return false;
          });
          // Clear the generated identity
          let cleared = silentClearIdentity();
          localStorage.removeItem('hash');
          // Get the new identity
          let getIds = await getIdentities();
          // Update the hash of the identities and send to server
          var update = await identities.updateHash(hashIdentity);
          // Fill the form
          var paintIt = await paintForm(identity);
        } else {
          console.log("Error! Request status: ", request.status + " Request message: ", request.message);
          return false;
        }
        // Fill the form
      } else {
        return false;
      }
    }
  } catch(e) {
    // Display any errors
    console.log("Error: ", e);
    errorHandler.setError("Error", e);
    errorHandler.showError();
    return false;
  }
}


/**
 *
 *  Clears the identity that the user manually generated
 *
 */
function clearIdentity() {
  var result = confirm("Do you want to clear this identity from the screen?");
  if (result) {
    var identity = localStorage.getItem("generatedId");
    if (identity) {
      var idWrapper = document.querySelector("#idWrapper");
      try {
        localStorage.removeItem("generatedId");
        idWrapper.innerHTML = "";
        // Remove the buttons
        showClearButton();
        // Remove the fill button
        showFillButton();
        // Toggle the showUserGen button
        toggleShowUserGen();
        // Close the user gen menu
        closeUserGen();
        return true;
      } catch(e) {
        idWrapper.innerHTML = "Error: ", e.message;
        return false;
      }
    }
  } else {
    return false;
  }
}


/**
 *
 *  Clear the manually generated identity without asking for permission first
 *
 */
function silentClearIdentity() {
  var identity = localStorage.getItem("generatedId");
  if (identity) {
    var idWrapper = document.querySelector("#idWrapper");
    try {
      localStorage.removeItem("generatedId");
      idWrapper.innerHTML = "";
      // Remove the buttons
      showClearButton();
      // Remove the fill button
      showFillButton();
      // Toggle the showUserGen button
      toggleShowUserGen();
      // Close the user gen menu
      closeUserGen();
      return true;
    } catch(e) {
      idWrapper.innerHTML = "Error: ", e.message;
      return false;
    }
  }
}

/**
 *
 *
 *
 */
function copyDetails(e) {
  // Get the value of the row clicked
  // If statement clears up an issue when the user clicks the :after pseudo element
  if (e.target.classList.contains("id__userRow")) {
    var parent = e.target;
  } else {
    var parent = e.target.parentNode;
  }
  var value  = parent.querySelector(".id__value");
  var field  = parent.dataset.field;
  value      = value.innerText;
  // Copy the value to the clipboard
  var textArea = document.createElement("textarea");
  textArea.style.background = "transparent";
  textArea.style.border     = "none";
  textArea.style.boxShadow  = "none";
  textArea.style.height     = "2em";
  textArea.style.outline    = "none";
  textArea.style.position   = "fixed";
  textArea.style.bottom     = "-4em";
  textArea.style.left       = "-4em";
  textArea.style.width      = "2em";
  textArea.value = value;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();
  try {
    document.execCommand("copy");
    errorHandler.setError(field, " successfully copied!", false);
    errorHandler.showError(3500);
  } catch(e) {
    console.log("Value is: ", value);
    errorHandler.setError("Error", e.message);
    errorHandler.showError(3500);
  }
  document.body.removeChild(textArea);
  return null;
}


/**
 *
 *  Make all of the rows copy-able
 *
 */
function copyPasta() {
  var rows = document.querySelectorAll(".id__userRow");
  for (let i = 0; i < rows.length; i++) {
    rows[i].addEventListener("click", copyDetails, false);
  }
  return true;
}


/**
 *
 *  Check to see if there is a previously generated identity stored in the localStorage
 *  If a previous identity is found it is fetched, parsed, then displayed on the page for the user to see
 *
 */
function checkPreviousGeneratedId() {
  var prevId = localStorage.getItem("generatedId");
  prevId = JSON.parse(prevId);
  if (prevId) {
    displayGeneratedIdentity();
  }
  return true;
}


/**
 *
 *  Generates an identity for the user at their request
 *
 */
function genManualIdentity() {
  var currentURL = settings.getCurrentURL();
  currentURL.then(url => {
    var domain = string.distillUrl(url);
    var isAtQuota = genId.isAtQuota(domain);
    isAtQuota.then(result => {
      try {
        if (result === false) {
          try {
            var genId = generateIdentity();
            var errHandler = document.querySelector("#errorHandler");
            var idWrapper = document.querySelector("#idWrapper");
            errHandler.innerHTML  = "Generating...";
            errHandler.style.opacity = 1;
            errHandler.style.height = "20px";
            genId.then(identity => {
              // Set these values to prevent failures in the future
              identity.assignedEmail = identity.email;
              identity.dobDay        = identity.day;
              identity.dobMonth      = identity.month;
              identity.dobYear       = identity.year;
              // Clear any previous results
              idWrapper.innerHTML  = "";
              // Convert DOB date to words not just numbers, less confusing for the user
              // Ex: February 07, 1988
              const date = new Date(identity.year, Number(identity.month - 1), identity.day);
              const month = date.toLocaleString('en-us', { month: 'long' });
              const day = date.toLocaleString('en-us', { day: '2-digit' });
              // If the gender returned is "M" then gender = male otherwise gender = female
              var gender = identity.gender == "M" ? gender = "Male" : gender = "Female";
              // Display the new results...
              idWrapper.innerHTML  = "<div class='id__userRow' data-field='Full name'><div class='id__key'>Name:</div><div class='id__value'> " + identity.firstName + " " + identity.middleInitial + ". " + identity.lastName + "</div></div>";
              idWrapper.innerHTML  += "<div class='id__userRow' data-field='Gender'><div class='id__key'>Gender: </div><div class='id__value'> " + gender + "</div></div>";
              idWrapper.innerHTML  += "<div class='id__userRow' data-field='DOB'><div class='id__key'>Birthday:</div><div class='id__value'> " + month + " " + day + ", " + identity.year + "</div></div>";
              idWrapper.innerHTML  += "<div class='id__userRow' data-field='Address'><div class='id__key'>Address:</div><div class='id__value'> " + identity.streetAddress + "<br>" + identity.city + ", " + identity.state + " " + identity.zip + "</div></div>";
              idWrapper.innerHTML  += "<div class='id__userRow' data-field='Email Address'><div class='id__key'>Email: </div><div class='id__value'> " + identity.email + "</div></div>";
              // Generate username locally on-the-fly
              usernameGen.firstName = identity.firstName;
              usernameGen.lastName  = identity.lastName;
              var username = usernameGen.genUsername();
              // Generate password locally
              var password = generatePassword(18, true, true, true, true);
              idWrapper.innerHTML  += "<div class='id__userRow' data-field='Username'><div class='id__key'>Username: </div><div class='id__value'> " + username + "</div></div>";
              // Generate password locally on the fly
              idWrapper.innerHTML  += "<div class='id__userRow' data-field='Password'><div class='id__key'>Password: </div><div class='id__value'> " + password + "</div></div>";
              // Reset the errorHandler
              errHandler.style.opacity = 0;
              errHandler.style.height = 0;
              errHandler.innerText = "";
              // Set the generated identity in the localStorage
              var generatedIdentity = identity;
              generatedIdentity.username = username;
              generatedIdentity.password = password;
              localStorage.setItem("generatedId", JSON.stringify(generatedIdentity));
              // Show the fill form button
              showFillButton();
              // Show the clear identity button
              showClearButton();
              // Bind functionality to the close button
              bindBtnClose();
              // Show the user generated menu
              showUserGen();
              // ionno
              copyPasta();
            })
            .catch(err => {
              console.log("Error: ", err);
              errorHandler.setError("Error", err);
              errorHandler.showError();
              return false;
            });
          }
          catch(e) {
            errorHandler.setError("Error", e.message);
            errorHandler.showError();
            return false;
          }
        } else if (result === true) {
          errorHandler.setError("Error","You cannot create any more identities for this website");
          errorHandler.showError();
          return false;
        }
      } catch (e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        return false;
      }
    }).catch(err => {
      errorHandler.setError("Error", err.message);
      errorHandler.showError();
      return false;
    });
  }).catch(err => {
    errorHandler.setError("Error", err.message);
    errorHandler.showError();
    return false;
  });
}


/**
 *
 *  This will determine wether or not to display the clear button
 *
 */
function showClearButton() {
  // Get a handle on the clearButton
  var btnClear = document.querySelector("#btnClearId");
  // If an identity has been prepared for the user, display the clear button
  if (localStorage.generatedId !== undefined && localStorage.generatedId !== null) {
    try {
      // Check if the button is hidden
      if (btnClear.classList.contains("id__button--hidden")) {
        btnClear.classList.remove("id__button--hidden");
        if (btnClear.hasAttribute("disabled")) {
          btnClear.removeAttribute("disabled");
        }
        return true;
      }
    } catch(e) {
      errorHandler.setError("Error", e.message);
      errorHandler.showError(3500);
      console.log("Error: ", e.message);
      return false;
    }
  } else {
      if (!btnClear.classList.contains("id__button--hidden")) {
        btnClear.classList.add("id__button--hidden");
        // Disable the button
        btnClear.setAttribute("disabled", true);
      }
    return false;
  }
}


/**
 *
 *  show or hide the showUserGen button
 *
 */
function toggleShowUserGen() {
  try {
    var btnShowUserGen = document.querySelector("#showUserGen");
    // If the element is found in the DOM
    if (document.body.contains(btnShowUserGen)) {
      // If there is NOT a generatedId stored in the localStorage
      if (localStorage.generatedId === null || localStorage.generatedId === undefined) {
        // If the button doesnt have the hidden class applied then apply it and disable the button
        if (!btnShowUserGen.classList.contains("id__showUserGen--hidden")) {
          // Hide the button
          btnShowUserGen.classList.add("id__showUserGen--hidden");
          return true;
        } else {
          return true;
        }
        btnShowUserGen.classList.add();
        btnShowUserGen.innerText = "Hide generated ID";
        btnShowUserGen.removeEventListener("click", showUserGen);
        btnShowUserGen.addEventListener("click", closeUserGen);
      } else {
        // Determine if the Show generated identity menu is hidden or not
        var showGenUserMenu = document.querySelector("#idWrapper");
        if (showGenUserMenu.classList.contains("id__usergen--show")) {
          btnShowUserGen.innerText = "Hide generated ID";
          btnShowUserGen.removeEventListener("click", showUserGen);
          btnShowUserGen.addEventListener("click", closeUserGen);
          if (btnShowUserGen.classList.contains("id__showUserGen--hidden")) {
            btnShowUserGen.classList.remove("id__showUserGen--hidden");
          }
          return true;
          // If the show user generated button is not hidden then hide it
          // if (!btnShowUserGen.classList.contains("id__showUserGen--hidden")) {
            // Remove the hidden class
            // btnShowUserGen.classList.add("id__showUserGen--hidden");
            // Remove disabled attribute
            // btnShowUserGen.setAttribute("disabled", true);
          // }
        } else {
          btnShowUserGen.innerText = "Show generated ID";
          btnShowUserGen.removeEventListener("click", closeUserGen);
          btnShowUserGen.addEventListener("click", showUserGen);
        }
        return false;
      }
    } else {
      return false;
    }
  } catch(e) {
    // Set and show the error
    errorHandler.setError("Error", e.message);
    erorrHandler.showError(3500);
    return false;
  }
}


/**
 *
 *  Bind the functionality to the #idGenClose button
 *
 */
function bindBtnClose() {
  var btnClose = document.querySelector("#idGenClose");
  if (document.body.contains(btnClose)) {
    btnClose.addEventListener("click", closeUserGen, false);
    return true;
  } else {
    return false;
  }
}


/**
 *
 *  add functionality to the Show Genrated ID menu
 *
 */
function bindShowUserGen() {
  //
  var btnShowMenu = document.querySelector("#showUserGen");
  if (document.body.contains(btnShowMenu)) {
    btnShowMenu.addEventListener("click", showUserGen, false);
  } else {
    return false;
  }
}


/**
 *
 *  Close the userGen menu
 *
 */
function closeUserGen() {
  try {
    var userGenMenu    = document.querySelector("#idWrapper");
    var btnShowUserGen = document.querySelector("#showUserGen");
    if (document.body.contains(userGenMenu) && document.body.contains(btnShowUserGen)) {
      if (userGenMenu.classList.contains("id__usergen--show")) {
        userGenMenu.classList.remove("id__usergen--show");
        // Toggle the showing of the showUserGen button
        toggleShowUserGen();
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  } catch(e) {
    errorHandler.setError("Error", e.message);
    errorHandler.showError(3500);
    return false;
  }
}


/**
 *
 *  Show the userGen menu
 *
 */
function showUserGen() {
  try {
    var userGenMenu = document.querySelector("#idWrapper");
    // If the element exists...
    if (document.body.contains(userGenMenu)) {
      if (!userGenMenu.classList.contains("id__usergen--show")) {
        userGenMenu.classList.add("id__usergen--show");
        // Add the close functionality to the button
        bindBtnClose();
        // Hide the showUserGenMenu button
        toggleShowUserGen();
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  } catch(e) {
    errorHandler.setError("Error", e.message);
    errorHandler.showError(3500);
    return false;
  }
}


/**
 *
 *  This will determine wether or not to display the fill button
 *
 */
function showFillButton() {
  // Get a handle on the clearButton
  var btnFill = document.querySelector("#btnFillForm");
  // If an identity has been prepared for the user, display the clear button
  if (localStorage.generatedId !== undefined && localStorage.generatedId !== null) {
    try {
      // Check if the button is hidden
      if (btnFill.classList.contains("id__button--hidden")) {
        btnFill.classList.remove("id__button--hidden");
        if (btnFill.hasAttribute("disabled")) {
          btnFill.removeAttribute("disabled");
        }
        return true;
      }
    } catch(e) {
      errorHandler.setError("Error", e.message);
      errorHandler.showError(3500);
      console.log("Error: ", e.message);
      return false;
    }
  } else {
    if (!btnFill.classList.contains("id__button--hidden")) {
      btnFill.classList.add("id__button--hidden");
      // Disable the button
      btnFill.setAttribute("disabled", true);
    }
    return false;
  }
}


window.addEventListener("load", async function() {
  if (window.location.pathname === "/identities.html") {
    // Bind the functions to the id generate buttons
    var btnGenId = document.querySelector("#btnGenId");
    btnGenId.addEventListener("click", genManualIdentity, false);
    //
    var btnFillForm = document.querySelector("#btnFillForm");
    btnFillForm.addEventListener("click", fillForm, false);
    //
    var btnClearId = document.querySelector("#btnClearId");
    btnClearId.addEventListener("click", clearIdentity, false);
    // Set the session tab so we can return later after closing the extension
    localStorage.sessionTab = "identities";
    // Clear out all the old stuff -- not necessarily a good idea every time
    // localStorage.removeItem("identities");
    // localStorage.removeItem("hash");
    // console.log("Removed identities and hash from local storage");
    // Do not need to get the identities from the server unless they're expired or something has been added/deleted
      var getIds = await getIdentities().catch((err) => { console.log(err);return false; });;
      // Get the identities
      if (getIds != "No identities found") {
        console.log("getIdentities: ", getIds);
        // Get the data from the chrome storage
        var getStorage  = await getStorageData().catch((err) => { console.log(err);return false; });
        console.log("getStorage: ", getStorage);
        // Hash the last identity
        var lastHash    = await identities.hashLastIdentity().catch((err) => { console.log(err);return false; });
        // Check the current hash
        var currentHash = localStorage.getItem('hash');
        // Compare the current hash to the last hash
        if (currentHash !== lastHash) {
          // Set the new hash in localStorage
          localStorage.setItem('hash', lastHash);
        }
        // Get the hash from the remote server and make sure it matches
        var remoteHash = await identities.getHash().catch((err) => { console.log(err);return false; });
        console.log("R E M O T E     H A S H: ", remoteHash);
        // Compare remote hash to the hash of the last identity
        if (remoteHash !== lastHash) {
          // Update the remote hash
          var updateHash = await identities.updateHash(lastHash).catch((err) => { console.log(err);return false; });
          console.log("Update Hash: ", updateHash);
        }
        //
        var getURLs = await getAllURLs().catch((err) => { console.log(err);return false; });
        //
        var createListItems = await createIdentityListItem().catch((err) => { console.log(err);return false; });
        //
        var showItems = await showListItems().catch((err) => { console.log(err);return false; });
        //
        // Bind the functionality to the nav
        nav.bindNav();
        // Check to see if there was a previously generated identity
        checkPreviousGeneratedId();
        // Decide wether or not to show the genID buttons
        showFillButton();
        showClearButton();
        bindBtnClose();
        bindShowUserGen();
        toggleShowUserGen();
      } else {
        // If no identities are found for the user
        var loading = document.querySelector("#idLoading");
        loading.innerHTML = "No identities found.";
      }
  }
});


/**
 *
 *  This is the object for generating identities eventually all of the functions
 *  will be moved in here
 *
 */
var genId = {

  /**
   *
   *  Has the user met their quota for identities on this url?
   *
   *  @param     url     String     The URL to check against
   *
   *  @return            Boolean    True|False
   *
   */
  isAtQuota : async function(url) {
    try {
      // Get the users role
      let userRole = await role.getRole();
      console.log("User Role: ", userRole);
      // Get the number of identities that this user has already for this url
      let totalIdentities = await countURLs(url);
      console.log("Total identities: ", totalIdentities);
      //
      switch(userRole) {
        // The void
        case 0:
          return true;
          break;
        // Economy plan
        case 1:
          if (totalIdentities >= 1) {
            return true;
          } else {
            return false;
          }
          break;
        // Regular plan
        case 2:
          if (totalIdentities >= 5) {
            return true;
          } else {
            return false;
          }
          break;
        // Premium plan
        case 3:
          if (totalIdentities >= 25) {
            return true;
          } else {
            return false;
          }
        // Trial plan
        case 9:
          if (totalIdentities >= 1) {
            return true;
          } else {
            return false;
          }
          break;
        // Everything else
        default:
          return true;
          break;
      }
      //
    } catch(e) {
      errorHandler.setError("Error", e.message);
      errorHandler.showError();
    }
  }
};
