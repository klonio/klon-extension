/**
 *
 *  This object is responsible for storing and retrieving sessions for the user
 *  Remembers where the user was when they closed the application and will return them
 *  to that same spot when the re-open the application, given a session is stored in localStorage
 *
 */
var session = {

  /**
   *
   *  Store the current session for the user to easily return if they close the application
   *
   *  @param    tab        String        The page we left off on, Example: main.html
   *  @param    focus      String        An element we had in focus, Example: <input name="something" type="text">
   *  @param    fn         Array         *Optional*  Name of the function to call (without the parenthesis () )
   *  @param    param      Array         *Optional*  Parameters for the function we call
   *
   *  @return              Boolean       True | False
   *
   */
  store : function(tab = "identities", focus = null, fn = null, param = null) {
    try {
      var stdObj = {"tab":tab, "elFocus":focus, "fn":fn, "param":param};
      stdObj = JSON.stringify(stdObj);
      localStorage.setItem("session", stdObj);
      return true;
    } catch(err) {
      console.log("Session Error: ", err);
      return false;
    }
  },


  /**
   *
   *  Look for and return a stored session
   *
   */
  read : function() {
    try {
      // Get the session object from the localStorage
      var objSession = localStorage.getItem("session");
      // If the object is found parse it and return the results
      if (objSession) {
        // Parse the object from the string
        objSession = JSON.parse(objSession);
        // Return the object
        return objSession;
      } else {
        // No session object was found
        return false;
      }
    } catch(err) {
      console.log("Error: ", err);
      return false;
    }
  },


  /**
   *
   *  Restore the application back to the last session found based on the object retrieved
   *
   *  @param      sesssion        Object      This is a session object containing details about the users last session
   *
   *  @return                     Boolean     True|False Based on successful operation
   *
   */
  restore : function(session = null) {
    // Determine if we have a session passed in or not...
    if (session && typeof session === "object") {
      // Session object is something we can work with... Check to see if we have the required properties
      if (session.hasOwnProperty("tab") && session.hasOwnProperty("elFocus")) {
        // Get these values and restore the last session
        // Tab is the page the session left off on
        var sessionTab   = session.tab;
        // Focus is the <input> or other element that had focus when the user
        var sessionFocus = session.elFocus;
        // Set the location to the tab
        window.location.href = sessionTab;
        // Set a bit of functions to run this is an array of functions without the ()
        var sessionFn = session.fn;
        //
        var sessionParam = session.param;
        // If the function argument is set and is of type object (an array)
        if (sessionFn && typeof sessionFn === "object") {
          // Loop through each function and apply any parameters
          for (let i = 0; i < sessionFn.length; i++) {
            // If parameters are found continue...
            if (sessionParam) {
              // If length is at least equal to the current iteration of 'i'
              if (sessionParam.length >= i) {
                // If session parameter is not null
                if (sessionParam[i]) {
                  // Execute the function
                  var result = sessionFn(sessionParam[i]);
                  return result;
                }
              }
            }
            // Parameters not set, return the result of the function
            var result = sessionFn();
            return result;
          }
        }
        // If the focus is set, apply it
        if (sessionFocus) {
          // Set the focus to the element
          elFocus.focus();
        }
        //
        return true;
      } else {
        // The session object passed in was null or not an object
        console.log("Session object missing properties");
        return false;
      }
    } else {
      // The session object passed in was null or not an object
      console.log("Invalid session object");
      return false;
    }
  }


};
