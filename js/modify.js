/**
 *
 *  The modify object deals with modifying the identity
 *
 */
var modify = {

  // This is the identity to modify
  identity      : {},
  key           : null,
  title         : null,
  firstName     : null,
  middleInitial : null,
  lastName      : null,
  email         : null,
  username      : null,
  password      : null,
  gender        : null,
  month         : null,
  day           : null,
  year          : null,
  streetAddress : null,
  city          : null,
  state         : null,
  zip           : null,


  /**
   *
   *  Submit the changes for the particular identity
   *
   *  @param     identityObject      Object         The identity object with all the modifications
   *
   *  @return                        Promise        true if successful or false on fail
   *
   */
  submitChanges : function() {
    return new Promise((resolve, reject) => {
      // Get the users encryption key
      var encKey = localStorage.getItem("key");
      encKey     = splitHexValue(encKey, 8);
      encKey.then(secKey => {
        var currId = {
          firstName     : document.querySelector("#firstName").value,
          middleInitial : document.querySelector("#middleInitial").value,
          lastName      : document.querySelector("#lastName").value,
          dobMonth      : document.querySelector("#dobMonth").value,
          dobDay        : document.querySelector("#dobDay").value,
          dobYear       : document.querySelector("#dobYear").value,
          username      : document.querySelector("#username").value,
          password      : document.querySelector("#password").value,
          streetAddress : document.querySelector("#streetAddress").value,
          city          : document.querySelector("#city").value,
          state         : document.querySelector("#state").value,
          zip           : document.querySelector("#zip").value
        }
        // This is the object that stores the modifications to make
        var mods = {};
        // Determine what values if any have changed.
        var objProperties = ["city", "state", "zip", "dobDay", "dobMonth", "dobYear", "firstName", "lastName", "middleInitial", "username", "password", "salt", "streetAddress"];
        // Discover the modifications made
        for (var i = 0; i < objProperties.length; i++) {
          if (modify.rawId[objProperties[i]] && modify.rawId[objProperties[i]] != currId[objProperties[i]]) {
            console.log("Different: ", objProperties[i]);
            mods[objProperties[i]] = currId[objProperties[i]];
          }
        }
        // Mods to send to the server
        // console.log("Length: ", Object.values(mods).length);
        //
        if (Object.values(mods).length > 0) {
          // Read off the modifications back to the users console
          // console.log("Modifications: ", mods);
          // Encrypt the data on a loop
          Object.keys(mods).forEach(function(key,index) {
            // key: the name of the object key
            // index: the ordinal position of the key within the object
            // console.log("Key: ", key);
            // console.log("Value: ", mods[key]);
            mods[key] = encryptDataWithKey(secKey, mods[key]);
            // console.log("Encrypted Value: ", mods[key]);
          });
          //
          var encId = JSON.parse(localStorage.getItem("currentId"));
          var token = localStorage.getItem("token");
          var data  = {"mods" : mods, "email" : encId.assignedEmail, "token" : token};
          console.log("Payload: ", data);
          data = formatPayload(data);
          data.then(payload => {
            console.log("Payload: ", data);
            payload = JSON.stringify(payload);
            // Send out the AJAX request
            var response = ajx.req("PUT", apiUrl + "/klon/modifyIdentity", payload);
            response.then(res => {
              // Upon a successful response update the current identity on the client side and display the success message for the user to see
              console.log("Server Response: ", res);
              // Set error message title
              localStorage.setItem("lastErrorStatus", res.status);
              // Set error message
              localStorage.setItem("lastErrorMessage", res.message);
              // Set expiration time to 30 seconds from now
              localStorage.setItem("lastErrorExpire", time.timestamp() + 30);
              // IF the response from the server is a success then replace the identity with this...
              if (res.status === "success") {
                // Replace the identity with this one...
                var replace = identities.replaceIdentity(JSON.parse(res.data).id, JSON.parse(res.data));
                replace.then(result => {
                  console.log("Replace: ", result);
                }).catch(err => {
                  console.log("Replace Error: ", err);
                })
                resolve(true);
              }
            })
            .catch(err => {
              console.log("Error: ", err);
              return false;
            });
          })
          .catch(err => {
            console.log("Error: ", err);
          });
        }
      })
      .catch(err => {
        console.log("Error: ", err);
      });
      // Print out server response
    });
  },


  /**
   *
   *  Get the users key and put it into Int32Array
   *
   */
  getKey : function() {
    return new Promise((resolve, reject) => {
      // If the key is not set within the details yet then set it
      if (modify.key === null || modify.key === undefined || modify.key === "") {
        // If the key IS set within the localStorage, get it and split it into a useful array
        if (localStorage.getItem("key") != undefined && localStorage.getItem("key") != null && localStorage.getItem("key") != "") {
          var key = splitHexValue(localStorage.getItem("key"), 8);
          return key.then(arrKey => {
            modify.key = arrKey;
            resolve(arrKey);
          })
          .catch(err => {
            console.log("Error: ", err);
            reject(false);
          });
        }
      } else {
        resolve(modify.key);
      }
    });
  },


  /**
   *
   *  Compose the page
   *
   */
  compose : function() {
    var currentId = localStorage.getItem("currentId");
    var rawId = new Object();
    // Turn the string into an object
    if (typeof currentId != "object") {
      currentId = JSON.parse(currentId);
    }
    if (typeof currentId === "object" && Object.values(currentId).length > 0) {
      //
      console.log("Current Identity: ", currentId);
      // Check to see if the key is NOT set
      if (modify.key == null || modify.key == undefined) {
        console.log("Getting Key");
        var getKey = modify.getKey();
        return getKey.then(result => {
          modify.compose();
        });
      } else {
        // var c = modify.getIdentityByProperties(currentId.firstName, currentId.lastName, currentId.email, currentId.streetAddress);
        // console.log("c: ", c);
        // c.then(result => {
        //   console.log("Result: ", result);
        //   var data = JSON.parse(result.data);
        //   localStorage.setItem('currentId', JSON.stringify(data));
        // });
        // Define the properties we want to retrieve from the object
        var objProperties = ["assignedEmail", "city", "state", "zip", "dobDay", "dobMonth", "dobYear", "firstName", "lastName", "middleInitial", "gender", "username", "password", "streetAddress", "url", "salt"];
        if (currentId.hasOwnProperty("assignedEmail") && currentId.hasOwnProperty("city") && currentId.hasOwnProperty("state") && currentId.hasOwnProperty("zip") && currentId.hasOwnProperty("dobDay") && currentId.hasOwnProperty("dobMonth") && currentId.hasOwnProperty("dobYear") && currentId.hasOwnProperty("firstName") && currentId.hasOwnProperty("lastName") && currentId.hasOwnProperty("middleInitial") && currentId.hasOwnProperty("gender") && currentId.hasOwnProperty("username") && currentId.hasOwnProperty("password") && currentId.hasOwnProperty("streetAddress") && currentId.hasOwnProperty("url") && currentId.hasOwnProperty("salt")) {
          console.log("current ID: ", currentId);
          console.log("Obj Properties: ", objProperties);
          console.log("Obj Properties Length: ", objProperties.length);
          var data;
          var field;
          //
          for (var i = 0; i < objProperties.length; i++) {
            //
            data = decryptIt(modify.key, currentId[objProperties[i]]);
            // If the data was successfully decrypted...
            if (data != false) {
              if (objProperties[i] === "gender") {
                // console.log("Gender: ", data);
                // Set the gender options
                if (data == "M") {
                  document.querySelector("#male").checked = true;
                } else {
                  document.querySelector("#female").checked = true;
                }
              }
              if (objProperties[i] == "dobMonth" || objProperties[i] == "dobDay") {
                // If the value is less than 10 prepend a 0 to the value
                if (Number(data) < 10) {
                  data = "0" + data;
                }
              }
              // If we are iterating through and get to the "url" property make sure to set it in the document
              if (objProperties[i] == "url") {
                let title = document.querySelector("#modifyTitle");
                let protocolIndex = data.indexOf("://");
                if (protocolIndex !== -1) {
                  data = data.substring(protocolIndex + 3);
                }
                // Eliminate the www. off of the URL if it is present
                let wwwIndex = data.indexOf("www.");
                if (wwwIndex !== -1) {
                  data = data.substring(wwwIndex + 4);
                }
                title.innerText = data;
              }
              //
              field = String("#" + objProperties[i]);
              console.log("Property: ", objProperties[i]);
              rawId[objProperties[i]] = data;
              // If the field exists...
              if (document.querySelector(field)) {
                // Set the field's value to the value of the decrypted data
                document.querySelector(field).value = data;
              }
            }
          }
          // Set the raw id so that people can compare decrypted identity with values in form
          modify.rawId = rawId;
          console.log("rawId: ", rawId);
        } else {
          console.log("Data is missing from this identity. Fetching...");
          var fetchIdentity = modify.getIdentityByProperties(currentId.firstName, currentId.lastName, currentId.email, currentId.streetAddress);
          fetchIdentity.then(result => {
            console.log("REEEEEEEZULT: ", result);
            console.log("fetcch: ", fetchIdentity);
            localStorage.setItem("currentId", result.data);
            modify.compose();
          });
        }
      }
    } else {
      console.log("details identity was not set");
      return false;
    }
  },


  /**
   *
   *  Get the current identity from the remote database
   *
   *  @param       String       token        The users authentication token
   *  @param       Integer      id           id of the identity
   *
   *  @return      Object                    Returns the encrypted data
   *
   */
  getIdentity : async function(id = false) {
    // Make sure the id was set
    if (id !== false) {
      // Cast id as a number
      id = Number(id);
      // Make sure the id is of type integer and is not NaN
      if (typeof id === "number" && isNaN(id) === false) {
        // Get the users token
        let token = localStorage.getItem("token");
        // Make sure the token was retrieved
        if (token !== null) {
          // Set the url to send the data to
          let url     = apiUrl + "/klon/getIdentity";
          // Put all the data together
          let data    = {"token" : token, "id" : id};
          // Format the payload to send to the server
          var payload = await formatPayload(data);
          // Send the request to the server
          var request = await ajx.req('POST', url, payload, 'json');
          // Return the data
          return request;
        } else {
          // Force the user to login if no token found
          credential.forceLogin();
          return false;
        }
      } else {
        errorHandler.setError("Error", "wrong data type for id");
        errorHandler.showError();
        return false;
      }
    } else {
      errorHandler.setError("Error", "id not set");
      errorHandler.showError();
      return false;
    }
  },


  /**
   *
   *  Get an identity by properties like firstName, lastName, url, assignedEmail etc...
   *
   *  @param       firstName        String          Cipher text of the firstName
   *  @param       lastName         String          Cipher text of the lastName
   *  @param       assignedEmail    String          Cipher text of the assignedEmail
   *  @param       streetAddress    String          Cipher text of the streetAddress
   *
   *  @return      Promise                        Returns the results from the server
   *
   */
  getIdentityByProperties : async function(firstName = false, lastName = false, assignedEmail = false, streetAddress = false) {
    // Make sure the arguments were sent
    if (firstName !== false) {
      if (lastName !== false) {
        if (assignedEmail !== false) {
          if (streetAddress !== false) {
            // Get the users token from localStorage
            var token = localStorage.getItem("token");
            if (token !== null) {
              // Setup the request URL
              let url   = apiUrl + "/klon/getIdentityByProperty";
              // Setup the data to send to the API
              let data  = {"token" : token, "firstName" : firstName, "lastName" : lastName, "assignedEmail" : assignedEmail, "streetAddress" : streetAddress};
              // Format the data to send to the API
              var payload = await formatPayload(data);
              // Send the data to the API
              var request = await ajx.req('POST', url, data, 'json');
              // console.log("RRREEEEKKKKT", request.data);
              if (request.data) {
                return request;
              }
              // Return response
              return request;
            } else {
              errorHandler.setError("token not found");
              errorHandler.showError();
              credential.forceLogin();
              return false;
            }
          } else {
            errorHandler.setError("firstName was not set");
            errorHandler.showError();
            return false;
          }
          errorHandler.setError("assignedEmail was not set");
          errorHandler.showError();
          return false;
        }
      } else {
        errorHandler.setError("lastName was not set");
        errorHandler.showError();
        return false;
      }
    } else {
      errorHandler.setError("firstName was not set");
      errorHandler.showError();
      return false;
    }
  },


  /**
   *
   *  When an identity is retrieved from the server this will extract that data and turn it into a meaningful json object
   *
   *  @param      String        data       Requires the raw output from modify.getIdentity
   *
   */
  extractIdentityData : async function(data) {
    var data;
     // Check to make sure there are identities stored for the user.
    if (!data) {
      // Return null, this means there's no identities found for the user
      return null;
    }
    var rawKey  = localStorage.getItem('key');
    try {
      // console.log("Data: ", data);
      // console.log("Raw Key: ", rawKey);
      var key   = await splitHexValue(rawKey, 8);
       // Assemble our lovely JSON object below...
       // Determine if the data is base64 encoded or not
      if (base64.isEncoded(data) === true) {
        // console.log("Base64 Encoded");
        // Base64 decode data
        data = window.atob(data);
      }
      // Get the index of the pipe (this is the end of the column names)
      var pipe = data.indexOf("|");
      // console.log("Pipe: ", pipe);
      // Split the string at the pipe index
      var columns = data.substr(0, pipe);
      // Split the string into column names in an array
      var columnNames = columns.split(",");
      // Get the identities (from the first pipe to the end of the string)
      var identities = data.substr(++pipe);
      identities = identities.split("|");
      var jsonId = "[";
      for (var i = 0; i < identities.length; i++) {
        var breakItUp = identities[i].split(",");
        for (var n = 0; n < breakItUp.length; n++) {
          if (n == 0) {
            jsonId = jsonId + "{" + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"' + ",";
          } else if (n == (breakItUp.length - 1)) {
            jsonId = jsonId + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"},';
          } else {
            jsonId = jsonId + '"' + columnNames[n].trim() + '"' + " : " + '"' + breakItUp[n] + '"' + ",";
          }
        }
        var prev = i;
      }
      // Remove the last comma
      jsonId = jsonId.substr(0,(jsonId.length - 1));
      // Add the closing bracket for the JSON object
      jsonId = jsonId + "]";
      // console.log("JSON BB: ", jsonId);
      jsonId = JSON.parse(jsonId);
      //
      return jsonId;
    } catch (err) {
      console.log('getStorageData Error: ', err);
      return false;
    }
  }

}
