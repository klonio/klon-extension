var ajx = {

  // Store the preformatted payload here for the last login, to facilitate seamless token fetching
  creds : "",

  /**
   *
   *  Checks to see if the request is within the rate limit
   *
   *  @param     method         String      The method used for the request (GET, POST, PUT, DELETE, etc...)
   *
   */
  isAllowed : async function(method, url, data = '', conType = 'json', shortCircuit = false) {
    if (method == 'POST' || method == 'PUT' || method == 'DELETE') {
      // Make sure we're not breaking out of the ratelimiter's speed limits
      let withinRateLimit = await rateLimiter.withinLimits();
      try {
        console.log("Within Rate Limit: ", withinRateLimit);
        if (typeof withinRateLimit === "boolean") {
          return withinRateLimit;
        }
      } catch(e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        return false;
      }
      // Set the request time
      let setLRT = await rateLimiter.setLastRequestTime();
      return true;
    } else {
      // Not the method we're looking for
      return true;
    }
  },


  /**
   *
   *  This function will make requests for us
   *
   *  Example: ajx.req('POST', 'https://api.klon.io/v1/klon/getIdentities', '{"payload" : "253f45e...98d6a0"}', 'json')
   *
   *  @param     method          String    Method the request should use. Ex: GET, POST, PUT, DELETE, etc...
   *  @param     url             String    Where to send the request
   *  @param     data            String    Any data to send to the endpoint    *Optional*
   *  @param     conType         String    The content type Ex: text, json, etc...  *Optional*
   *  @param     shortCircuit    Boolean   Should the request be subjected to the rate limiter? *Optional*
   *
   *  @return                Promise   Returns a Promise object with the response or false
   *
   */
  req : function (method = 'POST', url, data = '', conType = 'json') {
    return new Promise((resolve, reject) => {
      if (method, url, conType) {
        if (typeof data == "object") {
          data = JSON.stringify(data);
        }
        try {
          var request = new XMLHttpRequest();
          method = method.toUpperCase();
          request.open(method, url, true);
          if (conType.toLowerCase() == 'json') {
            request.setRequestHeader('Content-Type', 'application/json;');
          }
          console.log("REQUEST: ", request);
          console.log("AJAX DATA: ", data);
          request.send(data);
          request.onload = function () {
            if (request.readyState === request.DONE) {
              if (request.status === 200) {
                // Request was successful
                var msg = (JSON.parse(request.responseText || request.statusText));
                console.log("AJX: ", msg);
                resolve(msg);
              } else if(request.status === 0) {
                // Do something
                reject("there was a problem with the connection");
              } else {
                // Request was not successful
                var msg = request.responseText || request.statusText;
                console.log("AJX Error: ", msg);
                reject(msg);
              }
            }
          };
          request.onerror = function() {
            var msg = request.responseText || request.statusText;
            console.log("ERROR: ", msg);
            reject(msg);
          }
        } catch (err) {
          console.log("AJX Error: ", err);
          reject(msg);
        }
      } else {
        reject(false);
      }
    });
  }
}
