/**
 *
 *  This is the promise chain that does the following...
 *    1. Check the credentials to see if they match the regex pattern
 *    NOPE A. Play the shrinkButton animation
 *    NOPE B. Play the showLoader animation
 *    2. Hash & Encrypt credentials and send to the server for validation
 *    3. Get the response from the server and display success or fail
 *
 *  If Step 5 is a success...
 *    1. Set the token in localStorage
 *    2. Set the token expiration in localStorage
 *    3. Set the encryption key in localStorage
 *
 *  If Step 5 is a failure...
 *    NOPE 1. Display failure loader screen
 *    NOPE 2. Slide the failure loader screen down
 *    3. Display the error in the #error <span> on the login.html screen
 *    4. Focus the pointer in the #email input box
 *
 */

async function submitCredentials() {
  // Clear the local storage
  localStorage.clear();
  // Clear the chrome.storage.local
  chrome.storage.sync.set({identities: null});
  // Get the username and password to be submitted
  var email    = document.querySelector('#credEmail').value;
  var password = document.querySelector('#credPassword').value;
  localStorage.setItem("user", email);
  // Get the error field
  var error    = document.querySelector('#error');
  // Error check the email and password values before we do anything else
  if (validateEmail(email) && password.length >= 8) {
    try {
      //
      await showError();
      errorHandler.setError('', 'Validating credentials...', false);
      errorHandler.showError();
      // Hash, encrypt, and send credentials to the remote server then wait for a response
      var gtToken    = await getToken(email, password);
      console.log("gtToken: ", gtToken);
      if (gtToken.token) {
        // Get the token
        var expiration = gtToken.token;
        // Split the string between the 2 dots
        var firstDot   = Number(expiration.indexOf("."));
        var lastDot    = Number(expiration.lastIndexOf("."));
        expiration     = expiration.slice(++firstDot, lastDot);
        console.log("EXP: ", expiration);
        // Base64 decode the string
        expiration     = window.atob(expiration);
        // Parse to a JSON object
        if (typeof expiration == "string") {
          expiration = JSON.parse(expiration);
        }
        console.log("EXP: ", expiration);
        // Check to see if the token has the property "exp"
        if (expiration.hasOwnProperty("exp")) {
          console.log("hasExpiration: ", expiration.exp);
          // Set the expiration time of the token
          localStorage.setItem('tokenExpiration', expiration.exp);
        } else {
          errorHandler.setError("Error", "Expiration property not set. Try again later");
          errorHandler.showError(3500);
          window.location = 'login.html';
          return false;
        }
        // Store the token and expiration time in the localStorage
        localStorage.setItem('token', gtToken.token);
        // Since the token was returned this must be a legit user time to generate the AES key
        var key = await genKey(email, password);
        if (key) {
          // Set the key (in hex form) in localStorage
          localStorage.setItem('key', key);
          // Display the next screen
        }
        if (gtToken.isNew === 1) {
          window.location.pathname = '/welcome.html';
          return true;
        } else {
          window.location = 'identities.html';
          return true;
        }
      }
    } catch (err) {
      // Turn err into an object if it isn't already
      if (typeof err != "object") {
        err = JSON.parse(err);
      }
      // console.log("Error: " + err);
      // Show fail loader screen
      // await showError();
      // Display error on the login screen
      errorHandler.setError("Error", err.message);
      errorHandler.showError();
      if (err.message === "account is expired") {
        setTimeout(function() {
          window.location.pathname = "/payment.html";
        }, 3000);
      }
      return false;
    }
  } else {
    // Return error, "invalid email address and or password"
    // await showError();
    errorHandler.setError("Error", "invalid credentials");
    errorHandler.showError();
    return false;
  }
}
