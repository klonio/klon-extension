//@prepros-append crawler.js
//@prepros-append filler.js
//@prepros-append comms.js
//@prepros-append identities.js
//@prepros-append parasite.js
var content = {};
var generatedId = {};

/**
 *
 *  When the mouse hovers over an element highlight it
 *
 */
function highlightNode(e) {
  try {
    var el = e.target;
    style = window.getComputedStyle(el),
    background = style.getPropertyValue('background-color');
    border = style.getPropertyValue('border');
    content.oldBackground = background;
    content.oldBorder     = border;
    // Highlight the element by changing the background color and border
    el.style.backgroundColor = "#83a2ea";
    el.style.border = "solid 2px #3b6ce1";
    return true;
  } catch (err) {
    console.log("Error: ", err.message);
    return false;
  }
}


/**
 *
 *  Return things to normal on mouseout
 *
 */
function dehighlightNode(e) {
  var el = e.target;
  // Highlight the element by changing the background color and border
  if (content.oldBackground) {
    el.style.backgroundColor = content.oldBackground;
  } else {
    el.style.backgroundColor = "transparent";
  }
  //
  if (content.oldBorder) {
    el.style.border = content.oldBorder;
  } else {
    el.style.border = "none";
  }
}


/**
 *
 *  This is the form the user selected to be manually filled
 *
 */
function nodeSelect(e) {
  var el = e.target;
  // Find the form based on the node the user selected
  var form = el.querySelector("form");
  // If a form is found, cool proceed
  if (!form) {
    // If no form is search up the DOM
    form = e.target.closest("form");
  }

  // Fill the form with the identity
  // 1. Get the inputs organized from the form the user has selected
  var result = theRinger("signup", form);
  // console.log("The Result of the Ringer: ", result);
  // 2. Fill the form with the identity
  var fill   = filler.fillForm(result, generatedId);
  if (fill) {
    alert("Successfully filled form!");
  }
  // console.log("nodeSelect Identity is: ", generatedId);

  // console.log("Form: ", form);
  // Add the new id to the user's klon identities

  // Remove all the event listeners and remove their effects
  dehighlightNode(e);
  document.body.removeEventListener("mouseover", highlightNode);
  document.body.removeEventListener("mouseout", dehighlightNode);
  document.body.removeEventListener("click", nodeSelect);
  return true;
}


/**
 *
 *  Show the klonLoginMenu when the user clicks the klon logo contained inside the <input>
 *
 */
function klonBtnClick(evt) {
  // Show the menu where the user can select between different id's for the current URL
  var injectLoginKlon = document.querySelector("#klonLoginMenu");
  //
  if (injectLoginKlon != undefined || injectLoginKlon != null) {
    // Set the "Left" style property/value to be 30 px more than the button that was clicked to activate it
    var l = evt.target.style.left;
    // Convert the value of the "left" property from ie: "210px" to 210
    l = Number(l.slice(0,-2));
    // Evaluate the mathematical equation as opposed to concatenation
    l = eval(l - 277);
    // Add the "px" units to the end of the thing again
    l = l + "px";
    // Set the "Top" style property/value to be 30 px more than the button that was clicked to activate it
    var t = evt.target.style.top;
    t = Number(t.slice(0,-2));
    // eval forces the evaluation of the mathematical equation as opposed to concatenation
    t = eval(t + 30);
    t = t + "px";
    //
    injectLoginKlon.setAttribute("style", "left:" + l + ";top:" + t + ";");
    //
    injectLoginKlon.classList.toggle("klonHide");
    return true;
  } else {
    return false;
  }
}


/**
 *
 *  Determine if we are signing up or logging in
 *
 */
function determineAction() {
  // Determine what we are doing based on the URL ie signing up or logging in to the site
  var action = crawler.signupRegisterURL(window.location.href);
  // If we are neither obviously signing up nor logging in then we're most likely logging in.
  if (action != "signup" && action != "login") {
    action = "login";
  }
  return action;
}


/**
 *
 *  Find all the inputs for the given form and assign them to their respective key pair. example: <input name="username" type="text"> is assigned to identity.username
 *
 *
 *
 */
function theRinger(action, form = null) {
  // Find all of the forms on the page related to the particular action we have decided upon (registration or login)
  var formFields = crawler.seek(action, form);
  // Setup all the regular expressions...
  // Name RegExp
  var regExAutoCompName = new RegExp(/^given-name$|^additional-name$|^family-name$/i);
  var regExNameSmall    = new RegExp(/^name$/i);
  var regExNameBig      = new RegExp(/customerName|fullname|full.name|^name$|firstname|first.name|lastname|last.name|middleinitial|^mi$|middlename|middle.name|\bfname|\bname.f\b|\bname.l\b/i);
  var regExNameArray  = [regExAutoCompName, regExNameSmall, regExNameBig];
  // Email RegExp
  var regExEmail = new RegExp(/email|e.mail/i);
  var regExEmailArray = [regExEmail];
  // Username RegExp
  var regExUsername      = new RegExp(/username|user.name/i);
  var regExUsernameArray = [regExUsername];
  // Password RegExp
  var regExAutoPassword = new RegExp(/new.password|current-password|current.password|^password$/i);
  var regExPassword     = new RegExp(/^password|^pass$|pass.word|^pw$|passwd/i);
  var regExPasswdArray  = [regExAutoPassword, regExPassword];
  // Address RegExp
  var regExAddress      = new RegExp(/^address|street.address|streetaddress|address.line|^country$|zip|postal.code|postalcode|country.name|countryname/i);
  var regExAddressArray = [regExAddress];
  // DOB RegExp
  var regExBirthday  = new RegExp(/^bday$|^dob$|^birthday$/i);
  var regExBdayMonth = new RegExp(/bday.month|dobmonth|dob.month|birthday.month|birth.month|birthmonth/i);
  var regExBdayDay   = new RegExp(/bday.day|dobday|dob.day|birthday.day|birth.day/i);
  var regExBdayYear  = new RegExp(/bday.year|dobyear|dob.year|birthday.year|birthyear|birth.year/i);
  var regExDobArray  = [regExBirthday, regExBdayMonth, regExBdayDay, regExBdayYear];
  // Gender Array RegExp
  var regExGender      = new RegExp(/sex|gender|male|female/i);
  var regExGenderArray = [regExGender];
  //
  var nameFields       = new Array();
  var emailFields      = new Array();
  var usernameFields   = new Array();
  var passwdFields     = new Array();
  var addressFields    = new Array();
  var dobFields        = new Array();
  var genderFields     = new Array();
  //
  for (var i = 0; i < formFields.length; i++) {
    // Find name related fields
    if (crawler.isField(formFields[i], regExNameArray) != null) {
      nameFields[i]     = crawler.isField(formFields[i], regExNameArray);
    }
    // Find email related fields
    if (crawler.isField(formFields[i], regExEmailArray) != null) {
      emailFields[i]    = crawler.isField(formFields[i], regExEmailArray);
    }
    // Find username related fields
    if (crawler.isField(formFields[i], regExUsernameArray) != null) {
      usernameFields[i] = crawler.isField(formFields[i], regExUsernameArray);
    }
    // Find password related fields
    if (crawler.isField(formFields[i], regExPasswdArray) != null) {
      passwdFields[i]   = crawler.isField(formFields[i], regExPasswdArray);
    }
    // Find address related fields
    if (crawler.isField(formFields[i], regExAddressArray) != null) {
      addressFields[i]  = crawler.isField(formFields[i], regExAddressArray);
    }
    // Find dob related fields
    if (crawler.isField(formFields[i], regExDobArray) != null) {
      dobFields[i]      = crawler.isField(formFields[i], regExDobArray);
    }
    // Find gender related fields
    if (crawler.isField(formFields[i], regExGenderArray) != null) {
      genderFields[i]   = crawler.isField(formFields[i], regExGenderArray);
    }
  }
  /**
  *
  *  Remove the null values from the arrays...
  *
  */
  nameFields = nameFields.filter(function (el) {
    return el != null;
  });
  emailFields = emailFields.filter(function (el) {
    return el != null;
  });
  usernameFields = usernameFields.filter(function (el) {
    return el != null;
  });
  passwdFields = passwdFields.filter(function (el) {
    return el != null;
  });
  addressFields = addressFields.filter(function (el) {
    return el != null;
  });
  dobFields = dobFields.filter(function (el) {
    return el != null;
  });
  genderFields = genderFields.filter(function (el) {
    return el != null;
  });
  // Create the identity object to store all of the <inputs> as to their relationship with the identity
  var identity = {
    firstName : '',
    middleInitial : '',
    lastName : '',
    fullName: '',
    gender : '',
    genderM : '',
    genderF : '',
    streetAddress : '',
    road : '',
    state : '',
    city : '',
    zip : '',
    dob : '',
    dobMonth : '',
    dobDay : '',
    dobYear : '',
    email : '',
    emailConf : '',
    username : '',
    password : '',
    passwordConf : ''
  };
  // Loop through each name field
  for (var i = 0; i < nameFields.length; i++) {
    crawler.translateField(nameFields[i], "name", identity);
  }
  for (var i = 0; i < emailFields.length; i++) {
    crawler.translateField(emailFields[i], "email", identity);
  }
  for (var i = 0; i < usernameFields.length; i++) {
    crawler.translateField(usernameFields[i], "username", identity);
  }
  for (var i = 0; i < passwdFields.length; i++) {
    crawler.translateField(passwdFields[i], "password", identity);
  }
  for (var i = 0; i < addressFields.length; i++) {
    crawler.translateField(addressFields[i], "address", identity);
  }
  for (var i = 0; i < dobFields.length; i++) {
    crawler.translateField(dobFields[i], "dob", identity);
  }
  for (var i = 0; i < genderFields.length; i++) {
    crawler.translateField(genderFields[i], "gender", identity);
  }
  console.log("FOUND MATCH FIELDS: ", identity);
  //
  return identity;
}


/**
 *
 *  Start the routine...
 *
 */
var t = setTimeout(() => {
  // If the document is loaded
  if (document.readyState == "complete") {
    var action   = determineAction();
    // console.log("action = " + action);
    var identity = theRinger(action);
    // console.log("identity: ", identity);
    //
    clearTimeout(t);
    // If we are signing up
    if (action === "signup") {
      //
      parasite.identityFields = identity;
      // Pop the form into the content
      // parasite.injectPayload();
      // Add Event listeners to the buttons on the form so that they work when you click them
      // parasite.injectFunctions();
      // Request a new identity from the extension. The extension then gets the
      // request and attempts to get a new identity from the API
      // var respo = parasite.createNewIdentity();
    }
    //
    if (action === "login") {
      console.log("LOGGING IN");
      // Stores all of the inputs and maps them to their respective property ie {username : <input type="text" name="username">}
      parasite.identityFields = identity;
      // Find and get a handle on the login for on the page
      var loginForm = crawler.targetForms("login");
      // Inject the button into the form so the user can auto login
      if (typeof loginForm === "object") {
        //
        var getAssocIds = comm.sendMessage({"msg" : "getAssociatedIdentities", "url" : window.location.protocol + "//" + window.location.host});
        getAssocIds.then(assocIds => {
          // console.log("IDENTITIES : ", assocIds);
          // Set these identities in the parasite object for later use.
          parasite.assocId = assocIds;
          // console.log("assocIds Type: ", typeof parasite.assocId);
          // console.log("assocIds Length: ", parasite.assocId.length);
          // If the URL is found in the user's identities then inject into the form...
          if (parasite.assocId.length > 0) {
            // console.log("SHOE BUDDY");
            // Add the login menu to the page
            var ijlb = parasite.injectLoginMenu(assocIds);
            ijlb.then(ijlbResult => {
              // Add the button inside the email <input>
              var btnInjectEmail    = parasite.injectInputButtons(parasite.identityFields.email);
              btnInjectEmail.then(btnInjectEmailResult => {
                // Add the button inside the username <input>
                var btnInjectUsername = parasite.injectInputButtons(parasite.identityFields.username);
                btnInjectUsername.then(btnInjectUsernameResult => {
                  //
                  var bindLoginMenuFunctions = parasite.bindLoginMenuFunctions();
                  bindLoginMenuFunctions.then(bound => {
                    // Loop through all of the elements on the page that have the class klonBtn and add functionality to them
                    var klonBtns = document.querySelectorAll(".klonBtn");
                    for (let c = 0; c < klonBtns.length; c++) {
                      var myId = klonBtns[c].id;
                      klonBtns[c].addEventListener("click", klonBtnClick);
                    }
                  })
                  .catch(error => {
                    console.log("Error: ", error);
                  });
                }).catch(error => {
                  console.log("Error: ", error);
                });
              })
              .catch(error => {
                console.log("Error: ", error);
              });
            })
            .catch(error => {
              console.log("Error: ", error);
            });
          }
          //
        }).catch(error => {
          console.log("Error: ", error);
        });
      }
      //
    }
    clearTimeout(t);
  }
}, 1000);


/**
 *
 *  Make sure the buttons are always in the correct position within the <input>
 *
 */
function positionLoginButtons() {
  var klonBtns = document.querySelectorAll(".klonBtn");
  var klonLoginMenu = document.querySelector("#klonLoginMenu") || false;
  if (klonLoginMenu) {
    // If the klonLoginMenu is not hidden, hide it.
    if (!klonLoginMenu.classList.contains("klonHide")) {
      klonLoginMenu.classList.add("klonHide");
    }
  }
  // If there are klonBtns on the page let's position them properly
  if (klonBtns.length > 0) {
    for (let i = 0; i < klonBtns.length; i++) {
      try {
        // Get the id of the current klonButton
        var id = klonBtns[i].id;
        // Remove the klonBtn_ portion of the id
        id = id.slice(8);
        // Get a handle on the proper input for the klonBtn
        var input = document.querySelector("[data-klon='" + id + "']");
        // Get the distance from the top of the page
        var t = window.pageYOffset + input.getBoundingClientRect().top;
        // Get the distance from the left of the page
        var l = window.pageXOffset + input.getBoundingClientRect().left;
        // Get the height of the input
        var h = input.clientHeight;
        // Get the width of the input
        var w = input.clientWidth;
        // Get the width of the button to inject
        var btnW = 20;
        // Get the height of the button to inject
        var btnH = 20;
        // Set the top and left style properties on the button
        klonBtns[i].style.top  = t + ((h - btnH) / 2) + "px";
        klonBtns[i].style.left = l + (w - btnW) + "px";
        return true;
      } catch (e) {
        console.log("Error: ", e.message);
        return false;
      }
    }
  }
}


// Bind the positionLoginButtons function to the window.onResize event
window.addEventListener("resize", positionLoginButtons);
// document.body.addEventListener("mouseover", highlightNode, false);
// document.body.addEventListener("mouseout", dehighlightNode, false);
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  console.log("Content Script received message: ", request.msg);
  switch (request.msg) {
    //
    case "prepareFillForm":
      generatedId = request.identity;
      // Begin highlighting elements on mouseover
      document.body.addEventListener("mouseover", highlightNode, false);
      // Undo the highlight effect on mouseout
      document.body.addEventListener("mouseout", dehighlightNode, false);
      // When the user clicks on the form return the result
      document.body.addEventListener("click", nodeSelect, false);
      sendResponse("Response");
      break;
  }
});
