//@prepros-append time.js

// Turn this whole thing into an object to make it easier to work with

var identityDetails = {
  title : "",
  fullName : "",
  url : "",


  /**
   *
   *  Visit the URL
   *
   *  @param      url       String      URL to visit
   *
   *  @return               Boolean     True | False based on success of the operation
   *
   */
  visitURL : async function(url = null) {
    if (url !== null) {
      if (typeof url === "string") {
        let createProperties = {
          "windowId" : null,
          "index" : null,
          "url" : url,
          "active" : true,
          "selected" : true,
          "pinned" : false,
          "openerTabId" : null
        };
        chrome.tabs.create(createProperties);
        return true;
      } else {
        throw("identityDetails.visitURL Exception: Argument 'url' must be of type string");
      }
    } else {
      // Throw the exception
      throw("identityDetails.visitURL Exception: Argument 'url' cannot be null");
    }
  },


  /**
   *
   *  Get the current identity and spit it out
   *
   */
  getCurrentIdentity : function() {
    let currentIdentity = localStorage.getItem("currentId");
    if (currentIdentity !== null) {
      try {
        return JSON.parse(currentIdentity);
      } catch (e) {
        console.log("identityDetails.getCurrentIdentity Error: ", e);
        return false;
      }
    } else {
      return false;
    }
  },


  /**
   *
   *  Prepare the current identity by decrypting the required parts of the current identity for display to the user
   *
   *  @param      identity       JSON Object                 JSON Object of the identity to be prepared
   *
   *  @return                    JSON Object | Boolean       Decrypted values in object form | False
   *
   */
  prepareIdentity : async function(identity) {
    // Make sure identity is an object
    if (typeof identity === "object") {
      // Pull the properties we need from the identity
      let firstName     = identity.firstName;
      let lastName      = identity.lastName;
      let email         = identity.assignedEmail || identity.email;
      let url           = identity.url;
      let streetAddress = identity.streetAddress;
      try {
        // Get the decryption key
        var key = await decrypt.getKey();
        // Decrypt each thing and return them
        firstName     = decryptIt(key, firstName);
        lastName      = decryptIt(key, lastName);
        email         = decryptIt(key, email);
        url           = decryptIt(key, url);
        streetAddress = decryptIt(key, streetAddress);
        // Wrap it up in a pretty little object
        let preparedIdentity = {
          "name"  : firstName + " " + lastName,
          "email" : email,
          "url"   : url,
          "streetAddress" : streetAddress
        };
        // Return the thing
        return preparedIdentity;
      } catch(e) {
        console.log("identityDetails.prepareIdentity Error: ", e);
        return e;
      }
    } else {
      console.log("identityDetails.prepareIdentity Error: Parameter 'identity' must be of type Object");
      return false;
    }
  },


  /**
   *
   *  Permanently delete an identity
   *
   *  @param    identityId     Integer     The ID of the identity to be deleted
   *  @param    token          String      The users token for authentication
   *  @param    hash           String      The hash of the identity
   *
   *  @return                  String      Returns JSON with a message about the success/fail of the operation
   *
   */
  deleteIdentity : async function(e) {
    // Confirm the user wants to delete the identity
    var conf = confirm("Are you sure you want to permanently delete this identity?");
    if (conf === true) {
      // Get the users token
      let token = localStorage.getItem('token');
      if (token !== null) {
        // Get the ID of the identity
        let objIdentity = localStorage.getItem('currentId');
        if (objIdentity !== null) {
          try {
            JSON.parse(objIdentity);
          } catch(e) {
            errorHandler.setError("Error", e.message);
            errorHandler.showError();
            return false;
          }
        } else {
          errorHandler.setError("Error", "currentId not found");
          errorHandler.showError();
          return false;
        }
        //
        //
        //
        let identityId = Number(JSON.parse(localStorage.getItem('currentId')).id);
        // Make sure identityId was found and is the correct type
        if (typeof identityId === "number" && !isNaN(identityId)) {
          console.log("TRUE");
          // Get the hash of this identity
          let hash = e.target.dataset.hash;
          //
          if (hash !== null && hash !== undefined) {
            // Set the URL to send the request to
            var url     = apiUrl + "/klon/deleteIdentity";
            // Define the data to be sent to the server
            var data    = {"token" : token, "identityId" : identityId, "hash" : hash};
            // Format the data to send to the server
            var payload = await formatPayload(data);
            // Send the request to the server
            var request = await ajx.req('DELETE', url, payload, 'json');
            console.log("Response: ", request);
            if (request['status'] == "success") {
              // Clear the hash from localStorage
              localStorage.removeItem('hash');
              errorHandler.setError("Success!", request['message'], false);
              errorHandler.showError();
              return request['message'];
            } else if (request['status'] == "fail") {
              errorHandler.setError("Error", request['message']);
              errorHandler.showError();
              return false;
            } else {
              errorHandler.setError("Error", "An error has occurred");
              errorHandler.showError();
              return false;
            }
          } else {
            errorHandler.setError("Error", "Hash not found");
            errorHandler.showError();
            return false;
          }
        } else {
          console.log("FALSE");
        }
      } else {
        errorHandler.setError("Error", "Token not found");
        errorHandler.showError();
        return false;
      }
    } else {
      return false;
    }
  },


  /**
   *
   *  Compose the page
   *
   *  @param        preparedIdentity      JSON Object        The decrypted values we will use to display the identity to the user
   *
   *  @return                             Boolean            True|False based on success of the operation
   *
   */
  composePage : async function(preparedIdentity = undefined) {
    // Make sure the argument is an object
    if (typeof preparedIdentity === "object") {
      // Make sure our identity contains all the information we want to display
      if (preparedIdentity.name !== undefined && preparedIdentity.email !== undefined && preparedIdentity.url !== undefined) {
        // Get the details we need from the object
        let name          = preparedIdentity.name;
        let email         = preparedIdentity.email;
        let url           = preparedIdentity.url;
        let streetAddress = preparedIdentity.streetAddress;
        console.log("Street Address: ", streetAddress);
        let originalURL = url;
        console.log("URL: ", url);
        // Eliminate the protocol off of the URL if it is present
        let protocolIndex = url.indexOf("://");
        if (protocolIndex !== -1) {
          url = url.substring(protocolIndex + 3);
        }
        // Eliminate the www. off of the URL if it is present
        let wwwIndex = url.indexOf("www.");
        // If "www." was found...
        if (wwwIndex !== -1) {
          url = url.substring(wwwIndex + 4);
        }
        // Find the elements on the page that we want to fill with this information
        // This one will be the host like Example.com
        let title = document.querySelector("#detailTitle");
        if (title === null) {
          return false;
        }
        // This one will contain `${name} : ${email}`
        let brief = document.querySelector("#detailBrief");
        if (brief === false) {
          return false;
        }
        let visitLink = document.querySelector("#detailURL");
        if (visitLink !== false) {
          visitLink.addEventListener("click", function(url) {
            identityDetails.visitURL(originalURL);
          });
        }
        // Calculate the hash for this identity...
        var hash = await identities.hashIdentity(preparedIdentity.email + preparedIdentity.streetAddress);
        console.log("Ze Hash: ", hash);
        // Get the current id
        // MAYBE GET RID OF THIS
        // let currId = localStorage.getItem("currentId");
        // if (currId !== null) {
        //   try {
        //     currId = JSON.parse(currId);
        //   } catch(e) {
        //     errorHandler.setError("Error", e.message);
        //     errorHandler.showError();
        //     return false;
        //   }
        // }
        // console.log("currId=", currId);
        // Find the Modify button and add the event listener to it
        // let btnModify = document.querySelector("#btnModify");
        // btnModify.setAttribute('href', 'modify.html?id=' + currId);
        // Find the 'Delete Identity' button and add the event listener to it
        let btnDelete = document.querySelector("#btnDeleteIdentity");
        btnDelete.dataset.hash = hash;
        if (btnDelete !== null) {
          // Add the event listener to the button
          btnDelete.addEventListener("click", function(e) {
            identityDetails.deleteIdentity(e);
          });
        }
        // Put the data on the page
        title.innerText = url;
        brief.innerHTML = name + "<br>" + email;
        //
        return true;
      } else {
        console.log("identityDetails.composePage Error: Missing required properties in the preparedIdentity object argument");
        return false;
      }
    } else {
      console.log("identityDetails.composePage Error: Parameter 'preparedIdentity' must be of type Object");
      return false;
    }
  }
};


// When the page loads, do this...
// window.addEventListener("load", async function() {
//   if (window.pathname === "/identity-details.html") {
//     // Get the current identity
//     let identity = identityDetails.getCurrentIdentity();
//     try {
//       // Prepare the identity to go on the page
//       let prepare  = identityDetails.prepareIdentity(identity);
//       // Build the page
//       let compose  = identityDetails.composePage(prepare);
//       //
//       return true;
//     } catch(e) {
//       console.log("Error: ", e);
//       return false;
//     }
//   }
// });
