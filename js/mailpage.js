window.addEventListener("load", function() {
  // Check the user's credentials
  credential.hasToken();
  // Set the session tab to "mail" so the user returns to the mail tab upon closing the extension
  localStorage.setItem("sessionTab", "mail");
  // Get the users role so we know what capabilities to grant them
  // Get the sessionAction(What we were doing at the end of the last session) and current mailbox
  var sessionAction  = localStorage.getItem("sessionAction");
  var mailboxCurrent = localStorage.getItem("mailboxCurrent");
  // If the current mailbox is set...
  if (mailboxCurrent) {
    // If the mail is downloaded already, display it
    if (localStorage.mail != null && localStorage.mail != undefined && localStorage.mail != "undefined") {
      var mailMessages = localStorage.getItem("mail");
      if (mailMessages !== "no messages") {
        console.log("POINT 1: " + mailboxCurrent);
      }
    } else {
      console.log("POINT 2: " + mailboxCurrent);
      // We don't have mail, get it and display it.
      var pm = mail.parseMail(mailboxCurrent);
      pm.then(res => {
        if (localStorage.mail === "no messages" || localStorage.mail === false || localStorage.mail === "false") {
          arrMail = localStorage.mail;
        } else {
          arrMail = JSON.parse(localStorage.mail);
        }
        var dm = mail.displayMail(arrMail);
        // Bind the mail item functionality
        mail.bindMailItem();
        // Bind the mail navigation
        mail.bindMainNav();
      }).catch(err => {
        console.log("Error: ", err.message);
        return false;
      });
    }
    // If the pageAction is set...
    if (sessionAction) {
      console.log("POINT 3: " + mailboxCurrent);
      switch(sessionAction) {
        case "displayMail":
        console.log("POINT 3a: " + mailboxCurrent);
        // Parse mail
        var pm = mail.parseMail(localStorage.mailboxCurrent.trim().toLowerCase());
        pm.then(result => {
          console.log("POINT 3b: " + mailboxCurrent);
          // Display the mail for the given mailbox
          var mailMessages = localStorage.mail;
          if (mailMessages !== "no messages") {
            mailMessages = JSON.parse(mailMessages);
          }
          var dispMail = mail.displayMail(mailMessages);
          dispMail.then(result => {
            console.log("POINT 3c: " + mailboxCurrent);
            mail.bindMailItem();
            mail.bindMainNav();
            console.log("mailMessages: " + mailMessages);
            console.log("Result: " + result);
            return true;
          }).catch(err => {
            // Initialize the error handler
            var errInit   = errorHandler.init();
            // If successful...
            if (errInit === true) {
              // Set the error
              var errSet = errorHandler.setError("Error", err);
              // If successful...
              if (errSet === true) {
                // Show the error for 3500 milliseconds
                errShow = errorHandler.showError(3500);
              }
            }
            return false;
          });
        }).catch(err => {
          console.log("Result: " + result);
          return false;
        });
        break;
        case "readMail":
        break;
        case "something":
        break;
      }
    } else {
      console.log("POINT 4: " + mailboxCurrent);
      var gm = mail.getMail("inbox");
      gm.then(msgs => {
        var pm = mail.parseMail("inbox");
        pm.then(res => {
          localStorage.setItem("mailboxCurrent", "inbox");
          console.log("Res: ", res);
          // Stringify the object if needed, to store in localStorage
          if (typeof res == "object") {
            res = JSON.stringify(res.message);
          }
          try {
            localStorage.setItem("mail", res);
          } catch(e) {
            console.log("Error: ", err.message);
            localStorage.setItem("mail", false);
          }
          var arrMail = JSON.parse(localStorage.mail);
          var dm = mail.displayMail(arrMail);
          dm.then(response => {
            localStorage.setItem("sessionAction", "displayMail");
            console.log("Response: ", response);
            mail.bindMailItem();
            mail.bindMainNav();
            return true;
          }).catch(err => {
            console.log("Error: ", err.message);
            return false;
          });
          // mail.bindCheckboxFunction();
        }).catch(err => {
          console.log("Error: ", err);
          return false;
        });
      }).catch(err => {
        console.log("Error: ", err);
        return false;
      });
    }
  } else {
    console.log("POINT 5: " + mailboxCurrent);
    //
    var gm = mail.getMail("inbox");
    gm.then(msgs => {
      var pm = mail.parseMail("inbox");
      pm.then(res => {
        localStorage.setItem("mailboxCurrent", "inbox");
        console.log("Res: ", res);
        // Stringify the object if needed, to store in localStorage
        if (typeof res == "object") {
          res = JSON.stringify(res.message);
        }
        try {
          localStorage.setItem("mail", res);
        } catch(e) {
          console.log("Error: ", err.message);
          localStorage.setItem("mail", false);
        }
        var arrMail = JSON.parse(localStorage.mail);
        var dm = mail.displayMail(arrMail);
        dm.then(response => {
          localStorage.setItem("sessionAction", "displayMail");
          console.log("Response: ", response);
          mail.bindMailItem();
          mail.bindMainNav();
          return true;
        }).catch(err => {
          console.log("Error: ", err.message);
          return false;
        });
        // mail.bindCheckboxFunction();
      }).catch(err => {
        console.log("Error: ", err.message);
        return false;
      });
    }).catch(err => {
      console.log("Error: ", err.message);
      return false;
    });
  }
  // var userRole = role.getRole(getRoleResult => {
  //
  // });
  // userRole.then().catch(error => {
  //   console.log("Error: ", error);
  //   errorHandler.setError("Error", error);
  //   errorHandler.showError();
  //   return false;
  // });
// });



// Old
  // if (localStorage.getItem("inbox") == undefined || localStorage.getItem("inbox") == null || localStorage.getItem("mail") == null) {
  //   var gm = mail.getMail("inbox");
  //   gm.then( result => {
  //     var pm = mail.parseMail("inbox");
  //     pm.then(results => {
  //       var messages = JSON.parse(localStorage.getItem("mail")) || null;
  //       var dm = mail.displayMail(messages);
  //       dm.then(result => {
  //         mail.bindMailItem();
  //         mail.bindMainNav();
  //         // mail.bindCheckboxFunction();
  //       }).catch(err => {
  //         console.log("Error: ", err);
  //       });
  //     }).catch(err => {
  //       console.log("Error: ", err);
  //     });
  //   }).catch(err => {
  //     console.log("Error: ", err);
  //   });
  // } else {
  //   // Check to see if I have any new messages...
  //   var cm = mail.checkMail("inbox");
  //   if (cm === true) {
  //     var gm = mail.getMail("inbox");
  //     gm.then( result => {
  //       var pm = mail.parseMail("inbox");
  //       pm.then(results => {
  //         var messages = JSON.parse(localStorage.getItem("mail")) || null;
  //         mail.displayMail(messages);
  //       }).catch(err => {
  //         console.log("Error: ", err);
  //       });
  //     }).catch(err => {
  //       console.log("Error: ", err);
  //     });
  //   } else {
  //     var pm = mail.parseMail("inbox");
  //     pm.then(results => {
  //       var messages = JSON.parse(localStorage.getItem("mail")) || null;
  //       var dm = mail.displayMail(messages);
  //       dm.then(result => {
  //         mail.bindMailItem();
  //         mail.bindMainNav();
  //         // mail.bindCheckboxFunction();
  //       }).catch(err => {
  //         console.log("Error: ", err);
  //         return false;
  //       });
  //     })
  //     .catch(err => {
  //       console.log("Error: ", err);
  //       return false;
  //     });
  //   }
  // }
}, false);
