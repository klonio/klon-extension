var dobMonth  = document.querySelector("#dobMonth");
var dobDay    = document.querySelector("#dobDay");
var dobYear   = document.querySelector("#dobYear");
var btnUpdate = document.querySelector("#btnUpdate");
var txtHeader = document.querySelector("#modifyTitle");

window.addEventListener("load", function() {
  console.log("LOADED");
  // Get the key
  var key = modify.getKey();
  key.then(result => {
    // Assemble the page
    console.log("Compose");
    var build = modify.compose();
  })
  .catch(err => {
    console.log("Error: ", err);
  });
});

dobMonth.addEventListener("change", function() {
  console.log("DOB Month Value: ", dobMonth.value);
  dobMonth.value = Number(dobMonth.value);
  if (Number(dobMonth.value) < 10) {
    dobMonth.value = "0" + dobMonth.value;
  }
});

dobDay.addEventListener("change", function() {
  console.log("DOB Month Value: ", dobDay.value);
  dobDay.value = Number(dobDay.value);
  if (Number(dobDay.value) < 10) {
    dobDay.value = "0" + dobDay.value;
  }
});

btnUpdate.addEventListener("click", function() {
  console.log("Clicked");
  var submitMods = modify.submitChanges();
  submitMods.then(result => {
    console.log("Submit Changes: ", result);
    if (result === true) {
      // history.back();
      console.log("THIS IS WHERE ID GO BACK");
    }
  });
});
