/**
 *
 *  This file pulls/modifies/saves all of the settings from/to the Klon API Database
 *
 *
 */
var settings = {
  // Is the account enabled?
  enabled : null,

  // What level of subscription is the user?
  subscription : null,

  // Should identities be automatically generated(experimental) upon visiting a registration page?
  autogenerate : null,

  // Should forms be manually or automatically(experimental) filled?
  autofill : null,


  /**
   *
   *  Get the URL the user is currently on
   *
   */
   getCurrentURL : function() {
     return new Promise(function(resolve, reject) {
       chrome.tabs.query({"active":true}, function(result) {
         console.log("Result: ", result[0].url);
         let url = result[0].url;
         resolve(url);
       });
     });
   },


   /**
    *
    *  Get the favicon url for the page that the user is currently on
    *
    */
    getFavIcon : function() {
      return new Promise(function(resolve, reject) {
        chrome.tabs.query({"active":true}, function(result) {
          console.log("Result: ", result[0].favIconUrl);
          let icon = result[0].favIconUrl;
          resolve(icon);
        });
      });
    },


  /**
   *
   *  Get settings from the remote server
   *
   *  @param     Token      String             May or may not need the token
   *
   *  @return               Promise Object     Returns a JSON Object with the users settings
   *
   */
  getSettings : function() {
    console.log("GETTING SETTINGS");
    return new Promise(function(resolve, reject) {
      // The endpoint to send the request to
      var url     = apiUrl + "/subscriber/getSettings";
      // Get the token from the localStorage
      var token   = localStorage.getItem("token");
      // console.log("Token: ", token);
      if (token != undefined && token != null && token.length > 40) {
        console.log("Token: ", token);
        // Create the data payload to send to the server
        var data    = {"token" : token};
        // Format and encrypt the data to send to the server
        var payload = formatPayload(data);
        //
        payload.then( result => {
          result = JSON.stringify(result);
          console.log("Result: ", result);
          //
          var request = ajx.req('POST', url, result);
          request.then( response => {
            var ans = response['data'][0];
            console.log("Ans: ", ans);
            // Set the settings.hash setting
            localStorage.setItem('hash', ans['hash']);
            // Set the settings.subscription setting
            localStorage.setItem('subscription', Number(ans['subscription']));
            // Set the settings.enabled setting
            localStorage.setItem('enabled', Number(ans['enabled']));
            // Set the settings.autogenerate setting
            localStorage.setItem('autogenerate', Number(ans['enabled']));
            // Set the settings.autofill setting
            localStorage.setItem('autofill', Number(ans['enabled']));
            // Get the URLs
            var urls = getAllURLs();
            urls.then( result => {
              localStorage.setItem("urls", result);
              console.log("URL RESULTS: ", result);
              resolve(ans);
            }).catch(err => {
              console.log("Error: ", err);
              reject(err);
            });
            //
          }).catch(error => {
            // Log the error
            console.log("Error: ", error);
            // Destroy the token
            localStorage.removeItem('token');
            // Navigate to login screen
            window.location.href = "login.html";
            // Return false
            reject(false);
          });
        }).catch(err => {
          // Log the error
          console.log("Error: ", err);
          // Destroy the token
          localStorage.removeItem('token');
          // Navigate to login screen
          window.location.href = "login.html";
          // Return false
          reject(false);
        });
      } else {
        // Log the error
        console.log("Error: Invalid token");
        // Destroy the token
        localStorage.removeItem('token');
        // Navigate to login screen
        window.location.href = "login.html";
        reject(false);
      }
    });
  },


  /**
   *
   *  Pass settings from one part of the extension to another inside an object
   *
   */
  passSettings : function(url) {
    // console.log("HERE WE GO");
    return new Promise(function(resolve, reject) {
      var get = settings.getSettings().then(configuration => {
        try {
          // Get the hash setting -- This is the hash of all the identities... Purpose is to figure out if we need to request all the identities from the server
          if (localStorage.getItem("hash") !== null) {
            // Get the subscription setting. -- This is the status of the subscriber
            if (localStorage.getItem("subscription") !== null) {
              // Get the enabled setting -- Checks to see if the account is enabled or not
              if (localStorage.getItem("enabled") !== null) {
                //
                if (localStorage.getItem("urls") !== null) {
                  // Initiate the config object
                  var config = {};
                  // Set the properties of the config object
                  config.hash         = localStorage.getItem("hash");
                  config.subscription = Number(localStorage.getItem("subscription"));
                  config.enabled      = Number(localStorage.getItem("enabled"));
                  var urlCount        = countURLs(url);
                  urlCount
                  .then(count => {
                    config.urls       = count;
                    // console.log("PASS THIS CONFIG: ", config);
                    // Return the config object
                    resolve(config);
                  })
                  .catch(err => {
                    // console.log("Error: ", err);
                    reject("cannot count");
                  });
                } else {
                  // console.log("NOPEEEE!!!!");
                  reject("urls not found");
                }
              } else {
                // console.log("NOPEEEE!!!!");
                // There was an error, return false
                reject("not enabled");
              }
            } else {
              // console.log("NOPEEEE!!!!");
              // There was an error, return false
              reject("no subscription");
            }
          } else {
            // console.log("NOPEEEE!!!!");
            // There was an error, return false
            reject("no hash");
          }
        } catch(e) {
          // console.log("Error: ", e);
          reject("Error: ", e);
        }
      });
    });
  },


  /**
   *
   *  Get the users expiration time to display to them on the settings tab
   *
   *  @param       String       token      JWT used for authentication
   *
   *  @return      Timestamp               Return the Unix timestamp
   *
   */
   getExpiration : async function() {
     // Send a request to the server to fetch expiration time
     // method = 'POST', url, data = '', conType = 'json')
     let token = localStorage.getItem('token');
     let url   = apiUrl + "/subscriber/expire";
     let data  = {"token" : token};
     console.log("URL: " + url + "  Data: " + data);
     var response = await ajx.req('POST', url, data, 'json');
     return response;
   }

};
