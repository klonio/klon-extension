/**
 *
 *  Crawler is the object that will go out and find all the fields on a page
 *
 *  It is also the responsibility of crawler to determine what type of field he is
 *  dealing with and then to forward that field on to be formatted as such
 *
 *  Crawler also works closely with the settings to figure out what he is supposed
 *  to do
 *
 */
var crawler = {
  // Settings
  autofill : true,

  // Identity object
  firstName : '',
  middleInitial : '',
  lastName : '',
  road : '',
  city : '',
  DOB : '',
  username: '',
  email : '',
  emailConfirm : '',
  username : '',
  password : '',


  /**
   *
   *  Determine the different forms on the page and target the one we want
   *
   *
   */
  targetForms : function(target) {
    // Get all of the forms on the
    var forms = document.querySelectorAll('form');
    // Convert the NodeList into an Array
    forms = Array.prototype.slice.call(forms);
    // Create our new Array that will store the valid form values (forms that are visible)
    var validForms = new Array();
    // Make sure that the form(s) is visible to the user
    for (let k = 0; k < forms.length; k++) {
      if (forms[k].offsetParent === null) {
        continue;
      } else {
        validForms.push(forms[k]);
      }
    }
    // Re-assign the value of forms to only the visible forms
    forms = validForms;
    //
    var fbForm = new RegExp(/fb|facebook|google/i);
    var signup = new RegExp(/signup|sign.up|^reg|register|registration|\bnew./i);
    var login  = new RegExp(/login|log.in|^log|sign.in|signin/i);
    //
    for (var i = 0; i < forms.length; i++) {
      switch (target) {
        case "signup" :
          console.log("Form: ", forms[i]);
          for (var n = 0; n < forms[i].attributes.length; n++) {
            if (fbForm.test(forms[i].attributes[n].value)) {
              break;
            }
            if (signup.test(forms[i].attributes[n].value)) {
              return forms[i];
            }
          }
          break;
        case "login" :
          console.log("Form: ", forms[i]);
          for (var n = 0; n < forms[i].attributes.length; n++) {
            if (fbForm.test(forms[i].attributes[n].value)) {
              break;
            }
            if (login.test(forms[i].attributes[n].value)) {
              console.log("FORMS RETURNED: ", forms[i]);
              return forms[i];
            }
          }
          break;
        default:
          return false;
      }
    }
  },


  /**
   *
   *  crawler.seek();
   *  Gets all of the <inputs> on a page and returns them for later investigation
   *
   *  @param    target     String        "login" or "signup"
   *
   *  @return
   *
   */
  seek : function(target = "login", form = null) {
    //
    if (form == null) {
      var intel = this.targetForms(target);
      if (intel === null || intel === undefined || intel === false) {
        intel = document;
      }
    } else {
      intel = form;
    }
    //
    switch (target) {
      case 'signup' :
        var inputs = intel.querySelectorAll('input:not([type=hidden]):not([type=submit]):not([type=reset]):not([type=color]):not([type=search]):not([type=button]):not([type=image]):not([type=file]):not([type=range]), select');
        break;
      case 'login' :
        var inputs = intel.querySelectorAll('input:not([type=hidden]):not([type=submit]):not([type=reset]):not([type=color]):not([type=search]):not([type=button]):not([type=image]):not([type=file]):not([type=range]), select');
        break;
      case false :
      var inputs  = document.querySelectorAll('input:not([type=hidden]):not([type=submit]):not([type=reset]):not([type=color]):not([type=search]):not([type=button]):not([type=image]):not([type=file]):not([type=range]), select');
        break;
      default :
      var inputs  = document.querySelectorAll('input:not([type=hidden]):not([type=submit]):not([type=reset]):not([type=color]):not([type=search]):not([type=button]):not([type=image]):not([type=file]):not([type=range]), select');
        break;
    }
    return inputs;
  },


  /**
   *
   *  Identify the purpose of this field
   *
   */
  identify : function(input) {

    // Check if name field
    var isName = this.isNameField(input);
    if (isName != false) {
      return "Name: " + isName;
    }

    // Check if email address field
    var isEmail = this.isEmailField(input);
    if (isEmail != false) {
      return "Email: " + isEmail;
    }

    // Check if Address field
    var isAddress = this.isAddressField(input);
    if (isAddress != false) {
      return "Address: " + isAddress;
    }

    //
    var isPassword = this.isPasswordField(input);
    if (isPassword != false) {
      return "Password: " + isPassword;
    }

    // Check if Date of Birth input
    var isDob = this.isDobField(input);
    if (isDob != false) {
      return "Date: " + isDob;
    }

    return "No clue.";
  },


  /**
   *
   *  Determine if a field is required
   *
   */
  isRequired : function(input) {
    if (input.required == "true") {
      return true;
    } else {
      var req = input.getAttribute('aria-required');
      if (req != null) {
        req = req.toLowerCase();
        if (req == "true") {
          return true;
        } else {
          return false;
        }
      }
    }
    return false;
  },


  /**
   *
   *  Test each input
   *
   *  @param   input    HTMLObject
   *  @param   regex    Array             Array of regular expressions to test against
   *                                      Example: regex: 0 => /^autocomplete$/i, 1 => /^otherRegExToFollow$/
   *
   *  @return           Boolean
   *
   */
  isField : function(input, regex) {
    // Turn the input attributes object into an array to iterate through
    var attrs = Array.prototype.slice.call(input.attributes);
    // Loop through each attribute to determine what it is
    for (var i = 0; i < attrs.length; i++) {
      for (var n = 0; n < regex.length; n++) {
        // If the current attribute is the "id"
        if (attrs[i].name == "id") {
          // Search the document for the corresponding <label>
          var inputLabel = document.querySelector("label[for=" + attrs[i].value + "]");
          // If the label is found...
          if (inputLabel != undefined && inputLabel != null) {
            // Test the innerHTML of the <label> item against the Regular Expression
            if (regex[n].test(inputLabel.innerHTML)) {
              return input;
            }
          }
        }
        if (regex[n].test(attrs[i].value)) {
          return input;
        }
      }
    }
    return null;
  },


  /**
   *
   *  Translates the input fields into parts of an identity object
   *
   *  @param      input     HTMLObject   HTML Element to be translated
   *  @param      filter    String       Filter type
   *  @param      identity  Object       Identity object
   *
   *  @return                          Puts the object into the proper identity object
   *
   */
  translateField : function(input, filter, identity) {
    // Get the attribute values of the <input> element in the form of an array then the object into a string to be easily tested against for regular expressions
    var inputStr = Array.from(input.attributes).map(a => a.value).toString();
    switch (filter) {
      case "name" :
        var regexFullName   = new RegExp(/customername|fullname|full.name|\bname\b|first.last/i);
        var regexFirstName  = new RegExp(/given.name|firstname|first.name|\bfname|\bname.f\b/i);
        var regexMiddleName = new RegExp(/additional.name|middleinitial|^mi$|middlename|middle.name|mname/i);
        var regexLastName   = new RegExp(/family.name|lastname|last.name|\blname|\bname.l\b/i);

        // Check if firstName
        if (regexFirstName.test(inputStr)) {
          identity.firstName = input;
          return true;
        }
        //
        if (regexMiddleName.test(inputStr)) {
          identity.middleInitial = input;
          return true;
        }
        //
        if (regexLastName.test(inputStr)) {
          identity.lastName = input;
          return true;
        }
        //
        if (regexFullName.test(inputStr)) {
          identity.fullName = input;
          return true;
        }
        break;

      case "gender" :
        var regexGender  = new RegExp(/sex|gender/i);
        var regexGenderM = new RegExp(/\bmale/i);
        var regexGenderF = new RegExp(/female/i);
        var inputType    = input.getAttribute('type');
        var inputId      = input.getAttribute("id");
        if (regexGender.test(inputStr)) {
          if (regexGenderM.test(inputStr)) {
            // console.log('Male');
            identity.genderM = input;
            return true;
          }
          if (regexGenderF.test(inputStr)) {
            // console.log('Female');
            identity.genderF = input;
            return true;
          }
          if (inputType == "radio" && inputId != null) {
            var label = document.querySelector('[for=' + inputId + ']');
            if (regexGenderF.test(label.innerText)) {
              identity.genderF = input;
              return true;
            }
            if (regexGenderM.test(label.innerText)) {
              identity.genderM = input;
              return true;
            }
          }
          identity.gender = input;
          return true;
        }
        break;

      case "address" :
        // Example: 123 Main Street
        var regexStreetAddress = new RegExp(/street.address|streetaddress|^staddress|address.line1/i);
        // NY
        var regexState         = new RegExp(/address.level1|state/i);
        // Buffalo
        var regexCity          = new RegExp(/address.level2|city|town|village/i);
        // 80001
        var regexZipCode       = new RegExp(/zip|postal.code|postalcode/i);
        //
        if (regexStreetAddress.test(inputStr)) {
          identity.streetAddress = input;
          return true;
        }
        if (regexState.test(inputStr)) {
          identity.state = input;
          return true;
        }
        if (regexCity.test(inputStr)) {
          identity.city = input;
          return true;
        }
        if (regexZipCode.test(inputStr)) {
          identity.zip = input;
          return true;
        }
        break;

      case "email" :
        var regexEmail = new RegExp(/email|e.mail/i);
        var fieldType = input.getAttribute('type');
        if (regexEmail.test(inputStr)) {
          if (fieldType != null && (fieldType == "text" || fieldType == "email")) {
            if (identity.email == null || identity.email == undefined || identity.email == "") {
              identity.email = input;
            } else {
              identity.emailConf = input;
            }
            return true;
          }
        }
        break;

      case "username" :
        var regExUsername = new RegExp(/username|user.name/i);
        var fieldType     = input.getAttribute('type');
        var inputId       = input.getAttribute('id');
        if (fieldType != null && fieldType == "text") {
          if (inputId != null && inputId != undefined && inputId != "") {
            var inputLabel = document.querySelector("label[for=" + inputId + "]");
            if (inputLabel != null && inputLabel != undefined) {
              if (regExUsername.test(inputLabel.innerHTML)) {
                identity.username = input;
                return true;
              }
            }
          }
          if (regExUsername.test(inputStr)) {
            identity.username = input;
            return true;
          }
        }
        break;

      case "password" :
        var regexPassword = new RegExp(/new.password|^password$|^passwd|register.password|sign.up.password|\bpassword/i);
        if (regexPassword.test(inputStr)) {
          if (identity.password == null || identity.password == undefined || identity.password == '') {
            identity.password = input;
          } else {
            identity.passwordConf = input;
          }
          return true;
        }
        break;

      case "dob" :
        var regexDob      = new RegExp(/^bday$|^dob$|^birthday$/i);
        var regexDobMonth = new RegExp(/bday.month|dobmonth|dob.month|birthday.month|birth.month|birthmonth/i);
        var regexDobDay   = new RegExp(/bday.day|dobday|dob.day|birthday.day|birth.day/i);
        var regexDobYear  = new RegExp(/bday.year|dobyear|dob.year|birthday.year|birthyear|birth.year/i);
        if (regexDob.test(inputStr)) {
          identity.dob = input;
          return true;
        }
        if (regexDobMonth.test(inputStr)) {
          identity.dobMonth = input;
          return true;
        }
        if (regexDobDay.test(inputStr)) {
          identity.dobDay = input;
          return true;
        }
        if (regexDobYear.test(inputStr)) {
          identity.dobYear = input;
          return true;
        }
        break;
      return false;
    }
  },


  /**
   *
   *  Based on the URL determine if we're signing up or registering
   *
   *  @param   url    String           URL to be checked
   *
   *  @return         String|Boolean   Returns signup or login or false
   *
   */
  signupRegisterURL : function(url) {
    //
    var signup = new RegExp(/signup|sign.up|^reg|register|registration|join/i);
    var login  = new RegExp(/login|log.in|^log|sign.in/i);
    //
    if (signup.test(url)) {
      return "signup";
    } else if (login.test(url)) {
      return "login";
    } else {
      return false;
    }
  }

}
