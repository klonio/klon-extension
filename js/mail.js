var mail = {

  // Initialize an empty inbox object
  inbox : null,
  // Initialize an empty spam object
  spam  : null,
  // Initialize an empty spam object
  trash : null,
  // Initialize an empty spam object
  sent  : null,


  /**
   *
   *  Pull the email address from the mailfrom property
   *  Example:   John Doe <jdoe@klonmail.com>
   *  Becomes:   jdoe@klonmail.com
   *
   *  @return       String | Boolean         Returns sender or false if not found
   *
   */
  getEmailFromSender : function(sender = null) {
    // Make sure the sender is set
    if (sender !== null && sender !== undefined && sender.length > 0) {
      // If the string is base64 encoded, decode it
      if (base64.isEncoded(sender)) {
        var sender = window.atob(sender);
      } else {
        var sender = sender;
      }
      // Find the first instance of "<" -- This character tells us the beginning of the email address
      var senderOpening = sender.indexOf("<");
      var senderClosing = sender.indexOf(">");
      // If the characters were found in the string...
      if (senderOpening !== -1 && senderClosing !== -1) {
        // extract everything between the brackets, <>
        sender = sender.slice(++senderOpening, senderClosing);
      }
      sender = sender.trim();
      return sender;
    } else {
      return false;
    }
  },


  /**
   *
   *  Get the mail for the user's inbox
   *
   *
   */
  getMail : function(mailBox = "inbox") {
    return new Promise(function(resolve, reject) {
      // Get our token to send with the request
      var token = localStorage.getItem('token');
      // Format the data to send
      var str   = {"token" : token};
      // Generate the payload to be sent
      var payload = formatPayload(str).then(payload => {
        if (typeof payload == "object") {
          console.log("does not = object must stringify @ getMail");
          var strPayload = JSON.stringify(payload);
        } else {
          console.log("is string @ getMail");
          strPayload = payload;
        }
        var url = apiUrl + "/mail/getMail/" + mailBox;
        var results = ajx.req("POST", url, strPayload, "json");
        results.then(result => {
          // Set storage item
          localStorage.setItem(mailBox, result.message);
          console.log("@getMail Results: ", result.message);
          resolve(true);
        })
        .catch(err => {
          reject(err);
        });
      }).catch(err => {
        reject(err);
      });
    });
  },


  /**
   *
   *  Parse the incoming emails to be displayed to the user in a readable manner
   *
   */
  parseMail : async function(mailBox = "inbox") {
    // Check to see if the mail is sitting in localStorage
    if (localStorage.getItem(mailBox) === null) {
      // Get the mail for the user
      var getMail = await this.getMail(mailBox);
    }
    // Now that we are sure there is mail for the user let's parse the mail
    var mail = localStorage.getItem(mailBox);
    // Make sure there are messages to be parsed
    if (mail != "no messages") {
      if (base64.isEncoded(mail)) {
        // Base64 decode the data
        mail = window.atob(mail);
      }
      // Find the location of the pipe character
      var pipeIndex = mail.indexOf("|");
      // Split the columns and data apart
      var columns   = mail.substr(0, pipeIndex);
      // Split columns into an array
      var colNames  = columns.split(",");
      // Get the messages
      var messages  = mail.substr(++pipeIndex);
      // Split all the individual messages into an array
      messages      = messages.split("|");
      // Setup the message array within the object
      var msgDetails   = '{"message" : [';
      // Loop through the messages
      for (let i = 0; i < messages.length; i++) {
        // Break up each message into their own fields
        var messageFields = messages[i].split(",");
        // Loop through each field within the message and assign the key:value relationship
        for (let n = 0; n < messageFields.length; n++) {
          if (n === 0) {
            msgDetails = msgDetails + "{" + '"' + colNames[n].trim() + '"' + " : " + '"' + messageFields[n] + '"' + ",";
          } else if (n === (messageFields.length - 1)) {
            msgDetails = msgDetails + '"' + colNames[n].trim() + '"' + " : " + '"' + messageFields[n] + '"},';
          } else {
            msgDetails = msgDetails + '"' + colNames[n].trim() + '"' + " : " + '"' + messageFields[n] + '"' + ",";
          }
        }
      }
      // Remove the last comma
      msgDetails = msgDetails.substr(0, (msgDetails.length - 1));
      // console.log("msgDetails: ", msgDetails);
      // Add the closing bracket for the JSON object
      msgDetails = msgDetails + "]}";
      // Convert the thing to a JSON object
      msgDetails = JSON.parse(msgDetails);
      // Set the proper object values based on the mailbox
      localStorage.setItem("mail", JSON.stringify(msgDetails.message));
      // Return the data
      return msgDetails;
    } else {
      localStorage.setItem("mail", false);
      return false;
    }
  },


  /**
   *
   *  Check with the server to see if there are new messages for the user
   *
   *  @param      mailBox     String      The mailbox the user wants to check for new messages in (inbox, spam, sent, trash)
   *
   *  @return                 Boolean     Returns true or false
   *
   */
  checkMail : function(mailBox = "inbox") {
    return new Promise(function(resolve, reject) {
      // Hit the API /mail/checkMail/{mailbox} -- {token: etergERTG3453efdf, id: 4}
      // API returns TRUE or FALSE based on the 'id'
      // Return this value
      var box;
      // Check to see if we have mail for this mailbox
      switch(mailBox) {
        case "inbox":
          box = JSON.parse(localStorage.getItem("mail")) || null;
        break;
        case "sent":
          box = JSON.parse(localStorage.getItem("mail")) || null;
        break;
        case "spam":
          box = JSON.parse(localStorage.getItem("mail")) || null;
        break;
        case "trash":
          box = JSON.parse(localStorage.getItem("mail")) || null;
        break;
      }
      // console.log("Box: ", box);
      if (typeof box === "object" && box !== null) {
        if (box.length > 0) {
          // Get the id of the last email
          var id = Number(box[box.length - 1].id);
          // Get our token to send with the request
          var token = localStorage.getItem('token');
          // Format the data to send
          var str   = {"token" : token, "id" : id};
          // console.log("ID: ", id);
          // Generate the payload to be sent
          var payload = formatPayload(str).then(payload => {
            if (typeof payload != "object") {
              var strPayload = JSON.stringify(payload);
            } else {
              strPayload = payload;
            }
            console.log("Payload: ", strPayload);
            // Send this ID with the request to the server
            var url = apiUrl + "/mail/checkMail/" + mailBox;
            var results = ajx.req("POST", url, strPayload, "json");
            results.then(function(result) {
              // console.log("Results: ", result.message);
              resolve(result.message);
            })
            .catch(err => {
              reject(err);
            });
          }).catch(err => {
            reject(err);
          });
        } else {
          // Need to get mail for this mailbox first
          console.log("Error: Get mail first");
          reject("Error: get mail first");
        }
      } else {
        resolve(false);
      }
    });
    resolve(false);
  },


  /**
   *
   *  Bob Dole
   *
   */
  bindMailItem : function() {
    var mailItems = document.querySelectorAll(".message__details");
    for (let i = 0; i < mailItems.length; i++) {
      mailItems[i].addEventListener("click", mail.readMessage);
    }
  },


  /**
   *
   *  The functionality behind the mail navigation buttons
   *
   */
  mainNavFunction : function(evt) {
    // Remove all the "selected" classes from the other nav buttons
    var navBtns = document.querySelectorAll(".mail__navItem");
    for (let i = 0; i < navBtns.length; i++) {
      if (navBtns[i].classList.contains("mail__navItem--selected")) {
        navBtns[i].classList.remove("mail__navItem--selected");
      }
    }
    // Add the "selected" class to the current mailbox
    if (!evt.target.classList.contains("mail__navItem--selected")) {
      evt.target.classList.add("mail__navItem--selected");
    }
    // mailbox is the mail we want to retrieve
    var mailBox;
    // Messages is an array that contains json objects of all of the unencrypted data to display to the user
    var messages;
    // Get the inner text.. ie "Inbox" and turn it into the lowercase version of itself
    var mailBoxText = evt.target.innerText.toLowerCase();
    // Parse Mail
    var parsedMail  = mail.parseMail(mailBoxText);
    parsedMail
    .then(result => {
      // Get the "mail" object from localStorage
      messages = JSON.parse(localStorage.getItem("mail")) || null;
      switch(mailBoxText) {
        case "inbox":
          // console.log("This is the inbox button");
          mailBox = localStorage.getItem("inbox") || null;
          localStorage.setItem("mailboxCurrent", "inbox");
        break;
        case "sent":
          console.log("This is the sent messages");
          localStorage.setItem("mailboxCurrent", "sent");
        break;
        case "spam":
          // console.log("This is the spam messages");
          mailBox = localStorage.getItem("spam") || null;
          localStorage.setItem("mailboxCurrent", "spam");
        break;
        case "trash":
          // console.log("This is the trash messages");
          mailBox = localStorage.getItem("trash") || null;
          localStorage.setItem("mailboxCurrent", "trash");
        break;
      }
      // Check to see if we have mail for this mailbox
      if (mailBox) {
        console.log("Mail Box Text: " + mailBoxText);
        // Check to see if there are new messages for the inbox
        var cm = mail.checkMail(mailBoxText);
        cm.then(hasNewMail => {
          // If the user has new mail, get it and display it
          if (hasNewMail == true) {
            var gm = mail.getMail(mailBoxText);
            //
            gm.then(result => {
              var pm = mail.parseMail(mailBoxText);
              //
              pm.then(results => {
                var dm = mail.displayMail(messages);
                //
                dm.then(result => {
                  mail.bindMailItem();
                  return true;
                }).catch(err => {
                  console.log("Error: ", err);
                  return false;
                });
              }).catch(err => {
                console.log("Error: ", err);
                return false;
              });
            }).catch(err => {
              console.log("Error: ", err);
              return false;
            });
          } else {
            // No new mail just display what we have
            var dm = mail.displayMail(messages);
            dm.then(result => {
              mail.updateMail();
              mail.bindMailItem();
              return true;
            }).catch(err => {
              console.log("Error: ", err);
              return false;
            });
          }
        }).catch(err => {
          console.log("Error: ", err);
          return false;
        });
      } else {
        console.log("<--MailBox: ", mailBox);
        // Get the mail for the box, parse it, then display it.
        var gm = mail.getMail(mailBoxText);
        gm.then(results => {
          var pm = mail.parseMail(mailBoxText);
          pm.then(results => {
            var dm = mail.displayMail(messages);
            dm.then(result => {
              mail.bindMailItem();
              return true;
            }).catch(err => {
              console.log("Error: ", err);
              return false;
            });
          }).catch(err => {
            console.log("Error: ", err);
            return false;
          });
        }).catch(err => {
          console.log("Error: ", err);
          return false;
        });
      }
      return true;
    })
    .catch(err => {
      console.log('Error: ', err);
      return false;
    });
  },


  /**
   *
   *  Bind functionality to the main nav buttons ie (Inbox, Sent, Spam, Trash)
   *
   */
  bindMainNav : function() {
    // Bind the inbox button functionality
    var mainNavBtns = document.querySelectorAll(".mail__navItem");
    for (let i = 0; i < mainNavBtns.length; i++) {
      console.log("Binding click event to: ", mainNavBtns[i]);
      mainNavBtns[i].addEventListener("click", mail.mainNavFunction);
    }
  },


  /**
   *
   *  Show the secondary nav in place of the main mail nav. This menu changes as the mailbox changes
   *
   *
   */
  showSecondaryNav : function(theCount) {
    // Determine if this nav is already present
    var subNavHandle = document.querySelector("#mailSubNav");
    if (subNavHandle) {
      if (Number(theCount) != 1) {
        // If there are multiple messages checked, remove the "Reply" and "Forward" buttons if they exist
        var subNavItems = document.querySelectorAll(".mail__subNavItem");
        for (let i = 0; i < subNavItems.length; i++) {
          var regEx = /(reply)|(forward)/ig;
          if (subNavItems[i].innerText.match(regEx) || subNavItems[i].innerText.match(regEx)) {
            subNavItems[i].parentNode.removeChild(subNavItems[i]);
          }
        }
      }
      //
      return true;
    }
    var mailbox = localStorage.mailboxCurrent;
    // Determine which mailbox we are in
    switch (mailbox) {
      case "inbox" :
        var subnavText  = ["Spam","AllSpam","Delete","Unread"];
        var subnavIcon  = ["mail-spam-envelope","mail-global-spam","mail-delete", "mail-unread"];
        var subnavTitle = ["Mark spam for this address","Mark spam for all email addresses","Delete","Mark Unread"];
      break;
      //
      case "sent" :
      var subnavText  = ["Delete"];
      var subnavIcon  = ["mail-delete"];
      var subnavTitle = ["Delete"];
      break;
      //
      case "spam" :
      var subnavText  = ["Not Spam","Delete"];
      var subnavIcon  = ["mail-not-spam", "mail-delete"];
      var subnavTitle = ["Mark Not Spam","Delete"];
      break;
      //
      case "trash" :
      var subnavText  = ["Put Back","Delete"];
      var subnavIcon  = ["mail-move-inbox", "mail-delete"];
      var subnavTitle = ["Move to Inbox","Delete"];
      break;
    }
    // Destroy the main mail nav
    var mainMailNav = document.querySelector("#mailNav");
    mainMailNav.parentNode.parentNode.removeChild(mainMailNav.parentNode);
    // Build the DOM tree for the subnav
    var subNavWrapper = document.createElement("div");
    //
    var subNav = document.createElement("ul");
    subNav.setAttribute("id", "mailSubNav");
    subNav.className = "mail__nav mail__nav--subNav";
    //
    for (let i = 0; i < subnavText.length; i++) {
      var subNavItem = document.createElement("li");
      subNavItem.className = "mail__subNavItem";
      //
      var subNavIcon = document.createElement("img");
      //
      var subNavText = document.createElement("div");
      // Set the titles and labels
      subNavIcon.setAttribute("alt", subnavTitle[i]);
      subNavIcon.setAttribute("title", subnavTitle[i]);
      subNavIcon.setAttribute("aria-label", subnavTitle[i]);
      // Set the image source
      subNavIcon.setAttribute("src", "images/" + subnavIcon[i] + ".svg");
      // Set the image size
      subNavIcon.setAttribute("height", "18");
      subNavIcon.setAttribute("width", "18");
      // Set the css class
      subNavIcon.className = "mail__navIcon";
      //
      //  S E T U P   T E X T   N O D E
      //
      // Add the proper class name to the nav text
      subNavText.className = "mail__navText";
      // Set the inner text of the element
      subNavText.innerText = subnavText[i];
      // Now we're good to append the node to the DOM

      // Append the image to the subNavItem
      subNavItem.appendChild(subNavIcon);
      // Append the text to the subNavItem
      subNavItem.appendChild(subNavText);
      // Append the subNavItem to the subNav
      subNav.appendChild(subNavItem);
    }
    // Append the subNav to the subNavWrapper
    subNavWrapper.appendChild(subNav);
    // Get the #mailbox element
    var mbox = document.querySelector("#mailbox");
    // Append the subNavWrapper to the body
    mbox.insertAdjacentElement("beforebegin",subNavWrapper);
    mail.bindFunctionToSubnav();
  },


  /**
   *
   *  Bind functionality to the subNavMenu in the mail tab
   *  This will loop through and bind an action to the particular nav item
   *  Based on the Text within the thing
   *
   *  @param      NONE
   *  @return     Boolean       True|False based on success of the function
   *
   */
  bindFunctionToSubnav : function () {
    // Get a handle on the subNav
    var subNav = document.querySelector("#mailSubNav");
    // If the element exists within the document
    if (document.body.contains(subNav)) {
      // Get a handle on all of the listItems contained inside
      var navItems = subNav.querySelectorAll("li.mail__subNavItem");
      if (navItems.length > 0) {
        // Loop through the navItems and assign functions to them
        for (let i = 0; i < navItems.length; i++) {
          // Get the text inside the item
          var navItemText = navItems[i].querySelector(".mail__navText").innerText;
          // Make sure there is some text to read
          if (navItemText !== undefined && navItemText !== null && navItemText !== "") {
            // Trim the spaces and set to lowercase to make everything uniform
            navItemText = navItemText.toLowerCase().trim();
            switch(navItemText) {
              // What to do when the text is "put back"
              // Put the message back in the inbox -- remove from trash
              case "put back":
                navItems[i].addEventListener("click", mail.notTrash);
                break;

              // Delete the message from the current mailbox unless the mailbox is
              // The trash box then delete from the database completely
              case "delete":
                navItems[i].addEventListener("click", mail.deleteMail);
                break;

              // Mark the message as spam
              case "spam":
                navItems[i].addEventListener("click", function() {
                  mail.markSpam(false);
                });
                break;

              // Mark the message as spam globally
              case "allspam":
                navItems[i].addEventListener("click", function() {
                  mail.markSpam(true);
                });
                break;

              // Mark a message as unread
              case "unread":
                navItems[i].addEventListener("click", mail.unreadMail);
                break;

              default:
                console.log("ionno");
                break;
            }
          } else {
            return false;
          }
        }
        return true;
      } else {
        // No nav items available
        return false;
      }
    } else {
      return false;
    }
  },


  /**
   *
   *  Display the main navigation (Inbox, Sent, Spam, Trash)
   *
   */
  showMainNav : function() {
    // Array of buttons
    var arrNavBtn = ["Inbox","Sent","Spam","Trash"];
    // Time to destroy the subnav and show the main nav
    var mainNavWrapper = document.createElement("div");
    //
    var mainNav        = document.createElement("ul");
    mainNav.className  = "mail__nav";
    mainNav.setAttribute("id", "mailNav");
    //
    for (let n = 0; n < arrNavBtn.length; n++) {
      var navItem = document.createElement("li");
      // Set the current mailbox as "selected" again so the user knows what mailbox they're in
      if (localStorage.mailboxCurrent !== arrNavBtn[n].toLowerCase().trim()) {
        navItem.className = "mail__navItem";
      } else {
        navItem.className = "mail__navItem mail__navItem--selected";
      }
      navItem.setAttribute("id", "btn" + arrNavBtn[n]);
      navItem.innerText = arrNavBtn[n];
      mainNav.appendChild(navItem);
    }
    // Append the list to the wrapper
    mainNavWrapper.appendChild(mainNav);
    // Get the #mailbox element
    var mbox = document.querySelector("#mailbox");
    // Append the subNavWrapper to the body
    mbox.insertAdjacentElement("beforebegin",mainNavWrapper);
    // Find the subnav and destroy it
    var subNav = document.querySelector("#mailSubNav");
    //
    subNav.parentNode.parentNode.removeChild(subNav.parentNode);
    // Rebind the mail functions
    mail.bindMainNav();
    return true;

  },


  /**
   *
   *  Loops through all of the checkboxes in the mailbox. If they are all unchecked then show the main nav
   *
   *  @return      Boolean         True if main nav is revealed, else False
   *
   */
  checkboxUnchecked : function() {
    // Check to see if the main nav is already showing...
    var mainNavExist = document.querySelector("#mailNav");
    // If it exists there's no reason to continue on, return false
    if (mainNavExist) {
      return false;
    }
    var checkbox = document.querySelectorAll("input[type=checkbox]");
    var checkTotal = 0;
    for (let i = 0; i < checkbox.length; i++) {
      if (checkbox[i].checked) {
        checkTotal++;
      }
      if (checkTotal >= 2) {
        return false;
      }
    }
    // If only 1 box is checked make sure "Reply" and "Forward" are present as options
    if (checkTotal === 1) {
      // Check to see if "Reply" or "Forward" is present
      var subNavItems = document.querySelectorAll(".mail__subNavItem");
      // Set booleans to false
      var hasReply   = false;
      var hasForward = false;
      // Define regular expressions
      var regExForward = /(forward)/ig;
      var regExReply   = /(reply)/ig;
      // Loops through all of the subnav items to see if any of them are "Reply" or "Forward"
      for (let i = 0; i < subNavItems.length; i++) {
        // If forward is found change boolean to true
        if (subNavItems[i].innerText.match(regExForward)) {
          hasForward = true;
        }
        // If reply is found change boolean to true
        if (subNavItems[i].innerText.match(regExReply)) {
          hasReply = true;
        }
      }
      // Get a handle on the subnav menu
      var subNavMenu = document.querySelector("#mailSubNav");
      // Prepend HTML if "Forward" is missing
      if (hasForward === false) {
        // var fwdHTML = '<li class="mail__subNavItem"><img alt="Forward" title="Forward" aria-label="Forward" src="images/mail-forward.svg" height="18" width="18" class="mail__navIcon"><div class="mail__navText">Forward</div></li>';
        // subNavMenu.innerHTML = fwdHTML + subNavMenu.innerHTML;
        subNavMenu.innerHTML = subNavMenu.innerHTML;
      }
      // Prepend HTML if "Reply" is missing
      if (hasReply === false) {
        // var reHTML = '<li class="mail__subNavItem"><img alt="Reply" title="Reply" aria-label="Reply" src="images/mail-reply.svg" height="18" width="18" class="mail__navIcon"><div class="mail__navText">Reply</div></li>';
        // subNavMenu.innerHTML = reHTML + subNavMenu.innerHTML;
        subNavMenu.innerHTML = subNavMenu.innerHTML;
      }
      return false;
    }
    mail.showMainNav();
  },


  /**
   *
   *
   */
  bindCheckboxFunction : function() {
    // Get all of the checkboxes on the page
    var checkbox = document.querySelectorAll("input[type=checkbox]");
    var checkCount = 0;
    // Loop through the checkboxes
    for(let i = 0; i < checkbox.length; i++) {
      checkbox[i].addEventListener("change", function() {
        if (this.checked) {
          checkCount++;
          mail.showSecondaryNav(checkCount);
        } else {
          // Loop through the checkboxes and determine if any are checked
          // If none are checked show the main nav again
          // Otherwise leave the subnav up
          checkCount--;
          mail.checkboxUnchecked();
        }
        console.log("Boxes Checked: ", checkCount);
      });
    }
  },


  /**
   *
   *  Update the mail (inbox, spam, trash, etc...) after an action has been completed.
   *  Example: If the user marks something as spam, it is time now to update the email
   *
   *  @param        Boolean        display       true||false       Wether or not to display the mail after updating
   *
   *  @return       Boolean                      true||false       Successful or not
   *
   */
  updateMail : function(display = true) {
    // If the mailboxCurrent is set get that value, otherwise its the "inbox"
    var mailbox = localStorage.mailboxCurrent || "inbox";
    // Remove all the old mail
    localStorage.removeItem("inbox");
    localStorage.removeItem("sent");
    localStorage.removeItem("spam");
    localStorage.removeItem("trash");
    // Get new mail
    var getmail = mail.getMail(mailbox);
    //
    getmail.then(res => {
      // Parse the mail that was received
      parsemail = mail.parseMail(mailbox);
      parsemail.then(res => {
        // Display the mail
        var objMessage = localStorage.mail || "no messages";
        if (objMessage != "no messages") {
          objMessage = JSON.parse(objMessage);
        }
        //
        if (display === true) {
          //
          displaymail    = mail.displayMail(objMessage);
          displaymail.then(res => {
            console.log("Update successful: ", res);
            return true;
          }).catch(err => {
            // Error handling routine Here
            return false;
          });
        } else {
          console.log("Update successful: ", res);
          return true;
        }
      }).catch(err => {
        // Error handling routine Here
        return false;
      });
    }).catch(err => {
      // Error handling routine Here
      return false;
    });
  },


  /**
   *
   *  Show the messages (list) to the user
   *
   *  Example: mail.displayMail(mail.inbox.message)
   *
   */
  displayMail : function(arrMail = null) {
    return new Promise(function(resolve, reject) {
      //
      if (arrMail === null || arrMail == false) {
        //
        var mailbox  = document.querySelector("#mailbox");
        if (!mailbox.classList.contains("mail__box--empty")) {
          mailbox.classList.add("mail__box--empty");
        }
        if (mailbox.classList.contains("mail__box--message")) {
          mailbox.classList.remove("mail__box--message");
        }
        mailbox.innerText = "No Messages";
        resolve(true);
      }
      if (arrMail) {
        var mailbox  = document.querySelector("#mailbox");
        if (arrMail.length > 0) {
          mailbox.innerHTML = "";
          mailbox.className = "mail__box";
        }
        for(let i = 0; i < arrMail.length; i++) {
          // Get the message we're current on in the loop
          var message = arrMail[i];
          // Create the node to add the element to the document
          var listItem = document.createElement("LI");
          // Add the necessary styles to the element
          listItem.classList.add("mail__boxMessage");
          // Include the message id in the dataSet for later reference
          listItem.dataset.mailId = message.id;
          // Include the message id in the dataSet for later reference
          listItem.dataset.mailNumber = i;
          listItem.dataset.mailfrom   = message.mailfrom;
          listItem.dataset.mailto     = message.mailto;
          listItem.dataset.subject    = message.subject;
          if (isNaN(Number(message.timestamp))) {
            // Get the time set for the correct timezone
            var tStamp                = new Date();
            var tOffset               = tStamp.getTimezoneOffset();
            var tMsOffset             = (tOffset * 60) * 1000;
            tStamp                    = new Date(message.timestamp).getTime() - tMsOffset;
            tStamp = tStamp / 1000;
            listItem.dataset.maildate = tStamp;
            message.timestamp         = tStamp;
          } else {
            listItem.dataset.maildate = message.timestamp;
          }

          // If the message is not read yet style it that way
          if (Number(message.is_read) === 0) {
            listItem.classList.add("mail__boxMessage--unread");
          }
          // Add the <div> to wrap around the checkbox
          var checkboxWrapper = document.createElement("div");
          checkboxWrapper.classList.add("message__checkbox");
          // Add the checkbox to the <div>
          var checkBox = document.createElement("input");
          checkBox.setAttribute("type", "checkbox");
          checkBox.setAttribute("name", "checkbox");
          //-----------------------------------------------------//
          var msgDetails = document.createElement("div");
          msgDetails.classList.add("message__details");
          //-----------------------------------------------------//
          var msgTitle = document.createElement("h3");
          msgTitle.classList.add("message__title");
          // If the string is base64 encoded, decode it
          if (base64.isEncoded(message.subject)) {
            msgTitle.innerText = window.atob(message.subject);
          } else {
            msgTitle.innerText = message.subject;
          }
          //-----------------------------------------------------//
          var msgSender = document.createElement("div");
          msgSender.classList.add("message__sender");
          // If the string is base64 encoded, decode it
          if (base64.isEncoded(message.mailfrom)) {
            var sender = window.atob(message.mailfrom);
          } else {
            var sender = message.mailfrom;
          }
          var senderIndex = sender.indexOf("<");
          if (senderIndex !== -1) {
            sender = sender.substr(0,senderIndex);
          }
          msgSender.innerText = sender.trim();
          //-----------------------------------------------------//
          // Get the current date/time to compare with the timestamp provided
          var currDateTime   = new Date();
          // Adjust for timezone...
          var timezoneOffset = currDateTime.getTimezoneOffset();
          // Convert minutes to seconds
          timezoneOffset     = timezoneOffset * 60;
          // Convert seconds to milliseconds
          timezoneOffset     = timezoneOffset * 1000;
          // Put the time difference into the currDateTime
          currDateTime = new Date(currDateTime.getTime() - timezoneOffset);
          var cDate  = currDateTime.getDate();
          var cMonth = currDateTime.getMonth();
          var cYear  = currDateTime.getFullYear();
          // Get the timestamp of the message
          var timestamp = new Date(message.timestamp * 1000);
          // Get the date/time to compare with the timestamp provided
          var date   = timestamp.getDate();
          var month  = timestamp.getMonth();
          var year   = timestamp.getFullYear();
          var dateText;
          // Compare the date/time
          if (cDate === date && cMonth === month && cYear === year) {
            // Show only the HH:MM here instead of date
            var hour = Number(timestamp.getHours());
            var ampm;
            // AM or PM
            if (hour - 12 > 0) {
              hour = hour - 12;
              ampm = " PM";
            } else if (hour - 12 === 0) {
              ampm = " PM";
            } else {
              ampm = " AM";
            }
            //
            if (hour < 10) {
              hour = String("0" + String(hour));
            }
            //
            var minute = timestamp.getMinutes();
            if (minute < 10) {
              minute = String("0" + String(minute));
            }
            dateText = hour + ":" + minute + ampm;
          } else {
            // Display the MONTH, DATE format
            // Setup the months array
            var arrMonths = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            month         = arrMonths[Number(timestamp.getMonth())];
            dateText      = month + ", " + date + " " + year;
          }
          // Create the element
          var msgDate = document.createElement("div");
          // Add the relevent class for styling
          msgDate.classList.add("message__date");
          // Insert the proper text
          msgDate.innerText = dateText;
          // Create the element
          var msgExpiration = document.createElement("div");
          msgExpiration.classList.add("message__expiration");
          msgExpiration.setAttribute("title", "Time left before message is deleted");
          // 3 days = 259200 seconds
          var msgExpirationIcon = document.createElement("div");
          msgExpirationIcon.classList.add("message__expirationIcon");
          //
          var msgExpirationIconImg = document.createElement("img");
          msgExpirationIconImg.setAttribute("src", "images/mail-expire.svg");
          msgExpirationIconImg.setAttribute("alt", "Expiration Icon -- Clock");
          msgExpirationIconImg.setAttribute("height", "12");
          msgExpirationIconImg.setAttribute("width", "12");
          //
          var msgExpirationText = document.createElement("div");
          msgExpirationText.classList.add("message__expirationText");
          // Substract current timestamp from the provided timestamp
          var daysRemaining =  ((message.timestamp * 1000) + 259200000) - Date.now();
          // Divide by the number of milliseconds in a day
          // 3600000 milliseconds in an hour
          daysRemaining = Math.floor(daysRemaining / 3600000);
          // If there is 1 hour left then change the text from "hours" to "hour"
          if (daysRemaining !== 1) {
            daysRemaining = String(daysRemaining + "hours");
          } else {
            daysRemaining = String(daysRemaining + "hour");
          }
          msgExpirationText.innerText = daysRemaining;
          // Start appending the elements to the DOM
          checkboxWrapper.appendChild(checkBox);
          listItem.appendChild(checkboxWrapper);
          //
          msgDetails.appendChild(msgTitle);
          msgDetails.appendChild(msgSender);
          listItem.appendChild(msgDetails);
          //
          listItem.appendChild(msgDate);
          //
          msgExpirationIcon.appendChild(msgExpirationIconImg);
          msgExpiration.appendChild(msgExpirationIcon);
          msgExpiration.appendChild(msgExpirationText);
          listItem.appendChild(msgExpiration);
          //
          mailbox.appendChild(listItem);
        }
      }
      // Bind functionality to the checkboxes
      mail.bindCheckboxFunction();
      //
      mail.highlightNav();
      // Bind the functionality to the mail navItems
      mail.bindMailItem();
      // Set the current page
      var sessionTab     = localStorage.setItem("sessionTab", "mail");
      //  Set the page action
      var sessionAction  = localStorage.setItem("sessionAction", "displayMail");
      resolve(true);
    });
  },


  /**
   *
   *  Make sure the proper navigation item is highlighted
   *
   */
  highlightNav : function() {
    var currentMailbox = localStorage.getItem("mailboxCurrent");
    if (currentMailbox) {
      // Setup the regular expression with global and case insensitive flags
      var regExMailbox   = new RegExp(currentMailbox, "ig");
      // Get all the nav items
      var navItems       = document.querySelectorAll(".mail__navItem");
      // Loop through all nav items
      for (let n = 0; n < navItems.length; n++) {
        if (navItems[n].innerText.match(regExMailbox)) {
          // If it doesn't have the "selected" class then add it
          if (!navItems[n].classList.contains("mail__navItem--selected")) {
            navItems[n].classList.add("mail__navItem--selected");
          }
        } else {
          // If the "selected" class is found where it should not be then remove it
          if (navItems[n].classList.contains("mail__navItem--selected")) {
            navItems[n].classList.remove("mail__navItem--selected");
          }
        }
      }
    }
    return true;
  },


  /**
   *
   *  Get message contents -- ie Open a message
   *
   */
  readMessage : function(evt) {
    return new Promise(function(resolve, reject) {
      // Get the users role
      var userRole = role.getRole();
      userRole.then(userRoleResult => {
        //
        // mail.inbox.message || JSON.parse(localStorage.getItem("mail"))
        //
        var listItem    = evt.target.closest(".mail__boxMessage").dataset.mailId;
        var mailNumber  = evt.target.closest(".mail__boxMessage").dataset.mailNumber;
        var mailSubject = evt.target.closest(".mail__boxMessage").dataset.subject;
        var mailFrom    = evt.target.closest(".mail__boxMessage").dataset.mailfrom;
        var mfIndexOf   = mailFrom.indexOf("<");
        // mailFrom    = mailFrom.substr(0,mfIndexOf).trim();
        var mailTo      = evt.target.closest(".mail__boxMessage").dataset.mailto;
        var mailDate    = evt.target.closest(".mail__boxMessage").dataset.maildate;
        // console.log("MsgId: ", listItem);
        // Get our token to send with the request
        var token = localStorage.getItem('token');
        // Format the data to send
        var str   = {"token" : token};
        // Generate the payload to be sent
        var payload = formatPayload(str).then(payload => {
          if (typeof payload != "object") {
            var strPayload = JSON.stringify(payload);
          } else {
            strPayload = payload;
          }
          console.log("strPayload: ", strPayload);
          if (localStorage.mailboxCurrent === "sent") {
            console.log("MAILBOX: ", localStorage.mailboxCurrentx);
            var url = apiUrl + "/mail/getMessage/" + listItem + "/sent";
          } else {
            var url = apiUrl + "/mail/getMessage/" + listItem;
          }
          var results = ajx.req("POST", url, strPayload, "json");
          results.then(function(result) {
            console.log("getMessage: ", result);
            mail.content = result.message;
            mail.msgId   = listItem;
            var mailbox  = document.querySelector("#mailbox");
            mailbox.className = "mail__box mail__box--message"
            mailbox.innerHTML = "";
            //
            mailbox.classList.add("mail__box--message");
            // Create the elements required to display a nicely styled email
            var li = document.createElement("li");
            li.classList.add("mail__messageHeader");
            //
            var headerSub = document.createElement("div");
            headerSub.classList.add("message__headerSubject");
            //
            var headerSubLabel = document.createElement("span");
            headerSubLabel.innerText = "Subject: ";
            var headerSubText  = document.createElement("span");
            headerSubText.innerText  = window.atob(mailSubject);
            headerSubText.setAttribute("id", "messageSubject");
            //
            var headerFrom = document.createElement("div");
            headerFrom.classList.add("message__headerData");
            var headerFromLabel = document.createElement("span");
            headerFromLabel.innerText = "From: ";
            var headerFromText = document.createElement("span");
            headerFromText.innerText = window.atob(mailFrom);
            headerFromText.setAttribute("id", "messageMailFrom");
            //
            var headerTo = document.createElement("div");
            headerTo.classList.add("message__headerData");
            var headerToLabel = document.createElement("span");
            headerToLabel.innerText = "To: ";
            var headerToText = document.createElement("span");
            headerToText.innerText = window.atob(mailTo);
            headerToText.setAttribute("id", "messageMailTo");
            //
            var headerDate = document.createElement("div");
            headerDate.classList.add("message__headerData");
            var headerDateLabel = document.createElement("span");
            headerDateLabel.innerText = "Date: ";
            var headerDateText = document.createElement("span");
            headerDateText.innerText = new Date(mailDate * 1000);
            headerDateText.setAttribute("id", "messageTimestamp")
            // Add message__message class
            var messageMsg = document.createElement("li");
            messageMsg.classList.add("message__message");
            // Add message__navigation
            var messageNav = document.createElement("li");
            messageNav.classList.add("message__nav");
            // Add reply button to the message nav
            var messageNavReply = document.createElement("div");
            messageNavReply.classList.add("message__navItem");
            messageNavReply.setAttribute("title", "Reply");
            messageNavReply.innerHTML = "<img alt='Reply' aria-label='Reply' src='images/mail-reply.svg' title='Reply' href='#' height='24' width='24'>";
            // Add forward button to the message nav
            var messageNavForward = document.createElement("div");
            messageNavForward.classList.add("message__navItem");
            messageNavForward.setAttribute("title", "Forward");
            messageNavForward.innerHTML = "<img alt='Forward' aria-label='Forward' src='images/mail-forward.svg' title='Forward' href='#' height='24' width='24'>";
            //
            var msgContents = document.createElement("div");
            msgContents.classList.add("message__contents");
            msgContents.innerHTML = window.atob(mail.content);
            // Append the elements to the DOM
            //
            // Append headerSubject
            headerSub.appendChild(headerSubLabel);
            headerSub.appendChild(headerSubText);
            li.appendChild(headerSub);
            //
            headerFrom.appendChild(headerFromLabel);
            headerFrom.appendChild(headerFromText);
            li.appendChild(headerFrom);
            //
            headerTo.appendChild(headerToLabel);
            headerTo.appendChild(headerToText);
            li.appendChild(headerTo);
            //
            headerDate.appendChild(headerDateLabel);
            headerDate.appendChild(headerDateText);
            li.appendChild(headerDate);
            //
            mailbox.appendChild(li);
            //
            messageMsg.appendChild(msgContents);
            messageNav.appendChild(messageNavReply);
            messageNav.appendChild(messageNavForward);
            mailbox.appendChild(messageMsg);
            mailbox.appendChild(messageNav);
            //
            var btnReply   = document.querySelector(".message__navItem[title='Reply']");
            var btnForward = document.querySelector(".message__navItem[title='Forward']");
            //
            switch (userRoleResult) {
              // The Void  --  No fun for you
              case 0:
                credential.forceLogin();
                reject(false);
              break;
              // Basic Account
              case 1:
                // When the user clicks Reply or Forward prompt them to upgrade their account
                if (document.body.contains(btnReply)) {
                  btnReply.addEventListener("click", function() {prompt.upgradeAccount()});
                }
                // Forward button functionality based on user role
                if (document.body.contains(btnForward)) {
                  btnForward.addEventListener("click", function() {prompt.upgradeAccount("regular")});
                }
              break;
              // Regular Account
              case 2:
                // Add in the forward option -- When the user clicks the "reply" button they are prompted to upgrade to a premium account
                if (document.body.contains(btnReply)) {
                  btnReply.addEventListener("click", function() {prompt.upgradeAccount()});
                }
                if (document.body.contains(btnForward)) {
                  btnForward.addEventListener("click", function(e) {mail.forward(e)});
                }
              break;
              // Premium Account
              case 3:
                // Add in the forward and reply buttons
                if (document.body.contains(btnReply)) {
                  btnReply.addEventListener("click", mail.makeReplyPopUp);
                }
                // Forward button functionality based on user role
                if (document.body.contains(btnForward)) {
                  btnForward.addEventListener("click", function(e) {mail.forward(e)});
                }
              break;
              // Trial Account -- We now trial a premium account
              // case 9:
                // Add in the forward and reply buttons -- But they only say "Sign up for premium for this feature"
                // if (document.body.contains(btnReply)) {
                //   btnReply.addEventListener("click", function() {prompt.upgradeAccount()});
                // }
                // Forward button functionality based on user role
                // if (document.body.contains(btnForward)) {
                //   btnForward.addEventListener("click", function() {prompt.upgradeAccount("regular")});
                // }
              // break;
            }
            // Update data where applicable
            try {
              // If mailboxCurrent is found in the localStorage
              if ("mailboxCurrent" in localStorage) {
                // Get the current mailbox and edit the message inside to be read
                var rawMail;
                switch(localStorage.mailboxCurrent) {
                  case "inbox":
                  rawMail = localStorage.inbox;
                  break;
                  case "sent":
                  rawMail = localStorage.sent;
                  break;
                  case "spam":
                  rawMail = localStorage.spam;
                  break;
                  case "trash":
                  rawMail = localStorage.trash;
                  break;
                }
                // Update the local mail to report that the mail has been read
                // updateLocalMail(mailbox, mailnumber, prop, val)
                console.log("rawMail: ", rawMail);
                console.log("mailNumber: ", mailNumber);
                //
                // I Commented out the following code -- Now when reading a "Sent" email the email does not flicker away it is able to be read.
                var updateMail = mail.updateLocalMail(rawMail, mailNumber, "is_read", 1);
                console.log("updateMail: ", updateMail);
                mail.parseMail(localStorage.mailboxCurrent);
                mail.updateMail(false);
                //
                //
                //
              }
            } catch(e) {
              //
              console.log("Error: ", e.message);
              return false;
            }
            // Update data where applicable
            if (localStorage.getItem("mail")) {
              var m = JSON.parse(localStorage.getItem("mail"));
              m[mailNumber].is_read = "1";
              localStorage.setItem("mail", JSON.stringify(m));
            }
            resolve(true);
          }).catch(err => {
            reject(err);
          });
        }).catch(err => {
          reject(err);
        });
      }).catch(error => {
        console.log("readMessage Error: ", error);
        errorHandler.setError("Error", error);
        errorHandler.showError();
      });
    });
  },


  /**
   *
   *  Update the raw mail from getMail function in order to avoid sending another API call
   *
   *  @param      mailbox        String      The mailbox to update. Ex: localStorage.spam
   *  @param      mailnumber     Number      The number of the message to update
   *  @param      prop           String      The property to update. Ex: is_read, mailto, etc...
   *  @param      val            *           The new value of this property
   *
   *  @return               Boolean     True|False based on success
   *
   */
  updateLocalMail(mailbox, mailnumber, prop, val) {
    var mailbox = mailbox.trim();
    // Decode the base64 encoded string
    console.log("mailbox: \"", mailbox + "\"");
    console.log("mailbox type: ", typeof mailbox);
    console.log("isEncoded: ", base64.isEncoded(mailbox));
    console.log("aTOb: ", window.atob(mailbox));
    if (base64.isEncoded(mailbox)) {
      mailbox = window.atob(mailbox);
      console.log("mailbox base encoded: ", mailbox);
    }
    // Split the mail into an array
    var arrDecodedMail = mailbox.split("|");
    // Find the property within the "columns" portion of the array
    var columns        = arrDecodedMail[0];
    // If the column was found...
    if (columns.indexOf(prop) !== -1) {
      // Split the columns into their own array
      var arrColumns = columns.split(",");
      // Find out what number the target property is within this array
      var keyIndex   = arrColumns.indexOf(prop);
      // If the keyIndex was found update the value and store the value
      if (keyIndex !== -1) {
        // Get the columns from the array
        var updatedCols   = arrDecodedMail.shift();
        // Split the message array up further to isolate the message we want to update
        // var target        = arrDecodedMail.split(",");
        // Get the message object on its own
        var message       = arrDecodedMail[mailnumber];
        message = message.split(",");
        // Update the property
        message[keyIndex] = val;
        // Reassemble the thing and put it back in the localStorage
        // Add in the columns
        var arrUpdated = updatedCols;
        for (let i = 0; i < arrDecodedMail.length; i++) {
          if (i !== mailnumber) {
            // Add in the data
            arrUpdated += "|" + arrDecodedMail[i];
          } else {
            arrUpdated += "|" + message;
          }
        }
        console.log("arrUpdated: ", arrUpdated);
        // Determine the current mailbox
        var currentMailbox = localStorage.mailboxCurrent;
        // Update the current mailbox
        switch(currentMailbox) {
          case "inbox":
          if (base64.isEncoded(mailbox)) {
            mailbox = window.atob(mailbox);
            localStorage.inbox = window.atob(arrUpdated);
          } else {
            localStorage.inbox = arrUpdated;
          }
          break;
          case "sent":
          if (base64.isEncoded(mailbox)) {
            mailbox = window.atob(mailbox);
            localStorage.sent  = window.atob(arrUpdated);
          } else {
            localStorage.sent = arrUpdated;
          }
          break;
          case "spam":
          if (base64.isEncoded(mailbox)) {
            mailbox = window.atob(mailbox);
            localStorage.spam  = window.atob(arrUpdated);
          } else {
            localStorage.spam = arrUpdated;
          }
          break;
          case "trash":
          if (base64.isEncoded(mailbox)) {
            mailbox = window.atob(mailbox);
            localStorage.trash = window.atob(arrUpdated);
          } else {
            localStorage.trash = arrUpdated;
          }
          break;
        }
        // Completed.
        return true;
      } else {
        console.log("Column not found");
        return false;
      }
    } else {
      console.log("Column not found");
      // Column not found
      return false;
    }
  },


  /**
   *
   *  Get the sender of an email from a msgId
   *
   *  @param      msgId       Number       The id of the message to get the sender for
   *
   *  @return                 String       Returns the email address of the sender
   *
   */
  // getSender : function(msgId) {
  //   // If the msgId has been set and is a Number and is not "NaN"
  //   if (msgId && typeof msgId === "number" && !isNaN(Number(msgId))) {
  //     // Get the sender from the msgId
  //     var msg    = document.querySelector("[data-mail-id=" + Number(msgId) + "]");
  //     //
  //     var sender = msg.dataset.mailfrom;
  //     // Check to see if the sender is base64 encoded...
  //     if (base64.isEncoded(sender)) {
  //       // Decode it
  //       sender = window.btoa(sender);
  //     }
  //     // Return the result
  //     return sender;
  //   } else {
  //     return false;
  //   }
  // },


  /**
   *
   *  Mark a message as spam
   *
   *  @param     isGlobal     Boolean          Is this a global ban or not
   *
   *  @return                 Boolean          True | False  based on success
   *
   *  var result = mail.markSpam(true|false);
   *
   *
   */
  markSpam : async function(isGlobal = false) {
    // Get all of the checked emails
    let arrMsg = mail.getCheckedMail() || null;
    // If the arrMsg is not set, return false we're done
    if (arrMsg === null || !Array.isArray(arrMsg)) {
      mail.showMainNav();
      return false;
    } else {
      var msgId    = new Array();
      // Setup the loop for the array
      for (let n = 0; n < arrMsg.length; n++) {
        //
        console.log("Message: ", arrMsg[n]);
        try {
          // If the arrMsg has been set and is an object
          if (arrMsg[n] && typeof arrMsg[n] === "object") {
            // Make sure the sender exists as part of the object
            if (arrMsg[n].dataset.mailfrom && arrMsg[n].dataset.mailto) {
              // Get the sender/receiver from the object
              sender     = arrMsg[n].dataset.mailfrom;
              receiver   = arrMsg[n].dataset.mailto;
              // Decode if base64 encoded
              if (base64.isEncoded(String(sender))) {
                sender = window.atob(sender);
                // Find matches
                var senderMatch = sender.match(regExEmail);
                if (senderMatch.length > 0) {
                  senderMatch = senderMatch[0];
                }
                sender = senderMatch;
              }
              if (base64.isEncoded(String(receiver))) {
                receiver = window.atob(receiver);
                //
              }
            }
            /*
             *
             *  This section will move ALL emails with the same sender/receiver combo into spam
             *  I have not decided 100% if I want to do this by default or what yet so the functionality
             *  remains here, but is commented out.
             *
             *
            // Loop through the mail object and return all msgId's of spam from this user to this receiver
            var objMail = JSON.parse(localStorage.mail);
            // sender and receiver variables
            var s, r;
            // This will contain all the id's of messages to send to the script for updating to spam
            var msgId = new Array();
            //
            if (objMail) {
              for (let i = 0; i < objMail.length; i++) {
                // Sender
                // console.log("objMail Sender: ", objMail[i].mailfrom);
                // If encoded, decode
                if (base64.isEncoded(objMail[i].mailfrom)) {
                  s = objMail[i].mailfrom = window.atob(objMail[i].mailfrom);
                } else {
                  s = objMail[i].mailfrom;
                }
                sMatch = s.match(regExEmail);
                if (sMatch.length > 0) {
                  sMatch = sMatch[0];
                }
                // If encoded, decode
                // Receiver
                // console.log("objMail Receiver: ", objMail[i].mailto);
                if (base64.isEncoded(objMail[i].mailto)) {
                  r = objMail[i].mailto = window.atob(objMail[i].mailto);
                } else {
                  r = objMail[i].mailto;
                }
                rMatch = r.match(regExEmail);
                if (rMatch.length > 0) {
                  rMatch = rMatch[0];
                }
                // If sMatch == sender and rMatch == receiver then add the msgId on the thing
                if (sMatch == sender && rMatch == receiver) {
                  // console.log("ID: " + objMail[i].id);
                  msgId.push(objMail[i].id);
                } else {
                  // console.log("sMatch: " + sMatch);
                  // console.log("sMatch: " + sender);
                  // console.log("rMatch: " + rMatch);
                  // console.log("rMatch: " + receiver);
                }
              }
            }
            */
            // Setup an AJAX request to send to the server
            // Get our token to send with the request
            var token = localStorage.getItem("token");
            var str;
            msgId.push(Number(arrMsg[n].dataset.mailId));
            var url  = apiUrl;
            console.log("msgId: ", msgId);
            console.log("msgId Length: ", msgId.length);
            console.log("isGlobal: ", isGlobal);
            switch(isGlobal) {
              case false:
                // Regular spam routine
                // Format the data object to send
                if (msgId && msgId.length > 0) {
                  console.log("MORE THAN ZERO");
                  str   = {"token" : token, "sender" : sender, "receiver" : receiver, "msgId" : msgId};
                } else {
                  console.log("LESS THAN ZERO");
                  str   = {"token" : token, "sender" : sender, "receiver" : receiver, "msgId" : null};
                }
                url += "/mail/markSpam";
                break;
              //
              // Global spam routine
              case true:
                url += "/mail/markGlobalSpam";
                if (msgId && msgId.length > 0) {
                  str   = {"token" : token, "sender" : sender, "msgId" : msgId};
                } else {
                  str   = {"token" : token, "sender" : sender, "msgId" : null};
                }
                break;
            }
            console.log("String: ", str);
            try {
              // Generate the payload to be sent
              var payload = await formatPayload(str);
              console.log("Payload: ", payload);
              if (typeof payload != "object") {
                var strPayload = JSON.stringify(payload);
              } else {
                strPayload = payload;
              }
            } catch (e) {
              console.log("Error: ", e);
              mail.showMainNav();
              return false;
            }
            try {
              // Send the request
              // method = 'POST', url, data = '', conType = 'json'
              var req = await ajx.req("POST", url, strPayload, "json");
              console.log("Request: ", req);
            } catch (e) {
              console.log("Error: ", e);
              mail.showMainNav();
              return false;
            }
          } else {
            // msgId not set or not a number
            mail.showMainNav();
            return false;
          }
        } catch(e) {
          console.log("Error: ", e);
          mail.showMainNav();
          return false;
        }
      }
      // Update the mail
      var update = mail.updateMail();
      // Show the main nav again
      mail.showMainNav();
      // Operation completed successfully
      return true;
    }
  },


   /**
    *
    *   Delete an email
    *
    */
  deleteMail : async function() {
    if (localStorage.mailboxCurrent !== undefined && localStorage.mailboxCurrent !== null) {
      var currentMailbox = localStorage.mailboxCurrent;
      currentMailbox     = currentMailbox.toLowerCase().trim();
      if (currentMailbox === "trash" || currentMailbox === "sent") {
        var conf = confirm("This will permanently delete the selected email(s). Are you sure you want to proceed?");
        if (conf !== true) {
          return false;
        }
      }
    }
    // Get the messages that are checked
    let arrMail = mail.getCheckedMail() || null;
    if (arrMail === null || !Array.isArray(arrMail) || arrMail === undefined) {
      // Show the main nav
      mail.showMainNav();
      // Set and show the error if the mail array is null
      errorHandler.setError("Error","No messages selected");
      errorHandler.showError(3500);
      return false;
    } else {
      var arrMailId = new Array();
      // Loop through all the selected email and get their mail id's
      for (let i = 0; i < arrMail.length; i++) {
        // Push this mailId onto the end of the array
        arrMailId.push(Number(arrMail[i].dataset.mailId));
      }
      var url = apiUrl + "/mail/deleteMail";
      // Get the token
      var token = localStorage.getItem("token") || null;
      // If token is null then return false and show the error
      if (token === null) {
        // Show the main nav
        mail.showMainNav();
        // Set and show the error if the mail array is null
        errorHandler.setError("Error","Authentication token not found");
        errorHandler.showError(3500);
        return false;
      }
      //
      try {
        // msgId is an array of all message id's to put in the trash
        var str = {"token" : token, "arrMailId" : arrMailId};
        // Generate the payload to be sent
        var payload = await formatPayload(str);
        console.log("Payload: ", payload);
        if (typeof payload != "object") {
          var strPayload    = JSON.stringify(payload);
        } else {
          strPayload = payload;
        }
      } catch (e) {
        console.log("Error: ", e);
        errorHandler.setError("Error", e.message);
        errorHandler.showError(3500);
        mail.showMainNav();
        return false;
      }
      //
      try {
        // Send the request
        // method = 'POST', url, data = '', conType = 'json'
        // Submit the whole array to the API and await response
        var req = await ajx.req("POST", url, strPayload, "json");
        console.log("Request: ", req);
        // Update the mail
        var update = mail.updateMail();
        // Show the main nav again
        mail.showMainNav();
      } catch (e) {
        console.log("Error: ", e);
        mail.showMainNav();
        return false;
      }
      // Operation completed successfully
      return true;
    }
  },


   /**
    *
    *   Mark an email as 'unread'
    *
    *
    */
  unreadMail : async function() {
    // Get the messages that are checked
    let arrMail = mail.getCheckedMail() || null;
    if (arrMail === null || !Array.isArray(arrMail) || arrMail === undefined) {
      // Show the main nav
      mail.showMainNav();
      // Set and show the error if the mail array is null
      errorHandler.setError("Error","No messages selected");
      errorHandler.showError(3500);
      return false;
    } else {
      var arrMailId = new Array();
      // Loop through all the selected email and get their mail id's
      for (let i = 0; i < arrMail.length; i++) {
        // Push this mailId onto the end of the array
        arrMailId.push(Number(arrMail[i].dataset.mailId));
      }
      var url = apiUrl + "/mail/unreadMail";
      // Get the token
      var token = localStorage.getItem("token") || null;
      // If token is null then return false and show the error
      if (token === null) {
        // Show the main nav
        mail.showMainNav();
        // Set and show the error if the mail array is null
        errorHandler.setError("Error","Authentication token not found");
        errorHandler.showError(3500);
        return false;
      }
      //
      try {
        // msgId is an array of all message id's to put in the trash
        var str = {"token" : token, "arrMailId" : arrMailId};
        // Generate the payload to be sent
        var payload = await formatPayload(str);
        console.log("Payload: ", payload);
        if (typeof payload != "object") {
          var strPayload    = JSON.stringify(payload);
        } else {
          strPayload = payload;
        }
      } catch (e) {
        console.log("Error: ", e);
        errorHandler.setError("Error", e.message);
        errorHandler.showError(3500);
        mail.showMainNav();
        return false;
      }
      //
      try {
        // Send the request
        // method = 'POST', url, data = '', conType = 'json'
        // Submit the whole array to the API and await response
        var req = await ajx.req("POST", url, strPayload, "json");
        console.log("Request: ", req);
        // Update the mail
        var update = mail.updateMail();
        // Show the main nav again
        mail.showMainNav();
      } catch (e) {
        console.log("Error: ", e);
        mail.showMainNav();
        return false;
      }
      // Operation completed successfully
      return true;
    }
  },


   /**
    *
    *   Mark an email as 'not trash'
    *
    *
    */
  notTrash : async function() {
    // Get the messages that are checked
    let arrMail = mail.getCheckedMail() || null;
    if (arrMail === null || !Array.isArray(arrMail) || arrMail === undefined) {
      // Show the main nav
      mail.showMainNav();
      // Set and show the error if the mail array is null
      errorHandler.setError("Error","No messages selected");
      errorHandler.showError(3500);
      return false;
    } else {
      var arrMailId = new Array();
      // Loop through all the selected email and get their mail id's
      for (let i = 0; i < arrMail.length; i++) {
        // Push this mailId onto the end of the array
        arrMailId.push(Number(arrMail[i].dataset.mailId));
      }
      var url = apiUrl + "/mail/notTrash";
      // Get the token
      var token = localStorage.getItem("token") || null;
      // If token is null then return false and show the error
      if (token === null) {
        // Show the main nav
        mail.showMainNav();
        // Set and show the error if the mail array is null
        errorHandler.setError("Error","Authentication token not found");
        errorHandler.showError(3500);
        return false;
      }
      //
      try {
        // msgId is an array of all message id's to put in the trash
        var str = {"token" : token, "arrMailId" : arrMailId};
        // Generate the payload to be sent
        var payload = await formatPayload(str);
        console.log("Payload: ", payload);
        if (typeof payload != "object") {
          var strPayload    = JSON.stringify(payload);
        } else {
          strPayload = payload;
        }
      } catch (e) {
        console.log("Error: ", e);
        errorHandler.setError("Error", e.message);
        errorHandler.showError(3500);
        mail.showMainNav();
        return false;
      }
      //
      try {
        // Send the request
        // method = 'POST', url, data = '', conType = 'json'
        // Submit the whole array to the API and await response
        var req = await ajx.req("POST", url, strPayload, "json");
        console.log("Request: ", req);
        // Update the mail
        var update = mail.updateMail();
        // Show the main nav again
        mail.showMainNav();
      } catch (e) {
        console.log("Error: ", e);
        mail.showMainNav();
        return false;
      }
      // Operation completed successfully
      return true;
    }
  },


   /**
    *
    *   Mark an email as 'unread'
    *
    *
    */
  notSpam : async function() {
    // Get the messages that are checked
    let arrMail = mail.getCheckedMail() || null;
    if (arrMail === null || !Array.isArray(arrMail) || arrMail === undefined) {
      // Show the main nav
      mail.showMainNav();
      // Set and show the error if the mail array is null
      errorHandler.setError("Error","No messages selected");
      errorHandler.showError(3500);
      return false;
    } else {
      // Define the new arrays
      var arrMessages = {};
      arrMessages.messages = new Array();
      // Loop through all the selected email and get their mail id's
      for (let i = 0; i < arrMail.length; i++) {
        arrMessages.messages[i] = {};
        // Push this mailId onto the end of the array
        arrMessages.messages[i].mailId = Number(arrMail[i].dataset.mailId);
        arrMessages.messages[i].mailto = window.atob(arrMail[i].dataset.mailto);
        var sender               = mail.getEmailFromSender(arrMail[i].dataset.mailfrom);
        if (sender !== false) {
          arrMessages.messages[i].sender = sender;
        } else {
          arrMessages.messages[i].sender = window.atob(arrMail[i].dataset.mailfrom);
        }
      }
      // console.log(arrMessages);
      // return false;
      var url = apiUrl + "/mail/notSpam";
      // Get the token
      var token = localStorage.getItem("token") || null;
      // If token is null then return false and show the error
      if (token === null) {
        // Show the main nav
        mail.showMainNav();
        // Set and show the error if the mail array is null
        errorHandler.setError("Error","Authentication token not found");
        errorHandler.showError(3500);
        return false;
      }
      //
      try {
        // msgId is an array of all message id's to put in the trash
        var str = {"token" : token, "arrMessages" : arrMessages};
        console.log("STR: ", str);
        // Generate the payload to be sent
        var payload = await formatPayload(str);
        console.log("Payload: ", payload);
        if (typeof payload != "object") {
          var strPayload    = JSON.stringify(payload);
        } else {
          strPayload = payload;
        }
      } catch (e) {
        console.log("Error: ", e);
        errorHandler.setError("Error", e.message);
        errorHandler.showError(3500);
        mail.showMainNav();
        return false;
      }
      //
      try {
        // Send the request
        // method = 'POST', url, data = '', conType = 'json'
        // Submit the whole array to the API and await response
        var req = await ajx.req("POST", url, strPayload, "json");
        console.log("Request: ", req);
        // Update the mail
        var update = mail.updateMail();
        // Show the main nav again
        mail.showMainNav();
      } catch (e) {
        console.log("Error: ", e);
        mail.showMainNav();
        return false;
      }
      // Operation completed successfully
      return true;
    }
  },


  /**
   *
   *  Returns an array containing all of the checked emails
   *
   */
  getCheckedMail : function(action) {
    // Get all the checked mail
    var selectedMail = document.querySelectorAll(".message__checkbox :checked");
    var currentMail;
    var arrMail = new Array();
    // Loop through selected mail
    for (let i = 0; i < selectedMail.length; i++) {
      // Get the email element (the parent of this checkbox)
      currentMail  = selectedMail[i].parentNode.parentNode;
      arrMail.push(currentMail);
    }
    // If there are results pushed into the mail array return it.
    if (arrMail.length > 0) {
      return arrMail;
    } else {
      // Otherwise, return false
      return false;
    }
  },


  /**
   *
   *  Get the details of the email we are forwarding and wrap it up in an easy to work with object
   *
   *  @param       el      HTML Element         The clicked element that began the "Forward" process
   *
   *  @return              Object|Boolean       Returns a JSON object with all the reply mail details | false
   *
   */
  getForwardObject : function(el = null) {
    // We do not want a null or undefined element passed in as a parameter
    if (el !== null && el !== undefined) {
      // Check to see if this is the replyBox Reply button
      if (el.classList.contains("message__navItem")) {
        var header = document.querySelector(".mail__messageHeader");
        // Build an object from this data...
        var mailFrom     = document.querySelector("#messageMailFrom").innerText;
        var mailTo       = document.querySelector("#messageMailTo").innerText;
        var mailSubject  = "Fwd:" + document.querySelector("#messageSubject").innerText;
        var timestamp    = document.querySelector("#messageTimestamp").innerText;
        var hyphens      = String.fromCharCode(45, 45, 45, 45, 45, 45, 45);
        var prepend      = hyphens + " Original Message " + hyphens + " \r\n\r\n" + timestamp + ", " + mailFrom + " wrote:\r\n\r\n";
        var mailMessage  = prepend + document.querySelector(".message__contents").innerText;
        // Build the object
        var mailObject   = {"mailTo" : mailTo, "mailSubject" : mailSubject, "mailMessage" : mailMessage};
        console.log("TTRRRRUUUUUEEEE  ", mailObject);
        // Reply button was not clicked. Something weird is going on here...
      } else {
        return false;
      }
    } else {
      // Set and show the error
      errorHandler.setError("Error", "Mail object not found");
      errorHandler.showError(3500);
      return false;
    }
    // If the mailObject is not an object then make it one
    if (typeof mailObject !== "object") {
      mailObject       = JSON.parse(mailObject);
    }
    // Return the object
    return mailObject;
  },


  /**
   *
   *  This is the routine for sending an email to yourself
   *
   *  @return       Boolean       Based on success
   *
   */
  forward : async function(evt) {
    // Get the users token
    var token  = localStorage.token || null;
    if (token === null || token === undefined) {
      // Send them to the login screen
      credential.forceLogin();
      return false;
    }
    // Confirm with the user that it is okay to forward to their email address
    var conf = confirm("This will forward the email to your personal email address. If that is what you wish to do click OK. Otherwise, click Cancel.");
    if (conf) {
      // Get the message object
      var messageObject = mail.getForwardObject(evt.currentTarget);
      // Make sure there is a value we can work with in the mailObject
      if (Object.keys(messageObject).length > 0) {
        // Make sure the mailObject has the properties we require
        if (messageObject.hasOwnProperty("mailTo") && messageObject.hasOwnProperty("mailSubject") && messageObject.hasOwnProperty("mailMessage")) {
          // Reply to the email
          var urlReplyMail = apiUrl + "/mail/forwardMail";
          // Set the token
          messageObject.token = token;
          try {
            // Format the payload of the object to send to the replyMail function of the API
            var payload = await formatPayload(messageObject);
            // Send a request and get the response
            var result  = await ajx.req("POST", urlReplyMail, payload, "json");
          } catch (e) {
            errorHandler.setError("Error", e.message);
            errorHandler.showError(3500);
            return false;
          }
          // Check to see if we are getting an object back
          if (typeof result == "object") {
            // Make sure this object has the attributes we need to access
            if ("status" in result && "message" in result) {
              // Determine if this was a success
              if (result.status === "success") {
                // Good, get the message
                responseMsg = result.message;
                // Display "Message Sent" message
                errorHandler.setError("Success!", responseMsg, false);
                errorHandler.showError();
              } else {
                errorHandler.setError(result.status, result.message);
                errorHandler.showError();
                return false;
              }
            }
          }
        } else {
          errorHandler.setError("Error", "Mail object is missing required properties");
          errorHandler.showError(3500);
          return false;
        }
      } else {
        errorHandler.setError("Error", "No values in the mail object");
        errorHandler.showError(3500);
        return false;
      }
    } else {
      return false;
    }
  },


  /**
   *
   *  Reply popup -- This will make the replyBox appear on the screen
   *
   *  @return       Boolean    true|false based on success of the operation
   *
   */
  makeReplyPopUp : function() {
    var elReplyPop = document.querySelector("#replyBox");
    // Check to make sure replyPopUp does not exist
    if (!document.body.contains(elReplyPop)) {
      // Append the object onto the mailbox unordered list element
      var mailBox = document.querySelector("#mailbox");
      if (document.body.contains(mailBox)) {
        try {
          var replyBox = `<li class="message__replyBox">
          <div class="replyBox" id="replyBox">
            <textarea class="replyBox__textarea"></textarea>
            <button class="replyBox__button">Send</button>
            <button class="replyBox__button">Cancel</button>
            <span class="replyBox__btnMinimize">_</span>
          </div>
        </li>`;
        // Add the replyBox to the DOM
        mailBox.insertAdjacentHTML('beforeend', replyBox);
        // Add replyBox to the localStorage
        localStorage.setItem("isReplyBox", 1);
        // Bind functions to the buttons
        mail.bindReplyButtons();
        // Initiate the replyBox
        mail.initReply();
        return true;
      } catch(e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showErorr(3500);
        return false;
      }
        return true;
      } else {
        return false;
      }
    } else {
      console.log("NOPE!@#$%^&*()_+");
      return false;
    }
  },


  /**
   *
   *  Bind the functionality to the Send and Cancel buttons within the replyBox
   *
   *  @return      Boolean      true|false based on the success of the operation
   *
   */
  bindReplyButtons : function() {
    // Get a handle on the Send and Cancel buttons within the replyBox
    var replyBoxBtns = document.querySelectorAll("#replyBox .replyBox__button");
    var btnSend      = replyBoxBtns[0];
    var btnCancel    = replyBoxBtns[1];
    // Get a handle on the Minimize button within the replyBox
    var btnMinimize = document.querySelector(".replyBox__btnMinimize");
    // Make sure the buttons exist on the page
    if (document.body.contains(btnSend) && document.body.contains(btnCancel) && document.body.contains(btnMinimize)) {
      try {
        // Attempt to bind the functionality to these buttons
        btnSend.addEventListener("click", mail.reply);
        btnCancel.addEventListener("click", mail.replyBoxCancel);
        btnMinimize.addEventListener("mousedown", mail.replyBoxToggleMinimize);
        return true;
      } catch(e) {
        // Set and show the error then return false
        errorHandler.setError("Error", e.message);
        errorHandler.showErorr(3500);
        return false;
      }
    } else {
      return false;
    }
  },


  /**
   *
   *  Remove the replyBox from the DOM
   *
   */
  removeReplyBox : function() {
    // Attempt to find the replyBox
    var replyBox = document.querySelector("#replyBox");
    // If the replyBox is found delete it from the DOM
    if (document.body.contains(replyBox)) {
      var replyBoxGrandparent = replyBox.parentNode.parentNode;
      if (replyBoxGrandparent.contains(document.querySelector("li.message__replyBox"))) {
        try {
          //
          replyBoxGrandparent.removeChild(document.querySelector("li.message__replyBox"));
          // Add replyBox to the localStorage
          localStorage.setItem("isReplyBox", 0);
          return true;
        } catch(e) {
          // Set and show the error then return false
          errorHandler.setError("Error", e.message);
          errorHandler.showErorr(3500);
          return false;
        }
      }
    } else {
      return false;
    }
  },


  /**
   *
   *  This is the cancel functionality for the replyBox CANCEL button
   *
   *  @return      Bolean     true|false based on the success of the operation
   *
   */
  replyBoxCancel : function() {
    // Make sure they want to discard the reply
    var conf = confirm("This will delete the current reply.");
    if (conf) {
      mail.removeReplyBox();
    } else {
      return false;
    }
  },


  /**
   *
   *  Toggle the minimize style for the replyBox
   *
   *  @return     Boolean       true|false  based on the success of the operation
   *
   */
  replyBoxToggleMinimize : function() {
    // Find the replyBox element
    var replyBox = document.querySelector(".message__replyBox");
    // If the replyBox is found carry on
    if (document.body.contains(replyBox)) {
      try {
        // Toggle the class on the object
        replyBox.classList.toggle("message__replyBox--minimize");
        setTimeout(function() {
          // Add event listener to restore the replyBox
          if (replyBox.classList.contains("message__replyBox--minimize")) {
            replyBox.addEventListener("click", mail.replyBoxToggleMinimize);
          } else {
            replyBox.removeEventListener("click", mail.replyBoxToggleMinimize);
          }
        }, 500);
        return true;
      } catch(e) {
        // Set and show the error and return false
        errorHandler.setError("Error", e.message);
        errorHandler.showError(3500);
        return false;
      }
    } else {
      // Element not found
      return false;
    }
  },


  /**
   *
   *  Initiate the reply textarea
   *
   *  @return      Boolean      true|false  based on the success of the operation
   *
   */
  initReply : function() {
    // Look for the textArea
    var replyTextarea = document.querySelector("textarea.replyBox__textarea");
    // If the textArea exists lets initiate it
    if (document.body.contains(replyTextarea)) {
      try {
        var datetime = document.querySelector(".message__headerData #messageTimestamp");
        var mailfrom = document.querySelector(".message__headerData #messageMailFrom");
        var mailto   = document.querySelector(".message__headerData #messageMailTo");
        var subject  = document.querySelector(".message__headerSubject #messageSubject");
        var message  = document.querySelector(".message__message .message__contents");
        var messageArea = document.querySelector("#replyBox .replyBox__textarea");
        if (document.body.contains(datetime) && document.body.contains(mailfrom) && document.body.contains(mailto) && document.body.contains(subject) && document.body.contains(messageArea) && document.body.contains(message)) {
          datetime     = datetime.innerText;
          mailfrom     = mailfrom.innerText;
          mailto       = mailto.innerText;
          subject      = "RE:" + subject.innerText;
          message      = message.innerText;
          // Generate the hyphens from charCode to avoid screwing up the encryption/decryption
          var hyphens  = String.fromCharCode(45, 45, 45, 45, 45, 45, 45);
          // Setup the original message framing
          var prependMsg = hyphens + " Original Message " + hyphens + " \r\n\r\n" + datetime + ", " + mailfrom + " wrote:\r\n\r\n";
          // var prependMsg = "‐‐‐‐‐‐‐ Original Message ‐‐‐‐‐‐‐ \r\n\r\n" + datetime + ", " + mailfrom + " wrote:\r\n\r\n";
          messageArea.value = "\r\n\r\n" + prependMsg + message;
          // Set the message area in focus
          messageArea.focus();
          // Scroll the textarea back up
          messageArea.scrollTo(0, 0);
          // Set the cursor to the beginning of the message area
          messageArea.setSelectionRange(0,0);
          return true;
        } else {
          return false;
        }
      } catch(e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showError(3500);
        return false;
      }
    } else {
      return false;
    }
  },

  /**
   *
   *  Get the details of the email we are replying to and wrap it up in an easy to work with object
   *
   *  @param       el      HTML Element         The clicked element that began the "Reply" process
   *
   *  @return              Object|Boolean       Returns a JSON object with all the reply mail details | false
   *
   */
  getReplyObject : function(el = null) {
    // We do not want a null or undefined element passed in as a parameter
    if (el !== null && el !== undefined) {
      // Check to see if this is the replyBox Reply button
      if (el.classList.contains("replyBox__button")) {
        var header = document.querySelector(".mail__messageHeader");
        // Build an object from this data...
        var mailFrom     = document.querySelector("#messageMailFrom").innerText;
        var mailTo       = document.querySelector("#messageMailTo").innerText;
        var mailSubject  = document.querySelector("#messageSubject").innerText;
        var mailMessage  = document.querySelector(".replyBox__textarea").value;
        // Build the object
        var mailObject   = {"mailFrom" : mailFrom, "mailTo" : mailTo, "mailSubject" : "Re:" + mailSubject, "mailMessage" : mailMessage};
        // Reply button was not clicked. Something weird is going on here...
      } else {
        return false;
      }
    } else {
      // Set and show the error
      errorHandler.setError("Error", "Mail object not found");
      errorHandler.showError(3500);
      return false;
    }
    // If the mailObject is not an object then make it one
    if (typeof mailObject !== "object") {
      mailObject       = JSON.parse(mailObject);
    }
    // Return the object
    return mailObject;
  },


  /**
   *
   *  This is the routine for replying to an email
   *
   *  @return                       Boolean       Based on success
   *
   */
  reply : async function(evt) {
    // Get the mail object
    var mailObject = mail.getReplyObject(evt.target);
    if (mailObject !== false && typeof mailObject === "object") {
      // Let the user know what is going on
      errorHandler.setError("Status","Sending mail...");
      errorHandler.showError(2000);
      var responseMsg;
      // Get the users token
      var token  = localStorage.token || null;
      // If the token is not set or not found make them login
      if (token === null || token === undefined) {
        // Send them to the login screen
        window.location.href = "login.html";
        return false;
      }
      // console.log("mailObject: ", mailObject);
      // Make sure there is a value we can work with in the mailObject
      if (Object.keys(mailObject).length > 0) {
        // Make sure the mailObject has the properties we require
        if (mailObject.hasOwnProperty("mailFrom") && mailObject.hasOwnProperty("mailTo") && mailObject.hasOwnProperty("mailSubject") && mailObject.hasOwnProperty("mailMessage")) {
          // Reply to the email
          var urlReplyMail = apiUrl + "/mail/replyMail";
          // Set the token
          mailObject.token = token;
          try {
            // Format the payload of the object to send to the replyMail function of the API
            var payload = await formatPayload(mailObject);
            // Send a request and get the response
            var result  = await ajx.req("POST", urlReplyMail, payload, "json");
          } catch (e) {
            errorHandler.setError("Error", e.message);
            errorHandler.showError();
            return false;
          }
          // Check to see if we are getting an object back
          if (typeof result == "object") {
            // Make sure this object has the attributes we need to access
            if ("status" in result && "message" in result) {
              // Determine if this was a success
              if (result.status === "success") {
                // Good, get the message
                responseMsg = result.message;
                // Close the replyBox and display "Message Sent" message
                mail.removeReplyBox();
                errorHandler.setError("Success!", responseMsg, false);
                errorHandler.showError();
              } else {
                errorHandler.setError(result.status, result.message);
                errorHandler.showError();
                return false;
              }
            }
          }
        } else {
          errorHandler.setError("Error", "Mail object is missing required properties");
          errorHandler.showError();
          return false;
        }
      } else {
        errorHandler.setError("Error", "No values in the mail object");
        errorHandler.showError();
        return false;
      }
    } else {
      errorHandler.setError("Error", "Could not get mail object");
      errorHandler.showError();
      return false;
    }
  }

}
