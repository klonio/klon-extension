/**
 *
 *  Hash the password to send to the server for validation
 *
 *  @param    Email      String      Email address subscriber used to signup with
 *  @param    Password   String      Master password for the account
 *
 *  @return                          Return hash
 *
 */
function hashForLogin(email, password) {
  return new Promise(function(resolve, reject) {
    email          = email.toLowerCase().trim();
    password       = email + password + email;
    // Configure the sCrypt hashing function
    var salt       = hashPassword(email + password, 1000);
    var n          = 16384;
    var r          = 8;
    var p          = 1;
    var sCryptHash = sjcl.codec.hex.fromBits(sjcl.misc.scrypt(password, salt, n, r, p));
    // SHA512 the password before sending it to the server for validation
    var hashed     = sha512(sCryptHash, 2500);
    console.log("Hash: ", hashed);
    // Return the hash
    resolve(hashed);
  });
}


/**
 *
 *  Get a token from the Klon server
 *
 *  @param     String     email          Subscriber's email address
 *  @param     String     password       Subscriber's master password
 *
 *  @return    Object                    Returns a Promise
 *
 */
async function getToken(email, password) {
  // Hash the password before sending it to the server
  var hashPasswd    = await hashForLogin(email, password);
  // Build a JSON Object out of hashed pw and email
  var str           = {"email" : email, "password" : hashPasswd};
  // Convert to legit JSON Object
  var strJson       = JSON.stringify(str);
  //
  // console.log("strJSON: ", strJson);
  // Encrypt the data
  var encryptedData = await pubEncData(strJson);
  // Assemble the payload
  var payload       = {"payload" : encryptedData};
  // JSONify the payload
  payload           = JSON.stringify(payload);
  // Get the URL to send the data to
  var url           = apiUrl + "/auth/login";
  var response      = ajx.req('POST', url, payload, "json", true);
  // console.log('Response: ', response);
  return response;
}


/**
 *
 *  Extract token from the promise generated in getToken and store in localStorage
 *
 *  @param      Object      promiseToken
 *
 *  @return     Boolean     true if successful otherwise false
 *
 *  UNUSED??
 *
 */
function extractToken(promiseToken) {
  promiseToken
    // Upon promise resolution...
    .then(function(token) {
      // Get the token property from the token variable
      token = token.token;
      // Set the token in the localStorage
      localStorage.setItem('token', token);
      return true;
    })
    // Upon promise rejection...
    .catch(function(err) {
      // Console log the error for now
      console.log("Error retrieving token: ", err);
      return false;
    });
  return false;
}
