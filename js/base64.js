
/**
 *
 *  Handles everything base64 related
 *
 */
var base64 = {
  /**
   *
   *  Check to see if the data is base64 encoded.
   *
   *  @param     data       String      Data to determine if it is base64 encoded or not
   *
   *  @return               Boolean     Boolean to determined wether or not data is likekly to be base64 encoded or not
   *
   */
  isEncoded : function(data) {
    // console.log("Data: " + data);
    // Check to see if the data length is divisible by 4
    try {
      if (data.length % 4 === 0) {
        // Check to see if the character set is consistent with base64 encoding
        var regEx = new RegExp(/^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$/, "ig");
        if (regEx.test(data) === true) {
          // console.log("Regex: ", window.atob(data));
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } catch(error) {
      console.log("Error: ", error.message);
      return null;
    }
  }
};
