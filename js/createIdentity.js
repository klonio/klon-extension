/**
 *
 *  Generate an identity to potentially be used with the form
 *
 *
 */
async function generateIdentity() {
  // Get the token to be sent with the request
  var token = localStorage.getItem('token');
  // Make sure the token exists...
  if (token.length > 40 && token !== null && token !== undefined) {
    // Create the data payload to send to the server
    var data = {"token" : token}
    // console.log("Data: ", data);
    // Format and encrypt the data to send to the server
    var payload = await formatPayload(data);
    payload = JSON.stringify(payload);
    var url = apiUrl + "/klon/createCandidateForURL";
    // Send a request to the proper endpoint
    var request = await ajx.req('POST', url, payload);
    // log the response...
    console.log("Result: ", request);
    return request;
  } else {
    // Token does not exist
    return "nope";
  }
}
