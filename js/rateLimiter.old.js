/**
 *
 *  The rate limiter is just that, a client side rate limiter
 *  For the not-so-technically gifted this will serve as a good enough
 *  catch-all for those that would love to spam requests to the API
 *
 *  For the tech savvy there will also be a server side version
 *  implemented on the API. This is not to be depended upon for full rate limiting
 *  protection.
 *
 */

let rateLimiter = {

  // This is the timestamp of the last request
  lastRequest : 0,

  // This is the limit of requests per minute
  limitPerMinute : 12,

  //  - Make a function to compare right now with the last request time
  //  - Set the rate on a per minute basis
  //  - Set the rate as per role as well


  /**
   *
   *  Check to see if we have saved the rate limiter in the localStorage
   *
   *  @return      Object                  Returns {"status" : true|false, "value" : 1556442323} | false
   *
   */
  hasRateLimitBeenSet : function() {
    return new Promise(function(resolve, reject) {
      let localRateLimit = localStorage.getItem("rateLimit") || localStorage.getItem("tempRateLimit");
      if (localRateLimit !== null) {
        resolve({"status" : true, "value" : Number(localRateLimit)});
      } else {
        // Attempt to set the rate limit
        let setRate = rateLimiter.setRateLimit();
        setRate.then(setRateResult => {
          if (setRateResult == true) {
            // console.log("setRateResult is true");
            let setRateValue = localStorage.getItem("rateLimit") || localStorage.getItem("tempRateLimit");
            if (setRateValue !== null) {
              // console.log("Local Rate Limit: ", setRateValue);
              resolve({"status" : true, "value" : Number(setRateValue)});
            } else {
              // console.log("its still null");
              reject(false);
            }
          } else {
            // console.log("setRateResult is NOT true");
            reject(false);
          }
        }).catch(err => {
          console.log("setRate Error: ", err);
          errorHandler.setError("Error", err);
          errorHandler.showError();
          reject(false);
        });
      }
    });
  },


  /**
  *
  *  Set the users rate limit based upon their role level pulled from server
  *
  *  @return      Boolean                  True | False based on success of the function
  *
  */
  setRateLimit : function() {
    return new Promise(function(resolve, reject) {
      // Attempt to get the users role
      let userRole = role.getRole();
      userRole.then(userRoleResult => {
        console.log("userRoleResult: ", userRoleResult);
        if (userRoleResult !== false && typeof userRoleResult === "number") {
          try {
            switch (userRoleResult) {
              // Welcome to the void
              case 0:
              console.log("Welcome to the void");
              // 61 -- This user can't do anything because they're in the void, a sort of 4th dimensional hell
              localStorage.setItem("rateLimit", 61);
              break;
              // Economy Account
              case 1:
              // 12 -- The user can send a request to the server every 3 seconds
              localStorage.setItem("rateLimit", 20);
              break;
              // Regular Account
              case 2:
              // 20 -- The user can send a request to the server every 2 seconds
              localStorage.setItem("rateLimit", 30);
              break;
              // Premium Account
              case 3:
              // 30 -- The user can send a request to the server every 1.2 seconds
              localStorage.setItem("rateLimit", 50);
              break;
            }
            resolve(true);
          } catch(e) {
            console.log("Error ", e.message);
            reject(false);
          }
        } else {
          // The user is probably trying to login...
          if (window.location.pathname === "/login.html") {
            localStorage.setItem("tempRateLimit", 10);
            console.log("Ja1");
            resolve(true);
          } else {
            console.log("Ja2");
            reject(false);
          }
        }
      }).catch(err => {
        console.log("setRateLimit Error: ", err);
        errorHandler.setError("Error", err);
        errorHandler.showError();
        reject(false);
      });
    });
  },


  /**
   *
   *  Gets the users rate limit
   *
   *  @return      Number | Boolean        Returns the time in unix epoch format or returns false on error
   *
   */
  getRateLimit : function() {
    return new Promise(function(resolve, reject) {
      // First, check to see if the rate limiter has been set locally
      let objLocalRate = rateLimiter.hasRateLimitBeenSet();
      objLocalRate.then(result => {
        if (typeof result === "object" && result.hasOwnProperty("status")) {
          console.log("RESULT: ", result);
          if (result.status === true && !isNaN(result.value)) {
            resolve(result.value);
          } else {
            console.log("RRREEEEZZZZZ: ", result);
            reject(false);
          }
        } else {
          console.log("ReZuLtS: ", result);
          reject(false);
        }
      }).catch(error => {
        errorHandler.setError("Error", error);
        errorHandler.showError();
        reject(false);
      });
    });
  },


  /**
   *
   *  How many seconds have to pass before the user can send another request to the server
   *
   *  @return       Number       Number | False        Either returns a number of seconds or false
   *
   */
  secondsBetween : function() {
    return new Promise(function(resolve, reject) {
      let rateLimit = rateLimiter.getRateLimit();
      rateLimit.then(rateLimitResult => {
        if (rateLimitResult !== false) {
          try {
            // Divide the number of seconds in a minute by the rate limit
            let result = Math.round(60 / rateLimitResult);
            resolve(result);
          } catch(e) {
            console.log("Error: ", e);
            reject(false);
          }
        } else {
          reject(false);
        }
      }).catch(err => {
        console.log("Error: ", err);
        reject(false);
      });
    });
  },


  /**
   *
   *  Set or update the last request time
   *
   *  @return      Boolean             Returns True|False depending upon the success of the operation
   *
   */
  setLastRequestTime : function() {
    new Promise(function(resolve, reject) {
      try {
        // Get the current time in unix epoch format (seconds since January 1, 1970)
        let currentTime = Math.round((new Date()).getTime() / 1000);
        if (typeof currentTime !== "number") {
          reject(false);
        }
        if (isNaN(currentTime)) {
          reject(false);
        }
        // Store in the localStorage
        localStorage.setItem("lastRequestTime", currentTime);
        resolve(true);
      } catch(e) {
        console.log(e.message);
        reject(false);
      }
    });
  },


  /**
  *
  *  Get the time (in unix epoch format seconds) of the users last request
  *
  *  @return      Number | false
  *
  */
  getLastRequestTime : function() {
    return new Promise(function(resolve, reject) {
      // Check to see if the last request time has been set
      let lastRequestTime = localStorage.getItem("lastRequestTime");
      if (lastRequestTime !== null) {
        //
        if (!isNaN(Number(lastRequestTime))) {
          //
          resolve(Number(lastRequestTime));
        } else {
          reject(false);
        }
      } else {
        let currentTime = Math.round((new Date()).getTime() / 1000);
        localStorage.setItem("lastRequestTime", currentTime);
        resolve(currentTime);
      }
    });
  },


  /**
   *
   *  Make sure we are within the rate limits
   *
   */
  withinLimits : function() {
    return new Promise(function(resolve, reject) {
      // Get the last request
      let lastRequest = rateLimiter.getLastRequestTime();
      //
      lastRequest.then(lastRequestResult => {
        if (lastRequestResult !== false) {
          // Get the allowed amount of requests by the user
          let limit = rateLimiter.getRateLimit();
          limit.then(limitResult => {
            //
            if (limitResult !== false && typeof limitResult === "number") {
              // Get the current time
              let currentTime = Math.round((new Date()).getTime() / 1000);
              console.log("currentTime: ", currentTime);
              if ((currentTime - lastRequestResult) >= Math.round(60 / limitResult)) {
                console.log("timeBetween: ", 60 / limitResult);
                resolve(true);
              } else {
                console.log("cTime - lastReq: ", currentTime - lastRequestResult);
                console.log("60 / limit: ", Math.round(60 / limitResult));
                reject(false);
              }
            } else {
              // Set the users rate limit and call again
              let setLimit = rateLimiter.setRateLimit();
              setLimit.then(setLimitResult => {
                if (setLimitResult === true) {
                  reject(false);
                  // return rateLimiter.withinLimits(true);
                } else {
                  console.log("case: 1");
                  reject(false);
                }
              }).catch(err => {
                console.log("Error: ", err);
                reject(false);
              });
            }
          }).catch(err => {
            console.log("AYE611111");
            console.log("Error: ", err);
            reject(false);
          });
        }
        else {
          console.log("AYE5");
          // Set the last request time
          let setReq = rateLimiter.setLastRequestTime();
          setReq.then(setReqResult => {
            if (setReq === true) {
              console.log("TRY AGEEEENNN");
              reject(false);
              // return rateLimiter.withinLimits();
            } else {
              console.log("case: 2");
              reject(false);
            }
          }).catch(err => {
            console.log("Error: ", err);
            reject(false);
          });
        }
      }).catch(err => {
        console.log("Error: ", err);
        reject(false);
      });
    });
  }

}
