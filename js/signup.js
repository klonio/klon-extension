//@prepros-append sjcl.js
//@prepros-append scrypt.js
//@prepros-append sha1.js
//@prepros-append sha512.js
//@prepros-append sodium.min.js
//@prepros-append errorHandler.js
//@prepros-append constants.js
//@prepros-append login2.js
//@prepros-append ajax.js
//@prepros-append decrypt.js
//@prepros-append encrypt.js
//@prepros-append login3.js
//@prepros-append loginChain.js


/**
 *
 *  Make sure that the new credentails do not contain '@klonmail.com'
 *
 *  @param          String          email      The email address associated with the account
 *
 *  @return         Boolean                    True if valid, False otherwise
 *
 */
function validateCredentials(email = false) {
  // Make sure the email was set
  if (email !== false) {
    // Set the regular expression
    var regExpKlonmail = new RegExp(/(\@klonmail\.com)/);
    // Test the email address for a match
    if (!regExpKlonmail.test(email)) {
      return true;
    } else {
      return false;
    }
  } else {
    errorHandler.setError("Error", "Email not defined");
    errorHandler.showError();
    return false;
  }
}


/**
 *
 *  Hash the credentials before sending to the server for login
 *
 */
function hashCredentials() {
  var user     = document.querySelector("#email").value;
  var password = document.querySelector("#password").value;
  // lowercase and trim the user
  user         = user.toLowerCase().trim();
  // Make sure we have a valid email address...
  if (user.match(regExEmail)[0] == user) {
    // Make sure this is an acceptable email address
    let valid = validateCredentials(user);
    if (!valid) {
      errorHandler.setError("Error", "You cannot use this email address to signup for Klon");
      errorHandler.showError();
      document.querySelector("#email").focus();
      return false;
    }
    // Determine if the password is 8 characters long or not
    if (password.length < 8) {
      errorHandler.setError("Error", "Password must contain at least 8 characters");
      errorHandler.showError();
      document.querySelector("#password").focus();
      return false;
    }
    password     = user + password + user;
    console.log("User: " + user);
    // scrypt hash(emailAddress + password) 1,000 rounds
    var salt = hashPassword(user + password, 1000);
    // Difficulty
    var n = 16384;
    // Block size
    var r = 8;
    var p = 1;
    // Hex value of the scrypt hashing function
    var hash = sjcl.codec.hex.fromBits(sjcl.misc.scrypt(password, salt, n, r, p));
    console.log("sCrypt: " + hash);
    // SHA512 the scrypt hash 2,500 rounds
    var iterations = 2500;
    var hashed = hash;
    for(var i = 1; i <= iterations; i++) {
      var hashed = sjcl.codec.hex.fromBits(sjcl.hash.sha512.hash(hashed));
    }
    // Package the thing up into a pretty little package
    var jsonData = {"email" : user, "password" : hashed};
    jsonData = JSON.stringify(jsonData);
    console.log("jsonData: ", jsonData);
    // Send via AJAX to the server
    var req = ajx.req("POST", apiUrl + "/subscriber/create", jsonData, "json");
    req.then(result => {
      console.log("Success! ", result);
      errorHandler.setError("Success!", "You have successfully registered for Klon. Please check your email to validate your account.", false);
      errorHandler.showError();
      setTimeout(function() {
        window.location.pathname = "/login.html";
        return true;
      }, 5500);
    })
    .catch(error => {
      console.log("Error: ", error);
      try {
        error = JSON.parse(error);
      } catch(e) {
        errorHandler.setError("Error", e);
        errorHandler.showError();
        return false;
      }
      errorHandler.setError("Error", error.message);
      errorHandler.showError();
      return false;
    });
  } else {
    errorHandler.setError("Error", "Invalid email address");
    errorHandler.showError();
    document.querySelector("#email").focus();
    return false;
  }
}

window.addEventListener("load", function() {
  console.log("Signup Page");
  if (window.location.pathname === "/signup.html") {
    // Bind event to terms checkbox
    errorHandler.setError("","** Klon is <strong>only</strong> available to legal citizens of the United States of America **", false);
    errorHandler.showError(5000);
    var agreeToTerms = document.querySelector("#terms");
    var password     = document.querySelector("#password");
    var confPassword = document.querySelector("#confPassword");
    var signupBtn = document.querySelector("#signup");
    var checkBox  = document.querySelector("#agree");
    agreeToTerms.addEventListener("click", function() {
      if (checkBox.checked === false) {
        signupBtn.setAttribute("disabled", true);
      } else {
        if (password.value.length >= 8 && confPassword.value.length >= 8 && password.value === confPassword.value) {
          signupBtn.removeAttribute("disabled");
        }
      }
    });
    password.addEventListener("change", function() {
      if (password.value !== confPassword.value || password.value < 8) {
        signupBtn.setAttribute("disabled", true);
      } else {
        if (checkBox.checked === true) {
          signupBtn.removeAttribute("disabled");
        }
      }
    });
    confPassword.addEventListener("change", function() {
      if (confPassword.value !== password.value || confPassword.value < 8) {
        signupBtn.setAttribute("disabled", true);
      } else {
        if (checkBox.checked === true) {
          signupBtn.removeAttribute("disabled");
        }
      }
    });
    //
    var signupBtn = document.querySelector("#signup");
    if (signupBtn) {
      signupBtn.addEventListener("click", hashCredentials);
    }
  }
});
