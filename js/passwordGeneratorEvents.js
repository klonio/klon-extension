window.onload = function() {
  nav.bindNav();
  localStorage.sessionTab = "password-generator";
  var btnGenPass = document.querySelector("#genPass");
  if (btnGenPass) {
    btnGenPass.addEventListener('click', function() {
      var passLength   = document.querySelector("[name='passwordlength']").value;
      var useLowerCase = document.querySelector("[name='toggleLowercase']").checked;
      var useUpperCase = document.querySelector("[name='toggleUppercase']").checked;
      var useNumbers   = document.querySelector("[name='toggleNumbers']").checked;
      var useSpecial   = document.querySelector("[name='toggleSpecial']").checked;
      var password     = document.querySelector("#password");
      password.value   = generatePassword(passLength, useLowerCase, useUpperCase, useNumbers, useSpecial);
      strengthTest();
    });
    //
    var btnCopyPass = document.querySelector("#pwGenCopy");
    if (btnCopyPass) {
      btnCopyPass.addEventListener("click", copyPassword);
    }
  }
  var btnLogout = document.querySelector("#logout");
  if (btnLogout) {
    btnLogout.addEventListener("click", logout);
  }
}
