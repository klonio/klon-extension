/**
 *
 *  This object handles everything pertaining to roles
 *
 *  Here are the defined roles so far in Klon...
 *  **************************************************
 *  Role        Name
 *   0          Not subscribed -- The Void
 *   1          Economy Plan   -- $2.99/mo  -- Cannot reply or forward email.       Only 1 alias per site
 *   2          Regular Plan   -- $4.99/mo  -- Can forward & reply to "X" per "Y".  Up to 3 aliases per site
 *   3          Premium Plan   -- $9.99/mo  -- This plan is essentially unlimited -- Number of accounts on this plan may become limited though (1,000) -- Unlimited number of aliases per site
 *   9          Free Trial     -- $0.00/mo  -- This plan is the economy plan for free for 30 days?
 *
 */


let role = {

  // the Void = Banned, not signed up, bad bad bad
  theVoid : 0,

  // The most basic of plans
  economy : 1,

  // Pretty average plan. This will get you by but it's not the best we've got
  regular : 2,

  // This plan is the bees knees. Only 1,000 of these accounts will be available at first
  premium : 3,

  // Trial plan
  trial : 9,


  /**
   *
   *  Get the users role from the remote server
   *
   *  @return      Integer(0-3) | Boolean      Returns an integer 0,1,2, or 3 | False upon failure
   *
   */
  getRoleRemote : async function() {
    // Check for the users token
    if (localStorage.token !== undefined && localStorage.token !== null) {
      try {
        // Set the URL for the request
        var url = apiUrl + "/auth/getRole";
        // Setup the JSON object to send
        var str = {"token" : localStorage.token};
        // Format the payload
        var payload = await formatPayload(str);
        // Send the payload to the server
        // ajx.req("POST", url, strPayload, "json")
        var request = await ajx.req("POST", url, payload, "json", true);
        // Parse JSON if required
        if (typeof request !== "object") {
          request = JSON.parse(request);
        }
        // Check if the server sent a coherent response
        if (request.hasOwnProperty("status") && request.hasOwnProperty("message")) {
          // Determine the response from the server
          if (request.status === "success") {
            localStorage.setItem("role", request.message);
            // Return the result
            return request.message;
          // If the status is a fail, let the user know
          } else if (request.status === "fail") {
            errorHandler.setError("Error", request.message);
            errorHandler.showError();
            return false;
          // If it's something else, let the user know
          } else {
            errorHandler.setError("Error", "Bad server response");
            errorHandler.showError();
            return false;
          }
        } else {
          // Server returns something other than what we expected
          errorHandler.setError("Error", "Unable to read the servers response");
          errorHandler.showError();
          return false;
        }
      } catch(e) {
        // Set and show the error, return false
        errorHandler.setError("Error", e.message);
        errorHandler.showError();
        return false;
      }
    } else {
      // No token found, force the user to login
      credential.forceLogin();
      return false;
    }
  },


  /**
   *
   *  Get the users role from the localStorage
   *
   *  @return      role      Number      Returns the users role
   *
   */
  getRole : function() {
    return new Promise(function(resolve, reject) {
      // Check to see if role is stored in the localStorage
      if (localStorage.getItem("role") !== null) {
        let localRole = Number(localStorage.getItem("role"));
        if (!window.isNaN(localRole)) {
          // Seems like it's all good return the role
          resolve(localRole);
        } else {
          // Role returned was NaN
          // Delete the role and fetch from remote
          localStorage.removeItem("role");
          reject(false);
        }
      } else {
        let roleRemote = role.getRoleRemote();
        roleRemote.then(result => {
          console.log("LOCAL STORAGE ROLE IS NULL", result);
          // Make sure we received what we think we should have received
          if (localStorage.getItem("role") !== null) {
            resolve(Number(localStorage.getItem("role")));
          } else {
            reject(false);
          }
        }).catch(err => {
          errorHandler.setError("Error", err);
          errorHandler.showError();
          reject(false);
        });
      }
    });
  },


  /**
   *
   *  This is an example of working with the above async function
   *  Apparently I had to prove to myself that this works...
   *
   */
  checkRole : async function(uRole) {
    let myRole = await role.getRoleRemote();
    if (myRole === uRole) {
      console.log(true);
      return true;
    } else {
      console.log("myRole: ", myRole);
      return false;
    }
  }

};
