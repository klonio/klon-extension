/**
 *
 *  Use the identity for the given URL. Send to Klon API Endpoint for insertion into the DB
 *
 */
function useIdentity(identity) {
  return new Promise((resolve, reject) => {
    // console.log("Received Identity: ", identity);
    // Now that we are sure we are getting the identity object let's encrypt the values and return them
    // Get the users key
    console.log("useIdentity: ", identity);
    var newIdentity = new Object();
    var token = localStorage.getItem('token');
    if (token !== null) {
      var key = localStorage.getItem('key');
      // Check to see if our encryption key is stored in the localStorage
      if (key !== null) {
        splitHexValue(key, 8).then( result => {
          console.log("Key: ", result);
          // Our key is found in the localStorage, time to encrypt our identity...
          Object.getOwnPropertyNames(identity).forEach((val, idx, array) => {
            if (identity[val] != null && identity[val] != undefined && identity[val] != "") {
              // Logs identity details... Example: firstName : Edmund
              // Encrypt the value and return as long as it's not the email property
              if (val != "rEmail") {
                console.log("Index: " + idx);
                console.log("Value: " + val);
                // encrypt the data with the users key
                newIdentity[val] = encryptDataWithKey(result, String(identity[val]));
              } else {
                // base64 encode the email this way it looks like the rest of the data
                newIdentity['rEmail'] = window.btoa(identity['rEmail']);
              }
            }
          }
        );
        newIdentity.rEmail = window.btoa(identity['email']);
        newIdentity.token  = token;
        console.log("NEWIdentity: ", newIdentity);
      });
    } else {
      // We need to generate our private key for encryption
      // genKey(user,pass)
      console.log("No key found. Cannot encrypt data.");
      reject("No key found. Cannot encrypt data.");
      }
    } else {
      console.log("Re-Authenticate. Session token not found.");
      reject("Re-Authenticate. Session token not found.");
    }
    console.log("Encrypted Identity: ", newIdentity);
    resolve(newIdentity);
  });
}
