//@prepros-append loader.js
//@prepros-append string.js
//@prepros-append credential.js
//@prepros-append operator.js
//@prepros-append nav.js
//@prepros-append sjcl.js
//@prepros-append scrypt.js
//@prepros-append sha1.js
//@prepros-append sha512.js
//@prepros-append sodium.min.js
//@prepros-append errorHandler.js
//@prepros-append constants.js
//@prepros-append base64.js
//@prepros-append login2.js
//@prepros-append ajax.js
//@prepros-append time.js
//@prepros-append decrypt.js
//@prepros-append encrypt.js
//@prepros-append login3.js
//@prepros-append errorCheck.js
//@prepros-append loginChain.js
//@prepros-append passwordGenerator.js
//@prepros-append usernameGenerator.js
//@prepros-append createIdentity.js
//@prepros-append comms.js
//@prepros-append settings.js
//@prepros-append details.js
//@prepros-append modify.js
//@prepros-append useIdentity.js
//@prepros-append identityDetails.js
//@prepros-append identities.js
//@prepros-append session.js
//@prepros-append mail.js
//@prepros-append role.js
//@prepros-append prompt.js
//@prepros-append login.js
//@prepros-append payment.js
//@prepros-append subscription.js
//@prepros-append choice.js
//@prepros-append slideshow.js

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  console.log("Message: ", request.msg + " received.");
  if (request.msg === "ext auth") {
    // Reauthenticate the user
    localStorage.removeItem("token");
    window.location = "login.html";
    console.log("Message: ", request.msg + " received.");
    // Get a new identity
    var newIdentity = generateIdentity();
    newIdentity
    .then( result => {
      console.log("new identity: ", result);
      sendResponse(result);
    })
    .catch( error => {
      console.log("Error in generatePromise Promise: ", error);
      if (error === "Unauthorized.") {
        alert("Your session has expired! You must login to Klon Extension in order to continue");
        // Send a message to the extension itself
      }
    });
  }
  // Allows us to use this function asynchronously
  return true;
});


/**
 *
 *  Sleeps exection of code for 'x' ms
 *
 *  Example: var prom = await sleep(2000);
 *           console.log("Done");
 *
 */
// function sleep(ms) {
//   return new Promise(resolve => setTimeout(resolve, ms));
// }


/**
 *
 *  Retry an asynchronus function "r" amount of times
 *
 *  @param   fn         Function    The function to be retried
 *  @param   retries    Number      Number of times to retry the function
 *  @param   err        String      Error message to return upon failure
 *
 */
// function retry(fn, retries=3, err=null) {
//   if (retries === 0) {
//     return Promise.reject(err);
//   }
//   return fn().catch(err => {
//     return retry(fn, (retries - 1), err);
//   });
// }
