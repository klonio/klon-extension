window.onload = async function() {
  // If the user does not have a token set then set the temp variables
  // if (localStorage.getItem("token") === null) {
  //   // Set these so we can make basic calls to the API to login
  //   try {
  //     let setInitVals = await rateLimiter.setTempValues();
  //   } catch(e) {
  //     errorHandler.setError("Error", e);
  //     errorHandler.showError();
  //     return false;
  //   }
  // }
  if (window.location.pathname === "/login.html") {
    // Check to see if the user has a valid token
    try {
      // If the user is stored in the localStorage go ahead and auto fill the field
      if (localStorage.getItem("user") !== null) {
        document.querySelector("#credEmail").value = localStorage.getItem("user");
      }
      let validToken = await credential.hasToken();
      // console.log("result: ", validToken);
      // If the promise returns true
      if (validToken) {
        // See if we have the users role
        let userRole;
        if (localStorage.getItem("role") === null) {
          // Get the role from the remote server
          userRole = role.getRoleRemote();
          // console.log("userRole: ", userRole);
        }
        // Check to see if there is a previous session for the user
        var hasSession = operator.hasSession();
        if (hasSession) {
          // Redirect the user to the proper location
          var direct = operator.redirect(localStorage.sessionTab);
          if (direct) {
            return true;
          } else {
            window.location = 'mail.html';
            return false;
          }
        } else {
          window.location = 'identities.html';
          return false;
        }
      } else {
        // Set temporary values so we can get a new token from the server
        // let setVals = await rateLimiter.setTempValues();
        // Otherwise let's get comfy here on the login page
        var submitButton  = document.querySelector('#formSubmit');
        var passwordField = document.querySelector('#credPassword');
        // If you press "[Enter]" it submits the login form
        if (passwordField) {
          passwordField.addEventListener('keyup', function(event) {
            event.preventDefault();
            if (event.keyCode === 13) {
              submitCredentials();
            }
          });
        }
        submitButton.addEventListener('click', function() {
          submitCredentials();
        });
      }
    } catch(e) {
      errorHandler.setError("Error", e.message);
      errorHandler.showError();
      return false;
    }
  }
}
