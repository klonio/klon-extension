
/**
 *
 *  Validate an email address
 *
 *  @param    String     email      Email address to validate
 *
 *  @return   Boolean               True if successful false otherwise
 *
 */
function validateEmail(email) {
  var em = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return em.test(String(email).toLowerCase());
}
