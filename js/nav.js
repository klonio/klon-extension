var nav = {
  /**
   *
   *  Bind function to the nav
   *
   */
  bindNav : function() {
    // Get all the nav buttons
    var arrNavLink = document.querySelectorAll(".navList a");
    // Loop through all the nav links binding functionality to them
    for (let i = 0; i < arrNavLink.length; i++) {
      arrNavLink[i].addEventListener("click", function() {
        // Old functionality
        // var linkText = arrNavLink[i].innerText.toLowerCase().trim();
        // var tab;
        // switch(linkText) {
        //   case "home":
        //     tab = "main";
        //     break;
        //   case "identities":
        //     tab = "identities";
        //     break;
        //   case "pass gen":
        //     tab = "password-generator";
        //     break;
        //   case "settings":
        //     tab = "settings";
        //     break;
        //   case "mail":
        //     tab = "mail";
        //     break;
        // }
        // // localStorage.removeItem("sessionTab");
        // localStorage.setItem("sessionTab", tab);
        //
        // New functionality
        localStorage.removeItem("sessionTab");
      });
    }
    var t = localStorage.getItem("sessionTab");
    console.log("Tab: " + t);
    return true;
  },

    /**
     *
     *  Insert the navigation into the page
     *
     *  @return      Boolean      true|false depending upon the success of the operation
     *
     */
  insertNav : function() {
    // Find the nav ID
    var mainNav = document.querySelector("#nav");
    if (document.body.contains(mainNav)) {
      try {
        mainNav.innerHTML = `
        <div class="nav">
          <ul class="navList">
            <li class="navList__item navList__item--identities">
              <a href="identities.html">Identities</a>
            </li>
            <li class="navList__item navList__item--pwGenerator">
              <a href="password-generator.html">Pass Gen</a>
            </li>
            <li class="navList__item navList__item--settings">
              <a href="settings.html">Settings</a>
            </li>
            <li class="navList__item navList__item--mail">
              <a href="mail.html">Mail</a>
            </li>
          </ul>
        </div>`;
        return true;
      } catch(e) {
        errorHandler.setError("Error", e.message);
        errorHandler.showError(3500);
        return false;
      }
    } else {
      return false;
    }
  }


  /**
   *
   *
   *
   *
   */

}

// Add the nav upon loading of the page
window.addEventListener("load", nav.insertNav);
