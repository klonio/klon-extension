
/**
 *
 *  Generate Password  Example:            generatePassword(12, true, true, true, false);
 *
 *  @param   String    length              Length of the password to be generated
 *  @param   Boolean   useLowerChars       Use lower case letters in the password
 *  @param   Boolean   useUpperChars       Use upper case letters in the password
 *  @param   Boolean   useNumericChars     Use numbers in the password
 *  @param   Boolean   useSpecialChars     Use special characters in the password
 *
 *  @return  String    Returns a password based on the arguments provided
 *
 */

function generatePassword(length = 12, useLowerChars, useUpperChars, useNumericChars, useSpecialChars) {
  length = Number(length);
  if (length == undefined || typeof length == "string" || length == "") {
    length = 12;
  }
  // Add characters to the charset based on parameters
  var charset = "";
  var lowerCaseChars = "abcdefghjkmnpqrstuvwxyz";
  var upperCaseChars = "ABCDEFGHJKMNPQRSTUVWXYZ";
  var numericChars   = "123456789";
  var specialChars   = "~!@#$%^&*_+{}=";
  if (useLowerChars == true) {
    charset = charset + lowerCaseChars;
  }
  if (useUpperChars == true) {
    charset = charset + upperCaseChars;
  }
  if (useNumericChars == true) {
    charset = charset + numericChars;
  }
  if (useSpecialChars == true) {
    charset = charset + specialChars;
  }
  //
  var retVal = "";
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }

  // Make sure the string contains all of the things it is supposed to contain
  if (useLowerChars == true) {
    if (!retVal.match(/[a-z]/g)) {
      retVal = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    }
  }
  if (useUpperChars == true) {
    if (!retVal.match(/[A-Z]/g)) {
      retVal = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    }
  }
  if (useNumericChars == true) {
    if (!retVal.match(/[1-9]/g)) {
      retVal = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    }
  }
  if (useSpecialChars == true) {
    if (!retVal.match(/\~|\!|\@|\#|\$|\%|\^|\&|\*|\_|\+|\{|\}|\=/g)) {
      retVal = generatePassword(length, useLowerChars, useUpperChars, useNumericChars, useSpecialChars);
    }
  }
  return retVal;
}


/**
 *
 *  Make the copy password function work
 *
 */
function copyPassword() {
  var password     = document.querySelector("#password");
  var errorHandler = document.querySelector("#errorHandler");
  if (errorHandler) {
    try {
      //
      password.removeAttribute("readonly");
      // Focus on the field
      password.focus();
      // Select the password
      password.select();
      // Copy the password
      var success = document.execCommand("copy");
      // Deselect the text
      window.getSelection().removeAllRanges();
      // Add the readonly attribute back to the input
      password.setAttribute("readonly", "true");
      // Determine if copying was successful
      var msg = success ? "successfully" : "unsuccessfully";
      // Display message to the user
      errorHandler.innerText = "Password was " + msg + " copied.";
    } catch (err) {
      errorHandler.innerText = "Error: Password could not be copied.";
    }
  } else {
    alert("An error has occurred please contact support.");
  }
}


/**
 *
 *
 */
  function strengthTest() {

    var password = document.querySelector("#password");
    var meter    = document.querySelector("#strengthMeter");
    // var text     = document.querySelector("password-strength-text");

    var val  = password.value;
    var text = document.querySelector("#strengthText");
    var strength = {0 : "Worst", 1 : "Bad", 2 : "Weak", 3 : "Good", 4 : "Strong"};
    var result = zxcvbn(val);

    // Update the password strength meter
    meter.value = result.score;

    // Add text
    if (val !== "") {
      text.innerHTML = strength[result.score];
    } else {
      text.innerHTML = "";
    }
    return;
  }
