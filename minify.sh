#!/bin/bash
echo -e "\e[0;91mDeleting old dist directory..."
rm -rf js/dist
echo -e "\e[0;94mCreating new dist directory..."
mkdir js/dist
echo -e "\e[0;92mCompressing & Mangling..."
terser --compress --mangle -- js/scripts-dist.js > js/dist/scripts-dist.js
terser --compress --mangle -- js/createIdentity.js > js/dist/createIdentity.js
terser --compress --mangle -- js/background-dist.js > js/dist/background-dist.js
terser --compress --mangle -- js/comms.js > js/dist/comms.js
terser --compress --mangle -- js/useIdentity.js > js/dist/useIdentity.js
terser --compress --mangle -- js/identities.js > js/dist/identities.js
terser --compress --mangle -- js/settings.js > js/dist/settings.js
terser --compress --mangle -- js/contentScript-dist.js > js/dist/contentScript-dist.js
terser --compress --mangle -- js/modify-page.js > js/dist/modify-page.js
terser --compress --mangle -- js/nav.js > js/dist/nav.js
terser --compress --mangle -- js/logout.js > js/dist/logout.js
terser --compress --mangle -- js/passwordGeneratorEvents.js > js/dist/passwordGeneratorEvents.js
terser --compress --mangle -- js/passwordGenerator.js > js/dist/passwordGenerator.js
terser --compress --mangle -- js/settings-event.js > js/dist/settings-event.js
terser --compress --mangle -- js/postMessageHandler.js > js/dist/postMessageHandler.js
terser --compress --mangle -- js/mailpage.js > js/dist/mailpage.js
terser --compress --mangle -- js/logout-dist.js > js/dist/logout-dist.js
terser --compress --mangle -- js/signup-dist.js > js/dist/signup-dist.js
terser --compress --mangle -- js/history.js > js/dist/history.js
echo -e "\e[0;92mCopying Files..."
cp js/zxcvbn.js js/dist/zxcvbn.js
cp js/stripe3.js js/dist/stripe3.js
echo -e "\e[0;96mComplete!"


# RCol='\e[0m'    # Text Reset
# Regular             Bold                Underline           High Intensity      BoldHigh Intens     Background          High Intensity Backgrounds
#################     ################    ################    ################    #################   ################    ##########################
# Bla='\e[0;30m';     BBla='\e[1;30m';    UBla='\e[4;30m';    IBla='\e[0;90m';    BIBla='\e[1;90m';   On_Bla='\e[40m';    On_IBla='\e[0;100m';
# Red='\e[0;31m';     BRed='\e[1;31m';    URed='\e[4;31m';    IRed='\e[0;91m';    BIRed='\e[1;91m';   On_Red='\e[41m';    On_IRed='\e[0;101m';
# Gre='\e[0;32m';     BGre='\e[1;32m';    UGre='\e[4;32m';    IGre='\e[0;92m';    BIGre='\e[1;92m';   On_Gre='\e[42m';    On_IGre='\e[0;102m';
# Yel='\e[0;33m';     BYel='\e[1;33m';    UYel='\e[4;33m';    IYel='\e[0;93m';    BIYel='\e[1;93m';   On_Yel='\e[43m';    On_IYel='\e[0;103m';
# Blu='\e[0;34m';     BBlu='\e[1;34m';    UBlu='\e[4;34m';    IBlu='\e[0;94m';    BIBlu='\e[1;94m';   On_Blu='\e[44m';    On_IBlu='\e[0;104m';
# Pur='\e[0;35m';     BPur='\e[1;35m';    UPur='\e[4;35m';    IPur='\e[0;95m';    BIPur='\e[1;95m';   On_Pur='\e[45m';    On_IPur='\e[0;105m';
# Cya='\e[0;36m';     BCya='\e[1;36m';    UCya='\e[4;36m';    ICya='\e[0;96m';    BICya='\e[1;96m';   On_Cya='\e[46m';    On_ICya='\e[0;106m';
# Whi='\e[0;37m';     BWhi='\e[1;37m';    UWhi='\e[4;37m';    IWhi='\e[0;97m';    BIWhi='\e[1;97m';   On_Whi='\e[47m';    On_IWhi='\e[0;107m';
